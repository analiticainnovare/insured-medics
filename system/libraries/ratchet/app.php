<?php 
// create new instances
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use MyChat\Socket;

// start the session
require dirname(__FILE__) . '/vendor/autoload.php';

$server = IoServer::factory(
	new HttpServer(new WsServer(
		new Socket()
	)), 9090
);

$server->run();