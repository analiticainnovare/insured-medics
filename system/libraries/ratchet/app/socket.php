<?php

namespace MyChat;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Socket implements MessageComponentInterface {

	private $senderId;
	private $receiverId;
	private $message_id = null;
	private $db;
	private $baseUrl = "http://localhost/analitica_innovare/medics/";

	public function __construct() {
		$this->clients = new \SplObjectStorage;
		$this->dbConn();
	}

	/**
	 * Get the user information using the ID
	 * 
	 * @param String $userId
	 * 
	 * @return Object
	 */
	private function userInfo($userId) {

		try {

			$stmt = $this->db->prepare("SELECT firstname, name, email, item_id, image, phone_number FROM users WHERE item_id = ? LIMIT 1");
			$stmt->execute([$userId]);

			return $stmt->fetch(\PDO::FETCH_OBJ);

		} catch(\PDOException $e) {
			return $e->getMessage();
		}
	}
	

	/**
	 * Run the connection to the database
	 * 
	 * @return $this
	 */
	private function dbConn() {
		
		// CONNECT TO THE DATABASE
		$connectionArray = array(
			'hostname' => "localhost",
			'database' => "insured_medics",
			'username' => "root",//'insurehub365_imp',
			'password' => "",//'p3W0U1Hkee'
		);
		
		// run the database connection
		try {
			$conn = "mysql:host={$connectionArray['hostname']}; dbname={$connectionArray['database']}; charset=utf8";			
			$this->db = new \PDO($conn, $connectionArray['username'], $connectionArray['password']);
			$this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
			$this->db->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_BOTH);
		} catch(\PDOException $e) {
			die("Database Connection Error: ".$e->getMessage());
		}

		return $this->db;
	}

	public function onOpen(ConnectionInterface $conn) {
		$this->clients->attach($conn);
		$this->users[$conn->resourceId] = $conn;
	}

	public function onClose(ConnectionInterface $conn) {
		$this->clients->detach($conn);
		unset($this->users[$conn->resourceId]);
	}

	/**
	 * Common Functions
	 *
	 * Loads the base classes and executes the request.
	 *
	 * @package     Helpers
	 * @subpackage  Time Helper Functions
	 * @category    Core Functions
	 * @author      Analitica Innovare Dev Team
	 */

	public function time_diff($timestamp, $current_time = false) {
		
		$strTime = array("sec", "min", "hr", "day", "wk", "mnth", "yr");
		$length = array("60","60","24","4","30","12","10");

		$timestamp = !preg_match("/^[0-9]+$/", $timestamp) ? strtotime($timestamp) : $timestamp;

		$currentTime = !empty($current_time) ? $current_time : time();
		
		if($currentTime >= $timestamp) {

			$diff = $currentTime - $timestamp;

			for($i = 0; $diff >= $length[$i] && $i < count($length)-1; $i++) {
				$diff = $diff / $length[$i];
			}

			$diff = round($diff);

			return $diff . " " . $strTime[$i] . "s ago";

		} else {
			
			$diff = $timestamp - $currentTime;
			for($i = 0; $diff >= $length[$i] && $i < count($length)-1; $i++) {
			$diff = $diff / $length[$i];
			}

			$diff = round($diff);
			return $diff . " " . $strTime[$i] . "s to go";
		}
		
	}

	public function onMessage(ConnectionInterface $from, $data) {
		
		// set some variables
		$from_id = $from->resourceId;
		$data = json_decode($data);

		// set the user data
		print_r($data);

		/** Wipe all text */
		foreach($data as $key => $value) {
			$data->$key = $this->custom_clean($value);
		}

		// GET THE USER INFORMATION
		$senderInfo = isset($data->sender_id) ? $this->userInfo($data->sender_id) : null;
		$recipientInfo = isset($data->receiver_id) ? $this->userInfo($data->receiver_id) : null;
		
		// if the nofitication is typing
		if(isset($data->typing_notification)) {
			
			// loop through the clients list
			foreach($this->clients as $client) {
				// if the from is not the same as the client 
				if($from != $client) {
					// send the response
					$client->send(
						json_encode(
							array(
								"message" => "typing...", "receiver_id" => $data->receiver_id, "sender_id" => $data->sender_id, "type" => "notification"
							)
						)
					);
				}
			}

		} else {

			// if not isset message then stop the execution
			if(!isset($data->message) || !isset($data->user_agent)) {
				return;
			}

			// set the user id and the chat message
			$chatMsg = $data->message;

			// return false if the message is empty
			if(empty($chatMsg)) {
				return;
			}
			$chatMsg = nl2br($data->message);

			/** Trim the message to only 1000 characters long */
			$chatMsg = substr($chatMsg, 0, 1000);
			$clean_date = date("l, F jS");
			$time_sent = date("h:i A");
			$raw_date = date("Y-m-d");

			/** Random item id */
			$item_id = $this->random_string(32);

			// the message information
			$senderMessageContent = "
			<li class=\"message-item me\" id=\"sender_msg\" data-chat_item_id=\"{$item_id}\">
				<img src=\"{$this->baseUrl}{$senderInfo->image}\" class=\"img-xs rounded-circle\" alt=\"avatar\">
				<div class=\"content\">
					<div class=\"message\"><div class=\"bubble\">
						<div class=\"d-flex align-items-center\">
							<div class=\"mr-3\">{$chatMsg}</div>
							<div class=\"cursor\"><span class=\"font-10px delete_msg\" data-chat_item_id=\"{$item_id}\" title=\"Delete this message\"><i class=\"fa fa-trash\"></i></span></div>
						</div>
					</div>
					<span>{$time_sent}</span></div>
				</div>
			</li>";

			$recipientMessageContent = "
			<li class=\"message-item friend\" id=\"receiver_msg\" data-chat_item_id=\"{$item_id}\">
				<img src=\"{$this->baseUrl}{$senderInfo->image}\" class=\"img-xs rounded-circle\" alt=\"avatar\">
				<div class=\"content\">
					<div class=\"message\"><div class=\"bubble\">{$chatMsg}</div><span>{$time_sent}</span></div>
				</div>
			</li>";

			// initiate a connection and append the messages
			$this->saveMessage($data, $item_id);

			// limit the message and send
			$message = $this->limit_words($data->message, 12);
			
			// output the message
			$from->send(
				json_encode(
					array(
						"formated_message" => $senderMessageContent, "sender_info" => $senderInfo, "receiver_info" => $recipientInfo, "raw_message" => $chatMsg,
						"receiver_id" => $data->receiver_id, "sender_id" => $data->sender_id, "message_id" => $this->message_id, "type" => "message",
						"timestamp" => time(), "sent_time" => $time_sent, "seen_status" => 0, "clean_date" => $clean_date, 
						"raw_date" => $raw_date, "item_id" => $item_id, "message" => $message
					)
				)
			);

			// loop through the clients list
			foreach($this->clients as $client) {
				// if the from is not the same as the client 
				if($from != $client) {
					// send the response
					$client->send(
						json_encode(
							array(
								"formated_message" => $recipientMessageContent, "sender_info" => $senderInfo, "receiver_info" => $recipientInfo, "raw_message" => $chatMsg,
								"receiver_id" => $data->receiver_id, "sender_id" => $data->sender_id, "message_id" => $this->message_id, "type" => "message",
								"timestamp" => time(), "sent_time" => $time_sent, "seen_status" => 0, "clean_date" => $clean_date, 
								"raw_date" => $raw_date, "item_id" => $item_id, "message" => $message
							)
						)
					);
				}
			}
		}

	}

	/**
	* Remove Invisible Characters
	*
	* This prevents sandwiching null characters
	* between ascii characters, like Java\0script.
	*
	* @param	string
	* @param	bool
	* @return	string
	*/
	private function remove_invisible_characters($str, $url_encoded = TRUE)
	{
		$non_displayables = array();

		// every control character except newline (dec 10),
		// carriage return (dec 13) and horizontal tab (dec 09)
		if ($url_encoded)
		{
			$non_displayables[] = '/%0[0-8bcef]/i';	// url encoded 00-08, 11, 12, 14, 15
			$non_displayables[] = '/%1[0-9a-f]/i';	// url encoded 16-31
			$non_displayables[] = '/%7f/i';	// url encoded 127
		}

		$non_displayables[] = '/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F]+/S';	// 00-08, 11, 12, 14-31, 127

		do
		{
			$str = preg_replace($non_displayables, '', $str, -1, $count);
		}
		while ($count);

		return $str;
	}

	/** Clean the user message sent */
	private function custom_clean($str) {
	
		// Remove Invisible Characters
		// This prevents sandwiching null characters
		// between ascii characters, like Java\0script.
		$this->remove_invisible_characters($str);
		
		// Fix &entity\n;
		$str = str_replace(array('&amp;','&lt;','&gt;'), array('&amp;amp;','&amp;lt;','&amp;gt;'), $str);
		$str = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $str);
		
		$str = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $str);
		$str = html_entity_decode($str, ENT_COMPAT, 'UTF-8');
		
		// Remove any attribute starting with "on" or xmlns
		$str = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $str);
		
		// Remove javascript: and vbscript: protocols
		$str = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $str);
		
		$str = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $str);
		
		$str = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $str);
		
		// Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
		$str = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $str);
		
		$str = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $str);
		
		$str = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $str);
		
		// Remove namespaced elements (we do not need them)
		$str = preg_replace('#</*\w+:\w[^>]*+>#i', '', $str);
		
		do {
			// Remove really unwanted tags
			$old_data = $str;
			
			$str = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $str);
		} while ($old_data !== $str);
		
		// we are done...
		return $str;
	}
	
	/**
	 * Limit words
	 *
	 * Splits the words and then using array_splice it reduces the number of words.
	 *
	 * @param	string	$str	String to check
	 * @param	number	$word_limit	Number to limit the words to 
	 * @return	text
	 */
	private function limit_words($str, $word_limit = null) {
		
		$words = strip_tags($str);
		
		$words = explode(" ", $words);
		
		return implode(" ", array_splice($words, 0, $word_limit));
	}


    /**
	 * Create a "Random" String
	 *
	 * @param	string	type of random string.  basic, alpha, alnum, numeric, nozero, unique, md5, encrypt and sha1
	 * @param	int	number of characters
	 * @return	string
	 */
	private function random_string($len = 8) {
		$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		return substr(str_shuffle(str_repeat($pool, ceil($len / strlen($pool)))), 0, $len);
	}

	/**
	 * Save the chat message, if the message id is null then generate a new id and save it as part of the message
	 * 
	 * 
	 * @param Object $chatMsg
	 * 
	 * @return Bool
	 */
	private function saveMessage($data, $item_id) {
		/** Generate a new id if the message id is empty */
		$data->message_id = (empty($data->message_id) || $data->message_id == "null") ? strtoupper($this->random_string(24)) : $data->message_id;
		$this->message_id = $data->message_id;

		try {
			/** Update the sender's last seen status */
			$this->db->query("UPDATE users SET online='1', last_seen = now() WHERE item_id = '{$data->sender_id}' LIMIT 1");

			/** Update the seeen status for messages between these two users */
			$s_stmt = $this->db->prepare("UPDATE users_chat SET seen_status = ?, seen_date=now() WHERE receiver_id = ? AND sender_id = ? AND seen_status = ?");
			$s_stmt->execute([1, $data->sender_id, $data->receiver_id, 0]);

			/** Save the message log */
			$stmt = $this->db->prepare("INSERT INTO users_chat SET item_id = ?, message_unique_id = ?, sender_id = ?, receiver_id = ?, message = ?, user_agent = ?");
			return $stmt->execute([$item_id, $this->message_id, $data->sender_id, $data->receiver_id, $data->message, $data->user_agent]);

		} catch(\PDOException $e) {}
	}

	public function onError(ConnectionInterface $conn, \Exception $e) {
		$conn->close();
	}
	
}