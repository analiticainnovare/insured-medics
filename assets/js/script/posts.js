var post_user_id = "",
    container = $(`div[id="createPostModal"]`),
    createBtn = $(`button[class~="create-post-button"]`),
    shareLoader = $(`div[id="createPostModal"] div[class="form-content-loader"]`),
    shareBtn = $(`div[id="createPostModal"] button[class~="share-post"]`),
    cancelBtn = $(`div[id="createPostModal"] button[class~="cancel-post"]`),
    postContent = $(`trix-editor[id="create-post-content"]`),
    posts_container = $(`div[id="profile_posts_list"]`),
    usernameBtn = $(`div[id="changeUsername"] button[class~="btn-outline-success"]`);

var apply_posts_click_handlers = () => {
    $(`span[data-function="toggle-post-files-attachment-list"]`).on("click", function() {
        let reply_id = $(this).attr("data-post-id");
        $(`div[class~="attachments_list"][data-post-id="${reply_id}"]`).slideToggle("slow");
    });
    $(`a[class~="like-post"]`).on("click", function() {
        let post_id = $(this).data("post-id");
        user_liked_post(post_id);
    });
    init_image_popup();
}

createBtn.on("click", function() {
    container.modal("show");
    post_user_id = $(this).data("company-id");
    $(`div[class="upload-overlay-cover"]`).css("display", "none");
});

cancelBtn.on("click", function() {
    postContent.html("");
    $(`div[class="upload-overlay-cover"]`).css("display", "none");
});

shareBtn.on("click", async function() {
    shareLoader.css("display", "flex");
    let post_to_share = {
        post_content: htmlEntities(postContent.html()),
        post_user_id: post_user_id
    };
    await $.post(`${baseUrl}api/posts/share`, post_to_share).then((response) => {
        if (response.code == 200) {
            container.modal("hide");
            postContent.html("");
            $(`div[id="createPostModal"] div[class~="file-preview"]`).html("");
            if (response.data.additional.data) {
                let the_post = formatProfilePost(response.data.additional.data);
                if ($(`div[id="profile_posts_list"] div[id="posts-listing"]:first`).length) {
                    $(`div[id="profile_posts_list"] div[id="posts-listing"]:first`).before(the_post);
                } else {
                    $(`div[id="profile_posts_list"]`).append(the_post);
                }
            }
        }
        shareLoader.css("display", "none");
        apply_posts_click_handlers();
    }).catch(() => {
        shareLoader.css("display", "none");
    });
});

var formatProfilePost = (rv) => {

        return `<div class="col-md-12 p-0 grid-margin" id="posts-listing" data-post-container="${rv.post_id}">
        <div class="card rounded replies-item">
            <div class="card-header">
                <div class="d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <img class="img-xs rounded-circle" src="${baseUrl}${rv.replied_by.image}" alt="">
                        <div class="ml-2">
                            <p class="cursor underline" title="Click to view summary information about ${rv.replied_by.fullname}" onclick="return user_basic_information('${rv.shared_by}')" data-id="${rv.shared_by}">${rv.replied_by.fullname}</p>
                            <p title="${rv.modified_date}" class="tx-11 replies-timestamp text-muted">${rv.time_ago}</p>
                        </div>
                    </div>
                    ${rv.delete_button}
                </div>
            </div>
            <div class="card-body">
                <div class="tx-14">${rv.post_content}</div>
                <div class="${rv.attachment.files.length ? `border-top mt-2 pt-2` : ""}">
                    <p>
                        <span ${rv.attachment.files.length ? `data-function="toggle-post-files-attachment-list" data-post-id="${rv.post_id}" class="cursor" data-toggle="tooltip" title="Hide Attachments"` : ""}>
                        ${rv.attachment.files.length ? `${rv.attachment.files.length} files attached (${rv.attachment.files_size})<span class="ml-2"><i class="fa fa-arrow-alt-circle-right"></i></span>` : ""}
                        </span>
                    </p>
                </div>
                <div class="attachments_list" style="display:none" data-post-id="${rv.post_id}">${rv.attachment_html}</div>
            </div>
            <div class="card-footer">
                <div class="d-flex post-actions">
                    <a href="javascript:void(0);" data-post-is-liked="${rv.is_liked}" data-post-id="${rv.post_id}" class="d-flex like-post align-items-center text-muted mr-4">
                        <i data-post-id="${rv.post_id}" class="fa ${rv.is_liked ? "text-primary" : ""} fa-heart"></i>
                        <p class="d-none d-md-block ml-2 ${rv.is_liked ? "text-primary" : ""}" data-likes-count="${rv.likes_count}">${rv.is_liked ? "Liked" : "Likes"} (${rv.likes_count})</p>
                    </a>
                    <a href="javascript:void(0);" data-post-id="${rv.post_id}" class="d-flex comment-on-post align-items-center text-muted mr-4">
                        <i class="fa fa-comment-alt"></i>
                        <p class="d-none d-md-block ml-2" data-comments-count="${rv.comments_count}">Comments (${rv.comments_count})</p>
                    </a>
                    <a href="javascript:void(0);" data-post-id="${rv.post_id}" class="d-flex share-post align-items-center text-muted mr-4">
                        <i class="fa fa-share"></i>
                        <p class="d-none d-md-block ml-2" data-likes-count="${rv.shares_count}">Shares (${rv.shares_count})</p>
                    </a>
                </div>        
            </div>
        </div>
    </div>`;
}

var user_liked_post = async(post_id) => {
    let is_liked = $(`a[class~="like-post"][data-post-id="${post_id}"]`).attr("data-post-is-liked");
    let l_container = $(`a[class~="like-post"][data-post-id="${post_id}"] p`);
    var likes = parseInt(l_container.attr("data-likes-count"));
    if(is_liked === "true") {
        await $.post(`${baseUrl}api/posts/likes_count`,{post_id: post_id}).then((response) => {
            if(response.code == 200) {
                likes = response.data.result;
            }
        });
        $(`a[class~="like-post"][data-post-id="${post_id}"]`).attr("data-post-is-liked", "false");
        $(`i[class~="fa-heart"][data-post-id="${post_id}"]`).removeClass("text-primary");
        l_container.removeClass("text-primary")
            .attr("data-likes-count", (likes))
            .html(`Likes (${likes})`);
    } else {
        await $.post(`${baseUrl}api/posts/likes_count`,{post_id: post_id}).then((response) => {
            if(response.code == 200) {
                likes = response.data.result;
            }
        });
        $(`a[class~="like-post"][data-post-id="${post_id}"]`).attr("data-post-is-liked", "true");
        $(`i[class~="fa-heart"][data-post-id="${post_id}"]`).addClass("text-primary");
        l_container.addClass("text-primary")
            .attr("data-likes-count", (likes))
            .html(`Likes (${likes})`);
    }
}

var postsLoader = async(data) => {
    await $.get(`${baseUrl}api/posts/list`, data).then((response) => {
        if (response.code == 200) {
            if (response.data.result) {
                let result = response.data.result;
                posts_container.attr("data-last-post-id", result.last_post_id);
                if (result.posts_list) {
                    let html = "",
                        prev_date = "";
                    $.each(result.posts_list, function(_, value) {
                        if (!prev_date || prev_date != value.raw_date) {
                            html += `<div class="message_list_day_divider_label"><button class="message_list_day_divider_label_pill">${value.clean_date}</button></div>`;
                        }
                        html += formatProfilePost(value);
                        prev_date = value.raw_date;
                    });
                    posts_container.append(html);
                    $(`button[id="load-more-posts"]`).fadeIn("slow").html("Load more");
                }
                if (!result.posts_list) {
                    $(`button[id="load-more-posts"]`).html("No posts available").attr("disabled", true);
                }
                if ((result.last_post_id == "no_more_record") || (result.first_post_id == result.last_post_id)) {
                    $(`button[id="load-more-posts"]`).html("No posts available").attr("disabled", true);
                }
                apply_posts_click_handlers();
            }
        }
    });
}

if (posts_container.length) {
    $(`button[id="load-more-posts"]`).html("Loading posts...");
    let autoload = posts_container.attr("data-autoload");

    if (autoload === "true") {
        let data = {
            resource_id: posts_container.attr("data-id"),
            last_post_id: posts_container.attr("data-last-post-id"),
            limit: 10
        };
        postsLoader(data);
        posts_container.attr("data-autoload", "false");
    }
}

$(`button[id="load-more-posts"]`).on("click", function() {
    $(`button[id="load-more-posts"]`).html("Loading posts...");
    let last_post = posts_container.attr("data-last-post-id");

    if (last_post == "no_more_record") {
        $(`button[id="load-more-posts"]`).html("No posts available").attr("disabled", true);
        return false;
    }
    let data = {
        resource_id: posts_container.attr("data-id"),
        last_post_id: last_post,
        limit: 10
    };
    postsLoader(data);
});

var profilePhotosList = (data) => {
    let photos_list = "";

    if(data.length) {
        photos_list += `<div class="rs-gallery-4 rs-gallery">`;
        photos_list += `<div class="row">`;
    }
    $.each(data, function(_, v) {
        profile_photos_array_list[v.unique_id] = v;
        photos_list += `
            <div data-file_container="${v.record_id}_${v.unique_id}" class="col-lg-4 col-md-4 attachment-container text-center">
                <div class="gallery-item">
                    <div data-attachment_item="${v.record_id}_${v.unique_id}">
                        <img height="90px" width="100%" src="${baseUrl}${v.path}"> 
                        <div class="gallery-desc">
                            <a class="image-popup" href="${baseUrl}${v.path}" title="${v.name} (${v.size}) on ${v.datetime}">
                                <i class="fa fa-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>`;
    });
    if(data.length) {
        photos_list += "</div></div>";
    }
    if(photos_list !== "") {
        $(`div[data-threads="profile_post_photos"]`).html(photos_list);
        init_image_popup();
    }
    
}

$(`span[id="company_username_change"]`).on("click", function() {
    $(`div[id="changeUsername"]`).modal("show");
});

usernameBtn.on("click", function() {
    let theLoader = $(`div[id="changeUsername"] div[class="form-content-loader"]`),
    username = $(`div[id="changeUsername"] input[name="username"]`),
    company_id = $(`div[id="changeUsername"] input[name="company_id"]`).val();
    usernameBtn.prop("disabled", true);
    theLoader.css("display", "flex");

    if(!username.length) {
        usernameBtn.prop("disabled", false);
        theLoader.css("display", "none");
        return false;
    }
    
    $.post(`${baseUrl}api/company/save_username`, {username: username.val(), company_id}).then((response) => {
        Toast.fire({
            icon: responseCode(response.code),
            title: response.data.result
        });
        if(response.code == 200) {
            $(`div[id="changeUsername"]`).modal("hide");
            $(`span[class="the_username"]`).html(username.val())
        }
        usernameBtn.prop("disabled", false);
        theLoader.css("display", "none");
    }).catch((res) => {
        usernameBtn.prop("disabled", false);
        theLoader.css("display", "none");
    });
});