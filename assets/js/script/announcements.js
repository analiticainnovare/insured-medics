var no_available_record = `<div class="form-data-unavailable">
        <div class="inner-content text-center"><p><i class="fa text-warning fa-exclamation-triangle fa-2x"></i></p>
        <small class="text-warning" style='font-size:12px; padding-top:10px'>No content available to display at the moment!</small>
        <p><small class="font-weight-bold cursor" data-dismiss="modal">
            <a href="${baseUrl}announcements" class="text-primary">Refresh</a>
        </small></p></div></div>`,
    announcements_array = {},
    announcements_container = $(`div[id="announcements_list"]`);

var format_Announcement_Content = (data) => {
        return `
        <div class="col-lg-4 col-md-6 mb-3 announcement-item cursor" data-item-id="${data.item_id}">
            <div class="card">
                <div class="card-content" data-isviewed="${data.is_seen}" data-isactive="${data.is_active}" data-iseditable="${data.is_editable}" data-item-id="${data.item_id}">
                    <div class="card-header pr-1 pl-1 pb-1 d-flex justify-content-between">
                        <div><h6 class="card-title font-weight-bolder p-0 m-0">${data.subject}</h6></div>
                        <div><p class="text-muted">${data.time_ago}</p></div>
                    </div>
                    <div class="card-body pr-2 pl-2 pt-1 pb-1">
                        <div class="text-right text-muted"></div>
                        <div class='message'>${data.message}</div>
                        <div class="text-muted text-right mb-0 pt-2 pb-0">${data.related_to}</div>
                    </div>
                </div>
                <div class="card-footer p-1 d-flex justify-content-between" style="height:35px">
                    <div>
                        ${data.is_active ? `<span class="badge badge-success mr-2">Active</span>` : `<span class="badge badge-danger mr-2">Inactive</span>`}
                        ${!data.is_editable ? "" : `<span data-item-id="${data.item_id}" data-function="load-form" data-module-id="${data.item_id}" data-module="post_announcement" title="Update this announcement before publishing" class='btn-outline-primary btn p-1 pr-2 pl-2 cursor'><i class='fa fa-edit'></i> Update</span>`}
                    </div>
                    <div>${data.is_seen ? `<i title="Viewed announcement" class="fa text-success fa-check-circle"></i>` : ""}</div>
                </div>
            </div>
        </div>`;
};

var preview_Announcement = () => {
    $(`div[class~="announcement-item"] div[class="card-content"]`).on("click", function() {
        let item_id = $(this).data("item-id"),
            announcement = announcements_array[item_id];
        $(`div[id="announcement_preview"]`).html(announcement.content);

        $(`div[id="announcementModal_${item_id}"]`).modal("show");
    });
}

var ajax_Load_Announcements = () => {
    $(`div[class="form-content-loader"]`).css({ "display": "flex", "position": "fixed" });
    announcements_container.html("");
    $.get(`${baseUrl}api/announcements/list`, function(response) {
        $(`div[class="form-content-loader"]`).css({ "display": "none" });
        if (response.code == 200) {

            let announcements = response.data.result.list,
                announcements_content = "";

            if(!response.data.result.list.length) {
                announcements_container.html(no_available_record);
                return false;
            }
            $.each(announcements, function(ii, data) {
                announcements_array[data.item_id] = data;
                announcements_content += format_Announcement_Content(data);
            });

            announcements_container.html(announcements_content);
            preview_Announcement();
            init_click_handlers();            
        } else {
            announcements_container.html(no_available_record);
        }
    }).catch(() => {
        $(`div[class="form-content-loader"]`).css({ "display": "none" });
    });

}

$(() => {
    if (announcements_container.length) {
        ajax_Load_Announcements();
    }
});