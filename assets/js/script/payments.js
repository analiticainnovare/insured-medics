var checkout_button = $(`button[class~="create-checkout"]`),
    item = checkout_button.attr("data-item"),
    item_id = checkout_button.attr("data-item-id"),
    payment_cancel_button = $(`button[class~="cancel-ongoing-payment-activity"]`),
    payment_button = $(`button[class~="make-payment"]`),
    payment_medium = $(`select[id="payment_medium"]`),
    payment_info = payment_medium.attr("data-transaction_info"),
    policy_payment_button = $(`button[data-button="save-policy_payment_option"]`),
    paymentCheck, paymentWindow;

checkout_button.on("click", () => {
    checkout_button.prop("disabled", true);
    checkout_button.html(`Generating <i class="fa fa-spin fa-spinner"></i>`);
    $.post(`${baseUrl}api/payments/create`, { item, item_id }).then((response) => {
        if (response.code == 200) {
            checkout_button.html(`Redirecting <i class="fa fa-spin fa-spinner"></i>`);
            setTimeout(() => {
                window.location.href = response.data.additional.href;
            }, 2000);
        } else {
            checkout_button.prop("disabled", false);
            checkout_button.html(`Make Payment`);
        }
    }).catch(() => {
        checkout_button.prop("disabled", false);
        checkout_button.html(`Make Payment`);
    });
});

payment_medium.on("change", () => {
    let medium = payment_medium.val();
    if (medium == "null") {
        payment_button.addClass("hidden");
    } else {
        payment_button.removeClass("hidden");
    }
});

function checkPaymentStatus(payment_module, payment_info) {
    // $.post(`${baseUrl}api/payments/verify`, { payment_module, payment_info }).then((response) => {
    //     if (response.code === 200) {
    //         clearInterval(paymentCheck);
    //         Toast.fire({
    //             icon: "success",
    //             title: "Payment Sucessfully processed."
    //         });
    //     }
    // }).catch(() => {
    //     payment_button.prop("disabled", false);
    //     payment_button.html(`Process Payment`);
    // });
}

payment_button.on("click", () => {
    let payment_mode = payment_medium.val();
    payment_button.prop("disabled", true);
    payment_button.html(`Processing Request <i class="fa fa-spin fa-spinner"></i>`);

    $.post(`${baseUrl}api/payments/checkout`, { payment_mode, payment_info }).then((response) => {
        if (response.code !== 200 || (response.code == 200 && response.data.result.checkout_url == undefined)) {
            Toast.fire({
                icon: "error",
                title: response.data.result
            });
            payment_button.html(`Process Payment`);
            payment_button.prop("disabled", false);
        } else {
            $(`div[class~="payment-backdrop"]`).removeClass('hidden');
            paymentWindow = window.open(response.data.result.checkout_url,
                `Payment for #${response.data.result.transaction_id}`,
                `width=700,height=600,resizable,scrollbars=yes,status=1,left=${($(window).width())*0.25}`
            );
            $(`button[class~="return-to-payment-window"]`).on('click', function() {
                paymentWindow.focus();
            });
            paymentCheck = setInterval(function() {
                checkPaymentStatus(payment_mode, payment_info);
            }, 3000);
        }
    }).catch(() => {
        payment_button.prop("disabled", false);
        payment_button.html(`Process Payment`);
    });
});

payment_cancel_button.on("click", () => {
    paymentWindow.close();
    payment_button.prop("disabled", false);
    payment_button.html(`Process Payment`);
    $(`div[class~="payment-backdrop"]`).addClass('hidden');
    window.location.href = current_url;
});

var verify_payment = () => {
    if ($(`div[class~="verify_payment"]`).length) {
        let payment_module = $(`div[class~="verify_payment"]`).attr("data-payment_module"),
            transaction_id = $(`div[class~="verify_payment"]`).attr("data-transaction_id");

        $.get(`${baseUrl}api/payments/verify`, { payment_module, transaction_id }).then((response) => {
            if (response.code == 200) {
                Toast.fire({
                    icon: "success",
                    title: response.data.result.message
                });
                paymentWindow.close();
                clearInterval(paymentCheck);
            } else {
                $(`div[class~="verify_payment"]`).html(`
                    <div class="text-center text-danger">${response.data.result.message}</div>
                    <a class="btn btn-outline-success" href="${response.data.result.url}">Try Again</a>
                `);
            }
        });
    }
}

policy_payment_button.on("click", () => {
    let policy_id = $(`button[data-button="save-policy_payment_option"]`).attr("data-policy_id"),
        payment_option = $(`select[id="payment_method"]`).val(),
        payment_interval = $(`select[id="payment_interval"]`).val();

    if (payment_option == "null") {
        Toast.fire({
            icon: "error",
            title: "Sorry! Please Select payment option."
        });
        return false;
    }

    policy_payment_button.prop("disabled", true).html(`Saving... <i class="fa fa-spin fa-spinner"></i>`);

    $.post(`${baseUrl}api/user_policy/save_payment_option`, { policy_id, payment_option, payment_interval }).then((response) => {
        if (response.code == 200) {

        }
        policy_payment_button.prop("disabled", false).html(`Save`);
    }, 'json').catch(() => {
        policy_payment_button.prop("disabled", false).html(`Save`);
    });
});