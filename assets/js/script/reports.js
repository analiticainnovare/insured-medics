var data_stream = "summary",
    gridLineColor = 'rgba(77, 138, 240, .1)',
    colors = {
        primary: "#727cf5",
        secondary: "#7987a1",
        success: "#42b72a",
        warning: "#fbbc06",
        danger: "#ff3366",
        light: "#ececec",
        dark: "#282f3a",
        info: "#68afff",
        muted: "#686868"
    },
    reporting_type = $(`select[name="reporting_type"]`),
    custom_date_selector = $(`select[name="period_option"]`),
    policy_premium_chart = $(`canvas[id="policy_premium_chart"]`),
    init_report_generator = $(`div[id="report_generation_init"]`),
    report_params = $(`div[class="report_generation_controller"]`),
    reporting_type_filters = $(`div[id="reporting_type_filters"]`),
    main_date_report_filter = $(`button[class~="main_date_report_filter"]`),
    custom_date_report_filter = $(`button[class~="custom_date_report_filter"]`),
    the_loader = $(`div[id="report_generation_init"] div[class="form-content-loader"]`),
    period_selector = $(`div[aria-label="period_selector"] button[type="button"][class~="period_selector"]`);

$(function() {

    var chartOptions = {
            maintainAspectRatio: false,
            legend: {
                display: false,
                labels: {
                    display: false
                }
            },
            scales: {
                xAxes: [{
                    display: true,
                    barPercentage: .5,
                    categoryPercentage: .5,
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        fontColor: '#8392a5',
                        fontSize: 10
                    }
                }],
                yAxes: [{
                    gridLines: {
                        color: gridLineColor
                    },
                    ticks: {
                        fontColor: '#8392a5',
                        fontSize: 14
                    }
                }]
            }
        },
        simpleChartOptions = {
            legend: {
                display: false,
                labels: {
                    display: false
                }
            },
            responsive: true,
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            scales: {
                xAxes: [{
                    display: false,
                    gridLines: {
                        display: false
                    }
                }],
                yAxes: [{
                    display: false,
                    gridLines: {
                        color: gridLineColor
                    }
                }]
            }
        };

    reporting_type.on("change", async function() {
        let streaming = $(this).val();
        reporting_type_filters.html(``);
        init_report_generator.attr("data-report_stream", streaming)
        await generate_report("load_report_filters", streaming);
    });

    period_selector.on("click", function() {
        let the_btn = $(this),
            current_period = the_btn.attr("record-period"),
            prev_period = report_params.attr("data-period"),
            streaming = init_report_generator.attr("data-report_stream");

        if (current_period == prev_period) {
            return false;
        }

        $(`div[aria-label="period_selector"] button[type="button"]`)
            .removeClass("btn-primary")
            .addClass("btn-outline-primary")
            .attr("data-is_active", false);
        the_btn.removeClass("btn-outline-primary").addClass("btn-primary").attr("data-is_active", "true");

        generate_report(current_period, streaming);
    });

    custom_date_report_filter.on("click", async function() {
        let start_date = $(`input[name="start_date"]`).val(),
            end_date = $(`input[name="end_date"]`).val(),
            prev_period = report_params.attr("data-period");

        if (start_date.length < 10 && end_date.length < 10) {
            Toast.fire({
                icon: "error",
                title: "Sorry! Please select a date to continue."
            });
            return false;
        }

        let current_period = `${start_date}:${end_date}`,
            streaming = init_report_generator.attr("data-report_stream");

        if (current_period == prev_period) {
            return false;
        }
        await generate_report(current_period, streaming);
    });

    main_date_report_filter.on("click", function() {
        let current_period = $(`div[aria-label="period_selector"] button[data-is_active="true"]`).attr("record-period"),
            streaming = init_report_generator.attr("data-report_stream");
        /**
         *  prev_period = report_params.attr("data-period")
            if (current_period == prev_period) {
            return false;
        } */
        generate_report(current_period, streaming);
    });

    custom_date_selector.on("change", function() {
        let value = $(this).val();
        if (value == "custom_date") {
            $(`button[class~="period_selector"], button[class~="main_date_report_filter"]`).addClass("hidden");
            $(`[class~="custom_date_selector"]`).removeClass("hidden");
        } else if (value == "predefined") {
            $(`button[class~="period_selector"], button[class~="main_date_report_filter"]`).removeClass("hidden");
            $(`[class~="custom_date_selector"]`).addClass("hidden");
        }
    });

    var handle_select_onchange = () => {
        $(`select[name="policy_type"]`).on("change", async function() {
            let report_type = reporting_type.val(),
                policy_type = $(this).val();
            let label = {
                data: {
                    request: "users_list",
                    report_type: report_type,
                    policy_type: policy_type
                }
            };
            await $.post(`${baseUrl}api/filters/list`, label).then((response) => {
                if (response.code === 200) {
                    console.log(response.data.result);
                }
            });
        });
    }

    var load_report_filters = async(report_filters) => {
        let filter_content = "<div class='row'>";
        $.each(report_filters, async function(_, e) {
            await $.each(e, async function(_, ee) {
                filter_content += `<div class="form-group col-lg-4 col-md-4">`;
                filter_content += `<label>${ee.field._title}</label>`;
                filter_content += ee.list;
                filter_content += `</div>`;
            });
        });
        filter_content += "</div>";
        reporting_type_filters.html(filter_content);
        handle_select_onchange();
    }

    var summary_report = async(summary_data) => {
        if (summary_data.current !== undefined) {
            let difference = summary_data.difference !== undefined ? summary_data.difference : "not_found";

            $.each(summary_data.current.data, function(i, e) {
                $(`h3[class="mb-2"][data-record="${i}"]`).html(e);
                if (difference !== "not_found") {
                    try {
                        $(`p[data-item="summary_percent"][data-record="${i}"]`)
                            .addClass(difference[i].percentage.class)
                            .html(difference[i].percentage.text);
                    } catch (err) {}
                }

            });
        }
    }

    var claims_policy_report = async(claims_data, policy_data) => {

        $(`div[class~="claims_against_premium-wrapper"]`).html(`<canvas height="400px; width:100%" id="claims_against_premium"></canvas>`);
        claims_against_premium = $(`canvas[id="claims_against_premium"]`);

        if (claims_against_premium.length) {

            new Chart(claims_against_premium, {
                type: 'bar',
                data: {
                    labels: policy_data.grouped_list.current.premium.data.labels,
                    datasets: [{
                        data: policy_data.grouped_list.current.premium.data.data,
                        label: "Premium Payments",
                        borderColor: "#7ee5e5",
                        backgroundColor: "#c2fdfd",
                        fill: false
                    }, {
                        type: "line",
                        data: claims_data.grouped_list.current.claims_payment.data.data,
                        label: "Claims Payment",
                        borderColor: "#f77eb9",
                        backgroundColor: "#ffbedd",
                        fill: false
                    }]
                },
                options: chartOptions
            });
        }

    }

    var policy_report = async(policy_data) => {

        $(`div[class~="policy_premium_chart-wrapper"]`).html(`<canvas height="375px; width:100%" id="policy_premium_chart"></canvas>`);
        $(`div[class~="simple_policy_chart-wrapper"]`).html(`<canvas style="height:100px; width:100%" id="simple_policy_chart"></canvas>`);

        simple_policy_chart = $(`canvas[id="simple_policy_chart"]`);
        policy_premium_chart = $(`canvas[id="policy_premium_chart"]`);

        if (policy_premium_chart.length) {
            new Chart(policy_premium_chart, {
                type: 'bar',
                data: {
                    labels: policy_data.grouped_list.current.premium.data.labels,
                    datasets: [{
                        label: 'Premiums',
                        data: policy_data.grouped_list.current.premium.data.data,
                        backgroundColor: colors.primary
                    }]
                },
                options: chartOptions
            });
        }

        if (simple_policy_chart.length) {
            new Chart(simple_policy_chart, {
                type: 'line',
                data: {
                    labels: policy_data.grouped_list.current.policy_counter.data.labels,
                    bezierCurve: true,
                    datasets: [{
                        label: 'Premiums',
                        data: policy_data.grouped_list.current.policy_counter.data.data,
                        backgroundColor: colors.primary,
                        borderColor: colors.info,
                        lineWidth: 0.5,
                        pointRadius: 0.1,
                        pointBorderWidth: 1,
                        lineTension: 0.5,
                        fill: false
                    }]
                },
                options: simpleChartOptions
            });
        }

    }

    var claims_report = async(claims_data) => {

        $(`div[class~="simple_claims_chart-wrapper"]`).html(`<canvas style="height:100px; width:100%" id="simple_claims_chart"></canvas>`);
        simple_claims_chart = $(`canvas[id="simple_claims_chart"]`);

        if (simple_claims_chart.length) {
            new Chart(simple_claims_chart, {
                type: 'line',
                data: {
                    labels: claims_data.grouped_list.current.claims_counter.data.labels,
                    bezierCurve: true,
                    datasets: [{
                        label: 'Claims Payments',
                        data: claims_data.grouped_list.current.claims_counter.data.data,
                        backgroundColor: colors.primary,
                        borderColor: colors.info,
                        lineWidth: 0.5,
                        pointRadius: 0.1,
                        pointBorderWidth: 1,
                        lineTension: 0.5,
                        fill: false
                    }]
                },
                options: simpleChartOptions
            });
        }
    }

    var clients_report = async(clients_data) => {

        $(`div[class~="simple_clients_chart-wrapper"]`).html(`<canvas style="height:100px; width:100%" id="simple_clients_chart"></canvas>`);
        $(`div[class~="users_group_chart-wrapper"]`).html(`<canvas height="400px" style="width:100%" id="users_group_chart"></canvas>`);
        simple_clients_chart = $(`canvas[id="simple_clients_chart"]`);
        users_group_chart = $(`canvas[id="users_group_chart"]`);

        if (simple_clients_chart.length) {
            new Chart(simple_clients_chart, {
                type: 'line',
                data: {
                    labels: clients_data.grouped_list.current.users_counter.data.labels,
                    bezierCurve: true,
                    datasets: [{
                        label: 'Clients Registered',
                        data: clients_data.grouped_list.current.users_counter.data.data,
                        backgroundColor: colors.primary,
                        borderColor: colors.info,
                        lineWidth: 0.5,
                        pointRadius: 0.1,
                        pointBorderWidth: 1,
                        lineTension: 0.5,
                        fill: false
                    }]
                },
                options: simpleChartOptions
            });
        }

        if (users_group_chart.length) {
            let users_grouping = clients_data.users_count.current.users_group,
                theLabels = new Array(),
                theData = new Array();
            $.each(users_grouping, function(key, v) {
                theLabels.push(v.title);
                theData.push(v.count);
            });

            new Chart(users_group_chart, {
                type: 'doughnut',
                data: {
                    labels: theLabels,
                    datasets: [{
                        label: "Population (millions)",
                        backgroundColor: ["#ff3366", "#7ee5e5", colors.warning, colors.primary, colors.dark, colors.light, "#f77eb9"],
                        data: theData
                    }]
                }
            });
        }
    }

    var company_policy_report = async(policy_data) => {

        $(`div[class~="company_policy-wrapper"]`).html(`<canvas height="400px" style="width:100%" id="company_policy_chart"></canvas>`);
        company_policy_chart = $(`canvas[id="company_policy_chart"]`);

        new Chart(company_policy_chart, {
            type: 'bar',
            data: {
                labels: policy_data.application_counter.current.grouping.labels,
                datasets: [{
                    label: 'Premiums',
                    data: policy_data.application_counter.current.grouping.data,
                    backgroundColor: colors.primary
                }]
            },
            options: chartOptions
        });
    }

    var users_report = async(user_data) => {

        if (user_data.users_count.current.list !== undefined) {

            if (user_data.users_count.current.list.agents_list !== undefined) {
                $(`table[id="agents_performance_list"]`).dataTable().fnDestroy();
                $(`table[id="agents_performance_list"]`).dataTable({
                    "aaData": user_data.users_count.current.list.agents_list,
                    "iDisplayLength": $myPrefs.list_count,
                    "columns": [
                        { "data": 'fullname' },
                        { "data": 'clients_count' },
                        { "data": 'claims_count' },
                        { "data": 'policies_count' },
                        { "data": 'premium_paid' }
                    ]
                });
            }

            if (user_data.users_count.current.list.clients_list !== undefined) {
                $(`table[id="clients_performance_list"]`).dataTable().fnDestroy();
                $(`table[id="clients_performance_list"]`).dataTable({
                    "aaData": user_data.users_count.current.list.clients_list,
                    "iDisplayLength": $myPrefs.list_count,
                    "columns": [
                        { "data": 'fullname' },
                        { "data": 'policies_count' },
                        { "data": 'premium_paid' }
                    ]
                });
            }

        }

    }

    var generate_report = async(period, streaming) => {

        variables = {
            period: period,
            label: {
                "stream_period": init_report_generator.attr("data-stream_period"),
                "stream": streaming
            }
        };

        if ($(`div[data-report_content="user_type"]`).length) {
            variables["label"]["user_type"] = $(`div[data-report_content="user_type"]`).attr("data-report_stream")
        }

        $.each($(`div[id="reporting_type_filters"] select`), function(i, e) {
            let key = $(this).attr("name"),
                value = $(this).val();

            if (value) {
                variables["label"][key] = value;
            }
        });

        the_loader.css({ "display": "flex" });

        await $.get(`${baseUrl}api/reports/generate`, variables).then(async(response) => {

            if (response.code == 203) {
                Toast.fire({
                    icon: "error",
                    title: response.data.result
                });
            } else if (response.code == 200) {

                report_params.attr("data-period", period);

                let report_data = response.data.result;

                $(`p[data-item="summary_percent"]`).removeClass("text-danger text-success");

                if (report_data.summary_report !== undefined) {
                    await summary_report(report_data.summary_report);
                }

                if (report_data.claims_report !== undefined) {
                    await claims_report(report_data.claims_report);
                }

                if (report_data.policy_report !== undefined) {
                    await policy_report(report_data.policy_report);
                }

                if (report_data.claims_report !== undefined && report_data.policy_report !== undefined) {
                    await claims_policy_report(report_data.claims_report, report_data.policy_report);
                }

                if (report_data.clients_report !== undefined) {
                    await clients_report(report_data.clients_report);
                }

                if (report_data.company_policy_report !== undefined) {
                    await company_policy_report(report_data.company_policy_report);
                }

                if (report_data.users_report !== undefined) {
                    await users_report(report_data.users_report);
                }

                if (report_data.load_report_filters !== undefined) {
                    await load_report_filters(report_data.load_report_filters);
                }

                $(`select[class~="selectpicker"]`).select2();

            }

            the_loader.css({ "display": "none" });
        }).catch(() => {
            the_loader.css({ "display": "none" });
        });
    }

    if (init_report_generator.length) {
        let period = $(`div[aria-label="period_selector"] button[data-is_active="true"]`).attr("record-period"),
            streaming = init_report_generator.attr("data-report_stream");
        generate_report(period, streaming);
    }

});