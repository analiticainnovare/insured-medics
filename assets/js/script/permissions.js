var permission_button = $(`button[class~="btn-user-permissions"]`),
    permission_window = $(`div[id="permissionsModal"]`),
    permission_body = $(`div[id="permissionsModal"]  div[class~="modal-body"]`),
    save_permission = $(`div[id="permissionsModal"] button[class~="btn-outline-success"]`),
    permission_footer = $(`div[id="permissionsModal"] div[class~="modal-footer"]`),
    body_loader = $(`div[id="permissionsModal"] div[class~="form-content-loader"]`);

permission_button.on("click", function() {
    let user_id = $(this).attr("data-user_id");
    permission_window.modal("show");
    body_loader.css({ "display": "flex" });
    permission_footer.addClass("hidden");
    $.post(`${baseUrl}api/users/load_permissions`, { user_id: user_id }).then((response) => {
        if (response.code == 200) {
            if (response.data.result.permissions !== undefined) {
                var displayPermission = `
                        <div class="row">
                            <div class="settings-form-msg col-12"></div>`;

                $.each(response.data.result.permissions, function(key, page) {
                    var permitted = null;
                    displayPermission += `<div class="mb-3 col-md-4 col-sm-6">
                            <div class="card"><div class="card-body slim-scroll" style="height:250px; max-height: 250px; overflow-y:auto">
                            <h5 class="mt-0">${key[0].toUpperCase() + key.slice(1)}</h5><div class="general-label">`;
                    var ii = 0;
                    $.each(page, function(roleKey, roleValue) {
                        ii++;
                        permitted = (roleValue == 1) ? 'checked' : null;
                        displayPermission += `
                                <div class="form-group mb-2">
                                    <div class="col-12">
                                        <div class="checkbox my-2">
                                            <div class="custom-control custom-checkbox">
                                                <input data-value="${roleValue}" data-name="${key.toString().toLowerCase()},${roleKey.toString().toLowerCase()}" ${permitted} type="checkbox" class="custom-control-input user-access-levels" id="customCheck_${key}_${ii}" data-parsley-multiple="groups" data-parsley-mincheck="2">
                                                <label class="custom-control-label cursor" for="customCheck_${key}_${ii}">${roleKey[0].toUpperCase() + roleKey.slice(1)}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>`;

                    });
                    displayPermission += `</div></div></div></div>`;
                });
                displayPermission += `</div>`;
                permission_body.html(displayPermission);
            }
            permission_footer.removeClass("hidden");
            body_loader.css({ "display": "none" });
        } else {
            Toast({
                icon: "error",
                title: "Sorry! No results found for your request."
            });
        }
    }, 'json').catch(() => {
        Toast({
            icon: "error",
            title: "Sorry! Error processing request"
        });
        permission_window.modal("hidd");
        permission_footer.removeClass("hidden");
        body_loader.css({ "display": "none" });
    });
});

save_permission.on("click", function() {
    var permissions_list = [],
        user_id = permission_button.attr("data-user_id"),
        access_level = permission_button.attr("data-user_access_level"),
        isChecked = 0;

    $.each($(`input[class~="user-access-levels"]`), function(i, e) {
        isChecked = ($(this).is(":checked")) ? 1 : 0;
        permissions_list.push($(this).attr('data-name') + "," + isChecked);
    });
    save_permission.prop("disabled", true);
    body_loader.css({ "display": "flex" });

    $.post(`${baseUrl}api/users/save_permissions`, { user_id, access_level, permissions_list }).then((response) => {
        save_permission.prop("disabled", false);
        body_loader.css({ "display": "none" });
        Toast.fire({
            icon: responseCode(response.code),
            title: response.data.result
        });
        if (response.code == 200) {

        }
    }, 'json').catch(() => {
        Toast.fire({
            icon: "error",
            title: "Sorry! Error processing request."
        });
        body_loader.css({ "display": "none" });
        save_permission.prop("disabled", false);
    });
});