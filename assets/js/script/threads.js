var preview_user_info = (user_info, more_info = "") => {
        let $form_content;
        console.log(user_info);
        $form_content = ``;
        $form_content = `<div class='row'>`;
        $form_content += `<div class='col-lg-12'>`;
        $form_content += `<div class='text-center user-profile-container'>`;
        $form_content += `<img width="100%" src="${baseUrl}${user_info.image}" class="profile-image">`;
        $form_content += `<div class='user-fullname'>${user_info.name}</div>`;
        $form_content += `<div class='user-position'>${user_info.position ? user_info.position : ""}</div>`;
        $form_content += `<div class='user-position'>${user_info.employer ? `(${user_info.employer})` : ""}</div>`;

    $form_content += `</div>`;

    $form_content += `<div class='col-lg-12'>`;

    $form_content += `<div class='row'>`;
    $form_content += `<div class='col-lg-6 user-profile-content'>`;
    $form_content += `<div class='user-profile-label'>Phone Number</div> <div class='user-profile-value'> ${user_info.phone_number} ${ user_info.phone_number_2 ? ` <br> ${user_info.phone_number_2}` : ""}</div>`;
    $form_content += `</div>`;
    $form_content += `<div class='col-lg-6 user-profile-content'>`;
    $form_content += `<div class='user-profile-label'>Email Address</div> <div class = 'user-profile-value'> <a href='mailto:${user_info.email}'> ${user_info.email}</a></div>`;
    $form_content += `</div>`;
    $form_content += `</div>`;

    $form_content += `<div class='row'>`;
    $form_content += `<div class='col-lg-6 user-profile-content'>`;
    $form_content += `<div class='user-profile-label'>Address</div> <div class='user-profile-value'> ${user_info.address ? user_info.address : ""} </div>`;
    $form_content += `</div>`;
    $form_content += `<div class='col-lg-6 user-profile-content'>`;
    $form_content += `<div class='user-profile-label'>Date of Birth</div> <div class='user-profile-value'> ${user_info.date_of_birth ? user_info.date_of_birth : ""} </div>`;
    $form_content += `</div>`;
    $form_content += `</div>`;

    $form_content += `<div class='row'>`;
    $form_content += `<div class='col-lg-12 user-profile-content'>`;
    $form_content += `<div class='user-profile-label'>Country / Nationality</div> <div class='user-profile-value'> ${user_info.country_name ? user_info.country_name : ""} ${user_info.nationality ? user_info.nationality : ""} </div>`;
    $form_content += `</div>`;
    $form_content += `</div>`;

    $form_content += `<div class='row'>`;
    $form_content += `<div class='col-lg-6 user-profile-content'>`;
    $form_content += `<div class='user-profile-label'>Account Created</div> <div class='user-profile-value'> ${user_info.date_created ? user_info.date_created : ""} </div>`;
    $form_content += `</div>`;
    $form_content += `<div class='col-lg-6 user-profile-content'>`;
    $form_content += `<div class='user-profile-label'>Last Login</div> <div class = 'user-profile-value'> ${user_info.last_login ? user_info.last_login : ""} </div>`;
    $form_content += `</div>`;
    $form_content += `</div>`;

    $form_content += `<div class='row'>`;
    $form_content += `<div class='col-lg-12 user-profile-content'>`;
    $form_content += `<div class='user-profile-label'>Description</div> <div class='user-profile-value'> ${user_info.description ? user_info.description : ""} </div>`;
    $form_content += `</div>`;
    $form_content += `<div class='col-lg-12 user-profile-content'>`;
    $form_content += `${more_info}`;
    $form_content += `</div>`;
    $form_content += `</div>`;
    $form_content += `
    <div class="row">
        <div class="col-lg-6 pt-3 mb-2 mt-2 border-top text-left">
            <button data-dismiss="modal" class="btn btn-outline-secondary">Close</button>
        </div>
        <div class="col-lg-6 pt-3 mb-2 mt-2 border-top text-right">
            <a href="${baseUrl}profile/${user_info.item_id}" class="btn btn-outline-primary">View Profile</a>
        </div>
    </div>`;
    $form_content += `</div>`;

    $form_content += `</div>`;
    $form_content += `</div>`;

    $basic_data_body.html($form_content);
}

var preview_company_info = (company_info) => {
        let $form_content = ``;
        $form_content = `<div class='row'>`;
        $form_content += `<div class='col-lg-12'>`;
        $form_content += `<div class='text-center user-profile-container'>`;
        $form_content += `<img width="100%" src="${baseUrl}${company_info.logo}" class="profile-image">`;
        $form_content += `<div class='user-fullname'>${company_info.name}</div>`;
        $form_content += `</div>`;

        $form_content += `<div class='col-lg-12'>`;

        $form_content += `<div class='row'>`;
        $form_content += `<div class='col-lg-6 user-profile-content'>`;
        $form_content += `<div class='user-profile-label'>Phone Number</div> <div class='user-profile-value'> ${company_info.contact ? company_info.contact : ""} ${ company_info.contact_2 ? ` <br> ${company_info.contact_2}` : ""}</div>`;
    $form_content += `</div>`;
    $form_content += `<div class='col-lg-6 user-profile-content'>`;
    $form_content += `<div class='user-profile-label'>Email Address</div> <div class = 'user-profile-value'> <a href='mailto:${company_info.email}'> ${company_info.email ? company_info.email : ""}</a></div>`;
    $form_content += `</div>`;
    $form_content += `</div>`;

    $form_content += `<div class='row'>`;
    $form_content += `<div class='col-lg-12 user-profile-content'>`;
    $form_content += `<div class='user-profile-label'>Address</div> <div class='user-profile-value'> ${company_info.address ? company_info.address : ""} </div>`;
    $form_content += `</div>`;
    $form_content += `</div>`;

    $form_content += `<div class='row'>`;
    $form_content += `<div class='col-lg-6 user-profile-content'>`;
    $form_content += `<div class='user-profile-label'>Website</div> <div class = 'user-profile-value'> ${company_info.website ? company_info.website : ""} </div>`;
    $form_content += `</div>`;
    $form_content += `<div class='col-lg-6 user-profile-content'>`;
    $form_content += `<div class='user-profile-label'>Date of Establishment</div> <div class='user-profile-value'> ${company_info.date_established ? company_info.date_established : ""} </div>`;
    $form_content += `</div>`;
    $form_content += `</div>`;

    $form_content += `<div class='row'>`;
    $form_content += `<div class='col-lg-12 user-profile-content'>`;
    $form_content += `<div class='user-profile-label'>Description</div> <div class='user-profile-value'> ${company_info.description ? company_info.description : ""} </div>`;
    $form_content += `</div>`;
    $form_content += `</div>`;

    $form_content += `<div class='row'>`;
    $form_content += `<div class='col-lg-12 user-profile-content'>`;
    $form_content += `<div class='user-profile-label'>Awards</div> <div class='user-profile-value'> ${company_info.awards ? company_info.awards : ""} </div>`;
    $form_content += `</div>`;
    $form_content += `</div>`;

    $form_content += `<div class="row"><div class="col-lg-12 pt-3  mt-2 border-top mb-2 text-center"><button data-dismiss="modal" class="btn btn-outline-secondary">Close</button></div></div>`;

    $form_content += `</div>`;

    $form_content += `</div>`;
    $form_content += `</div>`;
    
    $basic_data_body.html($form_content);
}

var preview_company_policy_info = (policy_info) => {

    let attachments = "";

    if (policy_info.attachment) {
        $.each(policy_info.attachment.files, function(ie, iv) {
            attachments += `
                <div class="col-lg-12 col-md-12 border attachment-container text-left mb-2 p-2">
                    <div data-toggle="tooltip" data-original-title="Click to preview: ${iv.name}">
                        <a title="Click to download the file" target="_blank" href="${baseUrl}download?file_id=${iv.record_id}&file_uid=${iv.unique_id}">
                            <span class="text text-danger"><i class="${iv.favicon} fa-1x"></i></span> 
                            <strong class="text-primary">${iv.name}</strong>
                        </a> <span class="text-muted tx-11">(${iv.size})</span>
                    </div>
                </div>`;
        });
    }

    let $form_content = ``;
    $form_content = `<div class='row'>`;
    $form_content += `<div class='col-lg-12'>`;
    $form_content += `<div class='text-center user-profile-container'>`;
    $form_content += `<div class='user-fullname'>${policy_info.name}</div>`;
    $form_content += `</div>`;

    $form_content += `<div class='col-lg-12'>`;

    $form_content += `<div class='row'>`;
    $form_content += `<div class='col-lg-6 user-profile-content'>`;
    $form_content += `<div class='user-profile-label'>Policy Code</div> <div class='user-profile-value'> ${policy_info.policy_code ? policy_info.policy_code : null}</div>`;
    $form_content += `</div>`;
    $form_content += `</div>`;

    $form_content += `<div class='row'>`;
    $form_content += `<div class='col-lg-6 user-profile-content'>`;
    $form_content += `<div class='user-profile-label'>Year Enrolled</div> <div class='user-profile-value'> ${policy_info.year_enrolled ? policy_info.year_enrolled : null}</div>`;
    $form_content += `</div>`;
    $form_content += `</div>`;

    $form_content += `<div class='row'>`;
    $form_content += `<div class='col-lg-12 user-profile-content'>`;
    $form_content += `<div class='user-profile-label'>Category</div> <div class='user-profile-value'> ${policy_info.category ? policy_info.category : ""} </div>`;
    $form_content += `</div>`;
    $form_content += `</div>`;

    $form_content += `<div class='row'>`;
    $form_content += `<div class='col-lg-12 user-profile-content'>`;
    $form_content += `<div class='user-profile-label'>Policy Description</div> <div class='user-profile-value'> ${policy_info.description ? policy_info.description : ""} </div>`;
    $form_content += `</div>`;
    $form_content += `<div class='col-lg-12 user-profile-content'>`;
    $form_content += `<div class='user-profile-label'>Policy Requirements</div> <div class='user-profile-value'> ${policy_info.requirements ? policy_info.requirements : ""} </div>`;
    $form_content += `</div>`;
    $form_content += `<div class='col-lg-12 user-profile-content'>`;
    $form_content += `<div class='user-profile-label'>Policy Documents</div> <div class='user-profile-value row'> ${attachments ? attachments : "<div class='col-lg-12'><span class='font-italic'>No documents attached</span></div>"} </div>`;
    $form_content += `</div>`;
    $form_content += `</div>`;

    $form_content += `<div class="row"><div class="col-lg-12  mt-2 border-top pt-3 mb-2 text-center"><button data-dismiss="modal" class="btn btn-outline-secondary">Close</button></div></div>`;

    $form_content += `</div>`;

    $form_content += `</div>`;
    $form_content += `</div>`;

    $basic_data_body.html($form_content);
}

var preview_award_information = (award_id) => {

    let award_info = company_awards_array[award_id];
    
    let that = $(this), d, stream = "",
            display_content = $(`div[id="profile_content_display"]`),
            display_loader = $(`div[id="profile_content_loader"]`);

        display_loader.html($form_loader);
        display_content.addClass("blur");
        $(`div[class~="profile_interaction_wrapper"]`).addClass("blur");

    let $form_content = `
    <div class='row'>
        <div class='col-lg-12'>
            <div class="card">
                <div class="card-body p-0 pb-3">
                    <div class='text-center user-profile-container'>
                        <div class='user-fullname mb-2'>${award_info.title}</div>
                        <div class=''><i class="fa fa-tags"></i> ${award_info.category}</div>
                    </div>
                    <i title="Click to close view" class="fa fa-times-circle close-quick-view"></i>
                    <div class='col-lg-12'>
                        <div class='border-top mt-2 pt-2'>
                            ${award_info.institution !== "null" ? `<p><span class="mr-3"><i class="fa fa-home"></i> <strong>Awarded By:</strong> ${award_info.institution}</span><p>`: ""}
                            ${award_info.award_date !== "null" ? `<p><span class="mr-3"><i class="fa fa-calendar-check"></i> <strong>Awarded By:</strong> ${award_info.award_date}</span><p>`: ""}
                        </div>
                        <div class='border-top mt-2 pt-2'>
                            ${award_info.received_by ? `<p><span class="mr-3"><i class="fa fa-user"></i> <strong>Received By:</strong> ${award_info.received_by}</span></p>`: ""}
                            ${award_info.assisted_by ? `<p><span class="mr-3"><i class="fa fa-user"></i> <strong>Assisted By:</strong> ${award_info.assisted_by}</span></p>`: ""}
                        </div>
                        <div class='border-top mt-2 pt-2' align="justify">
                            ${award_info.picture !== undefined ? `<img class="manager-picture" align="left" src="${baseUrl}${award_info.picture}">`: ""}
                            ${award_info.description !== null ? `${award_info.description}`: ""}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>`;

    setTimeout(() => {
        display_loader.html("");
        $(`div[class~="profile_interaction_wrapper"]`).addClass("hidden");
        display_content.html($form_content).removeClass("blur");
        applyTheFunctions();
    }, 500);

}

var preview_manager_information = (manager_id) => {
    
    let manager_info = company_managers_array[manager_id];
    
    let that = $(this), d, stream = "",
            display_content = $(`div[id="profile_content_display"]`),
            display_loader = $(`div[id="profile_content_loader"]`);

        display_loader.html($form_loader);
        display_content.addClass("blur");
        $(`div[class~="profile_interaction_wrapper"]`).addClass("blur");

    let $form_content = `
    <div class='row'>
        <div class='col-lg-12'>
            <div class="card">
                <div class="card-body p-0 pb-3">
                    <div class='text-center user-profile-container'>
                        <div class='user-fullname'>${manager_info.fullname}</div>
                        <div class=''>(${manager_info.position}) - held since: ${manager_info.position_since}</div>
                    </div>
                    <i title="Click to close view" class="fa fa-times-circle close-quick-view"></i>
                    <div class='col-lg-12'>
                        <div class='border-top mt-2 pt-2'>
                            ${manager_info.primary_contact !== undefined ? `<span class="mr-3"><i class="fa fa-phone"></i> ${manager_info.primary_contact}</span>`: ""}
                            ${manager_info.secondary_contact ? `<span class="mr-3"><i class="fa fa-phone-square"></i> ${manager_info.secondary_contact}</span>`: ""}
                            <span class="mr-3"><i class="fa fa-envelope"></i> ${manager_info.email}</span>
                        </div>
                        <div class='border-top mt-2 pt-2' align="justify">
                            ${manager_info.picture !== undefined ? `<img class="manager-picture" align="left" src="${baseUrl}${manager_info.picture}">`: ""}
                            ${manager_info.description !== undefined ? `${manager_info.description}`: ""}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>`;


    setTimeout(() => {
        display_loader.html("");
        $(`div[class~="profile_interaction_wrapper"]`).addClass("hidden");
        display_content.html($form_content).removeClass("blur");
        applyTheFunctions();
    }, 500);
}

var noticesList = (data) => {
    return `<a href="javascript:;" class="dropdown-item">
        <div class="icon"><i class="${data.notice_favicon}"></i></div>
        <div title="${data.full_message}" class="content">
            <p>${data.message}</p>
            <p class="sub-text text-muted">${data.date_created}</p>
        </div>
    </a>`
}

var cancelResourceRecord_List = (data) => {

    let approve_btn = data.is_permitted ? `<button onclick="return approve_cancel_record_request('${data.slug}','${data.raw_name}')" data-request-id="${data.slug}" class="btn btn-sm btn-outline-success" style="padding:5px">Approve</button>` : ``,
        comments_link = (data.status == "Pending") ? true : false;

    return `<div class="cancel_policy_request border-bottom pb-3 mb-3" data-request-id="${data.slug}">
        <div title="Click to view full details of this cancel policy request" class="d-flex align-items-center pb-3 mb-3">
            <div onclick="return user_basic_information('${data.created_by}')" title="View quick information about ${data.created_by_name}" class="cursor mr-3">
                <img src="${baseUrl}${data.created_by_image}" class="rounded-circle wd-35" alt="user">
            </div>
            <div class="w-100">
                ${data.reason ? `<p class="text-muted border-bottom pb-2 mb-2">${data.reason}</p>` : ``}
                <h6 class="text-body underline mb-0" onclick="return user_basic_information('${data.created_by}')"><span>${data.created_by_name}</span></h6>
                <strong>Status:</strong> ${data.status_label}
                <p class="text-muted tx-12"><i class="fa fa-calendar"></i> ${data.date_created}</p>
            </div>
        </div>
        <div class="d-flex justify-content-between">
            <p onclick="return resourceInteractionThread('comments_list','${data.slug}',${comments_link});" class="text-muted cursor read-cancel-comments"><i class="fa cursor fa-comments"></i> (<span data-request-id="${data.slug}">${data.comments_count}</span>) Comments</p>
            ${data.status == "Pending" ? `<p> ${approve_btn}
                <button onclick="return cancel_cancel_policy_request('${data.slug}','${data.raw_name}')" data-request-id="${data.slug}" class="btn btn-sm btn-outline-danger" style="padding:5px">Cancel</button>
            </p>` : ""}
        </div>
    </div>`;
}

var complaintsList = (data) => {
    let complaints_list = "";

    if($(`[data-threads="complaints_list"]`).length){
        $.each(data, function(_, v) {
            complaints_list += `
            <div href="${baseUrl}complaints-view/${v.item_id}" title="Click to view full details of this complaint" class="d-flex align-items-center border-bottom pb-3 mb-3">
                <div onclick="return user_basic_information('${v.user_id}')" title="View quick information about ${v.complained_by}" class="cursor mr-3">
                    <img src="${baseUrl}${v.complainant_image}" class="rounded-circle wd-35" alt="">
                </div>
                <div class="w-100">
                    <a href="${baseUrl}complaints-view/${v.item_id}" title="Click to view full details of this complaint">
                        <div class="d-flex justify-content-between">
                            <h6 class="text-body underline mb-2">${v.subject}</h6>
                            <p class="text-muted tx-12">${v.date_created}</p>
                        </div>
                        <p class="text-muted tx-13">${v.message}</p>
                        ${v.status_label}
                    </a>
                </div>
            </div>`;
        });
        $(`[data-threads="complaints_list"]`).html(complaints_list);
    }

    if($(`[data-threads="user_complaints_list"]`).length) {
        complaints_list = "<div class='row m-0'>";

        complaints_list += `<div class="col-lg-12"><h6 class="card-title mb-1">Recent Complaints</h6></div>`;
        $.each(data, function(_, v) {
            complaints_list += `
                <div class="col-lg-6 mb-3">
                    <div class="card">
                        <div class="card-body p-3 policy_item">
                            <a href="${baseUrl}complaints-view/${v.item_id}">
                                <div class="mb-3">
                                    <div class="border-bottom mb-2 pb-2"><h6 class="text-body mb--0">${v.subject}</h6></div>
                                    <p class="text-muted tx-12">Related to: ${v.related_to}</p>
                                </div>
                                <p class="text-muted tx-13">${v.message}</p>
                                <div class="d-flex justify-content-between">
                                    ${v.status_label}
                                    <p class="text-muted tx-12">${v.date_created}</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>`;
        });
        if(!data.length) {
            complaints_list += `
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body p-3 font-italic">
                        No complaints submitted.
                    </div>
                </div>
            </div>`;
        }
        complaints_list += "</div>";
        $(`[data-threads="user_complaints_list"]`).html(complaints_list);
    }
}

var policiesList = (data) => {
    let policies_list = "";
    $.each(data, function(_, v) {
        policies_array_list[v.item_id] = v;
        policies_list += `
            <div title="Click to preview the policy" data-load-id="${v.item_id}" data-display_content="user_policy_display" data-load-item="preview_policy_information" class="d-flex policy_item border-bottom justify-content-between mb-2 pb-2">
                <div class="d-flex align-items-center hover-pointer">
                    <img class="img-xs cursor rounded-circle" src="${baseUrl}${v.client_image}" alt="">
                    <div class="ml-2">
                        <p>${v.policy_name}</p>
                        <p class="tx-11 text-muted"><i class="fa fa-tags"></i> ${v.policy_category}</p>
                        <p class="tx-11 text-muted"><i class="fa fa-calendar-check"></i> ${v.policy_start_date}</p>
                        <p class="tx-11 text-muted">${v.policy_status}</p>
                    </div>
                </div>
            </div>`;
    });
    if(!data.length) {
        policies_list += `
        <div class="col-lg-12">
            <div class="card-body p-2 pl-0 font-italic">
                No policies applied.
            </div>
        </div>`;
    }
    if(policies_list !== "") {
        $(`div[data-threads="user_policy_list"]`).html(policies_list);
    }
}

var companyPolicyList = (data) => {
    let policies_list = "";
    $(`[data-record="company_policy_count"]`).text(data.length);
    $.each(data, function(_, v) {
        company_policies_array_list[v.item_id] = v;
        policies_list += `
            <div title="Click to preview the policy" data-load-id="${v.item_id}" data-display_content="company_policy_display" data-load-item="preview_company_policy" class="d-flex policy_item border-bottom justify-content-between mb-2 pb-2">
                <div class="d-flex align-items-center hover-pointer">
                    <div>
                        <p>${v.plain_name}</p>
                        <p class="tx-11 text-muted"><i class="fa fa-tags"></i> ${v.policy_category}</p>
                        ${v.year_enrolled ? `<p class="tx-11 text-muted"><i class="fa fa-calendar-check"></i> ${v.year_enrolled}</p>` : ""}
                        <p class="tx-11 text-muted">${v.policy_status}</p>
                    </div>
                </div>
            </div>`;
    });
    if(policies_list !== "") {
        $(`div[data-threads="company_policy"]`).html(policies_list);
    }
}

var notesList = (data) => {
   let notes_list = "";
   $(`[data-record="company_notes_count"]`).text(data.length);
    $.each(data, function(_, v) {
        company_notes_array_list[v.item_id] = v;
        notes_list += `
            <div data-record-setid="${v.item_id}" title="Click to read this note" class="policy_item border-bottom mb-2 pb-2">
                <div class="d-flex align-items-center hover-pointer" data-load-item="preview_company_note" data-display_content="company_note_display"  data-note-id="${v.item_id}" data-load-id="${v.item_id}">
                    ${v.image ? `<img class="img-xs cursor rounded-circle" src="${baseUrl}${v.image}" alt="">` : ""}
                    <div class="${v.image ? 'ml-2' : "" }">
                        <p class="font-16px text-uppercase">${v.title}</p>
                        ${v.tags ? `<p class="tx-11 text-muted"><i class="fa fa-tags"></i> ${v.tags}</p>` : ""}
                        <div class="d-flex align-items-center mb-2 hover-pointer">
                            <p class="tx-11 text-muted"><i class="fa fa-calendar-check"></i> ${v.date_created}</p>
                            <p class="text-muted ml-3"><i class="fa fa-eye"></i> <span class="note_views_count">${v.views_count}</span> Views</p>
                        </div>
                    </div>
                </div>
                ${v.is_editable && !viewedAs ? `
                    <div class="d-flex justify-content-between mt-1 border-top pt-2">
                        <span title="Update note information" data-toggle="tooltip" class="text-primary p-1" onclick="return quick_form_load('company_notes','${v.item_id}')"><i class="fa fa-edit"></i> Update</span>
                        <span title="Delete this note" onclick="delete_record('company_notes', '${v.item_id}')" data-toggle="tooltip" class="text-danger p-1"><i class="fa fa-trash"></i></span>
                    </div>
                ` : ""}
            </div>`;
    });
    if(notes_list !== "") {
        $(`div[data-threads="company_notes"]`).html(notes_list);
    }
}

var companyProfileInformation = () => {
    var display_content = $(`div[id="profile_content_display"]`),
        display_loader = $(`div[id="profile_content_loader"]`),
        display_information = "";
    
    $(`div[data-display_content="company_note_display"]`).on("click", function() {
        let item_id = $(this).attr("data-load-id"),
        v = company_notes_array_list[item_id];

        display_loader.html($form_loader);
        display_content.addClass("blur");
        $(`div[class~="profile_interaction_wrapper"]`).addClass("blur");

        display_information = `
        <div class="col-md-12 p-0">
             <div class="card p-0 rounded">
                <div class="card-header">
                    <div class="pr-2" data-note-id="${item_id}">
                        <div class="mb-2">
                            <h4 class="text-uppercase">${v.title}</h4>
                            <div class="d-flex align-items-center mb-2 hover-pointer">
                                ${v.tags ? `<p class="mr-3"><i class="fa fa-tags"></i> ${v.tags}</p>` : ""}
                                <p class="text-muted"><i class="fa fa-calendar-check"></i> ${v.date_created}</p>
                                <p class="text-muted ml-3"><i class="fa fa-eye"></i> <span class="note_views_count">${v.views_count}</span> Views</p>
                                <i title="Click to close view" class="fa fa-times-circle close-quick-view"></i>
                            </div>
                            ${v.image ? `<img src="${baseUrl}${v.image}" height="400px" class="rounded img-fluid" width="100%">`:""}
                        </div>
                        <div class="mt-2">
                            ${v.content}
                        </div>
                    </div>
                </div>
                <!--<div class="card-footer">
                    <div class="d-flex post-actions">
                        <div href="javascript:;" class="d-flex align-items-center text-muted">
                            <a title="Click to download this note." target="_blank" data-href="${baseUrl}download?file=${v.download_link}&file_uid=note" class="d-none cursor d-md-block">
                                <i class="fa fa-download"></i>Download this Note
                            </a>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>`;

        setTimeout(() => {
            display_loader.html("");
            display_content.html(display_information).removeClass("blur");
            $(`div[class~="profile_interaction_wrapper"]`).addClass("hidden");
            company_notes_array_list[item_id].views_count = (parseInt(v.views_count)+1);
            $(`div[data-note-id="${item_id}"] span[class="note_views_count"]`).text(v.views_count);
            $(`div[id="quiz-scroll-top"]`).animate({scrollTop: 0}, 100);
            applyTheFunctions();
        }, 500);
    });

    $(`div[data-display_content="company_policy_display"]`).on("click", function() {
        let item_id = $(this).attr("data-load-id"),
            d = company_policies_array_list[item_id],
            display_information = "";

        display_loader.html($form_loader);
        display_content.addClass("blur");
        $(`div[class~="profile_interaction_wrapper"]`).addClass("blur");

        display_information += `
        <div class="col-md-12 p-0">
             <div class="card p-0 rounded">
                <div class="card-header">
                    <div class="d-flex align-items-center justify-content-between">
                        <div class="d-flex align-items-center">
                            <h4 class='text-muted'>${d.plain_name}</h4>
                        </div>
                        <div class="d-flex align-items-center">
                            <div class="ml-2">
                                ${d.policy_code ? `<p class="tx-11 text-muted">Policy Code: <strong>${d.policy_code}</strong></p>`:""}
                                ${d.policy_category ? `<p class="tx-11 text-muted"><span class="cursor"><i class="fa fa-tags"></i> ${d.policy_category}</span></p>` : ""}
                                ${d.year_enrolled ? `<p title="Date of application submission" class="tx-11 text-muted"><i class="fa fa-calendar-check"></i> ${d.year_enrolled}</p>` : ""}
                                <i title="Click to close view" class="fa fa-times-circle close-quick-view"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="text-uppercase mb-2">Description</h5>
                    ${d.description ? d.description : ""}
                </div>

                ${d.requirements ? `
                    <div class="card-body border-top">
                        <h5 class="text-uppercase mb-2">Requirements</h5>
                        ${d.requirements}
                    </div>` : ""
                }
                
                ${d.attachment_html ? `
                    <div class="card-body border-top">
                        <div class="attachments_list">${d.attachment_html}</div>
                    </div>` : ""
                }

                <div class="card-footer">
                    <div class="d-flex post-actions">
                        <div href="javascript:;" class="d-flex align-items-center text-muted mr-4">
                            <p title="Click to preview policy application form." onclick="return previewFormData('${item_id}', 'policy_form');" class="d-none cursor d-md-block mr-2">
                                <i class="fa fa-address-card"></i>
                                Preview Policy Form
                            </p>
                            <p title="Click to preview claims application form." onclick="return previewFormData('${item_id}', 'claims_form');" class="d-none cursor d-md-block ml-2">
                                <i class="fa fa-address-card"></i>
                                Preview Claims Form
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>`;

        setTimeout(() => {
            display_loader.html("");
            $(`div[class~="profile_interaction_wrapper"]`).addClass("hidden");
            display_content.html(display_information).removeClass("blur");
            $(`body, html`).animate({scrollTop: 0}, 1000);
            applyTheFunctions();
        }, 500);
    });
}

var claimsList = (data) => {
    let claims_list = "";
    $.each(data, function(_, v) {
        claims_array_list[v.item_id] = v;
        claims_list += `
            <div title="Click to preview the policy" data-load-id="${v.item_id}" data-display_content="user_policy_display" data-load-item="preview_policy_information" class="d-flex policy_item border-bottom justify-content-between mb-2 pb-2">
                <div class="d-flex align-items-center hover-pointer">
                    <img class="img-xs cursor rounded-circle" src="${baseUrl}${v.requested_by_image}" alt="">
                    <div class="ml-2">
                        <p>${v.policy_name}</p>
                        <p class="tx-11 text-muted"><i class="fa fa-user"></i> ${v.requested_by}</p>
                        <p class="tx-11 text-muted"><i class="fa fa-calendar-check"></i> ${v.date_submitted}</p>
                        <p class="tx-11 text-muted">${v.status}</p>
                    </div>
                </div>
            </div>`;
    });
    if(!data.length) {
        claims_list += `
        <div class="col-lg-12">
            <div class="card-body p-2 pl-0 font-italic">
                No claims submitted.
            </div>
        </div>`;
    }
    $(`div[data-threads="user_claims_list"]`).html(claims_list);
}

var formatThreadReply = (rv) => {
        return `<div class="col-md-12 grid-margin" data-reply-container="${rv.item_id}">
        <div class="card rounded">
            <div class="card-header">
                <div class="d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <img class="img-xs rounded-circle" src="${baseUrl}${rv.replied_by.image}" alt="">
                        <div class="ml-2">
                            <p onclick="return user_basic_information('${rv.user_id}')" data-id="${rv.user_id}">${rv.replied_by.fullname}</p>
                            <p title="${rv.modified_date}" class="tx-11 replies-timestamp text-muted">${rv.time_ago}</p>
                        </div>
                    </div>
                    ${rv.delete_button}
                </div>
            </div>
            <div class="card-body">
                <div class="tx-14">${rv.message}</div>
                <div class="${rv.attachment.files.length ? `border-top mt-2 pt-2` : ""}">
                    <p><span ${rv.attachment.files.length ? `data-function="toggle-files-list" data-reply-id="${rv.item_id}" class="cursor" data-toggle="tooltip" title="Hide Attachments"` : ""}>${rv.attachment.files.length ? `${rv.attachment.files.length} files (${rv.attachment.files_size})<span class="ml-2"><i class="fa fa-arrow-alt-circle-right"></i></span>` : ""}</span></p>
                </div>
                <div class="attachments_list" style="display:none" data-reply-id="${rv.item_id}">${rv.attachment_html}</div>
            </div>
        </div>
    </div>`;
}

var formatThreadComment = (rv) => {
    return `<div class="col-md-12 grid-margin" id="comment-listing" data-reply-container="${rv.item_id}">
        <div class="card rounded replies-item">
            <div class="card-header">
                <div class="d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <img class="img-xs rounded-circle" src="${baseUrl}${rv.replied_by.image}" alt="">
                        <div class="ml-2">
                            <p class="cursor underline" title="Click to view summary information about ${rv.replied_by.fullname}" onclick="return user_basic_information('${rv.user_id}')" data-id="${rv.user_id}">${rv.replied_by.fullname}</p>
                            <p title="${rv.modified_date}" class="tx-11 replies-timestamp text-muted">${rv.time_ago}</p>
                        </div>
                    </div>
                    ${rv.delete_button}
                </div>
            </div>
            <div class="card-body pt-2 pb-2">
                <div class="tx-14">${rv.message}</div>
                <div class="${rv.attachment.files.length ? `border-top mt-2 pt-2` : ""}">
                    <p>
                        <span ${rv.attachment.files.length ? `data-function="toggle-comments-files-attachment-list" data-reply-id="${rv.item_id}" class="cursor" data-toggle="tooltip" title="Hide Attachments"` : ""}>
                        ${rv.attachment.files.length ? `${rv.attachment.files.length} files (${rv.attachment.files_size})<span class="ml-2"><i class="fa fa-arrow-alt-circle-right"></i></span>` : ""}
                        </span>
                    </p>
                </div>
                <div class="attachments_list" style="display:none" data-reply-id="${rv.item_id}">${rv.attachment_html}</div>
            </div>
        </div>
    </div>`;
}

var quick_share_comment = (item_id, textarea) => {
    $(`button[class~="quick-share-button"][data-resource-id="${item_id}"]`)
        .attr("disabled", true)
        .html("Processing! Please wait...");
    let content = $(`textarea[name="${textarea}"]`).val(),
        comments_counter = $(`span[data-request-id="${item_id}"]`);
    let comment = {
        item_id: item_id,
        resource: "cancel_policy_comments",
        comment: content
    };
    if(content.length < 5) {
        Toast.fire({
            icon: "error",
            title: "The comment field cannot be empty"
        });
        $(`button[class~="quick-share-button"][data-resource-id="${item_id}"]`)
            .attr("disabled", false)
            .html("Share Comment");
        return false;
    }

    $(`[data-threads="cancel_policy_list"]`).attr("data-autoload", "true");

    $.post(`${baseUrl}api/replies/comment`, comment).then((response) => {
        if (response.code == 200) {
            Toast.fire({
                icon: "success",
                title: response.data.result
            });
            if (response.data.additional.data) {
                let the_comment = formatThreadComment(response.data.additional.data);
                if ($(`div[id="resourceInfoModal"] div[class="modal-body"] div[id="comment-listing"]:first`).length) {
                    $(`div[id="resourceInfoModal"] div[class="modal-body"] div[id="comment-listing"]:first`).before(the_comment);
                } else {
                    $(`div[id="resourceInfoModal"] div[class="modal-body"]`).append(the_comment);
                }
            }
            loadCancelPolicyRequests();
        } else {
            Toast.fire({
                icon: "error",
                title: response.data.result
            });
        }
        $(`button[class~="quick-share-button"][data-resource-id="${item_id}"]`)
            .attr("disabled", false)
            .html("Share Comment");
        $(`textarea[name="${textarea}"]`).val("");
    }).catch(() => {
        $(`button[class~="quick-share-button"][data-resource-id="${item_id}"]`)
            .attr("disabled", false)
            .html("Share Comment");
        Toast.fire({
            icon: "error",
            title: "Sorry! Error processing request."
        });
    });

}

var cancel_cancel_policy_request = (request_id, raw_name) => {
    let the_modal = $(`div[id="generalModal"]`);
    $(`div[id="generalModal"] div[class='modal-header']`).html("Cancel Request");
    $(`div[id="generalModal"] button[class~='btn-outline-success']`).attr({"data-record-id": request_id, "data-resource":"cancel_record_request"});
    $(`div[id="generalModal"] div[class='modal-body']`).html(`Are you sure you want to cancel the request placed initially to cancel the ${raw_name}? Please note that this action cannot be reversed.`);
    the_modal.modal("show");
}

var approve_cancel_record_request = (request_id, raw_name) => {
    let the_modal = $(`div[id="generalModal"]`);
    $(`div[id="generalModal"] div[class='modal-header']`).html(`Approve Cancel ${raw_name} Request`);
    $(`div[id="generalModal"] button[class~='btn-outline-success']`).attr({"data-record-id": request_id, "data-resource":"approve_cancel_record_request"});
    $(`div[id="generalModal"] div[class='modal-body']`).html(`Are you sure you want to approve the request to Cancel this ${raw_name}? Please note that this action cannot be reversed.`);
    the_modal.modal("show");
}

var resourceInteractionThread = (to_stream, item_id, post_comment = false) => {
    let stream_list = {}, comments = "";
    if(policies_array_list[item_id] !== undefined) {
        stream_list = policies_array_list[item_id][to_stream]["replies_list"];
    }
    else if(claims_array_list[item_id] !== undefined) {
        stream_list = claims_array_list[item_id][to_stream]["replies_list"];
    }
    else if(cancel_policy_notices[item_id] !== undefined) {
        stream_list = cancel_policy_notices[item_id][to_stream]["replies_list"];
    }

    $basic_data_modal.modal("show");
    $basic_data_header.html("Comments Thread");
    $basic_data_body.html($no_record);
    
    if(post_comment) {
        comments += `
        <div class="col-md-12 mt-0 pt-0" id="">
            <div class="form-group">
                <label>Post Comment</label>
                <textarea class="form-control" name="cancel_policy_comment" rows="6"></textarea>
            </div>
            <div class="form-group text-right">
                <button onclick="return quick_share_comment('${item_id}', 'cancel_policy_comment')" data-resource-id="${item_id}" class="btn quick-share-button btn-sm btn-outline-success">Share Comment</button>
            </div>
        </div>`;
    }

    if(stream_list.length) {
        let prev_date = "";
        $.each(stream_list, function(_, e) {
            if (!prev_date || prev_date != e.raw_date) {
                comments += `<div class="message_list_day_divider_label"><button class="message_list_day_divider_label_pill">${e.clean_date}</button></div>`;
            }
            comments += formatThreadComment(e);
            prev_date = e.raw_date;
        });
        comments += `<div class="col-md-12 mt-4 border-top pt-4 mb-3 text-center"><span class="btn btn-outline-secondary" data-dismiss='modal'>Close Modal</span></div>`;
    }

    if(comments) {
        $basic_data_body.html(comments);
    }
}

var previewFormData = (item_id, form_type = null) => {
    let form_fields = {}, form_answers = {}, title = "",
        form_preview_content = "",
        readonly = "readonly='readonly'";

    if(policies_array_list[item_id] !== undefined) {
        title = "Policy Application Form Data";
        form_fields = policies_array_list[item_id]["form_fields"];
        form_answers = policies_array_list[item_id]["form_answers"];
    }
    else if(claims_array_list[item_id] !== undefined) {
        title = "Claims Application Form Data";
        form_fields = claims_array_list[item_id]["form_fields"];
        form_answers = claims_array_list[item_id]["form_answers"];
    }
    else if(company_policies_array_list[item_id] !== undefined) {
        readonly = "";
        title = (form_type == "policy_form") ? "Policy Form" : "Claims Form";

        if(company_policies_array_list[item_id][form_type] !== undefined) {
            try {
                form_fields = company_policies_array_list[item_id][form_type]["fields"];
            } catch(err) {
                form_preview_content = $no_record;
            }
        }
    }

    $basic_data_modal.modal("show");
    $basic_data_header.html(title);
    $basic_data_body.html($no_record);
    

    $.each(form_fields, function(key, iv) {
        
        let required = (iv.required == "yes") ? "required" : "";
        let value = (form_answers[key] !== undefined) ? form_answers[key] : "";

        form_preview_content += `<div class="col-lg-12 p-0">`;
        form_preview_content += `<div class="form-group">`;
        form_preview_content += `<label for="field[${key}]">${iv.label} ${(required) ? " &nbsp;<span class='required'>*</span>" : ""}</label>`;

        if ($.inArray(iv.type, ["input", "date", "email"]) !== -1) {
            let type = ($.inArray(iv.type, ["input", "date"]) !== -1) ? "text" : iv.type;
            let the_class = (iv.type == "date") ? "datepicker" : "";
            form_preview_content += `<input ${readonly} value="${value}" type="${type}" ${required} class="form-control ${the_class}" name="field[${key}]" id="field[${key}]">`;
        }

        if ($.inArray(iv.type, ["textarea"]) !== -1) {
            form_preview_content += `<textarea ${readonly} rows="5" class="form-control" name="field[${key}]" id="field[${key}]">${value}</textarea>`;
        }

        if ($.inArray(iv.type, ["select"]) !== -1) {
            form_preview_content += `<select ${readonly} class="form-control selectpicker" name="field[${key}]" id="field[${key}]" data-width="100%">`;
            form_preview_content += `<option value="null">Please Select:</option>`;
            $.each(iv.select, function(_, sv) {
                form_preview_content += `<option ${((value == sv.value) ? "selected" : "null")} value="${sv.value}">${sv.label}</option>`;
            });
            form_preview_content += "</select>";
        }

        form_preview_content += "</div>";
        form_preview_content += "</div>";
    });

    form_preview_content += `<div class="col-md-12 mt-4 border-top pt-4 mb-3 text-center"><span class="btn btn-outline-secondary" data-dismiss='modal'>Close Modal</span></div>`;
    $basic_data_body.html(form_preview_content);
}

var applyTheFunctions = () => {
    $(`span[data-function="toggle-files-list"]`).on("click", function() {
        let reply_id = $(this).attr("data-reply-id");
        $(`div[class~="attachments_list"][data-reply-id="${reply_id}"]`).slideToggle("slow");
    });
    init_image_popup();

    $(`i[class~="close-quick-view"]`).on("click", function() {
        $(`div[id="user_policy_display"], div[id="profile_content_display"]`).html("");
        $(`div[class~="user_interaction_wrapper"], div[class~="profile_interaction_wrapper"]`).removeClass("hidden blur");
    });
}

var previewPolicyDetailedInformation = () => {
    
    $(`[data-load-item="preview_policy_information"]`).on("click", function() {
        let that = $(this), d, stream = "",
            policy_id = that.attr("data-load-id"),
            display_content = $(`div[id="${that.attr("data-display_content")}"]`),
            display_loader = $(`div[id="profile_content_loader"]`);

        if(policies_array_list[policy_id] !== undefined) {
            d = policies_array_list[policy_id];
            stream = "policies";
        }
        else if(claims_array_list[policy_id] !== undefined) {
            d = claims_array_list[policy_id];
            stream = "claims";
        }
        
        display_loader.html($form_loader);
        display_content.addClass("blur");
        $(`div[class~="user_interaction_wrapper"]`).addClass("blur");

        let policy_content = "";
        policy_content = `
        <div class="col-md-12">
            <div class="card rounded">
                <div class="card-header">
                    <div class="d-flex align-items-center justify-content-between">
                        <div class="d-flex align-items-center">
                            <div class="ml-2">
                                ${d.client_joined ? `<h4 class='text-muted'>Insurance Policy</h4>`  : "<h4 class='text-muted'>Policy Claim Request</h4>"}
                            </div>
                        </div>
                        <div class="d-flex align-items-center">
                            <div class="ml-2">
                                ${d.managed_by ? `
                                    <p class="tx-11 text-muted">Managed By:</p>
                                    <p><span class="underline cursor" onclick="return user_basic_information('${d.managed_by.id}')">${d.managed_by.name}</span></p>
                                    <p class="tx-11 text-muted">(${d.managed_by.title})</p>
                                    <p title="Date of application submission" class="tx-11 text-muted"><i class="fa fa-calendar-check"></i> ${d.date_submitted}</p>` : `
                                        <p class="tx-11 text-muted">Request Made By:</p>
                                        <p><span class="underline cursor" onclick="return user_basic_information('${d.created_by}')">${d.requested_by}</span></p>
                                        <p title="Date of application submission" class="tx-11 text-muted"><i class="fa fa-calendar-check"></i> ${d.date_submitted}</p>
                                    `
                                }
                                <i title="Click to close view" class="fa fa-times-circle close-quick-view"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="mt-0 pt-0"><span class="text-muted">Policy Name:</span> ${d.policy_name}<h5>
                    <p class="mb-0 tx-14"><span class="text-muted">Policy ID:</span> ${d.policy_id}</p>
                    ${stream == "policies" ? `
                        <p class="mb-0 tx-14"><span class="text-muted">Enrolled Date:</span> ${d.policy_start_date}</p>
                        <p class="mb-3 tx-14"><span class="text-muted">Policy Status:</span> ${d.policy_status}</p>` : `
                        <p class="mb-0 tx-14"><span class="text-muted">Enrolled Date:</span> <i class="fa fa-calendar-check"></i> ${d.policy_details.policy_start_date}</p>
                        <p class="mb-2 tx-14"><span class="text-muted">Status:</span> ${d.policy_details.policy_status}</p>
                        <p class="mb-0 pt-2 border-top tx-14"><span class="text-muted">Managed by:</span> ${d.policy_details.assigned_to_name}</p>
                        `
                    }
                    ${ d.pending_cancel_request ? `<span class="text-danger tx-14" type="button">Pending Cancel Request</span>` : ""}

                    <div class="${d.attachment.files.length ? `border-top mt-2 pt-2` : ""}">
                        <p class="text-muted tx-14">
                            <span ${d.attachment.files.length ? `data-function="toggle-files-list" data-reply-id="${d.item_id}" class="cursor" data-toggle="tooltip" title="Hide Attachments"` : ""}>
                            ${d.attachment.files.length ? `${d.attachment.files.length} files (${d.attachment.files_size})<span class="ml-2"><i class="fa fa-arrow-alt-circle-right"></i></span>` : ""}
                            </span>
                        </p>
                    </div>
                    <div class="attachments_list" style="display:none" data-reply-id="${d.item_id}">${d.attachment_html}</div>
                </div>
                <div class="card-footer">
                    <div class="d-flex post-actions">
                        <a href="javascript:;" class="d-flex align-items-center text-muted mr-4">
                            <i class="fa fa-comment"></i>
                            <p title="Click to read comments shared on this policy." onclick="return resourceInteractionThread('comments_list','${policy_id}');" class="d-none d-md-block ml-2">Comments (<span class="text-muted">${d.comments_count ? d.comments_count : 0}</span>)</p>
                        </a>
                        ${d.replies_list !== undefined ? `
                            <a href="javascript:;" class="d-flex align-items-center text-muted mr-4">
                                <i class="fa fa-comments"></i>
                                <p title="Click to read replies shared on this policy." onclick="return resourceInteractionThread('replies_list','${policy_id}');" class="d-none d-md-block ml-2">Private Interactions (<span class="text-muted">${d.replies_count ? d.replies_count : 0}</span>)</p>
                            </a>
                        ` : ""}
                        <a href="javascript:;" class="d-flex align-items-center text-muted mr-4">
                            <i class="fa fa-address-card"></i>
                            <p title="Click to preview policy application form." onclick="return previewFormData('${policy_id}');" class="d-none d-md-block ml-2">Preview Form</p>
                        </a>
                        <a title="Click to go to the policy page." href="${baseUrl}${stream}-view/${d.item_id}" class="d-flex align-items-center text-muted mr-4">
                            <i class="fa fa-link"></i>
                            <p class="d-none d-md-block ml-2">${stream == "policies" ? "Go to Policy" : "Go to Request"}</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>`;

        setTimeout(() => {
            display_loader.html("");
            $(`div[class~="user_interaction_wrapper"]`).addClass("hidden");
            display_content.html(policy_content).removeClass("blur");
            applyTheFunctions();
            $(`div[class~="user_interaction_wrapper"]`).animate({scrollTop: 0}, 10);
        }, 500);
    });
}