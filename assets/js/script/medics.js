var description_content, trix_instance, repliesList, lastReplyId, ajaxFormAction, ajaxFormData, app_formData, app_formAction,
    claims_array_list = {},
    policies_array_list = {},
    cancel_policy_notices = {},
    company_notes_array_list = {},
    profile_photos_array_list = {},
    company_policies_array_list = {},
    $form_modal = $(`div[id="formsModal"]`),
    imaggepoppup = $(`a[class~="image-popup"]`),
    $replies_modal = $(`div[id="repliesModal"]`),
    $basic_data_modal = $(`div[id="resourceInfoModal"]`),
    app_notification_box = $(`div[class~="email-notification"]`),
    $form_body = $(`div[id="formsModal"] div[class="modal-body"]`),
    $form_header = $(`div[id="formsModal"] h5[class="modal-title"]`),
    $replies_body = $(`div[id="repliesModal"] div[class~="modal-body"]`),
    $replies_header = $(`div[id="repliesModal"] h5[class="modal-title"]`),
    $form_loaded = $(`div[id="formsModal"] input[class="ajax-form-loaded"]`),
    $basic_data_body = $(`div[id="resourceInfoModal"] div[class="modal-body"]`),
    $basic_data_header = $(`div[id="resourceInfoModal"] h5[class="modal-title"]`),
    $replies_loaded = $(`div[id="repliesModal"] input[class="ajax-replies-loaded"]`),
    app_notification_content = $(`div[class~="email-notification"] div[class="content"]`),
    $form_error = `<div class="form-content-loader" style="display: flex; position: absolute;"><div class="offline-content text-center"><p><i class="fa text-warning fa-exclamation-triangle fa-2x"></i></p><small class="text-danger" style='font-size:12px; padding-top:10px'>Error processing request!</small><p><small class="text-danger font-weight-bold cursor" data-dismiss="modal" id="close-div">Close</small></p></div></div>`,
    $no_record = `<div class="form-content-loader" style="display: flex; position: absolute;"><div class="offline-content text-center"><p><i class="fa text-warning fa-exclamation-triangle fa-2x"></i></p><small class="text-warning" style='font-size:12px; padding-top:10px'>No content found to display at the moment!</small><p><small class="text-danger font-weight-bold cursor" data-dismiss="modal" id="close-div">Close</small></p></div></div>`,
    $form_loader = `<div class="form-content-loader" style="display: flex; position: absolute;"><div class="offline-content text-center"><p><i class="fa fa-spin fa-spinner fa-2x"></i></p><small style='font-size:12px; padding-top:10px'>Populating Data...</small></div></div>`;

$("html").on("dragover", function(e) {
    e.preventDefault();
    e.stopPropagation();
    $("h1").text("Drag here");
});

$("html").on("drop", function(e) {
    e.preventDefault();
    e.stopPropagation();
});

var init_datable = () => {
    $('table[class~="dataTable"]').DataTable({
        "iDisplayLength": $myPrefs.list_count,
        "language": { search: "" }
    });
}
init_datable();

$(`label[for="leave_comment_content"]`).on("click", function() {
    $(`div[id="leave-comment-content"]`).slideToggle();
});

$.cachedScript = function(url, options) {
    options = $.extend(options || {}, {
        dataType: "script",
        cache: true,
        url: url
    });
    return $.ajax(options);
};

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 6000,
    onOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
});

function format_currency(total) {
    var neg = false;
    if (total < 0) {
        neg = true;
        total = Math.abs(total);
    }
    return (neg ? "-" : '') + parseFloat(total, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
}

var select_options = async(item_name, array_list, title) => {
    $(`select[name="${item_name}"]`).find('option').remove().end();
    $(`select[name="${item_name}"]`).append(`<option value="null">${title}</option>`);
    $.each(array_list, function(val, text) {
        $(`select[name="${item_name}"]`).append(`<option value="${text.item_id}">${text.item_name}</option>`);
    });
}

var init_image_popup = () => {
    $(`a[class~="image-popup"]`).magnificPopup({
        type: 'image',
        callbacks: {
            beforeOpen: function() {
                this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure animated zoomInDown');
            }
        },
        gallery: {
            enabled: true
        }
    });
}

function htmlEntities(str) {
    return String(str).replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

function save_profile_picture() {

    let record_id = $('div[class="upload_file_url"]').attr("data-record-id");

    $(`button[id="save-profile-picture"]`).on("click", function() {
        $.post(`${baseUrl}api/users/save_image`, { user_id: record_id }).then((resp) => {
            if (resp.code == 200) {
                $(`div[id="profilePicture"]`).modal(`hide`);
                $(`div[class~="file-upload-preview"]`).html(``);
                $(`img[id="form-control-file"]`).attr("src", `${baseUrl}${resp.data.result}`);
                $(`img[class~="user_profile_image"]`).attr("src", `${baseUrl}${resp.data.result}`);
                Toast.fire({
                    icon: 'success',
                    title: "Profile picture successfully updated."
                });
            }
        });
    });

    $(`button[id="save-company-cover-picture"]`).on("click", function() {
        $.post(`${baseUrl}api/company/save_cover_image`, { company_id: record_id }).then((resp) => {
            if (resp.code == 200) {
                $(`div[id="profilePicture"]`).modal(`hide`);
                $(`div[class~="file-upload-preview"]`).html(``);
                $(`img[id="form-control-file"]`).attr("src", `${baseUrl}${resp.data.result}`);
                $(`img[class~="company_cover_image"]`).attr("src", `${baseUrl}${resp.data.result}`);
                Toast.fire({
                    icon: 'success',
                    title: "Cover picture successfully updated."
                });
            }
        });
    });
}

var deleteReply = function() {
    $(`a[class~="delete-reply"]`).on("click", function() {
        let reply_id = $(this).attr("data-reply-id");
        $.post(`${baseUrl}api/replies/delete`, { reply_id: reply_id }).then((response) => {
            if (response.code == 200) {
                $(`div[data-reply-container="${reply_id}"] div[class~="card-body"] [class="tx-14"]`).html(`<div class="font-italic text-danger">This message was deleted</div>`);
                $(`div[data-reply-container="${reply_id}"] [id="reply-option"]`).remove();
            }
        });
    });
}

var apply_comment_click_handlers = () => {
    $(`span[data-function="toggle-comments-files-attachment-list"]`).on("click", function() {
        let reply_id = $(this).attr("data-reply-id");
        $(`div[class~="attachments_list"][data-reply-id="${reply_id}"]`).slideToggle("slow");
    });

    $(".attachment-container .attachment-item").mouseenter(function() {
        let item = $(this).attr("data-attachment_item");
        $(`span[data-attachment_options='${item}']`).css("display", "block");
    }).mouseleave(function() {
        let item = $(this).attr("data-attachment_item");
        $(`span[data-attachment_options='${item}']`).css("display", "none");
    });

    init_image_popup();
    deleteReply();
}

const responseCode = (code) => {
    if (code == 200 || code == 201) {
        return "success";
    } else {
        return "error";
    }
}

const serializeSelect = (select) => {
    var array = [];
    select.each(function() {
        array.push($(this).val())
    });
    return array;
}

const init_datepicker = () => {
    $('input[class~="datepicker"]').datepicker({
        format: "yyyy-mm-dd",
        todayHighlight: true,
        autoclose: true
    });

    if ($('input[class~="report_datepicker"]').length) {
        var date = new Date(),
            today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        $('input[class~="report_datepicker"]').datepicker({
            format: "yyyy-mm-dd",
            todayHighlight: true,
            autoclose: true,
            endDate: today
        });
    }
}

const proc_adds = (data) => {
    $.each(data, async function(ikey, ivalue) {
        if (ivalue.modify == "idb") {
            await update_idb(data);
        }
    })
}

$(`form[class="search-form"]`).on("submit", function(evt) {
    evt.preventDefault();
});

if ($(`div[id="quicklinks-list"]`).length) {
    $.post(`${baseUrl}api/users/preference`, { label: "list_quick_links" }).then(async(response) => {
        if (response.code == 200) {
            $(`div[id="quicklinks-list"]`).html(response.data.result.text)
            if (response.data.result.scripts) {
                $.each(response.data.result.scripts, async function(ii, ie) {
                    await $.getScript(`${baseUrl}${ie}`);
                })
            }
        }
    }).catch(async(resp) => {});
}

$(`div[class="header-links"] ul[class~="links"] li`).on("click", function() {
    let $this = $(this);
    $(`div[class="header-links"] ul[class~="links"] li`).removeClass("active");
    $this.addClass("active");
});

$(`a[class~="add-profile"]`).on("click", function() {
    $(`div[id="profileModal"]`).modal("show");
    $(`div[id="profileModal"] form select`).val("null").change();
    $(`form div[class="form-content-loader"]`).css("display", "none");
    $(`div[id="profileModal"] [class="modal-title"]`).html("Add User Account");
    $(`div[id="profileModal"] form input, div[id="profileModal"] form textarea`).val("");
});

$(`a[class~="update-profile"], button[class~="btn-edit-profile"]`).on("click", function() {
    $(`div[id="profileModal"]`).modal("show");
    $(`div[id="profileModal"] [class="modal-title"]`).html("Update Profile");
});

$(`form[class="app-data-form"]`).on('submit', function(evt) {
    evt.preventDefault();

    app_formData = $(this).serialize(),
        app_formAction = $(this).attr("action");
    $(`form[class="app-data-form"] button[type="submit"]`).prop("disabled", true);

    $(`div[id="saveGeneralFormModal"] div[class='modal-header']`).html("Submit Form");
    $(`div[id="saveGeneralFormModal"] div[class='modal-body']`).html("Are you sure you want submit the form for processing?");
    $(`div[id="saveGeneralFormModal"]`).modal("show");
    $(`div[class="form-overlay-cover"]`).css("display", "flex");
});

$(`div[id="saveGeneralFormModal"] button[class~="btn-outline-danger"], button[data-dismiss="modal"]`).on("click", function() {
    $(`form[class="app-data-form"] button[type="submit"]`).prop("disabled", false);
    $(`div[class="upload-overlay-cover"]`).css("display", "none");
    $(`div[class="form-overlay-cover"]`).css("display", "none");
});

$(`div[id="saveGeneralFormModal"] button[class~="btn-outline-success"]`).on("click", function() {

    $(`div[id="saveGeneralFormModal"] div[class="form-content-loader"]`).css("display", "flex");

    $.ajax({
        type: `POST`,
        url: app_formAction,
        data: app_formData,
        dataType: 'json',
        success: function(response) {
            Toast.fire({
                title: response.data.result,
                icon: responseCode(response.code)
            });
            if (response.code == 200) {
                if ($(`input[id="attachment"]`).length) {
                    $(`input[id="attachment"]`).val('');
                }
                if (response.data.additional) {
                    if (response.data.additional.record) {
                        $.each(response.data.additional.record, function(ie, iv) {
                            $(`[data-record="${ie}"]`).html(iv);
                        });
                    }
                    if (response.data.additional.reload) {
                        setTimeout(() => {
                            window.location.href = response.data.additional.reload;
                        }, 1000);
                    }
                    if (response.data.additional.clear) {
                        $(`form[class="app-data-form"] input, form[class="app-data-form"] textarea`).val("");
                    }
                }
            }
        },
        complete: function(data) {
            $(`div[id="saveGeneralFormModal"]`).modal("hide");
            $(`div[class="form-overlay-cover"]`).css("display", "none");
            $(`div[id="saveGeneralFormModal"] div[class="form-content-loader"]`).css("display", "none");
            $(`form[class="app-data-form"] button[type="submit"]`).prop('disabled', false);
        },
        error: function(err) {
            $(`div[id="saveGeneralFormModal"]`).modal("hide");
            $(`div[class="form-overlay-cover"]`).css("display", "none");
            $(`form[class="app-data-form"] button[type="submit"]`).prop('disabled', false);
            $(`div[id="saveGeneralFormModal"] div[class="form-content-loader"]`).css("display", "none");
            Toast.fire({
                type: 'error',
                title: 'Sorry! There was an error while processing the request.'
            });
        }
    });

});

$(`span[class~="form-control-file"], img[id="form-control-file"]`).on("click", function() {
    $(`input[name="file_upload"]`).val(``);
    $(`div[id="profilePicture"]`).modal("show");
});

$(`a[data-loader="preferences"]`).on("click", function(evt) {
    evt.preventDefault();
    $(`div[id="userPreferences"]`).modal("show");
});

function file_upload(formdata) {
    let element = $('div[class="upload_file_url"]'),
        record_id = element.attr("data-record-id");

    $.ajax({
        url: element.attr("data-url"),
        type: 'post',
        data: formdata,
        contentType: false,
        processData: false,
        dataType: 'json',
        beforeSend: function() {
            $(`div[class="upload-area"] small`).html(`Uploading file <i class="fa fa-spin fa-spinner"></i>`);
        },
        success: function(resp) {
            if (resp.code == 200) {
                if (resp.data.result.preview) {
                    $(`div[class~="file-upload-preview"]`).html(`
                        <img style="width:100px" class="img-fluid" src="${baseUrl}${resp.data.result.href}">
                        <button class="btn btn-outline-success btn-sm mt-2 btn-block" id="${element.attr("data-button")}" data-record-id="${record_id}">Save Image</button>
                    `);
                }
            }
        },
        complete: function(data) {
            save_profile_picture();
            $(`input[name="file_upload"]`).val(``);
        },
        error: function(err) {}
    });
}

$("#file_upload").change(function() {
    var fd = new FormData();

    var files = $('#file_upload')[0].files[0];

    fd.append('file_upload', files);

    file_upload(fd);
});

var populate_policies = (data) => {
    $(`table[id="user_policies"]`).dataTable().fnDestroy();
    $(`table[id="user_policies"]`).dataTable({
        "aaData": data,
        "iDisplayLength": $myPrefs.list_count,
        "columns": [
            { "data": 'row_id' },
            { "data": 'policy_name_tag' },
            { "data": 'policy_start_date' },
            { "data": 'premium' },
            { "data": 'next_repayment_date' },
            { "data": 'the_status_label' },
            { "data": 'date_submitted' },
            { "data": 'action' }
        ]
    });
    $(`div[class="form-content-loader"]`).css({ "display": "none" });
}

var load_user_policies = (status = "", policy_type = "", date_range = "") => {
    $(`div[class="form-content-loader"]`).css("display", "flex");
    $.get(`${baseUrl}api/user_policy/list`, { status, policy_type, date_range }).then((response) => {
        if (response.code == 200 || response.code == 201) {
            populate_policies(response.data.result);
        } else {
            $(`div[class="form-content-loader"]`).css({ "display": "none" });
        }
    }).catch(async() => {
        try {
            let $idb_data = await read_idb(this_user_unique_key);
            let $user_policies = $idb_data.user_policies;
            populate_policies($user_policies);
        } catch (err) {
            $(`div[class="form-content-loader"]`).css({ "display": "none" });
        }
    });
}
if ($(`table[id="user_policies"]`).length) {
    let status = $(`table[id="user_policies"]`).attr("data-status");
    load_user_policies(status);
}

var filter_policy = () => {
    let policy_type = $(`select[id="policy_type"]`).val(),
        status = $(`select[id="status"]`).val(),
        start_date = $(`input[id="start_date"]`).val(),
        end_date = $(`input[id="end_date"]`).val();
    let date_range = "";
    if (start_date.length) {
        date_range += start_date;
        if (end_date.length) {
            date_range += `:${end_date}`;
        }
    }
    load_user_policies(status, policy_type, date_range);
}

var populate_companies = (data) => {
    $(`table[id="insurance_companies"]`).dataTable().fnDestroy();
    $(`table[id="insurance_companies"]`).dataTable({
        "aaData": data,
        "iDisplayLength": $myPrefs.list_count,
        "columns": [
            { "data": 'row_id' },
            { "data": 'the_company_name' },
            { "data": 'contact' },
            { "data": 'address' },
            { "data": 'email' },
            { "data": 'number_of_policies' },
            { "data": 'action' }
        ]
    });
    $(`div[class="form-content-loader"]`).css({ "display": "none" });
}
var load_insurance_companies = () => {
    $(`div[class="form-content-loader"]`).css("display", "flex");
    $.get(`${baseUrl}api/company/list?type=insurance`).then((response) => {
        if (response.code == 200 || response.code == 201) {
            populate_companies(response.data.result);
        } else {
            $(`div[class="form-content-loader"]`).css({ "display": "none" });
        }
    }).catch(() => {
        $(`div[class="form-content-loader"]`).css({ "display": "none" });
    });
}
if ($(`table[id="insurance_companies"]`).length) {
    load_insurance_companies();
}

var populate_company_policies = (data) => {
    $(`table[id="company_policies"]`).dataTable().fnDestroy();
    $(`table[id="company_policies"]`).dataTable({
        "aaData": data,
        "iDisplayLength": $myPrefs.list_count,
        "columns": [
            { "data": 'row_id' },
            { "data": 'policy_name' },
            { "data": 'policy_category' },
            { "data": 'year_enrolled' },
            { "data": 'the_status_label' },
            { "data": 'action' }
        ]
    });
    $(`div[class="form-content-loader"]`).css({ "display": "none" });
}
var load_company_policies = () => {
        $(`div[class="form-content-loader"]`).css("display", "flex");
        $.get(`${baseUrl}api/company_policy/list?company_id=${$(`table[id="company_policies"]`).attr("data-company-id")}`).then((response) => {
        if (response.code == 200 || response.code == 201) {
            populate_company_policies(response.data.result);
        } else {
            $(`div[class="form-content-loader"]`).css({ "display": "none" });
        }
    }).catch(async() => {
        try {
            let $idb_data = await read_idb(this_user_unique_key);
            let $company_policy = $idb_data.company_policy;
            populate_company_policies($company_policy);
        } catch (err) {}
    });
}
if ($(`table[id="company_policies"]`).length) {
    load_company_policies();
}

var populate_clients_list = (data) => {
    $(`table[id="clients_list"]`).dataTable().fnDestroy();
    $(`table[id="clients_list"]`).dataTable({
        "aaData": data,
        "iDisplayLength": $myPrefs.list_count,
        "columns": [
            { "data": 'row_id' },
            { "data": 'fullname' },
            { "data": 'contact_details' },
            { "data": 'policies_count' },
            { "data": 'claims_count' },
            { "data": 'assigned_details' },
            { "data": 'the_status_label' },
            { "data": 'action' }
        ]
    });
    $(`div[class="form-content-loader"]`).css({ "display": "none" });
}
var load_clients_list = () => {
    $(`div[class="form-content-loader"]`).css("display", "flex");
    $.get(`${baseUrl}api/users/list?user_type=user,business`).then((response) => {
        if (response.code == 200 || response.code == 201) {
            populate_clients_list(response.data.result);
        } else {
            $(`div[class="form-content-loader"]`).css({ "display": "none" });
        }
    }).catch(() => {
        $(`div[class="form-content-loader"]`).css({ "display": "none" });
    });
}
if ($(`table[id="clients_list"]`).length) {
    load_clients_list();
}

var populate_admins_list = (data) => {
    $(`table[id="admins_list"]`).dataTable().fnDestroy();
    $(`table[id="admins_list"]`).dataTable({
        "aaData": data,
        "iDisplayLength": $myPrefs.list_count,
        "columns": [
            { "data": 'row_id' },
            { "data": 'fullname' },
            { "data": 'contact_details' },
            { "data": 'the_status_label' },
            { "data": 'action' }
        ]
    });
    $(`div[class="form-content-loader"]`).css({ "display": "none" });
}
var load_admins_list = () => {
    $(`div[class="form-content-loader"]`).css("display", "flex");
    let user_type = $(`table[id="admins_list"]`).attr("data-user_type");
    $.get(`${baseUrl}api/users/list?user_type=${user_type}`).then((response) => {
        if (response.code == 200 || response.code == 201) {
            populate_admins_list(response.data.result);
        } else {
            $(`div[class="form-content-loader"]`).css({ "display": "none" });
        }
    }).catch(() => {
        $(`div[class="form-content-loader"]`).css({ "display": "none" });
    });
}
if ($(`table[id="admins_list"]`).length) {
    load_admins_list();
}

var populate_notifications_list = (data) => {
    $(`table[id="notifications_list"]`).dataTable().fnDestroy();
    $(`table[id="notifications_list"]`).dataTable({
        "aaData": data,
        "iDisplayLength": $myPrefs.list_count,
        "columns": [
            { "data": 'row_id' },
            { "data": 'subject' },
            { "data": 'message' },
            { "data": 'date_created' }
        ]
    });
    $(`div[class="form-content-loader"]`).css({ "display": "none" });
}
var load_notifications_list = () => {
    $(`div[class="form-content-loader"]`).css("display", "flex");
    $.get(`${baseUrl}api/notification/list`).then((response) => {
        if (response.code == 200 || response.code == 201) {
            populate_notifications_list(response.data.result);
        } else {
            $(`div[class="form-content-loader"]`).css({ "display": "none" });
        }
    }).catch(() => {
        $(`div[class="form-content-loader"]`).css({ "display": "none" });
    });
}
if ($(`table[id="notifications_list"]`).length) {
    load_notifications_list();
}

var populate_claims_list = (data) => {
    $(`table[id="claims_list"]`).dataTable().fnDestroy();
    $(`table[id="claims_list"]`).dataTable({
        "aaData": data,
        "iDisplayLength": $myPrefs.list_count,
        "columns": [
            { "data": 'row_id' },
            { "data": 'client_name' },
            { "data": 'inline_policy_info' },
            { "data": 'date_created' },
            { "data": 'amount_claimed' },
            { "data": 'approved_amount' },
            { "data": 'the_status_label' },
            { "data": 'action' }
        ]
    });
    $(`div[class="form-content-loader"]`).css({ "display": "none" });
}
var load_claims_list = (status = "", policy_type = "", date_range = "") => {
    $(`div[class="form-content-loader"]`).css("display", "flex");
    $.get(`${baseUrl}api/claims/list`,{status,policy_type,date_range}).then((response) => {
        if (response.code == 200 || response.code == 201) {
            populate_claims_list(response.data.result);
        } else {
            $(`div[class="form-content-loader"]`).css({ "display": "none" });
        }
    }).catch(() => {
        $(`div[class="form-content-loader"]`).css({ "display": "none" });
    });
}
if ($(`table[id="claims_list"]`).length) {
    let status = $(`table[id="claims_list"]`).attr("data-status");
    load_claims_list(status);
}


var filter_claims = () => {
    let policy_type = $(`select[id="policy_type"]`).val(),
        status = $(`select[id="status"]`).val(),
        start_date = $(`input[id="start_date"]`).val(),
        end_date = $(`input[id="end_date"]`).val();
    let date_range = "";
    if (start_date.length) {
        date_range += start_date;
        if (end_date.length) {
            date_range += `:${end_date}`;
        }
    }
    load_claims_list(status, policy_type, date_range);
}

var populate_agents_list = (data) => {
    $(`table[id="agents_list"]`).dataTable().fnDestroy();
    $(`table[id="agents_list"]`).dataTable({
        "aaData": data,
        "iDisplayLength": $myPrefs.list_count,
        "columns": [
            { "data": 'row_id' },
            { "data": 'fullname' },
            { "data": 'contact_details' },
            { "data": 'policies_count' },
            { "data": 'claims_count' },
            { "data": 'clients_count' },
            { "data": 'the_status_label' },
            { "data": 'action' }
        ]
    });
    $(`div[class="form-content-loader"]`).css({ "display": "none" });
}
var load_agents_list = () => {
    $(`div[class="form-content-loader"]`).css("display", "flex");
    $.get(`${baseUrl}api/users/list?user_type=agent`).then((response) => {
        if (response.code == 200 || response.code == 201) {
            populate_agents_list(response.data.result);
        } else {
            $(`div[class="form-content-loader"]`).css({ "display": "none" });
        }
    }).catch(() => {
        $(`div[class="form-content-loader"]`).css({ "display": "none" });
    });
}
if ($(`table[id="agents_list"]`).length) {
    load_agents_list();
}

var populate_brokers_list = (data) => {
    $(`table[id="brokers_list"]`).dataTable().fnDestroy();
    $(`table[id="brokers_list"]`).dataTable({
        "aaData": data,
        "iDisplayLength": $myPrefs.list_count,
        "columns": [
            { "data": 'row_id' },
            { "data": 'fullname' },
            { "data": 'contact_details' },
            { "data": 'policies_count' },
            { "data": 'claims_count' },
            { "data": 'clients_count' },
            { "data": 'the_status_label' },
            { "data": 'action' }
        ]
    });
    $(`div[class="form-content-loader"]`).css({ "display": "none" });
}
var load_brokers_list = () => {
    $(`div[class="form-content-loader"]`).css("display", "flex");
    $.get(`${baseUrl}api/users/list?user_type=broker`).then((response) => {
        if (response.code == 200 || response.code == 201) {
            populate_brokers_list(response.data.result);
        } else {
            $(`div[class="form-content-loader"]`).css({ "display": "none" });
        }
    }).catch(() => {
        $(`div[class="form-content-loader"]`).css({ "display": "none" });
    });
}
if ($(`table[id="brokers_list"]`).length) {
    load_brokers_list();
}

var populate_complaints = (data) => {
    $(`table[id="user_complaints"]`).dataTable().fnDestroy();
    $(`table[id="user_complaints"]`).dataTable({
        "aaData": data,
        "iDisplayLength": $myPrefs.list_count,
        "columns": [
            { "data": 'row_id' },
            { "data": 'complaint_subject' },
            { "data": 'related_to_name' },
            { "data": 'date_created' },
            { "data": 'status_label' },
            { "data": 'action' }
        ]
    });
    $(`div[class="form-content-loader"]`).css({ "display": "none" });
}

var load_user_complaints = (status = "", related_to = "", date_range = "") => {
    $(`div[class="form-content-loader"]`).css("display", "flex");
    $.get(`${baseUrl}api/complaints/list`, {status, related_to, date_range}).then((response) => {
        if (response.code == 200 || response.code == 201) {
            populate_complaints(response.data.result);
        } else {
            $(`div[class="form-content-loader"]`).css({ "display": "none" });
        }
    });
}
if ($(`table[id="user_complaints"]`).length) {
    load_user_complaints();
}

var filter_complaints = () => {
    let related_to = $(`select[id="related_to"]`).val(),
        status = $(`select[id="status"]`).val(),
        start_date = $(`input[id="start_date"]`).val(),
        end_date = $(`input[id="end_date"]`).val();
    let date_range = "";
    if (start_date.length) {
        date_range += start_date;
        if (end_date.length) {
            date_range += `:${end_date}`;
        }
    }
    load_user_complaints(status, related_to, date_range);
}

var populate_licenses_list = (data) => {
    $(`table[id="licenses_list"]`).dataTable().fnDestroy();
    $(`table[id="licenses_list"]`).dataTable({
        "aaData": data,
        "iDisplayLength": $myPrefs.list_count,
        "columns": [
            { "data": 'row_id' },
            { "data": 'license_label' },
            { "data": 'expiry_date' },
            { "data": 'date_created' },
            { "data": "applied_by" },
            { "data": 'the_status_label' },
            { "data": 'action' }
        ]
    });
    $(`div[class="form-content-loader"]`).css({ "display": "none" });
}
var load_licenses_list = () => {
    $(`div[class="form-content-loader"]`).css("display", "flex");
    $.get(`${baseUrl}api/licenses/list`).then((response) => {
        if (response.code == 200 || response.code == 201) {
            populate_licenses_list(response.data.result);
        } else {
            $(`div[class="form-content-loader"]`).css({ "display": "none" });
        }
    }).catch(() => {
        $(`div[class="form-content-loader"]`).css({ "display": "none" });
    });
}
if ($(`table[id="licenses_list"]`).length) {
    load_licenses_list();
}

var populate_report_requests = (data) => {
    $(`table[id="report_requests"]`).dataTable().fnDestroy();
    $(`table[id="report_requests"]`).dataTable({
        "aaData": data,
        "iDisplayLength": $myPrefs.list_count,
        "columns": [
            { "data": 'row_id' },
            { "data": 'subject_name' },
            { "data": 'request_type' },
            { "data": 'date_created' },
            { "data": 'expected_date' },
            { "data": 'the_status_label' },
            { "data": 'action' }
        ]
    });
    $(`div[class="form-content-loader"]`).css({ "display": "none" });
}
var report_requests_list = () => {
    $(`div[class="form-content-loader"]`).css("display", "flex");
    $.get(`${baseUrl}api/reports/requests_list`).then((response) => {
        if (response.code == 200 || response.code == 201) {
            populate_report_requests(response.data.result);
        } else {
            $(`div[class="form-content-loader"]`).css({ "display": "none" });
        }
    }).catch(() => {
        $(`div[class="form-content-loader"]`).css({ "display": "none" });
    });
}
if ($(`table[id="report_requests"]`).length) {
    report_requests_list();
}

var discard_form = () => {
    $(`div[id="discardFormModal"]`).modal("show");
    $(`div[id="discardFormModal"] button[class~="confirm_form_discard"]`).on("click", function() {
        $(`div[id="ajax-load-form-content"]`).html("");
        $(`div[id="ajax-load-form-content"]`).slideUp();
        $(`div[class~="ajax-load-form-header"]`).slideDown();
        $(`div[id="discardFormModal"]`).modal("hide");
    });
}

$(`div[id="ajaxFormSubmitModal"] button[class~="btn-outline-success"]`).on("click", async function(evt) {

    let formLoader = $(`form[class="ajax-data-form"] div[class="form-content-loader"]`),
        submitLoader = $(`div[id="ajaxFormSubmitModal"] div[class="form-content-loader"]`),
        formButton = $(`form[class="app-data-form"] button[type="button-submit"]`);

    formLoader.css({ "display": "flex", "position": "fixed" });
    submitLoader.css({ "display": "flex", "position": "fixed" });

    await $.ajax({
        url: `${ajaxFormAction}`,
        data: ajaxFormData,
        contentType: false,
        cache: false,
        type: `POST`,
        processData: false,
        success: function(response) {
            Toast.fire({
                title: response.data.result,
                icon: responseCode(response.code)
            });
            formButton.prop("disabled", false);
            if (response.code == 200) {
                if (response.data.additional !== undefined) {
                    if (response.data.additional.clear !== undefined) {
                        if ($(`textarea[name="faketext"]`).length) {
                            CKEDITOR.instances['ajax-form-content'].setData("");
                        }
                        if ($(`trix-editor[name="faketext"][id="ajax-form-content"]`).length) {
                            $(`trix-editor[name="faketext"][id="ajax-form-content"]`).html("");
                        }
                        $replies_loaded.attr("value", "0");
                        $replies_loaded.attr("data-form", "none");
                        $(`form[class="ajax-data-form"] select`).val("null").change();
                        $(`form[class="ajax-data-form"] input, form[class="ajax-data-form"] textarea`).val("");
                    }
                    if (response.data.additional.append !== undefined) {
                        $(`div[id="${response.data.additional.append.div_id}"]`).html(response.data.additional.append.data);
                    }
                    if (response.data.additional.record !== undefined) {
                        $.each(response.data.additional.record, function(ie, iv) {
                            $(`form[class="ajax-data-form"] input[name="${ie}"]`).val(iv);
                            $(`[data-record="${ie}"]`).html(iv);
                        });
                    }
                    if (response.data.additional.reload !== undefined) {
                        setTimeout(() => {
                            window.location.href = response.data.additional.reload;
                        }, 1000);
                    }
                }
                $(`form[class="ajax-data-form"] div[class~="file-preview"]`).html("");
            }
        },
        complete: function() {
            formLoader.css({ "display": "none" });
            submitLoader.css({ "display": "none"});
            formButton.prop("disabled", false);
            $(`div[id="ajaxFormSubmitModal"]`).modal("hide");
        },
        error: function() {
            formLoader.css({ "display": "none" });
            submitLoader.css({ "display": "none"});
            formButton.prop("disabled", false);
            $(`div[id="ajaxFormSubmitModal"]`).modal("hide");
            Toast.fire({
                title: "Error processing request!",
                icon: "error"
            });
        }
    });

});

var trigger_form_submit = () => {
    $(`form[class="ajax-data-form"] button[type="button-submit"]`).on("click", async function(evt) {
        evt.preventDefault();
        let theButton = $(this),
            draftButton = $(`form[class="ajax-data-form"] button[type="button-submit"][data-function="draft"]`),
            formAction = $(`form[class="ajax-data-form"]`).attr("action"),
            formButton = $(`form[class="app-data-form"] button[type="button-submit"]`);
        
        let optional_flow = "We recommend that you save the form as a draft, and review it before submitting. Do you wish to proceed with this action?";

        if (theButton.attr("data-function") == "save") {
            $(`div[id="ajaxFormSubmitModal"] div[class="modal-body"]`).html(`Are you sure you want to Submit this form? ${draftButton.length ? optional_flow : ""}`);
        } else if (theButton.attr("data-function") == "draft") {
            $(`div[id="ajaxFormSubmitModal"] div[class="modal-body"]`).html(`You have opted to save this form as a draft? Do you wish to proceed with the action?`);
        }

        formButton.prop("disabled", true);

        let myForm = document.getElementById('ajax-data-form-content');
        let theFormData = new FormData(myForm);

        let button = theButton.attr("data-function");
        theFormData.append("the_button", button);

        if ($(`textarea[name="faketext"]`).length) {
            theFormData.delete("faketext");
            let content = CKEDITOR.instances['ajax-form-content'].getData();
            theFormData.append("message", htmlEntities(content));
        }

        if ($(`textarea[name="faketext_2"]`).length) {
            theFormData.delete("faketext_2");
            let content = CKEDITOR.instances['ajax-form-content_2'].getData();
            theFormData.append("reason", htmlEntities(content));
        }

        $.each(["ad_target", "ad_context"], function(i, e) {
            if($(`select[name="${e}"]`).length) {
                theFormData.delete(e);
                let tar = serializeSelect($(`select[name="${e}"]`));
                theFormData.append(e, tar);
            }
        });

        if ($(`trix-editor[name="faketext"][id="ajax-form-content"]`).length) {
            theFormData.delete("faketext");
            let content = $(`trix-editor[id="ajax-form-content"]`).html();
            theFormData.append("message", htmlEntities(content));
        }

        if ($(`trix-editor[name="faketext_2"][id="ajax-form-content_2"]`).length) {
            theFormData.delete("faketext_2");
            let content = $(`trix-editor[id="ajax-form-content_2"]`).html();
            theFormData.append("reason", htmlEntities(content));
        }

        ajaxFormAction = formAction,
            ajaxFormData = theFormData;

        $(`div[id="ajaxFormSubmitModal"]`).modal("show");
    });
}

function validate_email(email_address) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email_address).toLowerCase());
}

var form_processing = (formLabel) => {

    $(`select[name="access_level"]`).on("change", function() {
        let value = $(this).val();
        $(`label[for="user_id_label"]`).html(formLabel.user_id_label[value]);
    });

    $(`select[name="u_client_id"][data-append="claims-request"]`).on("change", function() {
        let value = $(this).val(),
            module = "client-policy",
            client_policy_id = $(`select[class="policy_id"]`);
        if (value == "null") {
            client_policy_id.find('option').remove().end();
            client_policy_id.append(`<option value="">Select Client Policy</option>`);
            return;
        }
        $.get(`${baseUrl}api/related/list?module=${module}&related_item=${value}`).then((response) => {
            client_policy_id.find('option').remove().end();
            client_policy_id.append(`<option value="">Select Client Policy</option>`);
            if (response.code == 200) {
                select_options("policy_id", response.data.result, "Select Client Policy");
            }
        });
    });

    $(`select[name="policy_category"]`).on("change", function() {
        let value = $(this).val(),
            module = "company-insurance-policies",
            policy_id = $(`select[class="policy_id"]`);

        $(`div[id='policy-learn-more'], div[id="proceed-to-ajax-form"], div[id="ajax-load-form-content"]`).addClass("hidden");
        $(`div[id='policy-learn-more'] span[class~="learn-more-modal"]`).html(``);
        $.get(`${baseUrl}api/related/list?module=${module}&related_item=${value}&filter=category`).then((response) => {
            policy_id.find('option').remove().end();
            policy_id.append(`<option value="null">Select Insurance Policy</option>`);
            if (response.code == 200) {
                select_options("policy_id", response.data.result, "Select Insurance Policy");
            }
        });
    });

    $(`select[name="policy_id"]`).on("change", function() {
        let value = $(this).val();
        if (value !== "null") {
            $(`div[id="policy-learn-more"], div[id="proceed-to-ajax-form"]`).removeClass("hidden");
            $(`div[id='policy-learn-more'] span[class~="learn-more-modal"]`).html(`<strong>Click here to learn more</strong>`);
            $(`div[id='policy-learn-more'] span[class~="learn-more-modal"]`).attr({ "onclick": `return policy_basic_information('${value}')` });
        } else {
            $(`div[id='policy-learn-more'], div[id="proceed-to-ajax-form"], div[id="ajax-load-form-content"]`).addClass("hidden");
        }
    });

    $(`span[class~="proceed-to-form"]`).on("click", function() {
        $(`div[class~="ajax-load-form-header"]`).slideUp();
        $(`div[id="ajax-load-form-content"]`).html($form_loader).removeClass("hidden");
        let $module = {
                label: $(`div[class~="ajax-load-form-header"]`).attr("data-form_type"),
                item_id: $(`select[name="policy_id"]`).val(),
                content: "form",
                user_id: $(`input[id="u_client_id"], select[id="u_client_id"]`).val()
            },
            form_content = $(`div[id="ajax-load-form-content"]`);
        $.post(`${baseUrl}api/forms/load`, { module: $module }).then((response) => {
            if (response.code !== 200) {
                form_content.html($no_record);
            } else {
                form_content.html(response.data.result.form);
                $(`select[class~="selectpicker"]`).select2();
                $(`[data-toggle="tooltip"]`).tooltip();
                init_datepicker();
                trigger_form_submit();

                if (response.data.result.resources) {
                    var time = 100;
                    $.each(response.data.result.resources, function(ii, ie) {
                        setTimeout(function() {
                            $.cachedScript(`${baseUrl}${ie}`);
                        }, time);
                        time += 500;
                    });
                }

            }
        }).catch(() => {
            form_content.html($form_error);
        });
    });

    $(`select[name="related_to"]`).on("change", function() {
        let value = $(this).val(),
            module = $(this).attr("data-module"),
            related_to_id = $(`select[name="related_to_id"]`);
        if (value == "null") {
            related_to_id.find('option').remove().end();
            related_to_id.append(`<option value="">Please select item</option>`);
            return;
        }

        related_to_id.find('option').remove().end();
        related_to_id.append(`<option value="">Select select item</option>`);
        $.get(`${baseUrl}api/related/list?module=${module}&related_item=${value}`).then((response) => {
            if (response.code == 200) {
                select_options("related_to_id", response.data.result, "Please select item");
            }
        });
    });

    if($(`select[name="access_level"]`).length) {
        var _user_type = $(`select[name="access_level"]`).val();
        // if(_user_type === "broker") {
            if($(`input[data-autosearch_email="true"]`).length) {
                var _user_email_input = $(`input[data-autosearch_email="true"]`);
                _user_email_input.on("input", function() {
                    let email = _user_email_input.val();
                    if(validate_email(email)) {
                        let label = {data: {request: "confirm_broker_by_email",email: email}};
                        $.post(`${baseUrl}api/filters/list`, label).then((response) => {
                            $(`div[class='email_checker']`).html(`
                                <span style="font-size:12px" class="text-${response.data.additional.color}">${response.data.result}</span>
                            `);
                        });
                    } else {
                        $(`div[class='email_checker']`).html(``);
                    }
                });
            }
        // }
    }

    trigger_form_submit();

}

var form_loader = async(form_module, module_item_id, module_resource) => {
    let $module = {
        label: form_module,
        item_id: module_item_id,
        property: module_resource,
        content: "form"
    }
    $form_loaded.attr("value", 0);
    $form_loaded.attr("data-form", "none");
    await $.post(`${baseUrl}api/forms/load`, { module: $module }).then((response) => {
        if (response.code !== 200) {
            $form_body.html($form_error);
        } else {
            let formRecord = response.data.result;

            if (!formRecord.form) {
                $form_body.html($form_error);
                return false;
            }

            $form_loaded.attr("value", 1);
            $form_loaded.attr("data-form", form_module);
            $form_body.html(formRecord.form);

            $(`[data-toggle="tooltip"]`).tooltip();
            $(`select[class~="selectpicker"]`).select2();
            $(":input").inputmask();
            if(!$(`table[data-noinit="datatable"]`).length) {
                init_datable();
            }
            init_datepicker();
            form_processing(formRecord.labels);
            $(`div[class~="trix-button-row"] span[class~="trix-button-group--file-tools"], div[class~="trix-button-row"] span[class~="trix-button-group-spacer"]`).remove();

            if (formRecord.resources) {
                var time = 100;
                $.each(formRecord.resources, function(ii, ie) {
                    setTimeout(function() {
                        $.cachedScript(`${baseUrl}${ie}`);
                    }, time);
                    time += 500;
                });
            }
            if (formRecord.content) {
                $.each(formRecord.content, function(key, value) {
                    $(`trix-editor[id="${key}"]`).html(`${value}`);
                });
            }
        }
    }).catch(() => {
        $form_body.html($form_error);
    });
}

var user_basic_information = async(user_id, additional_data = null) => {
    let user_info = await load_idb_record("user_list", user_id);
    $basic_data_body.html($no_record);
    if (user_info) {
        $basic_data_modal.modal("show");
        $basic_data_header.html(form_modules["user_basic_information"]);

        var more_info = "";
        if (additional_data) {
            let $module = {
                label: "user_account",
                item_id: user_id,
                property: {
                    load: additional_data
                },
                content: "form"
            };
            await $.post(`${baseUrl}api/forms/load`, { module: $module }).then((response) => {
                if (response.code == 200) {
                    more_info = response.data.result.form;
                }
            });
        }

        preview_user_info(user_info, more_info);
        return false;
    }
}

var company_basic_information = async(company_id) => {
    let company_info = await load_idb_record("insurance_companies", company_id);
    $basic_data_body.html($no_record);
    if (company_info) {
        $basic_data_modal.modal("show");
        $basic_data_header.html(form_modules["company_basic_information"]);
        preview_company_info(company_info);
        return false;
    }
}

var policy_basic_information = async(policy_id) => {
    let policy_info = await load_idb_record("insurance_policies", policy_id);
    $basic_data_body.html($no_record);
    if (policy_info) {
        $basic_data_modal.modal("show");
        $basic_data_header.html(form_modules["policy_basic_information"]);
        preview_company_policy_info(policy_info);
        return false;
    }
}

var quick_form_load = (module, module_id = "", module_resource = "") => {
    $form_header.html(form_modules[module]);
    $form_modal.modal("show");
    $form_body.html($form_loader);
    form_loader(module, module_id, module_resource);
}

var init_click_handlers = () => {
    $(`[data-function="load-form"]`).on("click", async function(evt) {
        evt.preventDefault();
        $replies_modal.modal("hide");
        let module = $(this).data("module"),
            module_item_id = $(this).data("module-id"),
            module_resource = $(this).data("resource");
        $form_header.html(form_modules[module]);

        if (module == "user_basic_information") {
            let user_info = await load_idb_record("user_list", module_item_id);
            if (user_info) {
                $basic_data_modal.modal("show");
                $basic_data_header.html(form_modules[module]);
                preview_user_info(user_info);
                return false;
            }
        }

        $form_modal.modal("show");
        if (($form_loaded.attr("value") == 1) && ($form_loaded.attr("data-form") == module)) {
            // return false;
        }
        $form_body.html($form_loader);

        form_loader(module, module_item_id, module_resource);
    });
}

var replies_click_handlers = () => {
    $(`div[id="repliesModal"] span[data-function="toggle-files-list"]`).on("click", function() {
        let reply_id = $(this).attr("data-reply-id");
        $(`div[id="repliesModal"] div[class~="attachments_list"][data-reply-id="${reply_id}"]`).slideToggle("slow");
    });
    $(document).on('click', function(e) {
        e.stopPropagation();
        if (!$(e.target).closest('.toggle-reply-options').length) {
            $(`div[class~="replies-item"] div[class~="dropdown-options"]`).addClass("no-option");
        }
    });
    $(`div[id="repliesModal"] button[class~="toggle-reply-options"]`).on("click", function() {
        let reply_id = $(this).attr("data-reply-id");
        $(`div[class~="replies-item"][data-reply-id="${reply_id}"] div[class~="dropdown-options"]`).removeClass("no-option");
    });
    $(`[data-dismiss="modal"]`).on("click", function() {
        $(`div[id="repliesModal"] div[class~="attachments_list"]`).slideUp("slow");
        $(`div[id="repliesModal"] div[class~="modal-body"]`).attr("data-last_reply_id", "");
    });
}

var replies_loader = async(form_module, module_item_id, module_resource, last_reply_id = null) => {
    let $module = {
        label: form_module,
        item_id: module_item_id,
        last_reply_id: last_reply_id,
        property: module_resource,
        content: "form"
    }
    $replies_loaded.attr("value", 0);
    $replies_loaded.attr("data-form", "none");

    let replies_list = await $.post(`${baseUrl}api/forms/load`, { module: $module }).catch(() => {
        $replies_body.html($form_error);
    });

    if (replies_list.code == 200) {
        repliesList = replies_list.data.result.replies.replies_list;
        lastReplyId = replies_list.data.result.replies.last_reply_id;
        repliesResource = replies_list.data.result.replies.replies_resource;

        $(`div[id="repliesModal"] div[class~="modal-body"]`).attr("data-last_reply_id", lastReplyId);
        $(`div[id="repliesModal"] div[class~="modal-body"]`).attr("data-module_item_id", module_item_id);
        $(`div[id="repliesModal"] div[class~="modal-body"]`).attr("data-replies_resource", repliesResource);

        if (replies_list.data.result.resources) {
            var time = 100;
            $.each(replies_list.data.result.resources, function(ii, ie) {
                setTimeout(function() {
                    $.cachedScript(`${baseUrl}${ie}`);
                }, time);
                time += 500;
            });
        }
    }

    if (repliesList.length) {
        $replies_loaded.attr("value", 1);
        $replies_loaded.attr("data-form", form_module);

        let repliesContent = ``,
            idbList = {},
            prev_date = "";
        $.each(repliesList, function(rk, rv) {

            idbList[rv.item_id] = rv;

            if (!prev_date || prev_date != rv.raw_date) {
                repliesContent += `<div class="message_list_day_divider_label"><button class="message_list_day_divider_label_pill">${rv.clean_date}</button></div>`;
            }

            repliesContent += formatThreadReply(rv);
            prev_date = rv.raw_date;
        });

        if (last_reply_id) {
            let $prev_replies_body = $(`div[id="repliesModal"] div[class~="modal-body"] div[class~="replies-item"]:last`);
            $prev_replies_body.after(repliesContent);
        } else {
            repliesContent += `
                <div class="row">
                    <div class="col-lg-12 mb-2 text-center">
                        <button data-function="load-form" data-resource="complaints" data-module-id="${module_item_id}" data-dismiss="modal" class="btn btn-outline-secondary">Close</button>
                    </div>
                </div>`;
            $replies_body.html(repliesContent);
        }
        replies_click_handlers();
        init_image_popup();
        deleteReply();
        $(`div[id="repliesModal"] [data-toggle="tooltip"]`).tooltip();

        idbList = {
            "user_replies": {
                [module_item_id]: idbList
            }
        };
    } else {
        $replies_body.html($no_record);
    }

}

var init_reply_handler = () => {
    $(`[data-function="load-replies-form"]`).on("click", async function(evt) {
        evt.preventDefault();
        let module = $(this).data("module"),
            module_item_id = $(this).data("module-id"),
            module_resource = $(this).data("resource");
        $replies_header.html(form_modules[module]);
        $replies_modal.modal("show");
        $(`div[id="repliesModal"] div[class~="modal-body"]`).attr("data-last_reply_id", "");

        if (($replies_loaded.attr("value") == 1) && ($replies_loaded.attr("data-form") == module)) {}
        $replies_body.html($form_loader);
        replies_loader(module, module_item_id, module_resource, null);
    });
}

if ($(`div[id="cancel_existing_request"]`).length) {
    var loadCancelPolicyRequests = () => {
        let main_container = JSON.parse($(`div[id="cancel_existing_request"]`).attr("data-json")),
            div_id = main_container.container,
            field = main_container.field,
            threads_container = $(`[data-threads="${div_id}"]`),
            policy_id = threads_container.attr("data-policy-id"),
            autoload = threads_container.attr("data-autoload");
        
        if (autoload === "true") {
            $.get(`${baseUrl}api/${main_container.url}/cancel_list`, { [field]: policy_id }, async function(response) {
                if (response.code == 200) {
                    if (response.data.result) {
                        let cancel_policy_list = "";
                        $.each(response.data.result, function(key, v) {
                            cancel_policy_notices[v.slug] = v;
                            cancel_policy_list += cancelResourceRecord_List(v);
                        });
                        if (cancel_policy_list) {
                            threads_container.html(cancel_policy_list);
                        }
                    }
                }
                threads_container.attr("data-autoload", "false");
            }, "json");
        }
    }
    loadCancelPolicyRequests();
}

$(`div[class="leave-comment-wrapper"] button[class~="share-comment"]`).on("click", function() {

    let item_id = $(this).attr("data-id"),
        resource = $(this).attr("data-resource"),
        content = $(`trix-editor[id="leave_comment_content"]`),
        theLoader = $(`div[class="leave-comment-wrapper"] div[class~="absolute-content-loader"]`);

    let comment = {
        item_id: item_id,
        resource: resource,
        comment: htmlEntities(content.html())
    };

    theLoader.css({ "display": "flex" });

    $.post(`${baseUrl}api/replies/comment`, comment).then((response) => {
        theLoader.css({ "display": "none" });
        if (response.code == 200) {
            content.html("");
            $(`div[class="leave-comment-wrapper"] div[class~="file-preview"]`).html("");
            Toast.fire({
                icon: "success",
                title: response.data.result
            });
            if (response.data.additional.record) {
                $.each(response.data.additional.record, function(ie, iv) {
                    $(`[data-record="${ie}"]`).html(iv);
                });
                let comment = formatThreadComment(response.data.additional.data);
                if ($(`div[id="comments-container"] div[id="comment-listing"]:first`).length) {
                    $(`div[id="comments-container"] div[id="comment-listing"]:first`).before(comment);
                } else {
                    $(`div[id="comments-container"]`).append(comment);
                }
            }
        } else {
            Toast.fire({
                icon: "error",
                title: response.data.result
            });
        }
        apply_comment_click_handlers();
    }).catch(() => {
        theLoader.css({ "display": "none" });
        Toast.fire({
            icon: "error",
            title: "Sorry! Error processing request."
        });
    });
});

var delete_existing_file_attachment = async(record_id) => {
    let module = {
        "module": "remove_existing",
        "label": record_id
    };
    await $.post(`${baseUrl}api/files/attachments`, module).then((response) => {
        if (response.code == 200) {
            if (response.data.result == "File deleted!") {
                $(`div[data-file_container="${record_id}"]`).remove();
            }
        }
    });
}

var show_Notification = (notice, notice_type = "success", timer = 5000) => {
    app_notification_box.fadeIn();
    app_notification_box.removeClass(`text-success text-danger`).addClass(`text-${notice_type}`);
    app_notification_content.html(notice);
    setTimeout(() => {
        app_notification_box.fadeOut("");
    }, timer);
}

var delete_record = (resource, resource_id, reload = "null") => {
    let submit_button = $(`div[id="generalModal"] button[class~='btn-outline-success']`);
        submit_button.attr({"data-resource" : `delete.${resource}`, "data-record-id": resource_id});
    if(reload !== "null") {
        submit_button.attr({"data-reload_href": reload});
    }
    $(`div[id="generalModal"] h5[class="modal-title"]`).html(`Delete Record`);
    $(`div[id="generalModal"] div[class="modal-body"]`).html(`Are you sure you want to delete this record from the system? Take note that this action cannot be reversed.`);
    $(`div[id="generalModal"]`).modal("show");
}

$(`button[data-function="pay-claim"]`).on("click", function() {
    app_formAction = `${baseUrl}api/claims/issue_payment`,
    app_formData = {
        "claim_id": $(this).attr("data-claim_id"),
        "approved_amount": $(`input[name="approved_amount"]`).val(),
    };
    $(`div[id="saveGeneralFormModal"] h5[class="modal-title"]`).html("Issue Claims Payment");
    $(`div[id="saveGeneralFormModal"] div[class="modal-body"]`).html(`
        Are you sure you want to issue payment for the amount claimed?
        This action cannot be reversed once confirmed. Do you wish to proceed?
    `);
    $(`div[id="saveGeneralFormModal"]`).modal("show");
});

$(`a[data-function="mark_all_as_read"]`).on("click", function(evt) {
    evt.preventDefault();
    let record = {
        resource: $(this).attr("data-resource"),
        resource_id: $(this).attr("data-id")
    };
    $.post(`${baseUrl}api/records/save`, record);
});

var load_reportReport = async () => {
    let item = $(`div[data-request_type="report_request"]`).attr("data-request_item"),
        company_id = $(`div[data-request_type="report_request"]`).attr("data-company_id"),
        record_id = $(`div[data-request_type="report_request"]`).attr("data-record_id");
    
    await $.get(`${baseUrl}api/reports/request_content`, {item, company_id, record_id}).then((response) => {
        if(response.code == 200) {
            $(`div[data-request_type="report_request"]`).html(response.data.result);
        }
        $(".selectpicker").select2();
    });
}

if($(`div[data-request_type="report_request"]`).length) {
    load_reportReport();    
}

$(function() {
    'use strict'

    init_click_handlers();
    init_reply_handler();
    init_image_popup();

    if ($('input[class~="datepicker"], input[class~="report_datepicker"]').length) {
        init_datepicker();
    }

    if($(`input[data-inputmask-alias]`).length) {
        $(":input").inputmask();
    }

    if ($(`[data-toggle="maxlength"]`).length) {
        $(`[data-toggle="maxlength"]`).maxlength({
            alwaysShow: true,
            warningClass: "badge mt-1 badge-success",
            limitReachedClass: "badge mt-1 badge-danger"
        });
    }

    $(`label[for="leave_comment_content"]`).on("click", function() {
        $(`div[id="leave-comment-content"]`).toggleClass("hidden");
    });

    $(`div[class~="email-notification"] i[class~="fa-times-circle"]`).on("click", function() {
        app_notification_box.fadeOut("");
    });

    $(`div[id="submitRecordModal"] button[class~="btn-outline-success"], div[id="generalModal"] button[class~='btn-outline-success']`).on("click", function() {
        let the_button = $(this),
            resource = the_button.attr("data-resource"),
            resource_id = the_button.attr("data-record-id");
        $(`div[id="submitRecordModal"] div[class="form-content-loader"], div[id="generalModal"] div[class="form-content-loader"]`).css({ "display": "flex" });
        let record = {
            resource: resource.toLowerCase(),
            resource_id: resource_id
        };
        $.post(`${baseUrl}api/records/save`, record).then((response) => {
            $(`div[id="submitRecordModal"] div[class="form-content-loader"], div[id="generalModal"] div[class="form-content-loader"]`).css({ "display": "none" });
            Toast.fire({
                icon: responseCode(response.code),
                title: response.data.result
            });
            if (response.code == 200) {
                $(`div[id="submitRecordModal"], div[id="generalModal"]`).modal("hide");
                $(`div[id="saveGeneralFormModal"]`).modal("hide");
                if(response.data.additional.remove !== undefined){
                    $.each(response.data.additional.remove, function(i, e) {
                        $(`div[data-record-setid="${e}"]`).remove();
                    });
                }

                if(the_button.attr("data-reload_href") !== undefined) {
                    setTimeout(() => {
                        window.location.href = the_button.attr("data-reload_href");
                    }, 1000);
                }
                if (response.data.additional !== undefined) {
                    if (response.data.additional.reload) {
                        window.location.href = current_url;
                    }
                }
            }
        }).catch(() => {
            $(`div[id="submitRecordModal"], div[id="generalModal"]`).modal("hide");
            $(`div[id="saveGeneralFormModal"]`).modal("hide");
            Toast.fire({
                icon: "error",
                title: "An unexpected error occured! Try again later."
            });
            $(`div[id="submitRecordModal"] div[class="form-content-loader"], div[id="generalModal"] div[class="form-content-loader"]`).css({ "display": "none" });
        });
    });

    $(`button[class~="submit-popup-button"]`).on("click", function() {
        $(`div[id="submitRecordModal"]`).modal("show");
    });

    $(`a[id="logout-account"]`).on("click", async function() {
        await $.post(`${baseUrl}api/auth/logout`).then((resp) => {
            if (resp.result.code == 200) {
                Toast.fire({
                    icon: 'success',
                    title: resp.result.data
                });
                setTimeout(() => {
                    window.location.href = `${baseUrl}`
                }, 1500)
            }
        });
    });

    $(`select[name="assigned_to"], select[name="status"]`).on("change", function() {
        if ($(this).val() == "Approved") {
            $(`input[name="approved_amount"]`).prop("disabled", false);
        }
        $(`button[class~="save-changes"]`).removeClass("hidden");
    });

    $(`input[name="amount_claimed"], input[name="approved_amount"]`).on("keyup", function() {
        $(`button[class~="save-changes"]`).removeClass("hidden");

    });

    let countVars = {
        "records_count": {
            "messages": {
                "record_id": $(`[data-record="messages_count"]`).attr("data-id"),
                "column": "messages_count"
            },
            "notifications": {
                "record_id": $(`[data-record="notices_count"]`).attr("data-id"),
                "column": "notices_count"
            },
            "claims": {
                "record_id": $(`[data-record="claims_count"]`).attr("data-id"),
                "column": "claims_count"
            },
            "policies": {
                "record_id": $(`[data-record="policies_count"]`).attr("data-id"),
                "column": "policies_count"
            },
            "complaints": {
                "record_id": $(`[data-record="complaints_count"]`).attr("data-id"),
                "column": "complaints_count"
            },
            "replies": {
                "record_id": $(`[data-record="replies_count"]`).attr("data-id"),
                "column": "replies_count"
            },
            "comments": {
                "record_id": $(`[data-record="comments_count"]`).attr("data-id"),
                "column": "comments_count"
            }
        },
        "user_id": $myPrefs.userId
    };

    $.post(`${baseUrl}api/records/threads`, { param: countVars }, async function(response) {
        if (response.code == 200) {
            if (response.data.result) {
                $.each(response.data.result.records_count, function(ie, iv) {
                    $(`[data-record="${ie}"]`).html(iv);
                });

                $.each(response.data.result.notifications, function(ie, iv) {
                    let notices_list = "";
                    $.each(iv, function(_, value) {
                        notices_list += noticesList(value);
                    });
                    $(`[data-threads="${ie}"]`).html(notices_list);

                });
            }
        }
    }, "json");


    if ($(`div[id="quick_minimal_load"]`).length) {

        let minimals = {
            "minimal_load": true,
            "user_id": $(`div[id="quick_minimal_load"]`).attr("data-stream_id"),
            "stream": $(`div[id="quick_minimal_load"]`).attr("data-stream")
        };

        $.post(`${baseUrl}api/records/threads`, { param: minimals }, async function(response) {
            if (response.code == 200) {
                if (response.data.result) {
                    $.each(response.data.result.minimal_load, function(ie, iv) {
                        let minimal_load = response.data.result.minimal_load;
                        if (minimal_load.complaints) {
                            complaintsList(minimal_load.complaints);
                        }
                        if (minimal_load.policies) {
                            policiesList(minimal_load.policies);
                        }
                        if (minimal_load.claims) {
                            claimsList(minimal_load.claims);
                        }
                        if (minimal_load.notes) {
                            notesList(minimal_load.notes);
                        }
                        if (minimal_load.company_policy) {
                            companyPolicyList(minimal_load.company_policy);
                        }
                        if (minimal_load.profile_post_photos) {
                            profilePhotosList(minimal_load.profile_post_photos);
                        }
                    });
                }
                companyProfileInformation();
                previewPolicyDetailedInformation();
            }
        }, "json");

    }

    if($(`div[id="user_performance_load"]`).length) {
        var populate_user_commission = (user_type, base_salary, user_data) => {
            let salary = parseFloat(base_salary),
                commission = 0,
                premium = 0,
                type_list = `${user_type}s_list`;
            if(user_data.list) {
                if(user_data.list[type_list] !== undefined) {
                    $.each(user_data.list[type_list], function(i, e) {
                        premium += parseFloat(e.premium_paid);
                    });
                }
            }
            let percent = parseFloat($(`div[id="user_performance_load"]`).attr("data-user_percentage"));
            commission = (premium !== 0) ? (percent*premium) : 0;
            let total = salary+commission;
            $(`div[id="user_performance_load"] span[class="commission_receivable"]`).html(format_currency(commission));
            $(`div[id="user_performance_load"] span[class="premium_paid_by_clients"]`).html(format_currency(premium));
            $(`div[id="user_performance_load"] span[class="total_amount_receivable"]`).html(format_currency(total));
        };

        var user_performance_load = (user_type, user_id, user_period = "this_month", base_salary = 0) => {
            let stream = {
                "period": "this_month",
                "label": {
                    "stream_period":"current",
                    "stream":"users_report",
                    "user_type":user_type,
                    "user_id":user_id
                }
            };
            $.get(`${baseUrl}api/reports/generate`, stream).then((response) => {
                if(response.code === 200) {
                    populate_user_commission(user_type, base_salary, response.data.result.users_report.users_count.current);
                }
            });
        }
        let _user_type = $(`div[id="user_performance_load"]`).attr("data-user_type"),
            _user_id = $(`div[id="user_performance_load"]`).attr("data-user_id"),
            _user_period = $(`div[id="user_performance_load"]`).attr("data-user_period"),
            _base_salary = $(`div[id="user_performance_load"]`).attr("data-base_salary");
        user_performance_load(_user_type, _user_id, _user_period, _base_salary);
    }

});

$(".selectpicker").select2();
$(`div[class~="trix-button-row"] span[class~="trix-button-group--file-tools"], div[class~="trix-button-row"] span[class~="trix-button-group-spacer"]`).remove();
$("body").prepend(`<div class="populating-data hidden" id="populating-data"><a href="javascript:;" class="btn btn-light btn-sm text-white font-weight-bold btn-icon-text"><div class="spinner-border text-success" role="status"><span class="sr-only">Loading...</span></div><div class="mt-1 text-gray" id="populating-notice">Loading...</div></a></div>`);
$("body").prepend(`<div class="populating-data hidden" id="populating-data"><a href="javascript:;" class="btn btn-light btn-sm text-white font-weight-bold btn-icon-text"><div class="spinner-border text-success" role="status"><span class="sr-only">Loading...</span></div><div class="mt-1 text-gray" id="populating-notice">Loading...</div></a></div>`);
$("body").prepend(`<div class="populating-data hidden" id="populating-data"><a href="javascript:;" class="btn btn-light btn-sm text-white font-weight-bold btn-icon-text"><div class="spinner-border text-success" role="status"><span class="sr-only">Loading...</span></div><div class="mt-1 text-gray" id="populating-notice">Loading...</div></a></div>`);