var populate_adverts = (data) => {
    $(`table[id="adverts_list"]`).dataTable().fnDestroy();
    $(`table[id="adverts_list"]`).dataTable({
        "aaData": data.ads_list,
        "iDisplayLength": $myPrefs.list_count,
        "columns": [
            { "data": 'row_id' },
            { "data": 'advert_title' },
            { "data": 'ad_objective' },
            { "data": 'budget' },
            { "data": 'date_range' },
            { "data": 'campaign_statistics.campaign_stats' },
            { "data": 'status_label' },
            { "data": 'action' }
        ]
    });
    $(`div[class="form-content-loader"]`).css({ "display": "none" });
}

var load_adverts_list = () => {
    $(`div[class="form-content-loader"]`).css("display", "flex");
    $.get(`${baseUrl}api/adverts/list`).then((response) => {
        if (response.code == 200 || response.code == 201) {
            populate_adverts(response.data.result);
        } else {
            $(`div[class="form-content-loader"]`).css({ "display": "none" });
        }
    }).catch(() => {
        $(`div[class="form-content-loader"]`).css({ "display": "none" });
    });
}
if ($(`table[id="adverts_list"]`).length) {
    load_adverts_list();
}