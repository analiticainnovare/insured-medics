var eduplus_content = $(`div[id="eduplus_content"]`),
    eduplus_category = $(`select[id="eduplus_category"]`),
    eduplus_subject_list = $(`div[id="eduplus_subject_list"]`),
    eduplus_subject_loader = $(`div[id="eduplus_subject_loader"]`);


var quick_Search_Topic = () => {
    let search_term = $(`div[id="eduplus_content"] input[name="topic_q"]`).val();
    eduplus_subject_loader.html(``);
    if (search_term.length) {
        eduplus_subject_loader.html(`<div class="p-2 text-center text-primary">Loading materials <i class="fa fa-spin fa-spinner"></i>.</div>`);
        $.get(`${baseUrl}api/eduplus/subject_list`, { q: search_term }).then((response) => {
            eduplus_subject_loader.html(``);
            if (response.code == 200) {
                let subjects_list = "",
                    count = response.data.result.length;
                subjects_list += `
                    <div class="col-lg-12 text-success pl-0 mb-2">
                        <strong>${count}</strong> results found for the search term: <strong>${search_term}</strong>
                    </div>`;
                $.each(response.data.result, function(i, subject) {
                    i++;
                    subjects_list += `
                    <div class="card mb-2">
                        <div class="card-body p-2">
                            <h5>${i}. <a href="${baseUrl}eduplus-view/${subject.item_id}?q=${search_term}">${subject.subject}</a></h5>
                            <div class="mt-2" style="font-size:12px;">
                                <span class="mr-3"><i class="fa fa-calendar-check"></i> ${subject.date_created}</span>
                                <span class="mr-3"><i class="fa fa-eye"></i> ${subject.viral_views}</span>
                                <span class="mr-3">
                                    <a onclick="return user_liked_topic('${subject.item_id}')" href="javascript:void(0);" data-post-is-liked="${subject.is_liked}" data-post-id="${subject.item_id}" class="like-post align-items-center text-muted">
                                        <i data-post-id="${subject.item_id}" class="fa ${subject.is_liked === 1 ? "text-primary" : ""} fa-heart"></i>
                                        <span class="ml-0 ${subject.is_liked === 1 ? "text-primary" : ""}" data-likes-count="${subject.likes_count}">${subject.is_liked === 1 ? "Liked" : "Likes"} (${subject.likes_count})</span>
                                    </a>
                                </span>
                                <span class="mr-2"><i class="fa fa-comments"></i> ${subject.comments_count}</span>
                            </div>
                        </div>                                
                    </div>`;
                });
                eduplus_subject_list.html(subjects_list);
            } else {
                eduplus_subject_list.html(`<div class="p-2 text-center text-danger">Sorry! No materials available to be listed for the search term.</div>`);
            }
        }).catch(() => {
            eduplus_subject_list.html(`<div class="p-2 text-center text-danger">Sorry! There was an error while processing the request.</div>`);
        });
    } else {
        eduplus_subject_list.html(`<div class="p-2 text-center text-danger">Sorry! No materials available to be listed for the search term.</div>`);
    }
}

$(`div[id="eduplus_content"] input[name="topic_q"]`).on('keyup', function(evt) {
    evt.preventDefault();
    if (evt.keyCode == 13 && !evt.shiftKey) {
        quick_Search_Topic();
    }
});

$(`div[id="eduplus_content"] a[class~="like-post"]`).on("click", function() {
    let subject_id = $(this).data("post-id");
    user_liked_topic(subject_id);
});

var user_liked_topic = async(subject_id) => {
    let is_liked = $(`a[class~="like-post"][data-post-id="${subject_id}"]`).attr("data-post-is-liked");
    let l_container = $(`a[class~="like-post"][data-post-id="${subject_id}"] span`);
    var likes = parseInt(l_container.attr("data-likes-count"));
    if (is_liked === "1") {
        await $.post(`${baseUrl}api/eduplus/liked`, { subject_id: subject_id }).then((response) => {
            if (response.code == 200) {
                likes = response.data.result;
            }
        });
        $(`a[class~="like-post"][data-post-id="${subject_id}"]`).attr("data-post-is-liked", "0");
        $(`i[class~="fa-heart"][data-post-id="${subject_id}"]`).removeClass("text-primary");
        l_container.removeClass("text-primary")
            .attr("data-likes-count", (likes))
            .html(`Likes (${likes})`);
    } else {
        await $.post(`${baseUrl}api/eduplus/liked`, { subject_id: subject_id }).then((response) => {
            if (response.code == 200) {
                likes = response.data.result;
            }
        });
        $(`a[class~="like-post"][data-post-id="${subject_id}"]`).attr("data-post-is-liked", "1");
        $(`i[class~="fa-heart"][data-post-id="${subject_id}"]`).addClass("text-primary");
        l_container.addClass("text-primary")
            .attr("data-likes-count", (likes))
            .html(`Likes (${likes})`);
    }
}