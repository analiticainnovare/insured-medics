var current_focused_user_id, current_focused_msg_id, chatWebSocket, users_array_list = {};

var connectWS = (port = 9090) => {
    chatWebSocket = new WebSocket(`ws://localhost:${port}`);
}

var format_UserChats = (chat) => {
        return `<li data-chat_item_id="${chat.item_id}" ${chat.sender_id === $myPrefs.userId ? 'id="sender_msg"' : 'id="receiver_msg"'} class="message-item ${chat.sender_id !== $myPrefs.userId ? "friend" : "me"}">
        <img title="${chat.sender_info.name}" src="${baseUrl}${chat.sender_info.image}" class="img-xs rounded-circle" alt="avatar">
        <div class="content">
            <div class="message">
                <div class="bubble">
                    <div class="d-flex align-items-center">
                        <div title="${(chat.sent_time && chat.sender_id === $myPrefs.userId) ? `Seen: ${chat.sent_time}`: ""}" class="mr-3">${chat.full_message}</div>
                        ${chat.sender_id == $myPrefs.userId ? `<div class="cursor"><span data-chat_item_id="${chat.item_id}" class="font-10px delete_msg" title="Delete this message"><i class="fa fa-trash"></i></span></div>` : ""}
                    </div>
                </div>
                <span>${chat.sent_time}</span>
            </div>
        </div>
    </li>`;
}

$(function() {
    'use strict';

    if ($('.chat-aside .tab-content').length) {
        const sidebarBodyScroll = new PerfectScrollbar('.chat-aside .tab-content');
    }

    if ($('.chat-content .chat-body').length) {
        const sidebarBodyScroll = new PerfectScrollbar('.chat-content .chat-body');
    }

    $('.chat-list .chat-item').each(function(index) {
        $(this).on('click', function() {
            $('.chat-content').toggleClass('show');
        });
    });

    $('#backToChatList').on('click', function(index) {
        $('.chat-content').toggleClass('show');
    });

    $(`input[id="enter_tosend"]`).on("click", async function() {
        let value = $(`input[id="enter_tosend"]`).prop("checked");
        value = (value == true) ? 1 : 0;
        let label = {"messages": {"enter_to_send": value}};
        await $.post(`${baseUrl}api/users/preference`, {label}).then((response) => {
            if(response.code == 200) {
                $(`select[id="enter_to_send"]`).val(value).change();
            }
        });
    });

    $(`button[data-request="attach-file"]`).on("click", function() {
        // $(`input[name="attach_chat_file"]`).trigger("click");
    });

    connectWS();
    chatWebSocket.onopen = function(e) {}
    chatWebSocket.onerror = function(e) {
        show_Notification(`<i class="fa fa-wifi"></i> Live chat connection failed!`, `danger`, 15000);
    }

    chatWebSocket.onmessage = async function(evt) {
        var json = JSON.parse(evt.data);

        if (json.type == "message") {
            if (((json.receiver_id == current_focused_user_id) && (json.sender_id == $myPrefs.userId)) || ((json.sender_id == current_focused_user_id) && (json.receiver_id == $myPrefs.userId))) {
                $(`div[class~="chat-body"] ul[class="messages"]`).append(`${json.formated_message}`);
                let idb_chat = {
                    "chats": {
                        [json.message_id]: {
                            [json.timestamp]: json
                        }
                    }
                };
                await save_chat(idb_chat, json.message_id);
                $(`div[class~="chat-body"]`).animate({scrollTop: $(`div[class~="chat-body"] ul[class="messages"]`).height()}, 10);
                delete_msg();
            }

            if ((json.receiver_id == $myPrefs.userId) && (json.sender_id !== current_focused_user_id)) {
                let unread_count = parseInt($(`li[class~="chat-item"][data-user_id="${json.sender_id}"] [class~="unread_count"]`).html());
                $(`span[id="chat-counter"][data-user_id="${json.sender_id}"]`).removeClass("text-primary").addClass("text-success");
                $(`li[class~="chat-item"][data-user_id="${json.sender_id}"] [class~="unread_count"]`).html((unread_count + 1));
                show_Notification(`${json.sender_info.firstname} sent you a new message.`);
            }

            if(json.receiver_id == $myPrefs.userId) {
                $(`span[class="previous_message"][data-user_id="${json.sender_id}"]`).html(json.message);
            }

            if (current_focused_msg_id == "null") {
                current_focused_msg_id = json.message_id;
                $(`li[class~="chat-item"][data-user_id="${json.receiver_id}"]`).attr("data-message_id", json.message_id);
            }
            $(`div[id="typing_notification"][data-user_id="${json.sender_id}"], span[id="typing_notification"][data-user_id="${json.sender_id}"]`).html("");
        } else {
            if (json.receiver_id == $myPrefs.userId) {
                $(`div[id="typing_notification"][data-user_id="${json.sender_id}"], span[id="typing_notification"][data-user_id="${json.sender_id}"]`).html(json.message);
            }
        }

    }

    $(`div[class~="chat-footer"] button[data-function="send_chat_message"]`).on("click", function() {
        if (chatWebSocket.readyState !== 1) {
            show_Notification(`<i class="fa fa-wifi"></i> Live chat connection failed!`, `danger`, 15000);
            return false;
        }
        var message = $(`div[class~="chat-footer"] textarea[id="chat_input"]`).val();

        if (message.length < 2) {
            return false;
        }
        chatWebSocket.send(
            JSON.stringify({
                'receiver_id': current_focused_user_id,
                'message_id': current_focused_msg_id,
                'sender_id': $myPrefs.userId,
                'user_agent' : userAgent,
                'message': message
            })
        );
        $(`div[class~="chat-footer"] textarea[id="chat_input"]`).val(``).focus();
    });

    function delay(callback, ms) {
        var timer = 0;
        return function() {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
            callback.apply(context, args);
            }, ms || 0);
        };
    }

    $(`div[class~="chat-footer"] textarea[id="chat_input"]`).keyup(delay(function (evt) {
        if (evt.keyCode !== 13 && !evt.shiftKey) {
            chatWebSocket.send(
                JSON.stringify({
                    'receiver_id': current_focused_user_id,
                    'sender_id': $myPrefs.userId,
                    'typing_notification': true
                })
            );
        }
    }, 500));

    $(`div[class~="chat-footer"] textarea[id="chat_input"]`).on('keyup', function(evt) {
        evt.preventDefault();
        var message = $(this).val();

        if (chatWebSocket.readyState !== 1) {
            show_Notification(`<i class="fa fa-wifi"></i> Live chat connection failed!`, `danger`, 15000);
            return false;
        }

        if (message.length < 2) {
            return false;
        }
        if (evt.keyCode == 13 && !evt.shiftKey) {

            let enter_to_send = $(`input[id="enter_tosend"]`).prop("checked");
            if(enter_to_send == false) {
                return false
            }

            chatWebSocket.send(
                JSON.stringify({
                    'receiver_id': current_focused_user_id,
                    'message_id': current_focused_msg_id,
                    'sender_id': $myPrefs.userId,
                    'user_agent' : userAgent,
                    'message': message
                })
            );
            $(this).val(``).focus();
        }
    });


    var delete_msg = () => {
        $(`li[class~="message-item"][id="sender_msg"] span[class~="delete_msg"]`).on("click", async function() {
            let msg_id = $(this).attr("data-chat_item_id");
            await $.post(`${baseUrl}api/chats/delete`, {msg_id: msg_id, action: "delete_message"}).then((response) => {
                if(response.code == 200) {
                    $(`li[class~="message-item"][id="sender_msg"][data-chat_item_id="${msg_id}"]`).remove();
                }
            });
        });
    }

    var trigger_userClicked = () => {

        $(`div[aria-labelledby="chats-tab"] ul[id="search_list"] li[class~="chat-item"]`).on("click", function() {
            $(`input[id="name_search"]`).val("");
        });

        $(`li[class~="chat-item"]`).on("click", async function() {

            let user_id = $(this).attr("data-user_id"),
                user_info = users_array_list[user_id];

            if (current_focused_user_id === user_id) {
                $(`div[class~="chat-footer"] textarea[id="chat_input"]`).val(``).focus();
                return false;
            }

            $(`li[class~="chat-item"][data-user_id="${current_focused_user_id}"]`).removeClass("active-chat");
            $(`div[class~="chat-body"] ul[class="messages"]`).html(``);
            $(`div[class~="chat-body"] div`).remove();

            current_focused_user_id = user_id;
            current_focused_msg_id = $(this).attr("data-message_id");

            $(`li[class~="chat-item"][data-user_id="${user_id}"]`).addClass("active-chat");

            $(`div[class~="chat-header"] div[id="current-user"]`).html(`
                <i data-feather="corner-up-left" id="backToChatList" class="icon-lg mr-2 ml-n2 text-muted d-lg-none"></i>
                <figure class="mb-0 mr-2">
                    <img src="${baseUrl}${user_info.image}" class="img-sm rounded-circle" alt="image">
                    <div class="status ${(user_info.online == 1) ? "online": "offline"}"></div>
                </figure>
                <div>
                    <p>${user_info.name}</p>
                    <p class="text-primary tx-13">${user_info.occupation !== null ? user_info.occupation : ""}</p>
                    <p class="text-muted tx-11">${user_info.online == 1 ? "" : `<span class="font-weight-bold">Last Seen:</span> ${user_info.last_seen}`}</p>
                </div>
            `);
            
            $(`div[id="current-user-options"] a`).attr("data-user_id", user_id);
            $(`div[class~="chat-footer"] button, div[class~="chat-footer"] textarea[id="chat_input"]`).prop("disabled", false);

            let user_chats = await load_idb_record("chats", current_focused_msg_id),
                the_chat_list = {};

            if (user_chats[current_focused_msg_id] !== undefined) {
                the_chat_list = user_chats[current_focused_msg_id];
            } else {
                let messages = await $.post(`${baseUrl}api/chats/list`, { user_id: user_id });
                if (messages.code == 200) {
                    let results_list = messages.data.result

                    the_chat_list = results_list.messages[results_list.message_id];
                    current_focused_msg_id = results_list.message_id;

                    if (current_focused_msg_id !== "null") {
                        replace_id_record(the_chat_list, "chats", current_focused_msg_id);
                    }
                }
            }

            if (the_chat_list) {
                let chats_list = "", prev_time = "", prev_date = "";
                $.each(the_chat_list, function(i, e) {
                    if (!prev_date || prev_date !== e.raw_date) {
                        chats_list += `<div class="message_list_day_divider_label"><button class="message_list_day_divider_label_pill">${e.clean_date}</button></div>`;
                    }
                
                    chats_list += format_UserChats(e);
                    prev_date = e.raw_date;
                });
                $(`div[class~="chat-body"] ul[class="messages"]`).html(chats_list);
            }

            $(`span[id="chat-counter"][data-user_id="${user_id}"]`).addClass("text-primary").removeClass("text-success");
            $(`li[class~="chat-item"][data-user_id="${user_id}"] [class~="unread_count"]`).html(0);
            $(`div[id="typing_notification"], span[id="typing_notification"]`).html(``);
            $(`div[id="file_attachment_wrapper"]`).removeClass("hidden");
            $(`div[class~="chat-content"]`).css({"display":"block"});            

            $(`div[class~="chat-body"]`).animate({scrollTop: $(`div[class~="chat-body"] ul[class="messages"]`).height()}, 10);
            $(`div[class~="chat-footer"] textarea[id="chat_input"]`).val(``).focus();
            delete_msg();
        });

    }

    $(`span[id="list_contacts"]`).on("click", function() {
        $(`li[class~="chat-item"][data-user_id="${current_focused_user_id}"]`).removeClass("active-chat");
        $(`div[class~="chat-content"]`).css({"display":"none"});
        current_focused_user_id = "null";
    });

    var format_UserInfo = (data) => {

        return `<li class="chat-item pr-1" data-user_id="${data.user_id}" data-message_id="${data.msg_id}">
            <a href="javascript:;" class="d-flex align-items-center">
            <figure data-user_id="${data.user_id}" title="Send message" class="mb-0 mr-2">
                <img src="${baseUrl}${data.image}" class="img-xs rounded-circle" alt="user">
                <div class="status ${(data.online == 1) ? "online": "offline"}"></div>
            </figure>
            <div class="d-flex align-items-center justify-content-between flex-grow border-bottom">
                <div>
                <p title="Send message" class="text-body">
                    ${data.name} 
                    <div id="typing_notification" data-user_id="${data.user_id}" class="tx-11 font-italic"></div>
                </p>
                <div class="d-flex align-items-center">
                    <p class="text-muted tx-13"></p>
                </div>
                </div>
                <div class="d-flex align-items-end text-body">
                    <span id="chat-counter" data-user_id="${data.user_id}" title="Send message" class="mr-2 text-primary">
                        <i class="fa fa-comments" class="icon-md text-primary"></i>
                        <span class="unread_count">0</span>
                    </span>
                </div>
            </div>
            </a>
        </li>`;
    };

    var format_Recent_ChatItem = (data, unread) => {

        if (unread !== 0) {
            $(`span[id="chat-counter"][data-user_id="${data.sender_id}"]`).removeClass("text-primary").addClass("text-success");
            $(`li[class~="chat-item"][data-user_id="${data.sender_id}"] [class~="unread_count"]`).html(unread);
        }

        return `
        <li class="chat-item pr-1" data-user_id="${data.sender_id}" data-message_id="${data.message_unique_id}">
            <a href="javascript:void(0);" class="d-flex align-items-center">
                <figure class="mb-0 mr-2">
                    <img src="${baseUrl}${data.sender_info.image}" class="img-xs rounded-circle" alt="user">
                    <div class="status ${(data.sender_info.online == 1) ? "online": "offline"}"></div>
                </figure>
                <div class="d-flex justify-content-between flex-grow border-bottom">
                    <div>
                        <p class="text-body">
                            ${data.sender_info.name} <span id="typing_notification" data-user_id="${data.sender_id}" class="tx-11 font-italic"></span>
                        </p>
                        <p class="text-muted tx-13">
                            <span class="previous_message" data-user_id="${data.sender_id}">${data.message}</span>
                        </p>
                    </div>
                    <div class="d-flex flex-column align-items-end">
                        <p class="text-muted tx-13 mb-1">${data.sent_ago}</p>
                        <div class="unread_count badge badge-pill badge-primary ml-auto">${unread}</div>
                    </div>
                </div>
            </a>
        </li>`;
    }

    var get_usersList = (search_term = "", display = "contacts_list") => {
        $.get(`${baseUrl}api/users/list?minified=chat_list_users&q=${search_term}`).then((response) => {
            if (response.code == 200 || response.code == 201) {
                let users_list = "",
                    users_chat_list = "";
                $.each(response.data.result.users_list, function(i, e) {
                    users_list += format_UserInfo(e);
                    users_array_list[e.user_id] = e;
                });
                if (display == "search_list") {
                    $(`div[aria-labelledby="chats-tab"] ul[id="search_list"]`).html(users_list);
                } else {
                    $(`div[aria-labelledby="contacts-tab"] ul[id="contacts_list"]`).html(users_list);
                }

                if (search_term == "") {
                    $.each(response.data.result.chats_list.messages, function(i, e) {
                        users_chat_list += format_Recent_ChatItem(e.message, e.unread_count);
                    });
                    $(`div[aria-labelledby="chats-tab"] ul[id="chats_list"]`).html(users_chat_list);
                }
                trigger_userClicked();
            } else {
                $(`div[class="form-content-loader"]`).css({ "display": "none" });
            }
        });
    }

    $(`input[id="name_search"]`).on("keyup", function() {
        let keyword = $(this).val();
        if (keyword.length < 3) {
            $(`div[aria-labelledby="chats-tab"] ul[id="chats_list"]`).removeClass("hidden");
            $(`div[aria-labelledby="chats-tab"] ul[id="search_list"]`).html("").addClass("hidden");
            $(`a[id="contacts-tab"]`).trigger("click");
        }
        if (keyword.length > 2) {
            $(`div[aria-labelledby="chats-tab"] ul[id="search_list"]`).removeClass("hidden");
            $(`div[aria-labelledby="chats-tab"] ul[id="chats_list"]`).addClass("hidden");
            $(`a[id="chats-tab"]`).trigger("click");

            get_usersList(keyword, "search_list");
        }
    });

    get_usersList();

    $(`button[data-request="attach-file"]`).on("click", function() {
        $(`input[name="attachment_file_upload"]`).trigger("click");
    });

});

$('.chat-attachment').on('drop', function(e) {
    e.stopPropagation();
    e.preventDefault();

    $(`div[class="upload-overlay-cover"]`).css("display", "none");

    var file = e.originalEvent.dataTransfer.files;

    $.each(file, function(i, the_file) {
        let fd = new FormData(),
            files = the_file,
            module = file_to_upload.attr("data-form_module"),
            item_id = file_to_upload.attr("data-form_item_id");

        fd.append('attachment_file_upload', files);
        fd.append('module', module);
        fd.append('label', "upload");
        fd.append('item_id', item_id);
        ajax_chat_post_file_upload(fd);
    });

});

$(`input[name="attachment_file_upload"]`).change(function() {
    file_to_upload = $(this);
    $.each(file_to_upload[0].files, function(ii, iv) {
        let fd = new FormData(),
            files = iv,
            module = file_to_upload.attr("data-form_module"),
            item_id = file_to_upload.attr("data-form_item_id");

        fd.append('attachment_file_upload', files);
        fd.append('module', module);
        fd.append('label', "upload");
        fd.append('item_id', item_id);

        ajax_chat_post_file_upload(fd);
    });
});

function delete_ajax_file_uploaded(module, item_id) {
    $(`div[data-document-link="${item_id}"] *`).prop("disabled", true);
    $.post(`${baseUrl}api/files/attachments`, { item_id: item_id, module: module, label: "remove" }).then((response) => {
        if (response.code == 200) {
            $(`div[data-document-link="${item_id}"]`).remove();
        }
    }, 'json').catch(() => {
        $(`div[data-document-link="${item_id}"] *`).prop("disabled", false);
    });
}

function load_ajax_chat_post_file_uploads(response) {
    let preview = $(`div[class~="file-preview"][preview-id="${response.module}"]`),
        preview_details = $(`div[class="file-attachment_logs"]`);
    preview.html(response.files);
    preview_details.html(response.details);
}

function ajax_chat_post_file_upload(formdata) {
    $(`div[class~="upload-document-loader"]`).removeClass("hidden");
    $.ajax({
        url: $('div[class="file_attachment_url"]').attr("data-url"),
        type: 'post',
        data: formdata,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(response) {
            $("input.attachment_file_upload").val("");
            if (response.code == 200) {
                load_ajax_chat_post_file_uploads(response.data.result);
            }
        },
        complete: function() {
            $(`div[class~="upload-document-loader"]`).addClass("hidden");
        },
        error: function() {
            $(`div[class~="upload-document-loader"]`).addClass("hidden");
        }
    });
}