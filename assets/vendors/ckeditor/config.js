CKEDITOR.editorConfig = function(config) {
    config.toolbarGroups = [
        { name: 'document', groups: ['mode'] },
        { name: 'clipboard', groups: ['clipboard', 'undo'] },
        { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
        { name: 'colors', groups: ['colors'] },
        { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph'] },
        { name: 'insert', groups: ['insert'] },
        { name: 'links', groups: ['links'] },
        '/',
        '/',
        { name: 'tools', groups: ['tools'] },
    ];

    config.removeButtons = 'PasteText,Save,Paste,Source,PasteFromWord,Image,StringThrough,Preview,Subscript,Superscript,Redo,Replace,Print,Checkbox,TextField,Textarea,Select,Form,HiddenField,ImageButton,Button,Radio,Iframe,PageBreak,Flash,Indent,Outdent,Language,BidiRtl,BidiLtr,Anchor,HorizontalRule,SpecialChar,Smiley,NewPage,Copy,Cut,Undo,ShowBlocks,Maximize,About,CreateDiv,CopyFormatting,SelectAll';
    config.extraPlugins = 'widget,dialog';
};