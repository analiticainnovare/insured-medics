<?php
// ensure this file is being included by a parent file
if( !defined( 'BASEPATH' ) ) die( 'Restricted access' );

class Forms extends Medics {

    private $hasit;
    private $userPrefs;
    private $thisUser;

    public function __construct() {
        parent::__construct();
        
        global $accessObject;
        $this->hasit = $accessObject;
    }

    /**
     * For label definitions
     * 
     * @param String $form
     */
    public function form_labels($form) {
        
        $labels = [
            "user_account" => [
                "user_id_label" => [
                    "admin" => "Admin ID",
                    "user" => "Client ID",
                    "agent" => "Agent ID",
                    "nic" => "Employee ID",
                    "broker" => "Unique ID",
                    "bank" => "Employee ID",
                    "business" => "Client ID",
                    "bancassurance" => "Agent ID",
                    "insurance_company" => "Employee ID",
                ]
            ]
        ];

        return $labels[$form] ?? [];
    }

    /**
     * Generate a form and return in the response back to the user
     * The variable module will contain all the needed parameters that will be used to generate the form
     * and parse back in the data key of the result set. The process will be very intellegent and take into consideration 
     * everything that should be considered in generating a form for the user.
     * 
     * @param \stdClass $params
     * @param Array $params->module
     * 
     * @return Array
     */
    public function load(stdClass $params) {

        /** Access object */
        global $accessObject, $usersClass;
        
        /** Set parameters */
        $this->thisUser = $params->userData;
        $this->hasit->userId = $params->userData->user_id;
        $this->hasit->userPermits = $params->userData->user_permissions;
        $this->userPrefs = $params->userData->preferences;
        $this_user_id = $params->userData->user_id;

        // set the user's default text edit if not already set
        $this->userPrefs->text_editor = isset($this->userPrefs->text_editor) ? $this->userPrefs->text_editor : "trix";

        /** Test Module Variable */
        if(!isset($params->module)) {
            return ["code" => 201, "data" => "Sorry! The module parameter is required."];
        }

        /** If module not an array */
        if(!is_array($params->module)) {
            return ["code" => 201, "data" => "Sorry! The module parameter is must be an array variable."];
        }
        
        /** If module not an array */
        if(!isset($params->module["label"])) {
            return ["code" => 201, "data" => "Sorry! The label key in the array must be supplied."];
        }

        /** If the label is not in the array list */
        if(!in_array($params->module["label"], array_keys($this->form_modules))) {
            return ["code" => 201, "data" => "Sorry! An invalid label value was parsed."];
        }

        /** Init variables */
        $result = null;
        $resources = [];
        $content = [];
        
        /** Form processing div */
        $the_form = $params->module["label"];

        /** content type */
        if(isset($params->module["content"]) && $params->module["content"] == "form") {
            
            /** Ajax Forms */
            if($the_form === "ajax_forms_loader") {
                // load the ajax form content
                $result = $this->ajax_forms_loader($params);
            }
            
            /** The label and method to load */
            elseif($the_form === "apply_policy") {
                /** Confirm the user has the permission to perform this action */
                if(!$accessObject->hasAccess("signup", "company_policy")) {
                    return ["code" => 201, "data" => $this->permission_denied];
                }
                
                /** Append to parameters */
                $params->apply_policy_form = true;
                
                /** Load the policy application form */
                $result = $this->apply_policy_form($params);
            }
            
            /** preview a form variable data set */
            elseif($the_form === "preview_form") {

                // return if no policy id was parsed
                if(!isset($params->module["item_id"])) {
                    return;
                }

                /** Load the policy application form */
                $result = $this->preview_form($params);
            }

            /** Load policy application form */
            elseif($the_form === "load_policy_form") {

                // return if no policy id was parsed
                if(!isset($params->module["item_id"]) || !isset($params->module["user_id"])) {
                    return;
                }

                // assign variable to the item id
                $item_id = xss_clean($params->module["item_id"]);
                $this_user_id = xss_clean($params->module["user_id"]);

                /** create a new object of the company_policy class */
                $recordObj = load_class("company_policy", "controllers");

                /** Parameters */
                $param = (object) [
                    "policy_type_id" => $item_id,
                    "remote" => true,
                    "limit" => 1
                ];

                // return the result
                $resources = ["assets/js/ajax-load.js"];
                $recordData = $recordObj->list($param);
                $itemFound = !empty($recordData["data"]) ? true : false;

                // if empty itemFound then return false
                if(empty($itemFound)) {
                    return;
                }

                /** Confirm that the user_id is valid */
                $i_params = (object) ["limit" => 1, "user_id" => $this_user_id];
                $userData = $usersClass->list($i_params)["data"];

                // if empty itemFound then return false
                if(empty($userData)) {
                    return;
                }

                // record data
                $recordData = $recordData["data"][0];
                $userData = $userData[0];
                
                // load the activity logs
                $result = $this->policy_application_form($recordData, $userData);
            }

            /** Load user notifications */
            elseif($the_form === "user_notifications") {

                /** If module not an array */
                if(!isset($params->module["item_id"])) {
                    return ["code" => 201, "data" => "Sorry! The User Id to update was not parsed in the item_id variable."];
                }

                /** Set the user id */
                $params->update_user_account = true;
                $params->this_user_id = $params->module["item_id"];

                /** Confirm that the user_id is valid */
                $i_params = (object) ["limit" => 1, "user_id" => $params->this_user_id];
                $the_user = $usersClass->list($i_params)["data"];

                // get the user data
                if(empty($the_user)) {
                    return ["code" => 201, "data" => "Sorry! Please provide a valid user id."];
                }
                /** Load the update form */
                $result = $this->user_notifications($params);
            }

            /** Load insurance claims application form */
            elseif($the_form === "load_claims_form") {

                // return if no policy id was parsed
                if(!isset($params->module["item_id"]) || !isset($params->module["user_id"])) {
                    return;
                }

                // assign variable to the item id
                $item_id = xss_clean($params->module["item_id"]);
                $this_user_id = xss_clean($params->module["user_id"]);

                /** create a new object of the company_policy class */
                $recordData = $this->pushQuery(
                    "   b.policy_form, b.claims_form, a.policy_id, a.policy_name, a.company_id, 
                        a.item_id, b.name, b.item_id AS main_policy_id",
                    "users_policy a LEFT JOIN policy_types b ON b.item_id = a.policy_type",
                    "a.item_id = '{$item_id}' AND a.user_id = '{$this_user_id}'"
                );

                // return the result
                $resources = ["assets/js/ajax-load.js"];
                $itemFound = !empty($recordData) ? true : false;

                // if empty itemFound then return false
                if(empty($itemFound)) {
                    return;
                }

                /** Confirm that the user_id is valid */
                $i_params = (object) ["limit" => 1, "user_id" => $this_user_id];
                $userData = $usersClass->list($i_params)["data"];

                // if empty itemFound then return false
                if(empty($userData)) {
                    return;
                }

                // record data
                $recordData = $recordData[0];
                $userData = $userData[0];

                //convert some of the elements to json
                $recordData->claims_form = json_decode($recordData->claims_form);
                $recordData->policy_form = json_decode($recordData->policy_form);
                
                // load the activity logs
                $result = $this->policy_application_form($recordData, $userData, "claims");
            }

            /** Load policy application form */
            elseif($the_form === "update_insurance_policy") {

                // return if no policy id was parsed
                if(!isset($params->module["item_id"])) {
                    return;
                }

                // assign variable to the item id
                $item_id = xss_clean($params->module["item_id"]);

                /** create a new object of the user_policy class */
                $recordObj = load_class("user_policy", "controllers");

                /** Parameters */
                $param = (object) [
                    "policy_id" => $item_id,
                    "userId" => $params->userId,
                    "userData" => $params->userData,
                    "limit" => 1
                ];

                /** Get the propery */
                $is_editable = isset($params->module["property"]) && $params->module["property"] == "view_insurance_policy" ? false : true;

                // return the result
                $resources = ["assets/js/ajax-load.js"];
                $recordData = $recordObj->list($param);
                $itemFound = !empty($recordData["data"]) ? true : false;

                // if empty itemFound then return false
                if(empty($itemFound)) {
                    return;
                }

                // record data
                $recordData = $recordData["data"][0];
                $recordData->userData = $params->userData;

                // convert the data string to an object if not already an object
                $recordData->form_fields = !is_object($recordData->form_fields) ? json_decode($recordData->form_fields) : $recordData->form_fields;
                $recordData->form_answers = !is_object($recordData->form_answers) ? json_decode($recordData->form_answers) : $recordData->form_answers;


                // load the activity logs
                $result = $this->preload_form_data($recordData, $recordData->form_answers, $is_editable, "user_policy");
            }

            /** Load policy application form */
            elseif($the_form === "update_claims") {

                // return if no policy id was parsed
                if(!isset($params->module["item_id"])) {
                    return;
                }

                // assign variable to the item id
                $item_id = xss_clean($params->module["item_id"]);

                /** create a new object of the user_policy class */
                $recordObj = load_class("claims", "controllers");

                /** Parameters */
                $param = (object) [
                    "claim_id" => $item_id,
                    "userId" => $params->userId,
                    "remote" => true,
                    "limit" => 1
                ];

                /** Get the propery */
                $is_editable = isset($params->module["property"]) && $params->module["property"] == "view_claims" ? false : true;

                // return the result
                $resources = ["assets/js/ajax-load.js"];
                $recordData = $recordObj->list($param);
                $itemFound = !empty($recordData["data"]) ? true : false;

                // if empty itemFound then return false
                if(empty($itemFound)) {
                    return;
                }

                // record data
                $recordData = $recordData["data"][0];
                $recordData->userData = $params->userData; 

                // convert the data string to an object if not already an object
                $recordData->form_fields = !is_object($recordData->form_fields) ? json_decode($recordData->form_fields) : $recordData->form_fields;
                $recordData->form_answers = !is_object($recordData->form_answers) ? json_decode($recordData->form_answers) : $recordData->form_answers;

                // load the activity logs
                $result = $this->preload_form_data($recordData, $recordData->form_answers, $is_editable, "claims");
            }
            
            /** Show the user account form */
            elseif($the_form === "user_account") {
                
                /** Load additional information of the user */
                if(isset($params->module["property"]["load"])) {
                    /** If module not an array */
                    if(!isset($params->module["item_id"])) {
                        return ["code" => 201, "data" => "Sorry! The User Id to load additional information is required."];
                    }

                    // set the user id
                    $this_user_id = $params->module["item_id"];
                    $data_to_request = $this->stringToArray($params->module["property"]["load"]);

                    /** Confirm that the user_id is valid */
                    $i_params = (object) ["limit" => 1, "user_id" => $this_user_id];
                    $the_user = $usersClass->list($i_params)["data"];

                    // get the user data
                    if(empty($the_user)) {
                        return ["code" => 201, "data" => "Sorry! Please provide a valid user id."];
                    }

                    //  load, prepare and return the user request
                    $result = $this->user_additional_information($this_user_id, $data_to_request);

                } else {
                    
                    /** Append to parameters */
                    $params->user_account_form = true;
                    
                    /** Load the user form */
                    $result = $this->user_account_form($params);
                }
            }
            
            /** Update the user account */
            elseif($the_form === "update_user_account") {
                
                /** If module not an array */
                if(!isset($params->module["item_id"])) {
                    return ["code" => 201, "data" => "Sorry! The User Id to update was not parsed in the item_id variable."];
                }

                /** Set the user id */
                $params->update_user_account = true;
                $params->this_user_id = $params->module["item_id"];
                $resources = ["assets/js/ajax-load.js"];

                /** Confirm that the user_id is valid */
                $i_params = (object) ["limit" => 1, "user_id" => $params->this_user_id];
                $the_user = $usersClass->list($i_params)["data"];
                // get the user data
                if(empty($the_user)) {
                    return ["code" => 201, "data" => "Sorry! Please provide a valid user id."];
                }
                /** Load the update form */
                $result = $this->user_account_form($params);
            }
            
            /** Load complaints form */
            elseif($the_form === "lodge_complaint") {
                /** Confirm the user has the permission to perform this action */
                if(!$accessObject->hasAccess("lodge", "complaints")) {
                    return ["code" => 201, "data" => $this->permission_denied];
                }
                $resources = ["assets/js/ajax-load.js"];
                // append the complaint id
                if(isset($params->module["item_id"])) {
                    // set the variable
                    $params->the_complaint_id = $params->module["item_id"];
                    // load the complait record
                    $complaint = $this->pushQuery(
                        "a.*, (SELECT b.description FROM files_attachment b WHERE b.record_id = a.item_id) AS attachment",
                        "users_complaints a",
                        "a.user_id = '{$params->userId}' AND a.item_id='{$params->the_complaint_id}' AND a.submit_status='draft'"
                    );
                    // if the result is empty then end the query
                    if(empty($complaint)) {
                        return;
                    }
                    // assign the variable
                    $params->complaintsData = $complaint[0];
                }
                /** Append to parameters */
                $params->complaints_form = true;
                /** Load the form */
                $form_data = $this->complaints_form($params);
                $result = $form_data["form"];
                $content = [
                    "ajax-form-content" => $form_data["message"]
                ];
            }
            
            /** Load complaints form */
            elseif($the_form === "make_claim") {
                /** Confirm the user has the permission to perform this action */
                if(!$accessObject->hasAccess("request", "claims")) {
                    return ["code" => 201, "data" => $this->permission_denied];
                }
                $resources = ["assets/js/ajax-load.js"];
                /** Append to parameters */
                $params->claims_request_form = true;
               
                /** Load the form */
                $result = $this->claims_request_form($params);
            }

            /** Share announcement */
            elseif($the_form === "post_announcement") {

                /** Confirm the user has the permission to perform this action */
                if(!$accessObject->hasAccess("add", "announcements")) {
                    return ["code" => 201, "data" => $this->permission_denied];
                }
                $resources = ["assets/js/script/attachments.js"];

                // load the announcement if the id was parsed
                if(isset($params->module["item_id"])) {
                    // set the id
                    $announcement_id  = $params->module["item_id"];
                    // create a new object
                    $announceObj = load_class("announcements", "controllers");
                    // query the 
                    $param = (object) [
                        "limit" => 1,
                        "minimal_load" => true,
                        "clean_date" => true,
                        "userId" => $params->userId,
                        "userData" => $params->userData,
                        "announcement_id" => $announcement_id
                    ];
                    $data = $announceObj->list($param);

                    if(empty($data)) {
                        return;
                    }
                    $data = $data[0];
                    
                    // append the data record to the object
                    $params->announcementData = $data;
                }
               
                /** Load the form */
                $result = $this->share_announcement($params);

            }
            
            /** Load the reply form */
            elseif($the_form === "share_a_reply") {
                // close the query if no item id was parsed
                if(!isset($params->module["item_id"])) {
                    return;
                }
                /** Get the item */
                $item = $params->module["property"];
                $item_id = xss_clean($params->module["item_id"]);
                // load the resources
                $resources = ["assets/js/ajax-load.js"];
                /** Share a Reply */
                $result = $this->share_a_reply_form($params->userData, $item, $item_id);

            }

            /** License application */
            elseif($the_form === "license_application") {

                // close the query if no item id was parsed
                $item_id = isset($params->module["item_id"]) ? $params->module["item_id"] : null;
                
                // init
                $data = null;
                $resources = ["assets/js/script/attachments.js"];

                // get the application information 
                if(!empty($item_id)) {

                    /** load the company licenses */
                    $recordObj = load_class("licenses", "controllers");

                    /** Parameters */
                    $param = (object) [
                        "application_id" => $item_id,
                        "userData" => $params,
                        "company_id" => $params->userData->company_id,
                        "remote" => false,
                        "limit" => 1
                    ];

                    // return the result
                    $recordData = $recordObj->list($param);

                    // if the result is empty
                    if(empty($recordData["data"])) {
                        return null;
                    } else {
                        $data = $recordData["data"][0];
                    }
                }

                // load the application form
                $result = $this->license_application($params, $data);

            }

            /** Load the item activity history */
            elseif($the_form === "activity_history") {
                if(!isset($params->module["item_id"])) {
                    return;
                }
                // assign variable to the item id
                $item_id = xss_clean($params->module["item_id"]);
                
                // load the activity logs
                $result = $this->property_activity_logs($item_id);
            }
            
            /** Load the replies list */
            elseif($the_form === "view_replies_list") {
                // stop query if the item id was not parsed
                if(!isset($params->module["item_id"])) {
                    return;
                }
                // stop the query if a remote call was made
                if(isset($params->remote) && $params->remote) {
                    return;
                }

                // load the resources
                $result = [];
                $resources = [
                    "assets/js/ajax-load.js"
                ];

                $params->last_reply_id = null;
                $params->feedback_type = "reply";
                if(isset($params->module["last_reply_id"]) && !empty($params->module["last_reply_id"])) {
                    $params->last_reply_id = $params->module["last_reply_id"];
                    $resources = [];
                }
                
                // show the replies list
                if($params->last_reply_id !== "no_more_record") {
                    $result = $this->view_replies_list($params);
                }

                // return the results
                return [
                    "code" => 200,
                    "data" => [
                        "resources" => $resources,
                        "replies" => $result
                    ]
                ];
            }
            
            /** Load the user information */
            elseif($the_form === "user_basic_information") {
                /** If module not an array */
                if(!isset($params->module["item_id"])) {
                    return ["code" => 201, "data" => "Sorry! The User Id to update was not parsed in the item_id variable."];
                }

                /** Set the user id */
                $params->update_user_account = true;
                $params->this_user_id = $params->module["item_id"];

                /** Confirm that the user_id is valid */
                $i_params = (object) ["limit" => 1, "user_id" => $params->this_user_id];
                $the_user = $usersClass->list($i_params)["data"];
                $params->user_basic_information = true;
                
                // get the user data
                if(empty($the_user)) {
                    return ["code" => 201, "data" => "Sorry! Please provide a valid user id."];
                }
                /** Load the update form */
                $result = $this->user_basic_information($params); 
            }
            
            /** Load the company manager information */
            elseif($the_form === "company_manager") {

                /** Confirm the user has the permission to perform this action */
                if(!$accessObject->hasAccess("update", "insurance_company")) {
                    //return ["code" => 201, "data" => $this->permission_denied];
                }

                /** Init */
                $manager_data = [];

                /** If module not an array */
                if(isset($params->module["item_id"])) {
                    
                    /** Set the user id */
                    $params->update_manager_account = true;
                    $params->this_manager_id = explode("_", $params->module["item_id"]);

                    /** Confirm that the user_id is valid */
                    $i_params = (object) ["limit" => 1, "company_id" => $params->this_manager_id[0], "columns" => "managers, item_id, name"];
                    $the_user = load_class("company", "controllers")->list($i_params)["data"];
                    
                    /** if a record was found */
                    if(empty($the_user)) {
                        return;
                    }

                    /** if the second parameter was not set */
                    if(!isset($params->this_manager_id[1])) {
                        return;
                    }

                    /** Get the manager information and convert to an array */
                    $managers_list = (array) $the_user[0]->managers;

                    /** Get the column value of the array list */
                    $item_key = array_column($managers_list, "manager_id");
                    
                    /** if not in array */
                    if(!in_array($params->module["item_id"], $item_key)) {
                        return;
                    }

                    /** Assign the manager's data */
                    $manager_data = $managers_list[$params->this_manager_id[1]];

                }

                $manager_data = (object) $manager_data;
                
                /** Load the company manager form */
                $result = $this->company_manager_form($manager_data); 
            }
            
            /** Load the company awards information */
            elseif($the_form === "company_award") {

                /** Confirm the user has the permission to perform this action */
                if(!$accessObject->hasAccess("update", "insurance_company")) {
                    //return ["code" => 201, "data" => $this->permission_denied];
                }

                /** Init */
                $award_data = [];

                /** If module not an array */
                if(isset($params->module["item_id"])) {
                    
                    /** Set the user id */
                    $params->this_award_id = explode("_", $params->module["item_id"]);

                    /** Confirm that the user_id is valid */
                    $i_params = (object) ["limit" => 1, "company_id" => $params->this_award_id[0], "columns" => "awards, item_id, name"];
                    $the_user = load_class("company", "controllers")->list($i_params)["data"];
                    
                    /** if a record was found */
                    if(empty($the_user)) {
                        return;
                    }

                    /** if the second parameter was not set */
                    if(!isset($params->this_award_id[1])) {
                        return;
                    }

                    /** Get the award information and convert to an array */
                    $awards_list = (array) $the_user[0]->awards;

                    /** Get the column value of the array list */
                    $item_key = array_column($awards_list, "award_id");
                    
                    /** if not in array */
                    if(!in_array($params->module["item_id"], $item_key)) {
                        return;
                    }

                    /** Assign the award's data */
                    $award_data = $awards_list[$params->this_award_id[1]];

                }

                $award_data = (object) $award_data;
                
                /** Load the company award form */
                $result = $this->company_award_form($award_data); 
            }
            
            /** Load the company information */
            elseif($the_form === "manage_company_information") {

                /** Confirm the user has the permission to perform this action */
                if(!$accessObject->hasAccess("update", "insurance_company")) {
                    //return ["code" => 201, "data" => $this->permission_denied];
                }

                /** Init */
                $company_data = [];

                /** If module not an array */
                if(isset($params->module["item_id"])) {
                    
                    /** Set the user id */
                    $params->this_company_id = $params->module["item_id"];

                    /** Confirm that the user_id is valid */
                    $i_params = (object) ["limit" => 1, "company_id" => $params->this_company_id];
                    $the_company = load_class("company", "controllers")->list($i_params)["data"];
                    
                    /** if a record was found */
                    if(empty($the_company)) {
                        return;
                    }

                    /** Assign the manager's data */
                    $company_data = $the_company[0];

                }

                $company_data = (object) $company_data;
                
                /** Load the company manager form */
                $result = $this->company_form($company_data); 
            }
            
            /** Load the company information */
            elseif($the_form === "post_advertisement") {

                /** Confirm the user has the permission to perform this action */
                if(!$accessObject->hasAccess("add", "adverts")) {
                    return ["code" => 201, "data" => $this->permission_denied];
                }

                /** Init */
                $adverts_data = [];

                /** If module not an array */
                if(isset($params->module["item_id"])) {

                    /** Confirm the user has the permission to perform this action */
                    if(!$accessObject->hasAccess("update", "adverts")) {
                        return ["code" => 201, "data" => $this->permission_denied];
                    }
                    
                    /** Set the user id */
                    $params->this_advert_id = $params->module["item_id"];

                    /** Confirm that the user_id is valid */
                    $i_params = (object) [
                        "limit" => 1, 
                        "show_button" => false,
                        "advert_id" => $params->this_advert_id, 
                        "userData" => $params->userData
                    ];
                    $the_advert = load_class("adverts", "controllers")->list($i_params)["data"]["ads_list"];
                    
                    /** if a record was found */
                    if(empty($the_advert)) {
                        return;
                    }

                    /** Assign the manager's data */
                    $adverts_data = $the_advert[0];

                }

                $adverts_data = (object) $adverts_data;
                
                /** Load the company manager form */
                $result = $this->manage_adverts_form($adverts_data, $params); 
            }

            /** Report request form */
            elseif($the_form === "request_report") {

                /** Load the policy application form */
                $result = $this->request_report($params);

            }

            /** Eduplus */
            elseif(in_array($the_form, ["eduplus_category", "eduplus_subject"])) {

                // close the query if no item id was parsed
                $item_id = isset($params->module["item_id"]) ? $params->module["item_id"] : null;
                $expl = explode("_", $item_id);

                /** Get the item list or the  */
                if($the_form === "eduplus_subject") {
                    $item_id = isset($expl[1]) ? $expl[1] : null;
                }

                // set the category id as the init
                $params->category_id = $expl[0];
                
                // init
                $data = null;
                
                /** load the company licenses */
                $params->eduClass = load_class("eduplus", "controllers");

                /** Include a script */
                $resources = ["assets/js/script/attachments.js"];

                // get the application information 
                if(!empty($item_id)) {
                    
                    /** Parameters */
                    $param = (object) [
                        "subject_id" => $item_id,
                        "category_id" => $params->category_id,
                        "userData" => $params->userData,
                        "userId" => $params->userId,
                        "limit" => 1
                    ];

                    $method = ($the_form == "eduplus_category") ? "category_list" : "subject_list";

                    // return the result
                    $recordData = $params->eduClass->{$method}($param);

                    // if the result is empty
                    if(empty($recordData["data"])) {
                        return null;
                    } else {
                        $data = $recordData["data"][0];
                    }
                }

                // load the application form
                $result = $this->{$the_form}($params, $data);

            }

            /** Payment History List */
            elseif(in_array($the_form, ["policy_payment_history"])) {
                /** If module not an array */
                if(!isset($params->module["item_id"])) {
                    return ["code" => 201, "data" => "Sorry! The Record ID to load the payment history was not provided."];
                }

                // set the variable
                $item_id = xss_clean($params->module["item_id"]);

                // load the results
                $result = $this->view_payment_history($the_form, $item_id);
            }

        }

        // the result set to return
        $result_set = ["form" => $result];

        // if the content is not empty
        if(!empty($content)) {
            $result_set["content"] = $content;
        }
        // if resources was parsed
        if(!empty($resources)) {
            $result_set["resources"] = $resources;
        }
        // if the form data is not empty
        if(!empty($the_form)) {
            $result_set["labels"] = $this->form_labels($the_form);
        }

        // return the result
        return [
            "code" => !empty($result) ? 200 : 201,
            "data" => $result_set
        ];
    }

    /**
     * This method will be used to list attachments onto the page
     * 
     * @param Array $params
     * @param String $user_id
     * @param Bool $is_deletable            This allows the user to delete the file
     * @param Bool $show_view               This when set to false will hide the view icon
     * 
     * @return String
     */
    public function list_attachments($attachment_list = null, $user_id = null, $list_class = "col-lg-4 col-md-6", $is_deletable = false, $show_view = true) {

        // variables
        $list_class = empty($list_class) ? "col-lg-4 col-md-6" : $list_class;
        
        // images mimetypes for creating thumbnails
        $image_mime = ["jpg", "jpeg", "png", "gif"];
        $docs_mime = ["pdf", "doc", "docx", "txt", "rtf", "jpg", "jpeg", "png", "gif"];
        $video_mime = ["mp4", "mpeg", "movie", "webm", "mov", "mpg", "mpeg", "qt"];

        // set the thumbnail path
        $tmp_path = "assets/uploads/{$user_id}/tmp/thumbnail/";

        // create directories if none existent
        if(!is_dir("assets/uploads/{$user_id}")) {
            mkdir("assets/uploads/{$user_id}");
            mkdir("assets/uploads/{$user_id}/tmp/");
            mkdir("assets/uploads/{$user_id}/tmp/thumbnail/");
        }
        // create the tmp directory if non existent
        if(!is_dir("assets/uploads/{$user_id}/tmp/")) {
            mkdir("assets/uploads/{$user_id}/tmp/");
        }
        // create the thumbnail directory
        if(!is_dir("assets/uploads/{$user_id}/tmp/thumbnail/")) {
            mkdir("assets/uploads/{$user_id}/tmp/thumbnail/");
        }

        // confirm if the variable is not empty and an array
        if(!empty($attachment_list) && is_array($attachment_list)) {

            $files_list = "<div class=\"rs-gallery-4 rs-gallery\">";
            $files_list .=  "<div class=\"row\">";

            // loop through the array text
            foreach($attachment_list as $eachFile) {
                
                // if the file is deleted then show a note
                if($eachFile->is_deleted) { 

                    $files_list .= "<div class=\"{$list_class} attachment-container text-center p-3\">
                            <div class=\"col-lg-12 p-2 font-italic border\">
                                This file is deleted
                            </div>
                        </div>";

                } else {
                    // if the file exists
                    if(is_file($eachFile->path) && file_exists($eachFile->path)) {

                        // is image check
                        $isImage = in_array($eachFile->type, $image_mime);
                        $isVideo = in_array($eachFile->type, $video_mime);

                        // set the file to download
                        $record_id = isset($eachFile->record_id) ? $eachFile->record_id : null;

                        // set the file id and append 4 underscores between the names
                        $file_to_download = base64_encode($eachFile->path."{$this->underscores}{$record_id}");

                        // preview link
                        $preview_link = "data-function=\"load-form\" data-resource=\"file_attachments\" data-module-id=\"{$user_id}_{$eachFile->unique_id}\" data-module=\"preview_file_attachment\"";
                        
                        // set init
                        $thumbnail = "
                            <div><span class=\"text text-{$eachFile->color}\"><i class=\"{$eachFile->favicon} fa-6x\"></i></span></div>
                            <div title=\"Click to preview: {$eachFile->name}\" data-toggle=\"tooltip\">
                                <a href=\"javascript:void(0)\" {$preview_link}><strong class=\"text-primary\">{$eachFile->name}</strong></a> <span class=\"text-muted tx-11\">({$eachFile->size})</span>
                            </div>";

                        $view_option = "";
                        $image_desc = "";
                        $delete_btn = "";

                        // if document list the show the view button
                        if($show_view) {
                            // if the type is in the array list
                            if(in_array($eachFile->type, $docs_mime)) {
                                $view_option .= "
                                <a title=\"Click to Click\" {$preview_link} class=\"btn btn-sm btn-primary\" style=\"padding:5px\" href=\"javascript:void(0)\">
                                    <i style=\"font-size:12px\" class=\"fa fa-eye fa-1x\"></i>
                                </a>";
                            }
                        }

                        // display this if the object is deletable.
                        if($is_deletable) {
                            $delete_btn = "&nbsp;<a href=\"javascript:void(0)\" onclick=\"return delete_existing_file_attachment('{$record_id}_{$eachFile->unique_id}');\" style=\"padding:5px\" class='btn btn-sm btn-outline-danger'><i class='fa fa-trash'></i></a>";

                        }

                        // init a padding
                        $padding = "style='padding:10px'";
                        
                        // if the file is an type
                        if($isImage) {

                            // get the file name
                            $filename = "{$tmp_path}{$eachFile->unique_id}.{$eachFile->type}";
                            
                            // if the file does not already exist
                            if(!is_file($filename) && !file_exists($filename)) {
                                
                                // create a new thumbnail of the file
                                create_thumbnail($eachFile->path, $filename);
                            } else {
                                $thumbnail = "<img height=\"100%\" width=\"100%\" src=\"{$this->baseUrl}{$filename}\">";
                            }
                            $image_desc = "
                                <div class=\"gallery-desc\">
                                    <a class=\"image-popup\" href=\"{$this->baseUrl}{$eachFile->path}\" title=\"{$eachFile->name} ({$eachFile->size}) on {$eachFile->datetime}\">
                                        <i class=\"fa fa-search\"></i>
                                    </a>
                                </div>";
                        } else if($isVideo) {
                            // get the file name
                            $filename = "{$eachFile->path}";
                            $padding = "style='padding:0px'";
                            // set the video file
                            $thumbnail = "<video  style='display: block; cursor:pointer; width:100%' controls='true' src='{$this->baseUrl}{$filename}'></video>";
                        }

                        // append to the list
                        $files_list .= "<div data-file_container='{$record_id}_{$eachFile->unique_id}' class=\"{$list_class} attachment-container text-center p-3\">";
                        $files_list .= $isImage ? "<div class=\"gallery-item\">" : null;
                            $files_list .= "
                                <div class=\"col-lg-12 attachment-item border\" {$padding} data-attachment_item='{$record_id}_{$eachFile->unique_id}'>
                                    <span style=\"display:none\" class=\"file-options\" data-attachment_options='{$record_id}_{$eachFile->unique_id}'>
                                        {$view_option}
                                        <a title=\"Click to Download\" target=\"_blank\" class=\"btn btn-sm btn-success\" style=\"padding:5px\" href=\"{$this->baseUrl}download?file={$file_to_download}\">
                                            <i style=\"font-size:12px\" class=\"fa fa-download fa-1x\"></i>
                                        </a>
                                        {$delete_btn}    
                                    </span> {$thumbnail} {$image_desc}
                                </div>
                            </div>";
                        $files_list .= $isImage ? "</div>" : null;
                    }
                }

            }

            $files_list .=  "</div>";
            $files_list .=  "</div>";

            return $files_list;

        }

    }

    /**
     * A global space for uploading temporary files
     * 
     * @param \stdClass $params
     * 
     * @return String
     */
    public function form_attachment_placeholder(stdClass $params = null) {
        
        // initialize
        $preloaded_attachments = "";
        
        // set a new parameter for the items
        $files_param = (object) [
            "userData" => $params->userData ?? null,
            "label" => "list",
            "module" => $params->module ?? null,
            "item_id" => $params->item_id ?? "temp_attachment",
        ];

        // preload some file attachments
        $attachments = load_class("files", "controllers")->attachments($files_param);
       
        // get the attachments list
        $fresh_attachments = !empty($attachments) && isset($attachments["data"]) ? $attachments["data"]["files"] : null;

        // if attachment list was set
        if(isset($params->attachments_list) && !empty($params->attachments_list)) {

            // set a new parameter for the items
            $files_param = (object) [
                "userData" => $params->userData ?? null,
                "label" => "list",
                "is_deletable" => isset($params->is_deletable) ? $params->is_deletable : false,
                "module" => $params->module ?? null,
                "item_id" => $params->item_id ?? null,
                "attachments_list" => $params->attachments_list
            ];

            // create a new object
            $attachments = load_class("files", "controllers")->attachments($files_param);

            // get the attachments list
            $preloaded_attachments = !empty($attachments) && isset($attachments["data"]) ? $attachments["data"]["files"] : null;
        }
        
        // set the file content
        $html_content = "
            <div class='post-attachment'>
                <div class=\"col-lg-12\" id=\"".($params->module ?? null)."\">
                    <div class=\"file_attachment_url\" data-url=\"{$this->baseUrl}api/files/attachments\"></div>
                </div>
                <div class=\"".(isset($params->class) ? $params->class : "col-md-12")." text-left\">
                    <div class='form-group row justify-content-start'>";
                    if(!isset($params->no_title)) {
                        $html_content .= "<label>Attach a Document <small class='text-danger'>(Maximum size <strong>{$this->max_attachment_size}MB</strong>)</small></label><br>";
                    }
                $html_content .= "
                        <div class=\"ml-3\">
                            <input class='form-control cursor attachment_file_upload' data-form_item_id=\"".($params->item_id ?? "temp_attachment")."\" data-form_module=\"".($params->module ?? null)."\" type=\"file\" name=\"attachment_file_upload\" id=\"attachment_file_upload\">
                        </div>
                        <div class=\"upload-document-loader hidden\"><span class=\"float-right\">Uploading <i class=\"fa fa-spin fa-spinner\"></i></span></div>
                    </div>
                </div>
            </div>
            
            <div class=\"col-md-12\">
                <div class=\"file-preview slim-scroll\" preview-id=\"".($params->module ?? null)."\">{$fresh_attachments}</div>
                <div class='form-group text-center mb-1'>{$preloaded_attachments}</div>
            </div>";
            $html_content .= !isset($params->no_footer) ? "<div class=\"col-lg-12 mb-3 border-bottom mt-3\"></div>" : null;

        return $html_content;
        
    }

    /**
     * A global space for uploading temporary files
     * 
     * @param \stdClass $params
     * 
     * @return String
     */
    public function comments_form_attachment_placeholder(stdClass $params = null) {
        
        // existing
        $preloaded_attachments = "";
        
        // set a new parameter for the items
        $files_param = (object) [
            "userData" => $params->userData ?? null,
            "label" => "list",
            "module" => $params->module ?? null,
            "item_id" => $params->item_id ?? "temp_attachment",
        ];

        // preload some file attachments
        $attachments = load_class("files", "controllers")->attachments($files_param);
       
        // get the attachments list
        $fresh_attachments = !empty($attachments) && isset($attachments["data"]) ? $attachments["data"]["files"] : null;

        // if attachment list was set
        if(isset($params->attachments_list) && !empty($params->attachments_list)) {
 
            // set a new parameter for the items
            $files_param = (object) [
                "userData" => $params->userData ?? null,
                "label" => "list",
                "is_deletable" => isset($params->is_deletable) ? $params->is_deletable : false,
                "module" => $params->module ?? null,
                "item_id" => $params->item_id ?? null,
                "attachments_list" => $params->attachments_list
            ];

            // create a new object
            $attachments = load_class("files", "controllers")->attachments($files_param);

            // get the attachments list
            $preloaded_attachments = !empty($attachments) && isset($attachments["data"]) ? $attachments["data"]["files"] : null;

        }

        // set the file content
        $html_content = "
            <div class='post-attachment'>
                <div class=\"col-lg-12\" id=\"".($params->module ?? null)."\">
                    <div class=\"file_attachment_url\" data-url=\"{$this->baseUrl}api/files/attachments\"></div>
                </div>
                ".upload_overlay()."
                <div class=\"".(isset($params->class) ? $params->class : "col-md-12")." text-left\">
                    <div class='form-group row justify-content-start'>";
                    if(!isset($params->no_title)) {
                        $html_content .= "<label>Attach a Document <small class='text-danger'>(Maximum size <strong>{$this->max_attachment_size}MB</strong>)</small></label><br>";
                    }
                $html_content .= "
                        <div class=\"ml-3\">
                            <input multiple accept=\"".($params->accept ?? "")."\" class='form-control cursor comment_attachment_file_upload' data-form_item_id=\"".($params->item_id ?? "temp_attachment")."\" data-form_module=\"".($params->module ?? null)."\" type=\"file\" name=\"comment_attachment_file_upload\" id=\"comment_attachment_file_upload\">
                        </div>
                        <div class=\"upload-document-loader hidden\"><span class=\"float-right\">Uploading <i class=\"fa fa-spin fa-spinner\"></i></span></div>
                    </div>
                </div>
            </div>
            
            <div class=\"col-md-12 ".(isset($params->no_padding) ? "p-0": "")."\">
                <div class=\"file-preview slim-scroll\" preview-id=\"".($params->module ?? null)."\">{$fresh_attachments}</div>
            </div>";
            // list the attached documents
            $html_content .= "<div class='form-group text-center mb-1'>{$preloaded_attachments}</div>";
            $html_content .= !isset($params->no_footer) ? "<div class=\"col-lg-12 mb-3 border-bottom mt-3\"></div>" : null;

        return $html_content;
        
    }

    /**
     * User Nofitications list
     * 
     * @param StdClass
     * 
     * @return String
     */
    public function user_notifications(stdClass $params) {
        
        // get the param
        $n_param = (object) [
            "user_id" => $params->this_user_id
        ];
        // get the list of user notifications
        $notices = load_class("notification", "controllers")->list($n_param);
        $notices = !empty($notices) ? $notices["data"] : [];

        // once the user has loaded the notifications, change the see status
        if($params->this_user_id == $params->userId) {
            $stmt = $this->db->prepare("UPDATE users_notification SET seen_status = ?, seen_date = now() WHERE user_id = ? AND seen_status = ?");
            $stmt->execute(["Seen", $params->userId, "Unseen"]);
        }

        // notification list
        $notices_list = "<div class=\"row\">";

        // loop through the list of notices
        foreach($notices as $each) {
            $seen = 
            $notices_list .= "
            <div class=\"col-lg-12 col-md-12 mb-3 nofitication-item cursor\" data-item-id=\"{$each->item_id}\">
                <div class=\"card\">
                    <div class=\"card-content\" data-isviewed=\"false\" data-isactive=\"false\" data-iseditable=\"false\" data-item-id=\"{$each->item_id}\">
                        <div class=\"card-header pr-1 pl-1 pb-1 d-flex justify-content-between\">
                            <div><h6 class=\"card-title font-weight-bolder p-0 m-0\">{$each->subject}</h6></div>
                            <div><p class=\"text-muted\">{$each->date_created}</p></div>
                        </div>
                        <div class=\"card-body pr-2 pl-2 pt-1 pb-1\">
                            <div class=\"message\">{$each->message}</div>
                        </div>
                    </div>
                    <div class=\"card-footer p-1 d-flex justify-content-between\" style=\"height:35px\">
                        <div>{$each->seen_status}</div>
                        <div class=\"underline\" onclick=\"return user_basic_information('{$each->created_by}')\"><strong>By: </strong>{$each->created_by_info->name}</div>
                    </div>
                </div>
            </div>";
        }

        $notices_list .= "<div class=\"col-md-12 mt-4 border-top pt-4 mb-3 text-center\"><span class=\"btn btn-outline-secondary\" data-dismiss='modal'>Close Modal</span></div>";
        $notices_list .= "</div>";

        // return the information
        return $notices_list;
    }

    /**
     * Prepare the company manager form data
     * 
     * @param \stdClass $manager_data
     * 
     * @return String
     */
    public function company_manager_form(stdClass $manager_data = null) {

        // description
        $message = isset($manager_data->description) ? $manager_data->description : null;

        // construct the manager form 
        $html_content = "<form action='{$this->baseUrl}api/company/".(!empty($manager_data->manager_id) ? "update_manager" : "add_manager")."' method='POST' id='ajax-data-form-content' class='ajax-data-form'>";
        $html_content .= form_loader();
        $html_content .= "
        <div class='row'>
            <div class=\"col-md-6\">
                <div class='form-group'>
                    <label>Manager Picture</label>
                    <input type='file' accept='image/*' name='profile_picture' class='form-control'>
                </div>
            </div>
            <div class=\"col-md-6 text-right\">";
        // if the profile picture is set
        if(isset($manager_data->picture)) {
            $html_content .= "<img src='{$this->baseUrl}{$manager_data->picture}' width='100px' class='img-xs cursor rounded-circle'>";
        }
        $html_content .= "</div>
            <div class='col-lg-12'>
                <div class='form-group'>
                    <label>Fullname</label>
                    <input type='text' value='".($manager_data->fullname ?? null)."' required name='manager_info[fullname]' class='form-control'>
                </div>
            </div>
            <div class='col-lg-12'>
                <div class='form-group'>
                    <label>Email Address</label>
                    <input type='text' value='".($manager_data->email ?? null)."' data-inputmask=\"'alias': 'email'\" inputmode=\"email\" required name='manager_info[email]' class='form-control'>
                </div>
            </div>
            <div class='col-lg-6'>
                <div class='form-group'>
                    <label>Primary Contact</label>
                    <input type='text' value='".($manager_data->primary_contact ?? null)."' data-inputmask-alias=\"(+999) 999-999-999\" name='manager_info[primary_contact]' class='form-control'>
                </div>
            </div>
            <div class='col-lg-6'>
                <div class='form-group'>
                    <label>Primary Contact</label>
                    <input type='text' value='".($manager_data->secondary_contact ?? null)."' data-inputmask-alias=\"(+999) 999-999-999\" name='manager_info[secondary_contact]' class='form-control'>
                </div>
            </div>
            <div class='col-lg-9'>
                <div class='form-group'>
                    <label>Position</label>
                    <input type='text' value='".($manager_data->position ?? null)."' required name='manager_info[position]' class='form-control'>
                </div>
            </div>
            <div class='col-lg-3'>
                <div class='form-group'>
                    <label>Since Date</label>
                    <input type='text' value='".($manager_data->position_since ?? null)."' name='manager_info[position_since]' class='form-control datepicker'>
                </div>
            </div>
            <div class='col-md-12'>
                <div class='form-group'>
                    <label>Bio Information</label>
                    {$this->textarea_editor($this->userPrefs->text_editor, $message)}
                </div>
            </div>
            <div class=\"col-lg-12 border-top mb-4 mt-2\"></div>";
            // if the id is set
            if(isset($manager_data->manager_id)) {
                $html_content .= "<input type='hidden' hidden value='{$manager_data->manager_id}' name='manager_info[manager_id]' id='manager_id'>";
            }
            $html_content .= "
            <div class=\"col-md-6 text-left\">
                <button class=\"btn btn-outline-success btn-sm\" data-function=\"save\" type=\"button-submit\">Save Record</button>
            </div>
            <div class=\"col-md-6 text-right\">
                <button type=\"reset\" class=\"btn btn-outline-danger btn-sm\" class=\"close\" data-dismiss=\"modal\">Cancel</button>
            </div>
            <div class=\"col-lg-12 mb-2 mt-2\"></div>
        </div>
        </form>
        ";

        return $html_content;
    }

    /**
     * Request for Report Form
     * 
     * @param StdClass $params
     * @param stdClass $data
     * 
     * @return String
     */
    public function request_report(stdClass $params, $data = null) {

        $message = isset($data->description) ? $data->description : null;
        $disabled = $message ? "disabled" : null;


        // construct the manager form 
        $html_content = "<form action='{$this->baseUrl}api/reports/".(!empty($data->item_id) ? "update_request" : "add_request")."' method='POST' id='ajax-data-form-content' class='ajax-data-form'>";
        $html_content .= form_loader();
        $html_content .= "
            <div class='row'>
                <div class=\"col-lg-12\">
                    <div class=\"form-group\">
                        <div class=\"input-group\">
                            <div class=\"input-group-prepend\">
                            <div class=\"input-group-text\">Subject &nbsp;<span class='required'>*</span></div>
                            </div>
                            <input type=\"text\" value=\"".($data->subject ?? null)."\" name=\"subject\" id=\"subject\" class=\"form-control\">
                        </div>
                    </div>
                </div>
                <div class=\"col-md-6\">
                    <div class=\"form-group\">
                        <label>Expected Date of Report</label>
                        <input type=\"text\" value=\"".($data->expected_date ?? null)."\" name=\"expected_date\" id=\"expected_date\" class=\"form-control datepicker\">
                    </div>
                </div>
                <div class=\"col-md-6\"></div>
                <div class=\"col-lg-6\">
                    <div class=\"form-group\">
                        <label for=\"request_type\">Related to</label>
                        <select name=\"request_type\" id=\"request_type\" data-width=\"100%\" class=\"selectpicker\">
                            <option value=\"null\">Related to</option>";
                            // get the related to item
                            $request_type = isset($data->request_type) ? $data->request_type : null;
                            // loop through the list
                            foreach($this->pushQuery("id, name", "policy_form", "type='report_request'") as $each) {
                                $value = create_slug($each->name, "_");
                                $html_content .= "<option ".(($value === $request_type) ? "selected" : null)." value=\"{$value}\">{$each->name}</option>";
                            }
                            $html_content .= "
                        </select>
                    </div>
                </div>
                <div class=\"col-lg-6\">
                    <div class=\"form-group\">
                        <label for=\"requested_from\">Insurance Company</label>
                        <select name=\"requested_from\" id=\"requested_from\" data-width=\"100%\" class=\"selectpicker\">
                            <option value=\"null\">Please select item</option>";
                            // loop through the related information
                            foreach($this->pushQuery("item_id, name", "companies", "company_type='insurance' AND activated='1' AND deleted='0'") as $each) {
                                $html_content .= "<option ".(!empty($data) && ($each->item_id === $data->requested_from) ? "selected" : null)." value=\"{$each->item_id}\">{$each->name}</option>";
                            }
                    $html_content .= "
                        </select>
                    </div>
                </div>
                <div class=\"col-md-6\">
                    <div class=\"form-group\">
                        <label>Start Date</label>
                        <input type=\"text\" value=\"".($data->start_date ?? null)."\" name=\"start_date\" id=\"start_date\" class=\"form-control datepicker\">
                    </div>
                </div>
                <div class=\"col-md-6\">
                    <div class=\"form-group\">
                        <label>End Date</label>
                        <input type=\"text\" value=\"".($data->end_date ?? null)."\" name=\"end_date\" id=\"end_date\" class=\"form-control datepicker\">
                    </div>
                </div>
                <div class=\"col-md-12 mb-4\">
                    <div class=\"form-group\">
                        <label>Additional Information</label>
                        {$this->textarea_editor($this->userPrefs->text_editor, $message)}
                    </div>
                </div>
                <div class=\"col-md-6 text-left\">
                    <input type=\"hidden\" name=\"request_id\" id=\"request_id\" value=\"".($data->item_id ?? null)."\" hidden class=\"form-control\">
                    <button class=\"btn btn-outline-success btn-sm\" data-function=\"save\" type=\"button-submit\">Submit Request</button>
                </div>
                <div class=\"col-md-6 text-right\">
                    <button type=\"reset\" class=\"btn btn-outline-danger btn-sm\" class=\"close\" data-dismiss=\"modal\">Close</button>
                </div>
            </div>
        </form>";

        return $html_content;
    }
    /**
     * Prepare the company manager form data
     * 
     * @param \stdClass $award_data
     * 
     * @return String
     */
    public function company_award_form(stdClass $award_data = null) {

        // description
        $message = isset($award_data->description) ? $award_data->description : null;

        // construct the manager form 
        $html_content = "<form action='{$this->baseUrl}api/company/".(!empty($award_data->award_id) ? "update_award" : "add_award")."' method='POST' id='ajax-data-form-content' class='ajax-data-form'>";
        $html_content .= form_loader();
        $html_content .= "
            <div class='row'>
            <div class=\"col-md-6\">
                <div class='form-group'>
                    <label>Award Image</label>
                    <input type='file' accept='image/*' name='award_logo' class='form-control'>
                </div>
            </div>
            <div class=\"col-md-6 text-right\">";
        // if the profile picture is set
        if(isset($award_data->picture)) {
            $html_content .= "<img src='{$this->baseUrl}{$award_data->picture}' width='100px' class='img-xs cursor rounded-circle'>";
        }
        $html_content .= "</div>
            <div class='col-lg-12'>
                <div class='form-group'>
                    <label>Award Title</label>
                    <input type='text' value='".($award_data->title ?? null)."' required name='award_info[title]' class='form-control'>
                </div>
            </div>
            <div class='col-lg-12'>
                <div class='form-group'>
                    <label>Awarding Institution</label>
                    <input type='text' value='".($award_data->institution ?? null)."' required name='award_info[institution]' class='form-control'>
                </div>
            </div>
            <div class='col-lg-8'>
                <div class='form-group'>
                    <label>Award Category</label>
                    <input type='text' value='".($award_data->category ?? null)."' required name='award_info[category]' class='form-control'>
                </div>
            </div>
            <div class='col-lg-4'>
                <div class='form-group'>
                    <label>Date Awarded</label>
                    <input type='text' value='".($award_data->award_date ?? null)."' name='award_info[award_date]' class='form-control datepicker'>
                </div>
            </div>
            <div class='col-lg-12'>
                <div class='form-group'>
                    <label>Received By / Position</label>
                    <input type='text' value='".($award_data->received_by ?? null)."' required name='award_info[received_by]' class='form-control'>
                </div>
            </div>
            <div class='col-lg-12'>
                <div class='form-group'>
                    <label>Assisted By / Position</label>
                    <input type='text' value='".($award_data->assisted_by ?? null)."' required name='award_info[assisted_by]' class='form-control'>
                </div>
            </div>
            <div class='col-md-12'>
                <div class='form-group'>
                    <label>Additional Information</label>
                    {$this->textarea_editor($this->userPrefs->text_editor, $message)}
                </div>
            </div>
            <div class=\"col-lg-12 border-top mb-4 mt-2\"></div>";
            // if the id is set
            if(isset($award_data->award_id)) {
                $html_content .= "<input type='hidden' hidden value='{$award_data->award_id}' name='award_info[award_id]' id='award_id'>";
            }
            $html_content .= "
            <div class=\"col-md-6 text-left\">
                <button class=\"btn btn-outline-success btn-sm\" data-function=\"save\" type=\"button-submit\">Save Record</button>
            </div>
            <div class=\"col-md-6 text-right\">
                <button type=\"reset\" class=\"btn btn-outline-danger btn-sm\" class=\"close\" data-dismiss=\"modal\">Cancel</button>
            </div>
            <div class=\"col-lg-12 mb-2 mt-2\"></div>
        </div>
        </form>";

        return $html_content;
    }

    /**
     * Advert Form
     * 
     * Prepare and load the form to apply for a new ad
     * 
     */
    public function manage_adverts_form($advert_data = null, $params = null) {

        $html_content = "";

        // description
        $disabled = (isset($advert_data->status) && $advert_data->status !== "Pending") ? "disabled='disabled'" : null;

        if(isset($advert_data->advert_id)) {
            
            // set the params
            $para = (object) [
                "module" => "adverts",
                "user_id" => $params->userId,
                "user_type" => $params->userData->user_type,
                "userData" => $params->userData,
                "related_item" => $advert_data->related_to
            ];

            // get the info
            $related_info = load_class("related", "controllers")->list($para);
        }

        // construct the manager form 
        $html_content = "<form action='{$this->baseUrl}api/adverts/".(!empty($advert_data->advert_id) ? "update" : "add")."' method='POST' id='ajax-data-form-content' class='ajax-data-form'>";
        $html_content .= form_loader();
        $html_content .= "
        <div class='row'>
            <div class=\"col-md-5\">
                <div class='form-group'>
                    <label for='ad_image'>Campaign Image</label>
                    <input {$disabled} type='file' accept='image/*' name='ad_image' class='form-control'>
                </div>
            </div>
            <div class=\"col-md-7 text-right\">";
        // if the profile picture is set
        if(isset($advert_data->image)) {
            $html_content .= "<img src='{$this->baseUrl}{$advert_data->image}' width='300px' class='cursor'>";
        }
        $html_content .= "</div>
            <div class='col-lg-12'>
                <div class='form-group'>
                    <label for='advert_title'>Campaign Name</label>
                    <input {$disabled} type='text' class='form-control' value='".($advert_data->advert_title ?? null)."' name='advert_title' id='advert_title'>
                </div>
            </div>
            <div class=\"col-lg-6\">
                <div class=\"form-group\">
                    <label for=\"related_to\">Related to</label>
                    <select {$disabled} name=\"related_to\" data-module=\"adverts\" id=\"related_to\" data-width=\"100%\" class=\"selectpicker\">
                        <option value=\"null\">Related to</option>";
                        // get the related to item
                        $related_to = isset($advert_data->related_to) ? $advert_data->related_to : null;
                        // loop through the list
                        $value = create_slug("Company Insurance Policies");
                        $html_content .= "<option ".(($value === $related_to) ? "selected" : null)." value=\"{$value}\">Company Insurance Policies</option>";
            $html_content .= "</select>
                </div>
            </div>
            <div class=\"col-lg-6\">
                <div class=\"form-group\">
                    <label for=\"related_to_id\">Suggested Items <small>(Optional)</small></label>
                    <select {$disabled} name=\"related_to_id\" id=\"related_to_id\" data-width=\"100%\" class=\"selectpicker\">
                        <option value=\"null\">Please select item</option>";
                    // if the record has related data
                    if(isset($related_info["data"]) && !empty($related_info["data"])) {
                        // loop through the related information
                        foreach($related_info["data"] as $each) {
                            $html_content .= "<option ".(($each->item_id === $advert_data->related_to_id) ? "selected" : null)." value=\"{$each->item_id}\">{$each->item_name}</option>";
                        }
                    }
                $html_content .= "
                    </select>
                </div>
            </div>
            <div class=\"col-lg-12\">
                <div class=\"form-group\">
                    <label for=\"ad_context\">Campaign Context</label>
                    <select {$disabled}  name=\"ad_context\" id=\"ad_context\" data-width=\"100%\" class=\"selectpicker\">
                        <option value=\"null\">Please select item</option>";
                        // convert the targets into an array
                        $ad_context = isset($advert_data->ad_context) ? $this->stringToArray($advert_data->ad_context) : [];
                        // loop through the related information
                        foreach($this->pushQuery("id, target", "adverts_context") as $each) {
                            $html_content .= "<option ".(in_array($each->id, $ad_context) ? "selected" : null)." value=\"{$each->id}\">{$each->target}</option>";
                        }
                        $html_content .= "
                    </select>
                </div>
            </div>
            <div class='col-md-6'>
                <div class='form-group'>
                <label class='ad_objective'>Objective</label>
                <select {$disabled} data-width='100%' name='ad_objective' class='form-control selectpicker'>
                    <option value='null'>Select Objective</option>";
                    // add objective
                    $objective = isset($advert_data->ad_objective) ? $advert_data->ad_objective : null;
                    // loop through the objective list
                    foreach($this->ad_objective_list as $key => $value) {
                        $html_content .= "<option ".(($objective == $key) ? "selected" : null)." value='{$key}'>{$value}</option>";
                    }
                $html_content .= "</select>
                </div>
            </div>
            <div class='col-md-6'>
                <div class='form-group'>
                <label class='budget'>Campaign Budget</label>
                <input type='text' {$disabled} class='form-control' maxlength='12' value='".($advert_data->budget ?? null)."' name='budget' id='budget'>
                </div>
            </div>
            <div class=\"col-lg-12\">
                <div class=\"form-group\">
                    <label for=\"ad_target\">Campaign Target</label>
                    <select {$disabled} multiple name=\"ad_target\" id=\"ad_target\" data-width=\"100%\" class=\"selectpicker\">
                        <option value=\"null\">Please select item</option>";
                        // convert the targets into an array
                        $ad_target = isset($advert_data->ad_target) ? $this->stringToArray($advert_data->ad_target) : [];
                        // loop through the related information
                        foreach($this->pushQuery("id, target", "adverts_target") as $each) {
                            $html_content .= "<option ".(in_array($each->id, $ad_target) ? "selected" : null)." value=\"{$each->id}\">{$each->target}</option>";
                        }
            $html_content .= "</select>
                </div>
            </div>
            <div class='col-md-6'>
                <div class='form-group'>
                <label class='start_date'>Start Date</label>
                <input type='text' class='form-control datepicker' value='".($advert_data->start_date ?? null)."' name='start_date' id='start_date'>
                </div>
            </div>
            <div class='col-md-6'>
                <div class='form-group'>
                <label class='end_date'>End Date</label>
                <input type='text' class='form-control datepicker' value='".($advert_data->end_date ?? null)."' name='end_date' id='end_date'>
                </div>
            </div>";

            // if the id is set
            if(isset($advert_data->advert_id)) {
                $html_content .= "<input type='hidden' hidden value='{$advert_data->advert_id}' name='advert_id' id='advert_id'>";
            }
            $html_content .= "
            <div class=\"col-md-6 text-left\">
                <button type=\"reset\" class=\"btn btn-outline-danger btn-sm\" class=\"close\" data-dismiss=\"modal\">Cancel</button>
            </div>
            <div class=\"col-md-6 text-right\">";
            if(!isset($advert_data->advert_id) || (isset($advert_data->submit_status) && ($advert_data->submit_status == "draft"))) {
                $html_content .= "<button class=\"btn btn-outline-primary btn-sm\" data-function=\"draft\" type=\"button-submit\">Save as Draft</button>";
            }
            $html_content .= "<button class=\"btn btn-outline-success btn-sm\" data-function=\"save\" type=\"button-submit\">Post Ad</button>
            </div>
            <div class=\"col-lg-12 mb-2 mt-2\"></div>
        </div>
        </form>";

        return $html_content;

    }

    /**
     * Prepare the company manager form data
     * 
     * @param \stdClass $company_data
     * 
     * @return String
     */
    public function company_form(stdClass $company_data = null) {

        // description
        $message = isset($company_data->description) ? $company_data->description : null;

        // construct the manager form 
        $html_content = "<form action='{$this->baseUrl}api/company/".(!empty($company_data->item_id) ? "update" : "add")."' method='POST' id='ajax-data-form-content' class='ajax-data-form'>";
        $html_content .= form_loader();
        $html_content .= "
        <div class='row'>
            <div class=\"col-md-6\">
                <div class='form-group'>
                    <label>Company Logo</label>
                    <input type='file' accept='image/*' name='logo' class='form-control'>
                </div>
            </div>
            <div class=\"col-md-6 text-right\">";
        // if the profile picture is set
        if(isset($company_data->logo)) {
            $html_content .= "<img src='{$this->baseUrl}{$company_data->logo}' width='130px' class=''>";
        }
        $html_content .= "</div>
            <div class='col-lg-12'>
                <div class='form-group'>
                    <label>Company Name</label>
                    <input type='text' value='".($company_data->name ?? null)."' required name='name' class='form-control'>
                </div>
            </div>
            <div class='col-md-6'>
                <div class='form-group'>
                    <label>Email Address</label>
                    <input type='text' value='".($company_data->email ?? null)."' data-inputmask=\"'alias': 'email'\" inputmode=\"email\" required name='email' class='form-control'>
                </div>
            </div>
            <div class='col-md-6'>
                <div class='form-group'>
                    <label>Website</label>
                    <input type='text' value='".($company_data->website ?? null)."' inputmode=\"website\" required name='website' class='form-control'>
                </div>
            </div>
            <div class='col-md-6'>
                <div class='form-group'>
                    <label>Primary Contact</label>
                    <input type='text' value='".($company_data->contact ?? null)."' data-inputmask-alias=\"(+999) 999-999-999\" name='primary_contact' class='form-control'>
                </div>
            </div>
            <div class='col-md-6'>
                <div class='form-group'>
                    <label>Primary Contact</label>
                    <input type='text' value='".($company_data->contact_2 ?? null)."' data-inputmask-alias=\"(+999) 999-999-999\" name='secondary_contact' class='form-control'>
                </div>
            </div>
            <div class='col-md-12'>
                <div class='form-group'>
                    <label>Address</label>
                    <input type='text' value='".($company_data->address ?? null)."' name='address' class='form-control'>
                </div>
            </div>
            <div class='col-md-12'>
                <div class='form-group'>
                    <label>License ID</label>
                    <input type='text' value='".($company_data->license_id ?? null)."' name='license_id' class='text-uppercase form-control'>
                </div>
            </div>
            <div class='col-md-6'>
                <div class='form-group'>
                    <label>Date of Registration</label>
                    <input type='text' value='".($company_data->registration_date ?? null)."' name='registration_date' class='form-control datepicker'>
                </div>
            </div>
            <div class='col-md-6'>
                <div class='form-group'>
                    <label>Date of Incorporation</label>
                    <input type='text' value='".($company_data->establishment_date ?? null)."' name='establishment_date' class='form-control datepicker'>
                </div>
            </div>
            <div class='col-md-12'>
                <div class='form-group'>
                    <label>Description</label>
                    {$this->textarea_editor($this->userPrefs->text_editor, $message)}
                </div>
            </div>
            <div class=\"col-lg-12 border-top mb-4 mt-2\"></div>";
            // if the id is set
            if(isset($company_data->item_id)) {
                $html_content .= "<input type='hidden' hidden value='{$company_data->item_id}' name='company_id' id='company_id'>";
            }
            $html_content .= "
            <div class=\"col-md-6 text-left\">
                <button class=\"btn btn-outline-success btn-sm\" data-function=\"save\" type=\"button-submit\">Save Record</button>
            </div>
            <div class=\"col-md-6 text-right\">
                <button type=\"reset\" class=\"btn btn-outline-danger btn-sm\" class=\"close\" data-dismiss=\"modal\">Cancel</button>
            </div>
            <div class=\"col-lg-12 mb-2 mt-2\"></div>
        </div>
        </form>
        ";

        return $html_content;
    }

    /**
     * Prepare the announcement form and share
     * 
     * @param \stdClass $params
     * 
     * @return String 
     */
    public function share_announcement($params) {

        $start_date = date("Y-m-d");
        $end_date = date("Y-m-d", strtotime("next week"));

        $record = isset($params->announcementData) ? $params->announcementData : null;
        $recipient_group = isset($record->recipient_group) ? $record->recipient_group : null;
        $priority = isset($record->priority) ? $record->priority : null;
        $message = isset($record->message) ? $record->message : null;

        $form_params = (object) [
            "module" => "announcements",
            "userData" => $params->userData,
            "item_id" => "announcements",
            "no_footer" => true,
            "no_padding" => true
        ];

        // append the attachments list
        if(isset($record->attachment)) {
            // append the files and make it deletable
            $form_params->attachments_list = $record->attachment;
            $form_params->item_id = $record->announcement_id;
            $form_params->is_deletable = true;
        }

        $html_content = "<form action='{$this->baseUrl}api/announcements/".(isset($record->announcement_id) ? "update" : "apply")."' method='POST' id='ajax-data-form-content' class='ajax-data-form'>";
        $html_content .= form_loader();
        $html_content .= "
        <div class='row post-announcement'>
            <div class='col-lg-12'>
                <div class='form-group'>
                    <label>Subject <span class='required'>*</span></label>
                    <input type='text' class='form-control' value='".($record->subject ?? null)."' id='subject' name='subject'>
                </div>
            </div>
            <div class='col-md-6'>
                <div class='form-group'>
                    <label>Start Date of Announcement <span class='required'>*</span></label>
                    <input type='text' value='".($record->start_date ?? $start_date)."' class='form-control datepicker' name='start_date'>
                </div>
            </div>
            <div class='col-md-6'>
                <div class='form-group'>
                    <label>Announcement End Date</label>
                    <input type='text' class='form-control datepicker' value='".($record->end_date ?? $end_date)."' name='end_date'>
                </div>
            </div>
            <div class='col-md-7'>
                <div class='form-group'>
                    <label>Recipient Group <span class='required'>*</span></label>
                    <select data-width='100%' class='form-control selectpicker' name='recipient_group'>
                        <option value='null'>Please select recipient group</option>";
                // if the user type is nic or admin              
                if(in_array($params->userData->user_type, ["nic", "admin"])) {
                    $html_content .= "<option ".(($recipient_group == "insurance_company") ? "selected" : "")." value='insurance_company'>Insurance Companies</option>";
                    $html_content .= "<option ".(($recipient_group == "bank") ? "selected" : "")." value='bank'>Banks</option>";
                }
        // append more html content             
        $html_content .= "<option ".(($recipient_group == "broker") ? "selected" : "")." value='broker'>Insurance Brokers</option>
                        <option ".(($recipient_group == "agent") ? "selected" : "")." value='agent'>Agents</option>
                        <option ".(($recipient_group == "client") ? "selected" : "")." value='client'>Clients</option>
                    </select>
                </div>
            </div>
            <div class='col-md-5'>
                <div class='form-group'>
                    <label>Priority</label>
                    <select data-width='100%' class='form-control selectpicker' name='priority'>
                        <option value='null'>Please select priority</option>
                        <option ".(($priority == "low") ? "selected" : "")." value='low'>Low</option>
                        <option ".(($priority == "medium") ? "selected" : "")." value='medium'>Medium</option>
                        <option ".(($priority == "high") ? "selected" : "")." value='high'>High</option>
                    </select>
                </div>
            </div>
            <div class='col-md-12'>
                <div class='form-group'>
                    <label>Announcement Message <span class='required'>*</span></label>
                    {$this->textarea_editor($this->userPrefs->text_editor, $message)}
                </div>
            </div>
            <div class='col-lg-12 p-0 mb-3 border-top'>
                {$this->comments_form_attachment_placeholder($form_params)}
            </div>
            <div class=\"col-md-6 text-left\">
                <button class=\"btn btn-outline-success btn-sm\" data-function=\"save\" type=\"button-submit\">Post Announcement</button>
            </div>";
            // if the id is set
            if(isset($record->announcement_id)) {
                $html_content .= "<input type='hidden' hidden value='{$record->announcement_id}' name='announcement_id' id='announcement_id'>";
            }
            $html_content .= "<div class=\"col-md-6 text-right\">
                <button type=\"reset\" class=\"btn btn-outline-danger btn-sm\" class=\"close\" data-dismiss=\"modal\">Cancel</button>
            </div>
            <div class=\"col-lg-12 mb-2 mt-2\"></div>
            </div>
            </form>";

        return $html_content;
    }

    /**
     * License application
     * 
     * @param \stdClass $params
     * @param Object $data
     * 
     * @return String
     */
    public function license_application($params, $data = null) {

        // information
        $description = isset($data->description) ? $data->description : null;

        // license application upload
        $form_params = (object) [
            "module" => "licenses",
            "userData" => $params->userData,
            "item_id" => "licenses",
            "no_footer" => true,
            "no_padding" => true
        ];

        // append the attachments list
        if(isset($data->attachment)) {
            // append the files and make it deletable
            $form_params->attachments_list = $data->attachment;
            $form_params->item_id = $data->application_id;
            $form_params->is_deletable = true;
        }

        /** Prepare the license application form */
        $html_content = "<form action='{$this->baseUrl}api/licenses/".(isset($data->item_id) ? "update" : "apply")."' method='POST' id='ajax-data-form-content' class='ajax-data-form'>";
        $html_content .= form_loader();
        $html_content .= "
        <div class='row post-announcement'>
            <div class='col-lg-12'>
                <div class='form-group'>
                <label class='company_name'>Company Name</label>
                <input type='text' class='form-control' value='".($data->company_info->name ?? $this->thisUser->company_info->name)."' name='company_name' disabled id='company_name'>
                </div>
            </div>
            <div class='col-lg-6'>
                <div class='form-group'>
                <label class='company_contact'>Contact Number</label>
                <input type='text' class='form-control' value='".($data->company_info->contact ?? $this->thisUser->company_info->contact)."' name='company_contact' disabled id='company_contact'>
                </div>
            </div>
            <div class='col-lg-6'>
                <div class='form-group'>
                <label for='company_email'>Email Address</label>
                <input type='text' class='form-control' value='".($data->company_info->email ?? $this->thisUser->company_info->email)."' name='company_email' disabled id='company_email'>
                </div>
            </div>
            <div class='col-lg-12'>
                <div class='form-group'>
                <label>License ID <span class='required'>*</span></label>
                <input type='text' value='".($data->license_id ?? null)."' class='form-control text-uppercase' name='license_id' id='license_id'>
                </div>
            </div>
            <div class='col-lg-12'>
                <div class='form-group'>
                <label>Application Information <span class='required'>*</span></label>
                {$this->textarea_editor($this->userPrefs->text_editor, $description)}
                </div>
            </div>
            <div class='col-lg-12 p-0 mb-3'>
                {$this->comments_form_attachment_placeholder($form_params)}
            </div>";
            
            // if the id is set
            if(isset($data->item_id)) {
                $html_content .= "<input type='hidden' hidden value='{$data->item_id}' name='application_id' id='application_id'>";
            }
            $html_content .= "
            <div class=\"col-md-6 text-left\">
                <button type=\"reset\" class=\"btn btn-outline-danger btn-sm\" class=\"close\" data-dismiss=\"modal\">Cancel</button>
            </div>
            <div class=\"col-md-6 text-right\">
                <button class=\"btn btn-outline-success btn-sm\" data-function=\"draft\" type=\"button-submit\">Save Application</button>
            </div>
        </div>
        </form>";

        return $html_content;
    }

    /**
     * Text editor to show
     * 
     * @param String $preference
     * @param String $data
     * @param String $name          Default is faketext
     * @param String $id            Default is ajax-form-content
     * 
     * @return String
     */
    public function textarea_editor($preference, $data = null, $name = "faketext", $id = "ajax-form-content") {

        if($preference == "ckeditor") {
            $form_content = "<textarea class=\"form-control\" name=\"{$name}\" id=\"{$id}\" rows=\"7\">{$data}</textarea>";
        } else {
            $form_content = "<input type='hidden' hidden id='trix-editor-input' value='{$data}'>";
            $form_content .= "<trix-editor name=\"{$name}\" input='trix-editor-input' class=\"trix-slim-scroll\" id=\"{$id}\"></trix-editor>";
        }

        // return the results
        return $form_content;

    }

    /** 
     * Show the replies list
     * Load the feedback list for a resource populate the data stream unto the page
     */
    public function view_replies_list($params) {
        
        /** Item id */
        $repliesObj = load_class("replies", "controllers");

        $param = (object) [
            "resource_id" => $params->module["item_id"],
            "filesObject" => $this,
            "userId" => $params->userId,
            "feedback_type" => $params->feedback_type,
            "last_reply_id" => $params->last_reply_id,
            "limit" => 200
        ];
        $replies_list = $repliesObj->list($param);

        return $replies_list;   
    }

    /**
     * Ajax Form Loader
     * 
     * @@NOT IN USE
     */
    public function ajax_forms_loader(stdClass $params) {
        // auto close not parsed
        if(!isset($params->module["auto_close_modal"])) {
            return;
        }
        // set variable
        $auto_close_modal = $params->module["auto_close_modal"];

        // form values
        $form_content = "<div class=\"modal fade modal-dialog-right right\" id=\"addItemModal\"".( !$auto_close_modal ? null :  'data-backdrop="static" data-keyboard="false"')."\">
                <div class=\"modal-dialog modal-dialog-centered modal-lg\" style=\"width:100%\" role=\"document\">
                    <div class=\"modal-content\">
                        <div class=\"modal-header\">
                            <h5 class=\"modal-title\"></h5>
                            <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span>&times;</span></button>
                        </div>
                        <input hidden class=\"ajax-form-loaded\" value=\"0\" data-form=\"none\">
                        <div class=\"modal-body\"></div>
                    </div>
                </div>
            </div>
        ";

        $form_content .= "";

        return $form_content;
    }

    /**
     * Load the property activity logs
     * 
     * @param String $item_id
     * 
     * @return String
     */
    public function property_activity_logs($item_id, $limit = 200, $array_only = false) {

        /** Load the results */
        $activity_logs = $this->pushQuery("a.*, (SELECT b.name FROM users b WHERE b.item_id = a.user_id LIMIT 1) AS fullname", "users_activity_logs a", "a.item_id='{$item_id}' ORDER BY a.id DESC LIMIT {$limit}");
        
        // init
        if(!$array_only) {
            $property_activity = "<div class='row'><div class='col-lg-12'><div class='table-responsive slim-scroll'>";
            $property_activity .= "<table width='100%' class='nowrap dataTable table-bordered table-hover'>";
            $property_activity .= "<thead>";
            $property_activity .= "<tr>";
            $property_activity .= "<th width='65%' class='p-2'>ACTIVITY</th>";
            $property_activity .= "<th class='p-2'>MADE ON</th>";
            $property_activity .= "</tr>";
            $property_activity .= "</thead>";
            $property_activity .= "</body>";
        }

        // inner function
        function replace_text($text, $base_url) {
            return str_ireplace(["{{APPURL}}"], $base_url, $text);
        }

        // return the list if the limit was parsed
        if($array_only) {
            return $activity_logs;
        }

        // loop through the results
        foreach($activity_logs as $eachLog) {

            // append to the list to show
            $property_activity .= "<tr>";
            $property_activity .= "<td class='p-1'>
                {$eachLog->description} 
                <!--<a title=\"View log record details\" href=\"{$this->baseUrl}history/{$item_id}/{$eachLog->id}\" target=\"_blank\"><i class=\"fa fa-eye\"></i></a>-->
                <br>
                ".((create_slug($eachLog->subject) == "profile-picture") ? replace_text($eachLog->previous_record, $this->baseUrl) : null)."
            </td>";
            $property_activity .= "<td class='p-1'><i class='fa fa-calendar'></i> &nbsp; {$eachLog->date_recorded}
                <br><div class='text-primary'><i class='fa fa-desktop'></i> {$eachLog->user_agent}</div></td>";
            $property_activity .= "</tr>";
        }
        $property_activity .= "</tbody>";
        $property_activity .= "</table>";
        $property_activity .= "</div>
            <div class=\"text-center mt-3\"><a href=\"{$this->baseUrl}history/{$item_id}\">View detailed activity history</a></div>
        </div>
        <div class=\"col-md-12 mt-4 border-top pt-4 mb-3 text-center\"><span class=\"btn btn-outline-secondary\" data-dismiss='modal'>Close Modal</span></div>
        </div>";
        
        /** return the logs */
        return count($activity_logs) ? $property_activity : null;
    }

    /**
     * Share a reply
     * 
     * @param stdClass $details           This is a summary details of the item
     * @param String $item              This is the resource id
     * @param String $item_id           This is the unique id of the resource to add a reply
     * 
     * @return String
     */
    public function share_a_reply_form($user, $item, $item_id) {

        // the item details
        if(!is_object($user)) {
            return;
        }

        /** Set parameters for the data to attach */
        $form_params = (object) [
            "module" => "{$item}_replies_{$item_id}",
            "userData" => $user,
            "item_id" => $item_id ?? null
        ];
        
        /** Initiate the form content */
        $form_content = "<form action='{$this->baseUrl}api/replies/add' method='POST' id='ajax-data-form-content' class='ajax-data-form'>";
        $form_content .= form_loader();
        $form_content .= "<div class='row'>";
        $form_content .= "
        <div class=\"col-lg-12\">
            <div class=\"form-group\">
                <label>Additional information <span class='required'>*</span></label>";
            
            // addition description
            $form_content .= $this->textarea_editor($this->userPrefs->text_editor);
            
        $form_content .= "
            </div>
        </div>
        <div class='col-lg-12 mb-2 mt-2'>
            <div class='form-group text-center mb-1'>
                <div class='row'>
                    {$this->form_attachment_placeholder($form_params)}
                </div>
            </div>
        </div>";

        $form_content .= "
        <div class=\"col-md-6 text-left\">
            <input type=\"hidden\" name=\"resource\" id=\"resource\" value=\"{$item}\">
            <input type=\"hidden\" name=\"record_id\" id=\"record_id\" value=\"{$item_id}\" hidden class=\"form-control\">
            <button class=\"btn btn-outline-success btn-sm\" data-function=\"save\" type=\"button-submit\">Submit Reply</button>
        </div>
        <div class=\"col-md-6 text-right\">
            <button type=\"reset\" class=\"btn btn-outline-danger btn-sm\" class=\"close\" data-dismiss=\"modal\">Close</button>
        </div>";

        /** End the form content */
        $form_content .= "</div><div class=\"col-lg-12 mb-5 mt-5\"></div>";
        $form_content .= "</form>";
        

        /** Return the form data */
        return $form_content;
        
    }

    /**
     * Claims Form
     * 
     * @param \stdClass $params
     * 
     * @return String
     */
    public function claims_request_form(stdClass $params = null) {

        /** Return if not properly executed */
        if(!isset($params->claims_request_form)) {
            return ["code" => 201, "data" => $this->permission_denied];
        }

        /** Global */
        global $usersClass;

        /** User Data information */
        $userData = $params->userData;
        $user_type = $userData->user_type;
        $userId = $params->userData->user_id;

        /** Initiate the form content */
        $form_content = "
        <div class='row ajax-load-form-header' data-form_type=\"load_claims_form\">
        <style>
            trix-editor {
                border: 1px solid #bbb;
                border-radius: 3px;
                margin: 0;
                padding: 0.4em 0.6em;
                min-height: 150px;
                outline: none;
                max-height: 150px;
                overflow-y: auto;
            }
        </style>";

        // if agent, broker or bancassurance
        if(in_array($user_type, ["agent", "broker", "bancassurance"])) {
            /** Load the user options */
            $u_options = (object) [
                "the_user_type" => $user_type,
                "remote" => true,
                "userData" => $params->userData,
                "columns" => "a.name, a.item_id, a.phone_number, a.email",
                "userId" => $userId
            ];
            $users_list = $usersClass->list($u_options)["data"];

            $form_content .= "
            <div class=\"col-lg-12\">
                <div class=\"form-group\">
                    <label for=\"u_client_id\">Select Client <span class='required'>*</span></label>
                    <select name=\"u_client_id\" data-module=\"users\" data-append=\"claims-request\" id=\"u_client_id\" data-width=\"100%\" class=\"selectpicker\">
                        <option value=\"null\">Select Client</option>";
                        foreach($users_list as $each) {
                            $form_content .= "<option value=\"{$each->item_id}\">{$each->name} (Contact: {$each->phone_number} | Email: {$each->email})</option>";
                        }
            $form_content .= "</select>
                </div>
            </div>
            <div class=\"col-lg-12\">
                <div class=\"form-group\">
                    <label for=\"policy_id\">Select Client Policy <span class='required'>*</span></label>
                    <select name=\"policy_id\" data-module=\"users\" id=\"policy_id\" data-width=\"100%\" class=\"selectpicker\">
                        <option value=\"null\">Select Client Policy</option>
                    </select>
                </div>
            </div>";
        }
        // if the user is logged in 
        elseif(in_array($user_type, ["user", "business"])) {
            
            /** Load the user options */
            $param = (object) [
                "columns" => "a.item_id, a.policy_name, a.policy_id, a.policy_status, a.submit_status, a.policy_type",
                "user_id" => $userId
            ];
            $policyObj = load_class("user_policy", "controllers");
            $policy_list = $policyObj->list($param);

            $form_content .= "
                <input class=\"form-control\" hidden name=\"u_client_id\" id=\"u_client_id\" value=\"{$userId}\">
                <div class=\"col-lg-12\">
                    <div class=\"form-group\">
                        <label for=\"policy_id\">Select the Policy <span class='required'>*</span></label>
                        <select name=\"policy_id\" data-module=\"users\" id=\"policy_id\" data-width=\"100%\" class=\"selectpicker\">
                            <option value=\"null\">Select the Policy</option>";
                            foreach($policy_list["data"] as $each) {
                                $form_content .= "<option value=\"{$each->item_id}\">{$each->policy_name} ($each->policy_id): {$each->policy_status}</option>";
                            }
                $form_content .= "</select>
                    </div>
                </div>";
        }

        // show the apply 
        $form_content .= "
            <div class=\"col-lg-12 text-center mt-3 hidden\" id=\"proceed-to-ajax-form\">
                <span class=\"btn btn-outline-success proceed-to-form\">Click to complete claim form</span>
            </div>";

        /** End the form content */
        $form_content .= "</div>";

        /** Load the policy form content */
        $form_content .= "<div class=\"hidden\" id=\"ajax-load-form-content\"></div>";
        
        
        /** Return the form data */
        return $form_content;
    }

    /**
     * Lodge Complaints form
     * 
     * @param \stdClass $params
     * 
     * @return String
     */
    public function complaints_form(stdClass $params) {
        
        /** Return if not properly executed */
        if(!isset($params->complaints_form)) {
            return ["code" => 201, "data" => $this->permission_denied];
        }

        /* Global variable */
        global $accessObject;

        /** Load complaints data */
        $endpoint = "add";
        $complaintsData = (object) [];

        /** Set parameters for the data to attach */
        $form_params = (object) [
            "module" => "complaints",
            "userData" => $params->userData,
            "item_id" => $complaintsData->item_id ?? null
        ];

        $preloaded_attachments = "";

        // load a complaint record
        if(isset($params->complaintsData)) {

            $endpoint = "update";
            $complaintsData = $params->complaintsData;

            // load the related information
            $relatedObj = load_class("related", "controllers");

            // set the params
            $para = (object) [
                "module" => "complaints",
                "user_id" => $params->userId,
                "user_type" => $params->userData->user_type,
                "userData" => $params->userData,
                "related_item" => $complaintsData->related_to
            ];

            // get the info
            $related_info = $relatedObj->list($para);

            //load the attachments as well
            if(isset($complaintsData->attachment) && !empty($complaintsData->attachment)) {

                // set a new parameter for the items
                $files_param = (object) [
                    "userData" => $params->userData,
                    "label" => "list",
                    "is_deletable" => true,
                    "module" => "complaints",
                    "item_id" => $complaintsData->item_id,
                    "attachments_list" => json_decode($complaintsData->attachment)
                ];

                // create a new object
                $attachments = load_class("files", "controllers")->attachments($files_param);
            
                // get the attachments list
                $preloaded_attachments = !empty($attachments) && isset($attachments["data"]) ? $attachments["data"]["files"] : null;
            }

        }
        /** User Data information */
        $message = isset($complaintsData->message) ? htmlspecialchars_decode($complaintsData->message) : "";
        
        $userData = $params->userData;
        $user_type = $userData->user_type;

        /** Initiate the form content */
        $form_content = "<form action='{$this->baseUrl}api/complaints/{$endpoint}' method='POST' id='ajax-data-form-content' class='ajax-data-form'>";
        $form_content .= form_loader();
        $form_content .= "<div class='row'>";
        $form_content .= "
            <div class='col-lg-12 mb-2'>
            <div class='form-group mb-1'><h6>Complete the Complaint Lodge form below</h6></div></div>
            <div class=\"col-lg-12\">
                <div class=\"form-group\">
                    <div class=\"input-group\">
                        <div class=\"input-group-prepend\">
                        <div class=\"input-group-text\">Subject &nbsp;<span class='required'>*</span></div>
                        </div>
                        <input type=\"text\" value=\"".($complaintsData->subject ?? null)."\" name=\"subject\" id=\"subject\" class=\"form-control\">
                    </div>
                </div>
            </div>
            <div class=\"col-lg-5\">
                <div class=\"form-group\">
                    <label for=\"related_to\">Related to</label>
                    <select name=\"related_to\" data-module=\"complaints\" id=\"related_to\" data-width=\"100%\" class=\"selectpicker\">
                        <option value=\"null\">Related to</option>";
                        // get the related to item
                        $related_to = isset($complaintsData->related_to) ? $complaintsData->related_to : null;
                        // loop through the list
                        foreach($this->pushQuery("id, name", "policy_form", "type='complaints'") as $each) {
                            $value = create_slug($each->name);
                            $form_content .= "<option ".(($value === $related_to) ? "selected" : null)." value=\"{$value}\">{$each->name}</option>";
                        }
                        if(!in_array($user_type, ["insurance_company"])) {
                            $form_content .= "<option ".(($related_to === "insurance-company") ? "selected" : null)." value=\"insurance-company\">Insurance Company</option>";
                        }
                        if($accessObject->hasAccess("view", "licenses")) {
                            $form_content .= "<option ".(($related_to === "license-related") ? "selected" : null)." value=\"license-related\">License Related</option>";
                        }
            $form_content .= "</select>
                </div>
            </div>
            <div class=\"col-lg-7\">
                <div class=\"form-group\">
                    <label for=\"related_to_id\">Suggested Items <small>(Optional)</small></label>
                    <select name=\"related_to_id\" id=\"related_to_id\" data-width=\"100%\" class=\"selectpicker\">
                        <option value=\"null\">Please select item</option>";
                    // if the record has related data
                    if(isset($related_info["data"]) && !empty($related_info["data"])) {
                        // loop through the related information
                        foreach($related_info["data"] as $each) {
                            $form_content .= "<option ".(($each->item_id === $complaintsData->related_to_id) ? "selected" : null)." value=\"{$each->item_id}\">{$each->item_name}</option>";
                        }
                    }
                $form_content .= "
                    </select>
                </div>
            </div>
            <div class=\"col-lg-12\">
                <div class=\"form-group\">
                    <label>Complaint Content <span class='required'>*</span></label>";
                
        // if the users's preference is ckeditor
        $form_content .= $this->textarea_editor($this->userPrefs->text_editor, $message);

        $form_content .= "</div></div>
            <div class='col-lg-12 mb-2 mt-2'>
                <div class='form-group text-center mb-1'>
                    <div class='row'>{$this->form_attachment_placeholder($form_params)}</div>
                    <div class='form-group text-center mb-1'>{$preloaded_attachments}</div>
                </div>
            </div>";

        // the textarea editor to show
        $form_content .= "
            <div class=\"col-md-6 text-left\">
                <input type=\"hidden\" name=\"complaint_id\" id=\"complaint_id\" value=\"".($complaintsData->item_id ?? null)."\" hidden class=\"form-control\">
                <button class=\"btn btn-outline-success btn-sm\" data-function=\"save\" type=\"button-submit\">Lodge Complaint</button>
                <button class=\"btn btn-outline-primary btn-sm\" data-function=\"draft\" type=\"button-submit\">Save as Draft</button>
            </div>
            <div class=\"col-md-6 text-right\">
                <button type=\"reset\" class=\"btn btn-outline-danger btn-sm\" class=\"close\" data-dismiss=\"modal\">Close</button>
            </div>";

        /** End the form content */
        $form_content .= "</div><div class=\"col-lg-12 mb-5 mt-5\"></div>";
        $form_content .= "</form>";
        

        /** Return the form data */
        return [
            "form" => $form_content, 
            "message" => $message
        ];
    }

    /**
     * User Basic Information Form
     * 
     * This will be loaded either the user is updating the record or adding a new user account.
     * 
     * @return String
     */
    public function user_basic_information(stdClass $params) {

        // global variable
        global $usersClass;

        // the query parameter to load the user information
        $i_params = (object) ["limit" => 1, "user_id" => $params->this_user_id];

        // get the user data
        $userData = $usersClass->list($i_params)["data"][0];

        // set the form
        $form_content = "<div class='row'>";
        $form_content .= "<div class='col-lg-12'>";
        $form_content .= "<div class='text-center user-profile-container'>";
        $form_content .= "<img width=\"100%\" src=\"{$this->baseUrl}{$userData->image}\" class=\"profile-image\">";
        $form_content .= "<div class='user-fullname'>{$userData->name}</div>";
        $form_content .= "<div class='user-position'>{$userData->position}</div>";
        $form_content .= "<div class='user-position'>({$userData->employer})</div>";
        $form_content .= "</div>";
        
        $form_content .= "<div class='col-lg-12'>";

        $form_content .= "<div class='row'>";
        $form_content .= "<div class='col-lg-6 user-profile-content'>";
        $form_content .= "<div class='user-profile-label'>Phone Number</div>
                <div class='user-profile-value'>{$userData->phone_number} ".(!empty($userData->phone_number_2) ? " <br> {$userData->phone_number_2}" : null)."</div>";
        $form_content .= "</div>";
        $form_content .= "<div class='col-lg-6 user-profile-content'>";
        $form_content .= "<div class='user-profile-label'>Email Address</div>
                <div class='user-profile-value'><a href='mailto:{$userData->email}'>{$userData->email}</a></div>";
        $form_content .= "</div>";
        $form_content .= "</div>";

        $form_content .= "<div class='row'>";
        $form_content .= "<div class='col-lg-6 user-profile-content'>";
        $form_content .= "<div class='user-profile-label'>Address</div>
                <div class='user-profile-value'>{$userData->address}</div>";
        $form_content .= "</div>";
        $form_content .= "<div class='col-lg-6 user-profile-content'>";
        $form_content .= "<div class='user-profile-label'>Date of Birth</div>
                <div class='user-profile-value'>{$userData->date_of_birth}</div>";
        $form_content .= "</div>";
        $form_content .= "</div>";

        $form_content .= "<div class='row'>";
        $form_content .= "<div class='col-lg-12 user-profile-content'>";
        $form_content .= "<div class='user-profile-label'>Country / Nationality</div>
                <div class='user-profile-value'>{$userData->country_name} {$userData->nationality}</div>";
        $form_content .= "</div>";
        $form_content .= "</div>";

        $form_content .= "<div class='row'>";
        $form_content .= "<div class='col-lg-6 user-profile-content'>";
        $form_content .= "<div class='user-profile-label'>Account Created</div>
                <div class='user-profile-value'>{$userData->date_created}</div>";
        $form_content .= "</div>";
        $form_content .= "<div class='col-lg-6 user-profile-content'>";
        $form_content .= "<div class='user-profile-label'>Last Login</div>
                <div class='user-profile-value'>{$userData->last_login}</div>";
        $form_content .= "</div>";
        $form_content .= "</div>";

        $form_content .= "<div class='row'>";
        $form_content .= "<div class='col-lg-12 user-profile-content'>";
        $form_content .= "<div class='user-profile-label'>Description</div>
                <div class='user-profile-value'>{$userData->description}</div>";
        $form_content .= "</div>";
        $form_content .= "</div>";

        $form_content .= "</div>";

        $form_content .= "</div>";
        $form_content .= "</div>";

        return $form_content;

    }

    /**
     * Load additional information of a user
     * 
     * This includes information like the insurance policies of which they have signedup to
     * 
     * @param String $this_user_id      The id of the user to lookup
     * @param Array $data_to_request    This is an array of the data to lookup for
     * 
     * @return String
     */
    public function user_additional_information($this_user_id, $data_to_request) {

        /** Initialiazing */
        $html_content = "";

        /** If the request included the user policies */
        if(in_array("policies", $data_to_request)) {
            
            // create an object
            $policyObj = load_class("user_policy", "controllers");
            
            /** Set parameters for the data to attach */
            $param = (object) [
                "userData" => $this->thisUser,
                "user_id" => $this_user_id,
                "userId" => $this_user_id,
                "columns" => "a.user_id, a.policy_status, a.policy_name, a.policy_type, a.submit_status, a.policy_id, a.item_id, 
                    u.name AS client_name, u.phone_number AS client_contact, u.email AS client_email, u.image AS client_image,
                    a.policy_start_date, (SELECT name FROM companies WHERE companies.item_id = a.company_id LIMIT 1) AS insurance_company_name"
            ];

            // print the table header
            $html_content .= "
            <hr>
            <table class=\"table dataTable table-hover mb-0\" id=\"user_policies\">
                <thead><tr><th class=\"pt-0\">Policy Info</th>
                <th class=\"pt-0\">Enrolled Date</th><th class=\"pt-0\">Status</th></tr></thead><tbody>";
            
            // loop thorugh the policies list
            foreach($policyObj->list($param)["data"] as $eachPolicy) {
                $html_content .= "
                <tr>
                    <td>
                        {$eachPolicy->policy_name_tag}
                        <br><span class='badge badge-primary'>{$eachPolicy->insurance_company_name}</span>
                    </td>
                    <td>{$eachPolicy->policy_start_date}</td>
                    <td>{$eachPolicy->the_status_label}</td>
                </tr>";
            }

            // print the footer
            $html_content .= "</tbody></table>";
        }

        return $html_content;

    }

    /**
     * User Account Form
     * 
     * This will be loaded either the user is updating the record or adding a new user account.
     * 
     * @return String
     */
    public function user_account_form(stdClass $params = null) {

        /** Access Permissions */
        $predefined_access = isset($params->module["property"]) ? $this->stringToArray($params->module["property"]) : [];
        
        /** Return if not properly executed */
        if(!isset($params->user_account_form)) {
            return ["code" => 201, "data" => $this->permission_denied];
        }
        
        /** Global variable */
        global $usersClass;

        /** User Data information */
        $endpoint = "add";
        $userData = $params->userData;
        $user_type = $userData->user_type;

        /** If user wants to update content */
        if(isset($params->update_user_account)) {
            
            // get the logged in user id
            $loggedUserId = $params->userId;

            // the query parameter to load the user information
            $i_params = (object) ["limit" => 1, "user_id" => $params->this_user_id];

            // append to the search filter
            if(!in_array($userData->user_type, ["insurance_company", "admin"])) {
                $i_params->created_by = $loggedUserId;
            }

            // if the user is with an insurance company
            if(in_array($userData->user_type, ["insurance_company"])) {
                $i_params->company_id = $userData->company_id;
            }

            // get the user data
            $cUserData = $usersClass->list($i_params)["data"];
            // if the current user data is not empty
            if(!empty($cUserData)) {
                $cUserData = $usersClass->list($i_params)["data"][0];
            } else {
                return ["code" => 201, "data" => $this->permission_denied];
            }

            // is this the current user
            $endpoint = "update";
        }

        /** Form the user access permissions */
        $user_access_permission = null;

        /** Generate the content depending on the user currently logged in */
        if(!in_array($user_type, ["user", "business"])) {
            // load the user types that the logged in user can add
            $user_access_permission = "<div class=\"col-lg-6\"><div class=\"form-group\">";
            $user_access_permission .= "<label for=\"othername\">User Type <span class='required'>*</span></label>";
            $user_access_permission .= "<select name=\"access_level\" id=\"access_level\" data-width=\"100%\" class=\"selectpicker w-100\">
                        <option value=\"null\">Select User Type</option>";
            // access level permissions list
            foreach($this->permissions_list($user_type) as $key => $value) {
                $user_access_permission .= "<option ".(in_array($key, $predefined_access) ? "selected" : null)." value=\"{$key}\">{$value}</option>";
            }
            $user_access_permission .= "</select>";
            $user_access_permission .= "</div></div>";
            // set the user id of the person
            $user_access_permission .= "
                <div class=\"col-lg-6\">
                    <div class=\"form-group\">
                        <label for=\"user_id_label\">Unique ID</label>
                        <input type=\"text\" value=\"".($cUserData->client_id ?? null)."\" name=\"client_id\" id=\"client_id\" class=\"form-control text-uppercase\">
                    </div>
                </div>";
        }

        /** Initiate the form content */
        $form_content = "<form action='{$this->baseUrl}api/users/{$endpoint}' method='POST' id='ajax-data-form-content' class='ajax-data-form'>";
        $form_content .= form_loader();
        $form_content .= "<div class='row'>";
        $form_content .= "
                {$user_access_permission}
                <div class='col-lg-12 mb-2'><div class='form-group mb-1'><h5>BIO DATA</h5></div></div>
                <div class=\"col-lg-12\">
                    <div class=\"form-group\">
                        <div class=\"input-group\">
                            <div class=\"input-group-prepend\">
                            <div class=\"input-group-text\">Firstname &nbsp;<span class='required'>*</span></div>
                            </div>
                            <input type=\"text\" value=\"".($cUserData->firstname ?? null)."\" name=\"firstname\" id=\"firstname\" class=\"form-control\">
                        </div>
                    </div>
                </div>
                <div class=\"col-lg-12\">
                    <div class=\"form-group\">
                        <div class=\"input-group\">
                            <div class=\"input-group-prepend\">
                            <div class=\"input-group-text\">Lastname &nbsp;<span class='required'>*</span></div>
                            </div>
                            <input type=\"text\" value=\"".($cUserData->lastname ?? null)."\" name=\"lastname\" id=\"lastname\" class=\"form-control\">
                        </div>
                    </div>
                </div>
                <div class=\"col-lg-12\">
                    <div class=\"form-group\">
                        <div class=\"input-group\">
                            <div class=\"input-group-prepend\">
                            <div class=\"input-group-text\">Othernames</div>
                            </div>
                            <input type=\"text\" value=\"".($cUserData->othername ?? null)."\" name=\"othername\" id=\"othername\" class=\"form-control\">
                        </div>
                    </div>
                </div>
                <div class=\"col-lg-6\">
                    <div class=\"form-group\">
                        <label for=\"gender\">Gender</label>
                        <select name=\"gender\" id=\"gender\" data-width=\"100%\" class=\"selectpicker\">
                            <option value=\"null\">Select Gender</option>";
                            foreach($this->pushQuery("id, name", "policy_form", "type='gender'") as $each) {
                                if(isset($cUserData)) {
                                    $form_content .= "<option ".(($each->name == $cUserData->gender ? "selected" : null))." value=\"{$each->name}\">{$each->name}</option>";
                                } else {
                                    $form_content .= "<option value=\"{$each->name}\">{$each->name}</option>";
                                }
                            }
                $form_content .= "</select>
                    </div>
                </div>
                <div class=\"col-lg-6\">
                    <div class=\"form-group\">
                        <label for=\"date_of_birth\">Date of Birth</label>
                        <input type=\"text\" value=\"".($cUserData->date_of_birth ?? null)."\" name=\"date_of_birth\" id=\"date_of_birth\" class=\"form-control datepicker\">
                    </div>
                </div>
                <div class='col-lg-12 mb-2 mt-2'><div class='form-group mb-1'><h5>CONTACT DETAILS</h5></div></div>
                <div class=\"col-lg-12\">
                    <div class=\"form-group\">
                        <div class=\"input-group\">
                            <div class=\"input-group-prepend\">
                            <div class=\"input-group-text\">Email Address &nbsp;<span class='required'>*</span></div>
                            </div>
                            <input data-autosearch_email=\"true\" name=\"email\" value=\"".($cUserData->email ?? null)."\" id=\"email\" title=\"email\" class=\"form-control mb-4 mb-md-0\" data-inputmask=\"'alias': 'email'\" inputmode=\"email\">
                        </div>
                        <div class='email_checker'></div>
                    </div>
                </div>
                <div class=\"col-lg-6\">
                    <div class=\"form-group\">
                        <label for=\"phone\">Primary Number <span class='required'>*</span></label>
                        <input type=\"text\" value=\"".($cUserData->phone_number ?? null)."\" data-inputmask-alias=\"(+999) 999-999-999\" inputmode=\"text\" name=\"phone\" id=\"phone\" class=\"form-control\">
                    </div>
                </div>
                <div class=\"col-lg-6\">
                    <div class=\"form-group\">
                        <label for=\"phone_2\">Secondary Contact</label>
                        <input type=\"text\" value=\"".($cUserData->phone_number_2 ?? null)."\" data-inputmask-alias=\"(+999) 999-999-999\" inputmode=\"text\" name=\"phone_2\" id=\"phone_2\" class=\"form-control\">
                    </div>
                </div>
                <div class=\"col-lg-12\">
                    <div class=\"form-group\">
                        <div class=\"input-group\">
                            <div class=\"input-group-prepend\">
                            <div class=\"input-group-text\">Postal Address</div>
                            </div>
                            <input type=\"text\" value=\"".($cUserData->address ?? null)."\" name=\"address\" id=\"address\" class=\"form-control\">
                        </div>
                    </div>
                </div>
                <div class=\"col-lg-12\">
                    <div class=\"form-group\">
                        <div class=\"input-group\">
                            <div class=\"input-group-prepend\">
                            <div class=\"input-group-text\">Residential Address</div>
                            </div>
                            <input type=\"text\" value=\"".($cUserData->residence ?? null)."\" name=\"residence\" id=\"residence\" class=\"form-control\">
                        </div>
                    </div>
                </div>
                <div class=\"col-lg-6\">
                    <div class=\"form-group\">
                        <label for=\"nationality\">Nationality</label>
                        <input type=\"text\" value=\"".($cUserData->nationality ?? null)."\" name=\"nationality\" id=\"nationality\" class=\"form-control\">
                    </div>
                </div>
                <div class=\"col-lg-6\">
                    <div class=\"form-group\">
                        <label for=\"country\">Country</label>
                        <select name=\"country\" id=\"country\" data-width=\"100%\" class=\"selectpicker w-100\">
                            <option value=\"null\">Select Country</option>";
                            foreach($this->pushQuery("id, country_name", "country", "'1'") as $each) {
                                if(isset($cUserData)) {
                                    $form_content .= "<option ".(($each->id == $cUserData->country ? "selected" : null))." value=\"{$each->id}\">{$each->country_name}</option>";
                                } else {
                                    $form_content .= "<option value=\"{$each->id}\">{$each->country_name}</option>";
                                }
                            }
                $form_content .= "</select>
                    </div>
                </div>
                <div class='col-lg-12 mb-2 mt-2'><div class='form-group mb-1'><h5>OTHER INFORMATION</h5></div></div>
                <div class=\"col-lg-12\">
                    <div class=\"form-group\">
                        <div class=\"input-group\">
                            <div class=\"input-group-prepend\">
                            <div class=\"input-group-text\">Occupation</div>
                            </div>
                            <input type=\"text\" value=\"".($cUserData->occupation ?? null)."\" name=\"occupation\" id=\"occupation\" class=\"form-control\">
                        </div>
                    </div>
                </div>
                <div class=\"col-lg-12\">
                    <div class=\"form-group\">
                        <div class=\"input-group\">
                            <div class=\"input-group-prepend\">
                            <div class=\"input-group-text\">Employer</div>
                            </div>
                            <input type=\"text\" value=\"".($cUserData->employer ?? null)."\" name=\"employer\" id=\"employer\" class=\"form-control\">
                        </div>
                    </div>
                </div>
                <div class=\"col-lg-12\">
                    <div class=\"form-group\">
                        <div class=\"input-group\">
                            <div class=\"input-group-prepend\">
                            <div class=\"input-group-text\">Position</div>
                            </div>
                            <input type=\"text\" value=\"".($cUserData->position ?? null)."\" name=\"position\" id=\"position\" class=\"form-control\">
                        </div>
                    </div>
                </div>
                <div class=\"col-lg-12\">
                    <div class=\"form-group\">
                        <label for=\"description\">Description</label>
                        <textarea name=\"description\" id=\"description\" rows=\"3\" class=\"form-control\">".($cUserData->description ?? null)."</textarea>
                    </div>
                </div>";
                // instruction notice
                if(!isset($cUserData)) {
                    $form_content .= "
                    <div class='col-lg-12 mb-2 mt-2'>
                        <div class='form-group text-danger text-center mb-1'>
                            <strong>NB:</strong> Additional instructions will be sent to the user via email.
                        </div>
                    </div>";
                }
                $form_content .= "
                <div class=\"col-lg-6 text-left\">
                    <input type=\"hidden\" name=\"user_id\" id=\"user_id\" value=\"".($cUserData->user_id ?? null)."\" hidden class=\"form-control\">
                    <button class=\"btn btn-outline-success btn-sm\" data-function=\"save\" type=\"button-submit\">Save Record</button>
                </div>
                <div class=\"col-md-6 text-right\">
                    <button type=\"reset\" class=\"btn btn-outline-danger btn-sm\" class=\"close\" data-dismiss=\"modal\">Close</button>
                </div>";
        /** End the form content */
        $form_content .= "</div>";
        $form_content .= "</form>";

        /** Return the form data */
        return $form_content;

    }

    /**
     * The Policy Forms
     * 
     * By using the various user type depending on who is logged in, then display the form content
     * 
     * @return String
     */
    public function apply_policy_form(stdClass $params = null) {
       
        /** Return if not properly executed */
        if(!isset($params->apply_policy_form)) {
            return ["code" => 201, "data" => $this->permission_denied];
        }

        /** Reference some global variables */
        global $usersClass;

        /** User Data information */
        $userData = $params->userData;
        $user_type = $userData->user_type;
        $userId = $params->userData->user_id;

        /** Initiate the form content */
        $form_content = "<div class='row ajax-load-form-header' data-form_type=\"load_policy_form\">";
        
        /** If the current person is a user */
        if(in_array($user_type, ["user", "business", "agent", "broker", "bankassurance", "insurance_company"])) {
            
            // show this section for only user/business
            if(in_array($user_type, ["user", "business"])) {
                
                // load the user form data
                $formData = $this->pushQuery("*", "policy_form", "type='{$user_type}' AND status='1'");
                
                // show the select columns
                $form_content .= "<div class='col-lg-12'><div class='form-group'>";
            
                // artenary if clause
                $form_content .= ($user_type == "user") ? "<label>I require Insurance Policy for?</label>" : "<label>Kindly introduce yourself.</label>";
                $form_content .= "<select class='form-control selectpicker' data-width='100%' name='registering_type' id='registering_type'>
                        <option value='null'>Please select option</option>";
                    foreach($formData as $eachItem) {
                        $form_content .= "<option value='{$eachItem->id}'>{$eachItem->name}</option>";
                    }
                $form_content .= "</select></div></div>";
                $form_content .= "<input type=\"hidden\" value=\"{$userId}\" name=\"u_client_id\" id=\"u_client_id\" class=\"form-control\">";

            }
            
            // show this section for agent, brokers and bankassurance
            if(in_array($user_type, ["agent", "broker", "bankassurance", "insurance_company"])) {
                
                /** Load the user options */
                $u_options = (object) [
                    "the_user_type" => $user_type,
                    "userData" => $userData,
                    "columns" => "
                        a.name, a.item_id, a.phone_number, a.email, 
                        a.user_status, a.user_type, a.item_id AS user_id
                    ",
                    "userId" => $userId,
                    "remote" => true
                ];
                $users_list = $usersClass->list($u_options)["data"];

                $form_content .= "
                <div class=\"col-lg-12\">
                    <div class=\"form-group\">
                        <label for=\"u_client_id\">Select Client <span class='required'>*</span></label>
                        <select name=\"u_client_id\" data-module=\"users\" id=\"u_client_id\" data-width=\"100%\" class=\"selectpicker\">
                            <option value=\"null\">Select Client</option>";
                            foreach($users_list as $each) {
                                $form_content .= "<option value=\"{$each->item_id}\">{$each->name} (Contact: {$each->phone_number} | Email: {$each->email})</option>";
                            }
                $form_content .= "</select>
                    </div>
                </div>";

            }

            // if the person logged in is a business account
            if($user_type == "business") {
                
                // load the business category form data
                $formData = $this->pushQuery("*", "policy_form", "type='category' AND status='1'");
                
                // show the select columns
                $form_content .= "<div class='col-lg-12'><div class='form-group'>";
                
                // artenary if clause
                $form_content .= "<label>Which category is your business?</label>";
                $form_content .= "<select class='form-control selectpicker' data-width='100%' name='business_category' id='business_category'>
                        <option value='null'>Select Busines Category</option>";
                    foreach($formData as $eachItem) {
                        $form_content .= "<option value='{$eachItem->id}'>{$eachItem->name}</option>";
                    }
                $form_content .= "</select></div></div>";

                // load the business description
                $formData = $this->pushQuery("*", "policy_form", "type='description' AND status='1'");
                
                // show the select columns
                $form_content .= "<div class='col-lg-12'><div class='form-group'>";
                
                // artenary if clause
                $form_content .= "<label>Which of the following best describes your business?</label>";
                $form_content .= "<select class='form-control selectpicker' data-width='100%' name='business_description' id='business_description'>
                        <option value='null'>Select Busines Description</option>";
                    foreach($formData as $eachItem) {
                        $form_content .= "<option value='{$eachItem->id}'>{$eachItem->name}</option>";
                    }
                $form_content .= "</select></div></div>";

            }

            // policy category
            $formData = $this->pushQuery("*", "policy_form", "type='policy_category' AND status='1'");
            
            // show the select columns
            $form_content .= "<div class='col-lg-12'><div class='form-group'>";
            
            // artenary if clause
            $form_content .= "<label>Filter the insurance policy by category?</label>";
            $form_content .= "<select class='form-control selectpicker' data-width='100%' name='policy_category' id='policy_category'>
                    <option value='null'>Select Policy Category</option>";
                foreach($formData as $eachItem) {
                    $form_content .= "<option value='".create_slug($eachItem->name)."'>{$eachItem->name}</option>";
                }
            $form_content .= "</select></div></div>";

            // load the policy types available for the users
            $formData = $this->pushQuery(
                "a.id, a.item_id, a.name, a.policy_id, (SELECT name FROM companies WHERE companies.item_id = a.company_id LIMIT 1) AS company_name", 
                "policy_types a", "policy_status='Enrolled' AND deleted='0'"
            );
            
            // show the select columns
            $form_content .= "<div class='col-lg-12'><div class='form-group'>";
            
            // artenary if clause
            $form_content .= ($user_type == "user") ? "<label>The following Policies are recommended for you</label>" : "<label>The following Policies are recommeded for your business.</label>";
            $form_content .= "<select class='form-control selectpicker' data-width='100%' name='policy_id' id='policy_id'>
                    <option value='null'>Please select option</option>";
                foreach($formData as $eachItem) {
                    $form_content .= "<option data-name=\"{$eachItem->name}\" value='{$eachItem->item_id}'>{$eachItem->name} - by: {$eachItem->company_name}</option>";
                }
            $form_content .= "</select></div>
                <div class='text-right hidden' id='policy-learn-more'>
                    <span class='text-right text-primary cursor mb-2 learn-more-modal' title='Click to learn more about the select policy' data-toggle='tooltip'></span>
                </div>
            </div>";

        }
        // show the apply for policy button
        $form_content .= "
            <div class=\"col-lg-12 text-center mt-3 hidden\" id=\"proceed-to-ajax-form\">
                <span class=\"btn btn-outline-success proceed-to-form\">Click to complete insurance form</span>
            </div>";

        /** End the form content */
        $form_content .= "</div>";
        
        /** Load the policy form content */
        $form_content .= "<div class=\"hidden\" id=\"ajax-load-form-content\"></div>";

        /** Return the form data */
        return $form_content;

    }

    /**
     * Prepare the form to be completed by the user
     * 
     * Apply all the needed data requirement whereever necessary in the form
     * 
     * @param \stdClass $formData           This is the form fields to display to the user
     * @param \stdClass $userData           The details of the user information
     * 
     * @return String
     */
    public function policy_application_form($formData, $userData, $form_name = "user_policy") {
        
        // set initial variable
        $the_form = null;
        $message = "";

        // set the form to load
        if($form_name == "user_policy") {
            $the_form = "policy_form";
            $message = "Read on the selected policy";
            $main_item_id = $formData->item_id;
        } elseif($form_name == "claims") {
            $main_item_id = $formData->main_policy_id;
            $the_form = "claims_form";
            $message = "Read more on the policy to make the claim.";
        }
        /** If not isset form */
        if(empty($the_form)) {
            return;
        }
        
        /** if the policy form fields are empty then return also */
        if(empty($formData->$the_form->fields)) {
            return;
        }
        
        /** Prepare the user Data */
        $data = [
            "users" => $userData,
            "users_policy" => ""
        ];

        /** Set parameters for the data to attach */
        $form_params = (object) [
            "module" => "{$form_name}_{$formData->item_id}",
            "userData" => $userData,
            "item_id" => $formData->item_id
        ];

        /** Form content display */
        $form_content = "<form action='{$this->baseUrl}api/{$form_name}/add' method='POST' id='ajax-data-form-content' class='ajax-data-form'>";
        $form_content .= form_loader();

        /** Provide button to read more about the policy */
        $form_content .= "<div class='col-lg-12 mb-2 mt-2'>
                <div class='form-group text-right font-italic mb-1'>
                    <span title=\"Click to read more on the {$formData->name} Policy\" data-toggle=\"tooltip\" onclick=\"policy_basic_information('{$main_item_id}')\" class=\"text-primary cursor\">{$message}</span>
                </div>
            </div>";

        /** Load the form content */
        $form_content .= $this->form_enlisting($formData->$the_form, false, $data)["form"];

        /** If attachment is allowed */
        if($formData->$the_form->allow_attachment == "yes") {
            $form_content .= "<div class='col-lg-12 mb-2 mt-2'>
                <div class='form-group text-center mb-1'><div class='row'>{$this->form_attachment_placeholder($form_params)}</div></div>
            </div>";
        }

        /** Append the footnote of the form to the footer */
        $form_content .= "<div class='col-lg-12 mb-2 mt-2'>
                <div class='form-group text-center font-italic mb-1'>".htmlspecialchars_decode($formData->$the_form->form_footnote)."</div>
            </div>";

        /** Additional html for the form */
        $form_content .= "<div class=\"row mt-4 mb-4\">";
        $form_content .= "<div class=\"col-md-6 text-left\"><span class=\"btn btn-outline-danger\" onclick=\"return discard_form();\">Close</span></div>";
        $form_content .= "<div class=\"col-md-6 text-right\">";
        $form_content .= "<input type=\"hidden\" name=\"policy_id\" id=\"policy_id\" value=\"{$formData->item_id}\">";
        $form_content .= "<input type=\"hidden\" name=\"user_id\" id=\"user_id\" value=\"{$userData->user_id}\">";
        $form_content .= "<button class=\"btn btn-outline-primary btn-sm\" data-function=\"draft\" type=\"button-submit\">Save as Draft</button>";
        $form_content .= "&nbsp;<button class=\"btn btn-outline-success btn-sm\" data-function=\"save\" type=\"button-submit\">Submit Form</button>";
        $form_content .= "</div>";
        $form_content .= "</div>";
        $form_content .= "</form>";

        return $form_content;
    }

    /**
     * This method is used for display the form however fill in with the answers submitted by the user
     * 
     * @param \stdClass $formData        These are the form fields
     * @param \stdClass $form_answers       These are the answers provided by the user
     * @param Bool  $is_editable            This is to specify whether the user wants to 
     * 
     * @return String 
     */
    public function preload_form_data($formData, $form_answers, $is_editable = true, $form_name = null) {

        $readonly = !$is_editable ? "disabled=\"disabled\"" : null;
        $form_answers = (array) $form_answers;

        /** Set parameters for the data to attach */
        $form_params = (object) [
            "module" => "{$form_name}_{$formData->item_id}",
            "userData" => $formData->userData,
            "item_id" => $formData->item_id
        ];

        $preloaded_attachments = "";

        // get the attachment list
        if(isset($formData->attachment)) {
            
            // set a new parameter for the items
            $files_param = (object) [
                "userData" => $formData->userData,
                "label" => "list",
                "is_deletable" => $is_editable,
                "module" => "{$form_name}_{$formData->item_id}",
                "item_id" => $formData->item_id,
                "attachments_list" => $formData->attachment
            ];

            // create a new object
            $attachments = load_class("files", "controllers")->attachments($files_param);
           
            // get the attachments list
            $preloaded_attachments = !empty($attachments) && isset($attachments["data"]) ? $attachments["data"]["files"] : null;

        }

        /** Form content display */
        $html_content = "<form action='{$this->baseUrl}api/{$form_name}/update' method='POST' id='ajax-data-form-content' class='ajax-data-form'>";
        $html_content .= form_loader();

        // loop throught the list of form fields
        foreach($formData->form_fields as $key => $eachField) {

            // required field
            $required = $eachField->required == "yes" ? "required" : "";

            // form content
            $html_content .= "<div class=\"col-lg-12 p-0\">";
            $html_content .= "<div class=\"form-group\">";
            $html_content .= "<label for=\"field[{$key}]\">{$eachField->label} ".(($required) ? " &nbsp;<span class='required'>*</span>" : null)."</label>";

            // if prefill is true
            $value =  "";
            
            // if preload is not null and the value is set
            $value = isset($form_answers[$key]) ? $form_answers[$key] : null;

            // if the type is an input, date or email
            if(in_array($eachField->type, ["input", "date", "email"])) {
                
                // set some additional variables
                $type = (in_array($eachField->type, ["input", "date"])) ? "text" : $eachField->type;
                $class = $eachField->type == "date" ? "datepicker" : "";

                $html_content .= "<input {$readonly} value=\"{$value}\" type=\"{$type}\" {$required} class=\"form-control {$class}\" name=\"field[{$key}]\" id=\"field[{$key}]\">";

            }

            // if a textarea was parsed
            if(in_array($eachField->type, ["textarea"])) {
                $html_content .= "<textarea {$readonly} {$required} rows=\"7\" class=\"form-control\" name=\"field[{$key}]\" id=\"field[{$key}]\">{$value}</textarea>";
            }

            // if a select field was parsed
            if(in_array($eachField->type, ["select"])) {
                // show this list
                $html_content .= "<select {$readonly} class=\"form-control selectpicker\" {$required} name=\"field[{$key}]\" id=\"field[{$key}]\" data-width=\"100%\">";

                // show the values for the select fields
                $html_content .= "<option value=\"null\">Please Select:</option>";
                
                // loop through the list
                foreach($eachField->select as $eachItem) {
                    // append to the list
                    $html_content .= "<option ".(($value == $eachItem->value) ? "selected" : "null")." value=\"{$eachItem->value}\">{$eachItem->label}</option>";
                }
                
                $html_content .= "</select>";
            }

            $html_content .= "</div>";
            $html_content .= "</div>";

        }

        // show the attachments list if the form is editable
        if($is_editable) {
            /** If attachment is allowed */
            $html_content .= "<div class='col-lg-12 mb-2 mt-2'>
                <div class='form-group text-center mb-1'><div class='row'>{$this->form_attachment_placeholder($form_params)}</div></div>
            </div>";
        }

        // list the attached documents
        $html_content .= "<div class='form-group text-center mb-1'>{$preloaded_attachments}</div>";

        /** Additional html for the form */
        $html_content .= "<div class=\"row mt-4 mb-4\">";
        $html_content .= "<div class=\"col-md-6 text-left\"><span class=\"btn btn-outline-danger\" data-dismiss=\"modal\">Close</span></div>";
        
        // if it is editable
        if($is_editable) {
            $html_content .= "<div class=\"col-md-6 text-right\">";
            $html_content .= "<input type=\"hidden\" name=\"item_id\" id=\"item_id\" value=\"{$formData->item_id}\">";
            $html_content .= "<input type=\"hidden\" name=\"user_id\" id=\"user_id\" value=\"{$formData->user_id}\">";
            $html_content .= "<button class=\"btn btn-outline-primary btn-sm\" data-function=\"draft\" type=\"button-submit\">Save as Draft</button>";
            $html_content .= "&nbsp;<button class=\"btn btn-outline-success btn-sm\" data-function=\"save\" type=\"button-submit\">Submit Form</button>";
            $html_content .= "</div>";
        }
        $html_content .= "</div>";
        $html_content .= "</form>";

        return $html_content;

    }

    /**
     * Preview form and show to to the user
     * 
     * @param \stdClass $params
     * 
     * @return String
     */
    public function preview_form(stdClass $params) {
        /** Init the forms data */
        $form_content  = "";

        /** If the form fields is set */
        if(!isset($params->module["item_id"]["fields"])) {
            return;
        }
        
        /** Confirm the form string to an object */
        $form = json_decode($params->module["item_id"]["fields"]);

        /** End the query if the form is empty */
        if(empty($form)) {
            return;
        }

        $formFields = (object) [
            "fields" => $form 
        ];

        /** Load the form content */
        $form_content .= $this->form_enlisting($formFields, false, [], false)["form"];

        /** Confirm that the footnote was parsed */
        if(isset($params->module["item_id"]["footnote"])) {
            /** Append the footnote of the form to the footer */    
            $form_content .= "<div class='col-lg-12 mb-2 mt-2'>
                    <div class='form-group text-center font-italic mb-1'>".htmlspecialchars_decode($params->module["item_id"]["footnote"])."</div>
                </div>";
        }

        $form_content .= "<div class=\"col-md-12 mt-4 border-top pt-4 mb-3 text-center\"><span class=\"btn btn-outline-secondary\" data-dismiss='modal'>Close Preview</span></div>";

        return $form_content;
    }

    /**
     * This method will be enlisting the form
     * It displays both during the updating of the form by Admins or Completing it by the Client.
     * 
     * @param stdClass $params
     * 
     * @param Bool $is_editable         When set to false, the form will be presented in the form that users can fill in
     *                                  A true value will make it editable by an admin
     * 
     * @param Array $data               This holds data that is need for prefilling the form before displaying to the user
     * @param Bool  $preview            This when set to true will make all the fields readonly
     * 
     * @return Array
     */
    public function form_enlisting($params, $is_editable = true, $data = [], $preview = false) {
        
        // assign the data set
        $thisRowId = 0;
        $thisSelectRow = 0;
        $html_content = "";
        $readonly = $preview ? "readonly=\"readony\"" : null;
        $form_data = isset($params->fields) ? $params->fields : (object)[];

        // loop throught the list of form fields
        foreach($form_data as $key => $eachField) {

            // if the form is editable
            if($is_editable) {

                // append to the list
                $html_content .= "<div class='form-group' data-row=\"{$key}\">
                    <div class=\"input-group\">
                        <div class=\"input-group-prepend\">
                        <div class=\"input-group-text\">".strtoupper($eachField->type)." LABEL &nbsp;</div>
                        </div>
                        <input data-name=\"form[type][{$key}]\" hidden type=\"hidden\" value=\"{$eachField->type}\">
                        <input type=\"text\" value=\"{$eachField->label}\" input-type=\"{$eachField->type}\" data-row=\"{$key}\" required data-name=\"form[label][{$key}]\" id=\"input_label_{$key}\" class=\"form-control\">
                        <div class=\"input-group-prepend\">
                            <select class=\"form-control\" data-role=\"required\" data-row=\"{$key}\" style=\"width:130px\" data-name=\"form[required][{$key}]\">
                                <option ".(($eachField->required == "no") ? "selected" : null)." value=\"no\">Not Required</option>
                                <option ".(($eachField->required == "yes") ? "selected" : null)." value=\"yes\">Required</option>
                            </select>";

                        // show this section for only select fields
                        if(in_array($eachField->type, ["select"])) {
                            // show this list
                            $html_content .= "<select class=\"form-control\" data-role=\"values\" data-row=\"{$key}\" style=\"width:230px\" data-name=\"form[values][{$key}]\">";

                            // if the data is empty
                            if(empty($eachField->select)) {
                                $html_content .= "<option value=\"null\">No values added:</option>";
                            } else {
                                $html_content .= "<option value=\"null\">Values Added:</option>";
                                // loop through the list
                                foreach($eachField->select as $eachItem) {
                                    // append to the list
                                    $html_content .= "<option value=\"{$eachItem->value}\">{$eachItem->label}</option>";
                                }
                            }
                            $html_content .= "</select>";
                            // increment the select rows count
                            $thisSelectRow++;
                        }

                        // if the field type is in the list
                        if(in_array($eachField->type, ["input", "date", "email", "textarea"])) {
                            $html_content .= "
                                <select class=\"form-control\" data-role=\"preload\" data-row=\"{$key}\" style=\"width:230px\" data-name=\"form[preload][{$key}]\">
                                    <option value=\"null\">Preload this field with:</option>
                                    <option ".(($eachField->preload == "users") ? "selected" : null)." value=\"users\">User Bio Data</option>
                                    <option ".(($eachField->preload == "users_policy") ? "selected" : null)." value=\"users_policy\">User Policy Information</option>
                                </select>
                                <select class=\"form-control\" data-role=\"preload_value\" data-row=\"{$key}\" style=\"width:230px;".(!empty($eachField->preload_value) ? "" : "display:none")."\" data-name=\"form[preload_value][{$key}]\">
                                    <option value=\"null\">Select Value:</option>";
                                
                                // load the content
                                if(!empty($eachField->preload_value)) {
                                    // loop through the list
                                    foreach($this->formPreloader[$eachField->preload] as $ikey => $ivalue) {
                                        $html_content .= "<option ".(($ikey == $eachField->preload_value) ? "selected" : null)." value=\"{$ikey}\">{$ivalue}</option>";
                                    }
                                }

                            $html_content .= "</select>";
                        }

                        // show the buttons
                        $html_content .= "<div class=\"input-group-text\" style=\"background:none;padding:0px;border:0px; margin-left: 10px;\">";
                        // show the update button for select fields
                        if(in_array($eachField->type, ["select"])) {
                            $html_content .= "<button type=\"button\" data-row=\"{$key}\" class=\"btn update-row btn-outline-success mr-1 btn-sm\"><i class=\"fa fa-edit\"></i></button> &nbsp; ";
                        }                    
                        $html_content .= "<button type=\"button\" data-row=\"{$key}\" class=\"btn remove-row btn-outline-danger btn-sm\"><i class=\"fa fa-times\"></i></button>
                            </div>";
                        

                        $html_content .= "</div>
                    </div>  
                </div>";
                $thisRowId = $key;
                
            }

            // if the form is not editable
            elseif(!$is_editable) {
                
                // required field
                $required = $eachField->required == "yes" ? "required" : "";

                // form content
                $html_content .= "<div class=\"col-lg-12 p-0\">";
                $html_content .= "<div class=\"form-group\">";
                $html_content .= "<label for=\"field[{$key}]\">{$eachField->label} ".(($required) ? " &nbsp;<span class='required'>*</span>" : null)."</label>";

                // if prefill is true
                $value =  "";
                
                // if preload is not null and the value is set
                if($eachField->preload !== "null" && !empty($eachField->preload_value)) {
                    $value = isset($data[$eachField->preload]->{$eachField->preload_value}) ? $data[$eachField->preload]->{$eachField->preload_value} : null;
                }

                // if the type is an input, date or email
                if(in_array($eachField->type, ["input", "date", "email"])) {
                    
                    // set some additional variables
                    $type = (in_array($eachField->type, ["input", "date"])) ? "text" : $eachField->type;
                    $class = $eachField->type == "date" ? "datepicker" : "";

                    $html_content .= "<input {$readonly} value=\"{$value}\" type=\"{$type}\" {$required} class=\"form-control {$class}\" name=\"field[{$key}]\" id=\"field[{$key}]\">";

                }

                // if a textarea was parsed
                if(in_array($eachField->type, ["textarea"])) {
                    $html_content .= "<textarea {$readonly} {$required} rows=\"5\" class=\"form-control\" name=\"field[{$key}]\" id=\"field[{$key}]\"></textarea>";
                }

                // if a select field was parsed
                if(in_array($eachField->type, ["select"])) {
                    // show this list
                    $html_content .= "<select {$readonly} class=\"form-control selectpicker\" {$required} name=\"field[{$key}]\" id=\"field[{$key}]\" data-width=\"100%\">";

                    // show the values for the select fields
                    $html_content .= "<option value=\"null\">Please Select:</option>";
                    
                    // loop through the list
                    foreach($eachField->select as $eachItem) {
                        // append to the list
                        $html_content .= "<option ".(($value == $eachItem->value) ? "selected" : "null")." value=\"{$eachItem->value}\">{$eachItem->label}</option>";
                    }
                    
                    $html_content .= "</select>";
                }

                $html_content .= "</div>";
                $html_content .= "</div>";

            }

        }

        // return the results
        return [
            "form" => $html_content,
            "thisRowId" => $thisRowId+1,
            "thisSelectRow" => $thisSelectRow+1
        ];

    }

    /**
     * Eduplus Category
     * 
     * This displays the form for the Category
     * 
     * @param   stdClass $params
     * @param   stdClass $data
     * 
     * @return String
     */
    public function eduplus_category($params, $data = null) {

        $form_content = "<form action='{$this->baseUrl}api/eduplus/".(isset($data->name) ? "update_category" : "add_category")."' method='POST' id='ajax-data-form-content' class='ajax-data-form'>";
        $form_content .= '
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">CATEGORY NAME <span class="required">*</span></label>
                        <input type="text" placeholder="Category Enter" value="'.($data->name ?? null).'" class="form-control" name="name">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">DESCRIPTION</label>';
                        $form_content .= $this->textarea_editor($this->userPrefs->text_editor, $data->description ?? null);
                        $form_content .= '
                    </div>
                </div>
                <div class="col-md-6 text-left">
                    <button type="reset" class="btn btn-outline-secondary" class="close" data-dismiss="modal">Close</button>
                </div>
                <div class="col-md-6 text-right">
                    <input type="hidden" value="'.($data->item_id ?? null).'" name="category_id" readonly>
                    <button type="button-submit" class="btn btn-success"><i class="fa fa-save"></i> Save Record</button>
                </div>
            </div>
        </form>';

        return $form_content;

    }

    /**
     * Eduplus Subject
     * 
     * This displays the form for the Subject
     * 
     * @param   stdClass $params
     * @param   stdClass $data
     * 
     * @return String
     */
    public function eduplus_subject($params, $data = null) {

        // attachment parameters
        $form_params = (object) [
            "module" => "eduplus_subject",
            "userData" => $params->userData,
            "item_id" => $data->item_id ?? null,
        ];
        $preloaded_attachments = "";

        //load the attachments as well
        if(isset($data->attachment) && !empty($data->attachment)) {

            // set a new parameter for the items
            $files_param = (object) [
                "userData" => $params->userData,
                "label" => "list",
                "is_deletable" => true,
                "module" => "eduplus_subject",
                "item_id" => $data->item_id,
                "attachments_list" => $data->attachment
            ];

            // create a new object
            $attachments = load_class("files", "controllers")->attachments($files_param);
        
            // get the attachments list
            $preloaded_attachments = !empty($attachments) && isset($attachments["data"]) ? $attachments["data"]["files"] : null;
        }

        // set the form content
        $form_content = "<form action='{$this->baseUrl}api/eduplus/".(isset($data->subject) ? "update_subject" : "add_subject")."' method='POST' id='ajax-data-form-content' class='ajax-data-form'>";
        $form_content .= '
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="">CATEGORY<span class="required">*</span></label>
                        <select name="category_id" data-width="100%" id="category_id" class="selectpicker form-control">';
                foreach($params->eduClass->category_list($params)["data"] as $key => $category) {
                    $form_content .= "<option ".(($params->category_id == $category->item_id) ? "selected" : "")." value=\"{$category->item_id}\">{$category->name}</option>";
                }
            $form_content .= '</select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">ALLOW COMMENTS</label>
                        <select name="allow_comments" data-width="100%" id="allow_comments" class="selectpicker form-control">';
                $form_content .= "<option ".(!empty($data) && ($data->allow_comments === 1) ? "selected" : "")." value=\"1\">Allow Comments</option>";
                $form_content .= "<option ".(!empty($data) && ($data->allow_comments === 0) ? "selected" : "")." value=\"0\">Dont Allow Comments</option>";
            $form_content .= '</select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">SUBJECT NAME <span class="required">*</span></label>
                        <input type="text" placeholder="Enter subject name" value="'.($data->subject ?? null).'" class="form-control" name="subject">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">DESCRIPTION</label>';
                        $form_content .= $this->textarea_editor($this->userPrefs->text_editor, $data->description ?? null);
                        $form_content .= '
                    </div>
                </div>
                <div class="col-lg-12 p-0 mb-3 border-top">
                    '.$this->comments_form_attachment_placeholder($form_params).'
                    <div class="form-group text-center mb-1">'.$preloaded_attachments.'</div>
                </div>
                <div class="col-md-6 text-left">
                    <button type="reset" class="btn btn-outline-secondary" class="close" data-dismiss="modal">Close</button>
                </div>
                <div class="col-md-6 text-right">
                    <input type="hidden" value="'.($data->item_id ?? null).'" name="subject_id" readonly>
                    <button type="button-submit" class="btn btn-success"><i class="fa fa-save"></i> Save Record</button>
                </div>
            </div>
        </form>';

        return $form_content;

    }
    
    /**
     * Load the Payment History
     * 
     * @param String $type
     * @param String $record_id
     * 
     * @return String
     */
    public function view_payment_history($type, $record_id) {

        $types_array = [
            "policy_payment_history" => "policy"
        ];

        /** Load the Payment History */
        $params = (object) [
            "record_type" => $types_array[$type] ?? null,
            "record_id" => $record_id,
            "status" => "Paid"
        ];
        $payments_list = load_class("payments", "controllers")->list($params)["data"];

        // initialize the table
        $payment_info = "
        <div>
            <table class='table table-hover dataTable table-bordered'>
                <thead>
                    <th width='6%'>#</th>
                    <th>Info</th>
                    <th width='20%'>Amount</th>
                    <th width='15%'>Date</th>
                </thead>
                <tbody>
        ";

        // init
        $total_payment = 0;

        // confirm that the result is not empty
        if(!empty($payments_list) && is_array($payments_list)) {
            // loop through the results
            foreach($payments_list as $key => $payment) {
                // increment the amount paid
                $total_payment += $payment->amount;

                // set the table content
                $payment_info .= "
                    <tr>
                        <td>".($key+1)."</td>
                        <td>
                            <i class='fa fa-angle-double-right'></i> {$payment->payment_info->r_switch} <br>
                            <i class='fa fa-phone'></i> {$payment->payment_info->subscriber_number} <br>
                            <i class='fa fa-chalkboard-teacher'></i> ".ucwords($payment->payment_option)." <br>
                        </td>
                        <td>{$payment->amount}</td>
                        <td>{$payment->payment_date}</td>
                    </tr>
                ";
            }
            // format the total_payment
            $total_payment = number_format($total_payment, 2);
            // set the last row
            $payment_info .= "
                <tr class='font-weight-bolder'>
                    <td></td>
                    <td>Total</td>
                    <td>{$total_payment}</td>
                    <td></td>
                </tr>
            ";
        }
        $payment_info .= "
                </tbody>
            </table>
            <div class=\"col-md-12 mt-4 border-top pt-4 mb-3 text-center\"><span class=\"btn btn-outline-secondary\" data-dismiss='modal'>Close Modal</span></div>
            </div>";

        return $payment_info;

    }
}
?>