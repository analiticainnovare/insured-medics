<?php 

class Company_policy extends Medics {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Load the list of insurance policies for a company
     * 
     * @param \stdClass $params
     * 
     * @return Object
     */
    public function list(stdClass $params) {

        $params->query = "1";

        global $accessObject;

        // set the user type
        $the_user_type = isset($params->userData) ? $params->userData->user_type : null;
        
        // if the user id parameter was not parsed
		if(!isset($params->policy_type_id)) {
			// perform some checks
			$the_user_type = $params->userData->user_type;
			// go ahead
			if(in_array($the_user_type, ["insurance_company", "agent", "bancassurance", "bank"])) {
				$params->or_clause = " AND (a.company_id = '{$params->userData->company_id}')";
			}
            // if the user is not an agent or has 
            if(in_array($the_user_type, ["user", "business", "nic", "admin"])) {
				$params->or_clause = " AND (a.policy_status = 'Enrolled')";
			}
		}

		// if the field is null
		$params->query .= (isset($params->policy_type_id) && !empty($params->policy_type_id)) ? (preg_match("/^[0-9]+$/", $params->policy_type_id) ? " AND a.id='{$params->policy_type_id}'" : " AND a.item_id='{$params->policy_type_id}'") : null;
		$params->query .= (isset($params->status)) ? " AND a.policy_status='{$params->status}'" : null;
        $params->query .= (isset($params->company_id) && !empty($params->company_id)) ? " AND a.company_id='{$params->company_id}'" : null;
        $params->query .= (isset($params->or_clause) && !empty($params->or_clause)) ? $params->or_clause : null;
        $params->query .= (isset($params->policy_id) && !empty($params->policy_id)) ? " AND a.policy_id='{$params->policy_id}'" : null;
        $params->query .= (isset($params->category) && !empty($params->category)) ? " AND a.category='{$params->category}'" : null;
		$params->query .= (isset($params->date) && !empty($params->date)) ? " AND DATE(a.date_created) ='{$params->date_created}'" : null;
        $params->query .= (isset($params->date_range) && !empty($params->date_range)) ? $this->dateRange($params->date_range, "a") : null;

		// the number of rows to limit the query
		$params->limit = isset($params->limit) ? $params->limit : $this->global_limit;

        // this user has the permission to add a policy
        $addPolicy = $accessObject->hasAccess("add", "company_policy");
        $updatePolicy = $accessObject->hasAccess("update", "company_policy");
        $deletePolicy = $accessObject->hasAccess("delete", "company_policy");

        try {
            
            // make the request for the record from the model
            $stmt = $this->db->prepare("
                SELECT 
                    ".(isset($params->columns) ? $params->columns : "
                        a.*, a.name AS policy_name, a.item_id AS policy_type_id, 
                        (SELECT COUNT(*) FROM users_policy u WHERE u.policy_type = a.policy_id AND u.policy_status='Approved') AS clients_enrolled,
                        (SELECT COUNT(*) FROM users_policy_claims u WHERE u.policy_type = a.policy_id AND u.status='Confirmed') AS active_claims,
                        (SELECT COUNT(*) FROM users_policy_claims u WHERE u.policy_type = a.policy_id AND u.status NOT IN('Approved','Pending','Rejected')) AS total_claims,
                        (SELECT b.description FROM files_attachment b WHERE b.record_id = a.item_id ORDER BY b.id DESC LIMIT 1) AS attachment,
                        (SELECT name FROM users WHERE users.item_id = a.created_by LIMIT 1) AS created_by_name
                    ").", (SELECT name FROM companies WHERE companies.item_id = a.company_id LIMIT 1) AS company_name
                FROM policy_types a
                WHERE {$params->query} AND a.deleted='0' ORDER BY a.name LIMIT {$params->limit}
            ");
            
            $stmt->execute();

            // create new object
            $filesObject = load_class("forms", "controllers");
            
            $row = 0;
            $data = [];
            while($result = $stmt->fetch(PDO::FETCH_OBJ)) {
                // unset two parameters from the result set
                unset($result->id);

                // run this section if not a limited data was requested
                if(!isset($params->limited_data)) {

                    // if description was requested
                    $result->description = isset($result->description) ? custom_clean(htmlspecialchars_decode($result->description)) : null;
                    $result->requirements = isset($result->requirements) ? custom_clean(htmlspecialchars_decode($result->requirements)) : null;
                    
                    // decode some more variables
                    if(isset($result->policy_form)) {
                        $result->claims_form = json_decode($result->claims_form);
                        $result->policy_form = json_decode($result->policy_form);
                    }

                    // clean the category
                    if(isset($result->category)) {
                        $result->policy_category = ucwords(str_replace(["\"","-",","], ["", " ",", "], $result->category));
                    }

                    // plain policy name
                    $result->plain_name = $result->policy_name;
                    
                    // if a user or broker is viewing
                    if(!in_array($the_user_type, ["insurance_company", "agent"])) {
                        $result->policy_name .= "<br><strong>by:</strong> <span title=\"Click to view company details\" onclick=\"return company_basic_information('{$result->company_id}')\" class=\"badge cursor text-primary\">{$result->company_name}</span>";
                    }

                    // the action button to return
                    $result->action = "";
                    $result->attachment_html = "";

                    // if the policy id is parsed
                    if(isset($result->policy_type_id)) {
                        
                        // the label
                        $result->the_status_label = $this->the_status_label($result->policy_status);

                        // if not a remote request
                        if((isset($params->remote) && !$params->remote) || !isset($params->remote)) {
                            
                            // parse the action
                            if($updatePolicy) {
                                // if the user has the permissio to add a policy
                                $result->action = "<a class='btn btn-outline-success p-1 m-0 btn-sm' title='Click to view details' href='{$this->baseUrl}policy-view/{$result->policy_type_id}'><i class='fa fa-eye'></i></a>";
                            } else {
                                $result->action = "<a class='btn btn-outline-success p-1 m-0 btn-sm' title='Click to view details' href=\"javascript:void(0)\" onclick=\"return policy_basic_information('{$result->policy_type_id}')\"><i class='fa fa-eye'></i></a>";
                            }

                            // append the claim form information
                            if($updatePolicy && isset($result->policy_status)) {
                                $result->action .= " &nbsp; <a class='btn btn-outline-warning m-0 btn-sm' title='Click to view details of the Policy Claim Form' href='{$this->baseUrl}policy-claim-form/{$result->policy_type_id}'>Claim Form</a>";
                            }
                            
                            // if the user has permission to delete a policy
                            if($deletePolicy && isset($result->policy_status)) {
                                // if this user created the complaint and its still in a draft stage
                                if($result->policy_status == "Pending") {
                                    $result->action .= " &nbsp; <a class='btn btn-outline-danger p-1 m-0 modify-record btn-sm' href='javascript:void(0)' title='Click to delete policy' onclick='return delete_record(\"policy_type\",\"{$result->policy_type_id}\");'><i class='fa fa-trash-alt'></i></a>";
                                }
                            }

                        }
                    }

                    $result->attachment = isset($result->attachment) ? json_decode($result->attachment) : $this->fake_files;

                    // related to details if parsed
                    if(!empty($result->attachment->files)) {
                        $result->attachment_html = $filesObject->list_attachments($result->attachment->files, $params->userId, "col-lg-4 col-md-6", false, false);
                    }

                }

                // append the policy status to the name to list
                if(isset($result->item_name) && isset($result->policy_status)) {
                    $result->item_name = "{$result->item_name} ($result->policy_status)";
                }

                $row++;
                $result->row_id = $row;

                $data[] = $result;
            }

            // return the list only
            if(isset($params->minimal_load)) {
                return $data;
            }
            
            // return the data
            return [
                "data" => $data,
                "code" => !empty($data) ? 200 : 201
            ];
        } catch(PDOException $e) {
            return $e->getMessage();
        }

    }

    /**
     * Add a new company policy
     * 
     * @param \stdClass $params
     * 
     * @return Object
     */
    public function add(stdClass $params) {

        /** Confirm the user has the required permissions */
        global $accessObject;
        if(!$accessObject->hasAccess("add", "company_policy")) {
            return ["data" => $this->permission_denied, "code" => 501];
        }

        /** confirm that the form is not empty  */
        if(!isset($params->form)) {
            return ["data" => "Sorry! The policy form cannot be empty", "code" => 203];
        } 

        /** Convert the form data into an array json */
        $params->form = json_decode($params->form, true);
        
        if(!is_array($params->form)) {
            return ["data" => "Sorry! The policy form must be an array", "code" => 203];
        }

        /** Loop through the form and rearrange the data */
        $formArray = $params->form;
        
        /** Validate the user variables parsed */
        $params->_item_id = random_string("alnum", 32);

        /** Clean the text */
        $params->policy_description = addslashes($params->policy_description);
        $params->policy_requirements = isset($params->policy_requirements) ? addslashes($params->policy_requirements) : null;
        $params->form_footnote = isset($params->form_footnote) ? addslashes(nl2br($params->form_footnote)) : null;
        $params->allow_attachment = isset($params->allow_attachment) ? $params->allow_attachment : "yes";
        $params->policy_category = isset($params->policy_category) && is_array($params->policy_category) ? "\"".implode("\",\"", $params->policy_category)."\"" : $params->policy_category;

        /** Policy form data */
        $formData = [
            "fields" => $formArray,
            "form_footnote" => $params->form_footnote,
            "allow_attachment" => $params->allow_attachment
        ];

        // append the attachments
        $filesObj = load_class("files", "controllers");
        $attachments = $filesObj->prep_attachments("company_policy", $params->userData->user_id, $params->_item_id);

        /** Insert the record */
        try {
            $this->db->beginTransaction();
            
            $stmt = $this->db->prepare("
                INSERT INTO policy_types SET created_by = ?, date_created = now(), company_id = ?
                ".(isset($params->_item_id) ? ",item_id='{$params->_item_id}'" : null)."
                ".(isset($params->policy_category) ? ",category='{$params->policy_category}'" : null)."
                ".(isset($params->policy_description) ? ",description='{$params->policy_description}'" : null)."
                ".(isset($params->policy_name) ? ",name='{$params->policy_name}'" : null)."
                ".(isset($formData) ? ",policy_form='".json_encode($formData)."'" : null)."
                ".(isset($params->policy_id) ? ",policy_id='{$params->policy_id}'" : null)."
                ".(isset($params->policy_id) ? ",policy_code='{$params->policy_id}'" : null)."
                ".(!empty($params->policy_requirements) ? ",requirements='{$params->policy_requirements}'" : null)."
                ".(isset($params->policy_name) ? ",slug='".create_slug($params->policy_name)."'" : null)."
                ".(isset($params->year_enrolled) ? ",year_enrolled='{$params->year_enrolled}'" : null)."
            ");
            $stmt->execute([$params->userId, $params->userData->company_id]);

            // insert attachment
            $files = $this->db->prepare("INSERT INTO files_attachment SET resource= ?, resource_id = ?, description = ?, record_id = ?, created_by = ?, attachment_size = ?");
            $files->execute(["company_policy", $params->_item_id, json_encode($attachments), "{$params->_item_id}", $params->userId, $attachments["raw_size_mb"]]);

            // log the user activity
            $this->userLogs("company_policy", $params->_item_id, null, "<strong>{$params->userData->name}</strong> created a new policy: {$params->policy_name}.", $params->userId);

            // commit the transaction
            $this->db->commit();

            // return the success response
            return [
                "code" => 200,
                "data" => "The policy has been successfully saved! Proceed to add the claim form.",
                "additional" => [
                    "clear" => true,
                    "reload" => "{$this->baseUrl}policy-claim-form/{$params->_item_id}",
                ],
                "record_id" => $params->_item_id
            ];

        } catch(PDOException $e) {
            $this->db->rollBack();
            return $e->getMessage();
        }

    }

    /**
     * update a new company policy
     * 
     * @param \stdClass $params
     * 
     * @return Object
     */
    public function update(stdClass $params) {

        /** Confirm the user has the required permissions */
        global $accessObject;
        if(!$accessObject->hasAccess("add", "company_policy")) {
            return ["data" => $this->permission_denied, "code" => 501];
        }

        /** confirm that the form is not empty  */
        if(!isset($params->form)) {
            return ["data" => "Sorry! The policy form cannot be empty", "code" => 203];
        }

        /** Convert the form data into an array json */
        $params->form = json_decode($params->form, true);
        
        if(!is_array($params->form)) {
            return ["data" => "Sorry! The policy form must be an array", "code" => 203];
        }
        
        /** Confirm that this policy does not already exist */
        $policyCheck = $this->pushQuery("a.*, (SELECT b.description FROM files_attachment b WHERE b.record_id = a.item_id) AS attachment", 
            "policy_types a", "a.item_id='{$params->item_id}' AND a.deleted='0' AND a.company_id='{$params->userData->company_id}' LIMIT 1");

        // if the policy check is not empty
        if(empty($policyCheck)) {
            return ["data" => "Sorry! The policy record was not found"];
        }

        // convert the policy id to uppercase
        $params->policy_id = strtoupper($params->policy_id);

        /** Run another check */
        if($policyCheck[0]->policy_id !== $params->policy_id) {
            /** Lets confirm that the new policy id does not already exist */
            $policyCheck_2 = $this->pushQuery("id", "policy_types", "policy_id='{$params->policy_id}' AND deleted='0' AND company_id='{$params->userData->company_id}' LIMIT 1");
            /** If not empty then alert error */
            if(!empty($policyCheck_2)) {
                return ["data" => "Sorry! There is an existing record with the same policy id"];
            }
        }

        // initialize
        $initial_attachment = [];

        /** Confirm that there is an attached document */
        if(!empty($policyCheck[0]->attachment)) {
            // decode the json string
            $db_attachments = json_decode($policyCheck[0]->attachment);
            // get the files
            if(isset($db_attachments->files)) {
                $initial_attachment = $db_attachments->files;
            }
        }

        // initials
        $form_data = null;
        $footnote = null;

        /** Get the form data */
        if(!empty($policyCheck[0]->policy_form)) {
            // decode the json string
            $policy_form = json_decode($policyCheck[0]->policy_form);
            // get the form fields
            if(isset($policy_form->fields)) {
                $form_data = json_encode($policy_form->fields);
            }
            // if the footnote is set
            if(isset($policy_form->form_footnote)) {
                $footnote = $policy_form->form_footnote;
            }
        }

        /** Loop through the form and rearrange the data */
        $formArray = $params->form;
        
        /** Clean the text */
        $params->policy_description = addslashes($params->policy_description);
        $params->form_footnote = isset($params->form_footnote) ? addslashes(nl2br($params->form_footnote)) : null;
        $params->allow_attachment = isset($params->allow_attachment) ? $params->allow_attachment : "yes";
        $params->policy_category = isset($params->policy_category) ? $params->policy_category : null;

        /** Policy form data */
        $formData = [
            "fields" => $formArray,
            "form_footnote" => $params->form_footnote,
            "allow_attachment" => $params->allow_attachment
        ];

        // modify the return log
        $return_log = [
            "code" => 200,
            "data" => "The policy has been successfully updated!",
            "additional" => []
        ];

        // define the module
        $module = "company_policy_{$params->item_id}";

        // if there is a session log for attached files
        if(!empty($this->session->$module)) {
            // set the return log
            $return_log["additional"]["reload"] = "{$this->baseUrl}policy-view/{$params->item_id}";
        }

        // append the attachments
        $filesObj = load_class("files", "controllers");
        $attachments = $filesObj->prep_attachments($module, $params->userData->user_id, $params->item_id, $initial_attachment);

        try {
            // return $attachments;
            $this->db->beginTransaction();
            
            /** update the record */
            $stmt = $this->db->prepare("UPDATE policy_types SET date_updated = now()
                ".(!empty($params->policy_category) ? ",category='{$params->policy_category}'" : null)."
                ".(isset($params->policy_description) ? ",description='{$params->policy_description}'" : null)."
                ".(isset($params->policy_requirements) ? ",requirements='".addslashes($params->policy_requirements)."'" : null)."
                ".(isset($params->policy_name) ? ",name='{$params->policy_name}'" : null)."
                ".(isset($params->policy_id) ? ",policy_id='{$params->policy_id}'" : null)."
                ".(isset($params->policy_id) ? ",policy_code='{$params->policy_id}'" : null)."
                ".(isset($formData) ? ",policy_form='".json_encode($formData)."'" : null)."
                ".(isset($params->year_enrolled) ? ",year_enrolled='{$params->year_enrolled}'" : null)."
                ".(isset($params->policy_name) ? ",slug='".create_slug($params->policy_name)."'" : null)."
                ".(isset($params->status) ? ",policy_status='{$params->status}'" : null)."
                WHERE company_id = ? AND item_id = ? LIMIT 1
            ");
            $stmt->execute([$params->userData->company_id, $params->item_id]);

            // update attachment if already existing
            if(isset($db_attachments)) {
                $files = $this->db->prepare("UPDATE files_attachment SET description = ?, attachment_size = ? WHERE record_id = ? LIMIT 1");
                $files->execute([json_encode($attachments), $attachments["raw_size_mb"], $params->item_id]);
            } else {
                // insert the record if not already existing
                $files = $this->db->prepare("INSERT INTO files_attachment SET resource= ?, resource_id = ?, description = ?, record_id = ?, created_by = ?, attachment_size = ?");
                $files->execute(["company_policy", $params->item_id, json_encode($attachments), "{$params->item_id}", $params->userId, $attachments["raw_size_mb"]]);
            }

            // log the user activity
            // $this->userLogs("company_policy", $params->item_id, $policyCheck[0], "<strong>{$params->userData->name}</strong> triggered an update request to this Policy.", $params->userId);

            // if the previous is not the same as the current
            if($form_data !== json_encode($formArray)) {
                // save the form change information
                $this->userLogs("company_policy", $params->item_id, $form_data, "Policy form content has been altered.", $params->userId);
                // set the return log
                $return_log["additional"]["reload"] = "{$this->baseUrl}policy-view/{$params->item_id}";
            }
                        
            // if the previous is not the same as the current
            if($footnote !== $params->form_footnote) {
                // save the footnote change information
                $this->userLogs("company_policy", $params->item_id, $footnote, "Form footnote was changed from: <br><strong>{$footnote}</strong>.", $params->userId);
            }
            // save the log for the status change
            if(isset($params->status) && ($policyCheck[0]->policy_status !== $params->status)) {
                // set the return log
                $return_log["additional"]["reload"] = "{$this->baseUrl}policy-view/{$params->item_id}";
                // save the status change information
                $this->userLogs("company_policy", $params->item_id, $policyCheck[0]->policy_status, "<strong>Policy Status</strong> was changed from <strong>{$policyCheck[0]->policy_status}</strong> to <strong>{$params->status}</strong>.", $params->userId);
            }
            // save the policy description information change
            if(isset($params->policy_description) && ($policyCheck[0]->description !== $params->policy_description)) {
                // save the status change information
                $this->userLogs("company_policy", $params->item_id, $policyCheck[0]->description, "The <strong>policy description</strong> was altered.", $params->userId);
            }
            // save the policy requirements information change
            if(isset($params->policy_requirements) && ($policyCheck[0]->requirements !== $params->policy_requirements)) {
                // save the status change information
                $this->userLogs("company_policy", $params->item_id, $policyCheck[0]->requirements, "The <strong>requirements</strong> for the policy was altered.", $params->userId);
            }
            // if the policy id was changed
            if(isset($params->policy_id) && ($policyCheck[0]->policy_id !== $params->policy_id)) {
                // save the status change information
                $this->userLogs("company_policy", $params->item_id, $policyCheck[0]->policy_id, "<strong>Policy Code</strong> was changed from <strong>{$policyCheck[0]->policy_id}</strong> to <strong>{$params->policy_id}</strong>.", $params->userId);
            }
            // if the enrollment year
            if(isset($params->year_enrolled) && ($policyCheck[0]->year_enrolled !== $params->year_enrolled)) {
                // save the status change information
                $this->userLogs("company_policy", $params->item_id, $policyCheck[0]->policy_id, "<strong>Enrollment year</strong> was changed to <strong>{$params->year_enrolled}</strong>.", $params->userId);
            }
            // if the policy name was changed
            if(isset($params->policy_name) && ($policyCheck[0]->slug !== create_slug($params->policy_name))) {
                // save the status change information
                $this->userLogs("company_policy", $params->item_id, $policyCheck[0]->name, "<strong>Policy Name</strong> was changed from <strong>{$policyCheck[0]->name}</strong> to <strong>{$params->policy_name}</strong>.", $params->userId);
            }
            // commit the transaction
            $this->db->commit();

            // return the success response
            return $return_log;

        } catch(PDOException $e) {
            $this->db->rollBack();
            return $e->getMessage();
        }

    }

	/**
	 * array_keys_count
	 * 
	 * This loops through the array list and gets the count
	 */
	public function policy_status_array_count($array) {

		if(!is_array($array)) {
			return;
		}
        
		$status_count = [];

		// loop through the list of array values
		foreach($array as $key => $value) {
			// append to the clients count
			$status_count[$value->policy_status] = isset($status_count[$value->policy_status]) ? 
                $status_count[$value->policy_status] + $value->policy_application_count : $value->policy_application_count;
		}
		return $status_count;
	}

}