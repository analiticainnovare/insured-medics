<?php
class Reports extends Medics {

    /** This variable will be used for the loading of the information */
    public $stream = [];
    public $current_title = "Today";
    public $previous_title = "Yesterday";
    public $final_report = [];

    public function __construct() {
        global $medics;

        $this->db = $medics;
        $this->baseUrl = config_item('base_url');
        
        $this->default_stream = [
            "summary_report", "policy_report", "claims_report", "clients_report", 
            "company_policy_report", "users_report"
        ];

        $this->accepted_period = [
            "this_week", "last_week", "last_14days", "last_30days", "today",
            "this_month", "last_month", "last_3months", "last_6months", "this_year", "last_year", 
            "weekly", "bi-weekly", "monthly", "quarterly"
        ];

        $this->error_codes = [
            "invalid-date" => "Sorry! An invalid date was parsed for the start date.",
            "invalid-range" => "Sorry! An invalid date was parsed for the end date.",
            "exceeds-today" => "Sorry! The date date must not exceed today's date",
            "exceeds-count" => "Sorry! The days between the two ranges must not exceed 90 days",
            "invalid-prevdate" => "Sorry! The start date must not exceed the end date.",
        ];
    }

    /**
     * List report requests
     * 
     * @param stdClass $params
     * 
     * @return Array
     */
    public function requests_list(stdClass $params) {

        try {

            $where_clause = "1";
            $params->limit = isset($params->limit) ? $params->limit : $this->global_limit;
            
            $where_clause .= in_array($params->userData->user_type, ["insurance_company"]) ? " AND a.requested_from = '{$params->userData->company_id}'" : null;
            $where_clause .= isset($params->request_id) ? " AND a.item_id = '{$params->request_id}'" : null;

            // load the very first record in the query parsed by the user
            $stmt = $this->db->prepare("
                SELECT a.*,
                    (SELECT CONCAT(name,'|',email,'|',contact,'|',address,'|',website,'|',logo) FROM companies WHERE companies.item_id = a.requested_from LIMIT 1) AS company_info,
                    (SELECT CONCAT(name,'|',phone_number,'|',email,'|',image,'|',last_seen,'|',online) FROM users WHERE users.item_id = a.created_by LIMIT 1) AS requested_by_info
                FROM reports_request a 
                WHERE {$where_clause} AND a.status = '1' ORDER BY a.id ASC LIMIT {$params->limit}
            ");
            $stmt->execute();
            
            $data = [];
            $row = 0;
            while($result = $stmt->fetch(PDO::FETCH_OBJ)) {
                $row++;
                $result->row_id = $row;

                $result->action = "";
                $result->description = htmlspecialchars_decode($result->description);

                // view buttons
                $result->action .= " &nbsp; <a class='btn p-1 btn-outline-success m-0 btn-sm' title='Click to view details of this request' href='{$this->baseUrl}requests-view/{$result->item_id}'><i class='fa fa-eye'></i></a>";

                // show the delete button if the person who requested for it is logged in
                if(($result->created_by == $params->userId) && ($result->state == "Pending")) {
                    $result->action .= " &nbsp; <a onclick='return delete_record(\"report_request\",\"{$result->item_id}\");' class='btn btn-outline-danger p-1 m-0 modify-record btn-sm' href='javascript:void(0)' title='Click to delete request'><i class='fa fa-trash-alt'></i></a>";
                }

                // status lable
                $result->the_status_label = $this->the_status_label($result->state);

                // convert into object
                $result->requested_by_info = (object) $this->stringToArray($result->requested_by_info, "|", ["name", "contact", "email", "image","last_seen","online"]);
                $result->company_info = !empty($result->company_info) ? (object) $this->stringToArray($result->company_info, "|", ["name", "email", "contact", "address", "website", "logo"], true) : (object) [];

                $result->subject_name = "{$result->subject}
                    <br><p><i class=\"fa fa-user\"></i> <span class=\"underline\" onclick=\"return user_basic_information('$result->created_by')\">{$result->requested_by_info->name}</span></p>";
                // append to the data
                $data[] = $result;
            }

            return [
                "code" => 200,
                "data" => $data
            ];

        } catch(PDOException $e) {}
        
    }

    /**
     * Add a new request
     * 
     * @param stdClass $params
     * 
     * @return Array
     */
    public function add_request(stdClass $params) {

        // begin transaction
        $this->db->beginTransaction();

        try {

            /** Create a random string */
            $item_id = random_string("alnum", 32);

            // insert the record
            $stmt = $this->db->prepare("
                INSERT INTO reports_request SET date_created = now()
                ".(isset($params->userId) ? ",created_by='{$params->userId}'" : null)."
                ".(isset($item_id) ? ",item_id='{$item_id}'" : null)."
                ".(isset($params->subject) ? ",subject='{$params->subject}'" : null)."
                ".(isset($params->requested_from) ? ",company_id='{$params->requested_from}'" : null)."
                ".(isset($params->message) ? ",description='{$params->message}'" : null)."
                ".(isset($params->request_type) ? ",request_type='{$params->request_type}'" : null)."
                ".(isset($params->expected_date) ? ",expected_date='{$params->expected_date}'" : null)."
                ".(isset($params->start_date) ? ",start_date='{$params->start_date}'" : null)."
                ".(isset($params->end_date) ? ",end_date='{$params->end_date}'" : null)."
                ".(isset($params->requested_from) ? ",requested_from='{$params->requested_from}'" : null)."
            ");
            $stmt->execute();

            // log the user activity
            $this->userLogs("reports_request", $item_id, null, "<strong>{$params->userData->name}</strong> applied for a Report.", $params->userId);
            
            // commit the transaction
            $this->db->commit();

            return [
                "code" => 200,
                "data" => "Request was successfully placed.",
                "additional" => [
                    "clear" => true,
                    "reload" => "{$this->baseUrl}requests-view/{$item_id}",
                ]
            ];
        
        } catch(PDOException $e) {
            // roll back the transaction
            $this->db->rollBack();
        }

    }

    /**
     * Update the details the report request
     * 
     * @param stdClass $params
     * 
     * @return Array
     */
    public function update_request(stdClass $params) {

        // begin transaction
        $this->db->beginTransaction();

        // modify the return log
        $return_log = [
            "code" => 200,
            "data" => "Request was successfully updated.",
            "additional" => []
        ];

        try {

            // load the previous dataset
            $prevData = $this->pushQuery(
                "a.*", "reports_request a",
                "a.company_id = '{$params->userData->company_id}' AND a.item_id='{$params->request_id}'"
            );

            // end the query
            if(empty($prevData)) {
                return;
            }

            // get the first key
            $prevData = $prevData[0];

            // update the record
            $stmt = $this->db->prepare("
                UPDATE reports_request SET date_updated = now()
                ".(isset($params->userId) ? ",created_by='{$params->userId}'" : null)."
                ".(isset($params->state) ? ",state='{$params->state}'" : null)."
                ".(isset($params->record_id) ? ",record_id='{$params->record_id}'" : null)."
                ".(isset($params->expected_date) ? ",expected_date='{$params->expected_date}'" : null)."
                ".(isset($params->start_date) ? ",start_date='{$params->start_date}'" : null)."
                ".(isset($params->end_date) ? ",end_date='{$params->end_date}'" : null)."
                ".(isset($params->export_type) ? ",export_type='{$params->export_type}'" : null)."
                WHERE item_id = ? LIMIT 1
            ");
            $stmt->execute([$params->request_id]);

            // log the user activity
            $this->userLogs("reports_request", $params->request_id, null, "<strong>{$params->userData->name}</strong> triggered an update to the report request details.", $params->userId);
            
            if(isset($params->state) && ($prevData->state !== $params->state)) {
                $this->userLogs("reports_request", $params->request_id, $prevData->state, "Request Status was changed from {$prevData->state} to {$params->state}.", $params->userId);
                $return_log["additional"]["reload"] = "{$this->baseUrl}requests-view/{$params->request_id}";
            }

            if(isset($params->start_date) && ($prevData->start_date !== $params->start_date)) {
                $this->userLogs("reports_request", $params->request_id, $prevData->start_date, "Report Start Date was changed from {$prevData->start_date} to {$params->start_date}.", $params->userId);
            }

            if(isset($params->end_date) && ($prevData->end_date !== $params->end_date)) {
                $this->userLogs("reports_request", $params->request_id, $prevData->end_date, "Report End Date was changed from {$prevData->end_date} to {$params->end_date}.", $params->userId);
            }
            
            if(isset($params->record_id) && ($prevData->record_id !== $params->record_id)) {
                $this->userLogs("reports_request", $params->request_id, $prevData->record_id, "The Record ID was changed from {$prevData->record_id} to {$params->record_id}.", $params->userId);
            }

            // commit the transaction
            $this->db->commit();

            return $return_log;
        
        } catch(PDOException $e) {
            // roll back the transaction
            $this->db->rollBack();
        }

    }

    /**
     * Load additional content to to display on the reports generation page
     * 
     * @param String $params->item
     * @param String $params->company_id
     * 
     * @return Array
     */
    public function request_content(stdClass $params) {

        $result = "";
        $record_id = isset($params->record_id)  ? $params->record_id : null;
        
        // if the user is requesting for claims report
        if($params->item == "claims") {

            // make the request
            $request = $this->pushQuery(
                "a.item_id, a.policy_id, (SELECT b.name FROM users b WHERE b.item_id = a.user_id) AS fullname", 
                "users_policy_claims a",
                "a.company_id = '{$params->company_id}' AND a.deleted = '0' AND a.status != 'Pending'"
            );
            $result .= "<div class='mt-1'>";
            $result .= "<label>Select Claim Record</label>";
            $result .= "<select class='selectpicker form-control' name='record_id' id='record_id'>";
            $result .= "<option value='all_record'>Generate on All Claims</option>";
            foreach($request as $each) {
                $result .= "<option ".($record_id == $each->item_id ? "selected" : null)." value='{$each->item_id}'>{$each->fullname} ($each->policy_id)</option>";
            }
            $result .= "</select>";
            $result .= "</div>";
        }

        return $result;
    }

    /**
     * This will be used for the generation of reports
     * 
     * @param stdClass $params
     * 
     * @return Array
     */
    public function generate(stdClass $params) {

        /** Global variables */
        global $usersClass;

        /** set the date period */
        $params->period = $params->period ?? "this_week";

        /** Confirm the period */
        if($params->period == "load_report_filters") {
            /** Generate the items */
            return $this->load_report_filters($params);
        }

        /** Convert the stream into an array list */
        $params->stream = isset($params->label["stream"]) && !empty($params->label["stream"]) ? $this->stringToArray($params->label["stream"]) : $this->default_stream;

        /** Preformat date */
        if(in_array($params->period, $this->accepted_period)) {

            /** Get the date formatter */
            $the_date = $this->format_date($params->period);

            /** Save the user period parsed */
            $user_pref = (object) [
                "userData" => $params->userData,
                "reports_auto_push" => true,
                "label" => [
                    "reports" => [
                        "period" => $params->period
                    ]
                ]
            ];
            $usersClass->preference($user_pref);
            
        } else {
            $the_date = $this->preformat_date($params->period);
        }

        /** If invalid date then end the query */
        if(in_array($the_date, array_keys($this->error_codes))) {
            return ["code" => 203, "data" => $this->error_codes[$the_date]];
        }

        // date ranges to use for the query
        $this->date_range = [
            "previous" => [
                "start" => $this->prevstart_date,
                "end" => $this->prevend_date,
                "title" => $this->previous_title
            ],
            "current" => [
                "start" => $this->start_date,
                "end" => $this->end_date,
                "title" => $this->current_title
            ]
        ];

        // confirm if the user pushed a query set
        $this->query_date_range = isset($params->label["stream_period"]) ? $this->stringToArray($params->label["stream_period"]) : ["current", "previous"];

        /** Filter checks */
        $this->policy_status = isset($params->label["policy_status"]) ? "IN {$this->inList($params->label["policy_status"])}" : "NOT IN ('Deleted','Inactive','Ellapsed')";
        $this->claim_status = isset($params->label["claim_status"]) ? $params->label["claim_status"] : "Approved";
        $this->complaint_status = isset($params->label["complaint_status"]) ? $params->label["complaint_status"] : "Solved";
        $this->payment_status = isset($params->label["payment_status"]) ? $params->label["payment_status"] : "Paid";
        $this->user_query = isset($params->label["user_id"]) ? " AND b.user_id = '{$params->label["user_id"]}'" : null;
        $this->created_by_query_sub = isset($params->label["created_by"]) ? " AND b.created_by = '{$params->label["created_by"]}'" : null;
        $this->created_by_query_main = isset($params->label["created_by"]) ? " AND a.created_by = '{$params->label["created_by"]}'" : null;
        $this->assigned_to = isset($params->label["assigned_to"]) ? " AND b.assigned_to = '{$params->label["assigned_to"]}'" : null;

        $this->company_policy_status = isset($params->label["company_policy_status"]) ? " IN {$this->inList($params->label["company_policy_status"])}" : " IN ('Pending','In Review','Enrolled')";
        $this->company_policy_status_sub = isset($params->label["company_policy_status"]) ? " IN {$this->inList($params->label["company_policy_status"])}" : " IN ('Pending','In Review','Enrolled')";

        $this->company_id_sub = "";
        $this->company_id_main = "";

        // if the user has these permissions then limit the results to the company id
        if(in_array($params->userData->user_type, ["insurance_company", "agent", "bank", "bancassurance"])) {
            $this->company_id_main = " AND a.company_id = '{$params->userData->company_id}'";
            $this->company_id_sub = " AND b.company_id = '{$params->userData->company_id}'";
            
            // broker checker
            if(in_array($params->userData->user_type, ["agent"])) {
                $this->created_by_query_main = " AND (a.created_by = '{$params->userData->user_id}' OR a.assigned_to = '{$params->userData->user_id}')"; 
                $this->created_by_query_sub = " AND (b.created_by = '{$params->userData->user_id}' OR b.assigned_to = '{$params->userData->user_id}')";
            }
        }

        // broker checker
        elseif(in_array($params->userData->user_type, ["broker"])) {
            $this->created_by_query_main = " AND a.created_by = '{$params->userData->user_id}'";
            $this->created_by_query_sub = " AND b.created_by = '{$params->userData->user_id}'";
        }

        // append the summary if it is in the array list
        if(in_array("summary_report", $params->stream)) {

            // loop through the date ranges for the current and previous
            foreach($this->date_range as $range_key => $range_value) {

                // confirm that the period was in the request
                if(in_array($range_key, $this->query_date_range)) {

                    /** Load the information */
                    $stmt = $this->db->prepare("
                        SELECT 
                            (SELECT COUNT(*) FROM users_policy b 
                                WHERE b.policy_status {$this->policy_status} AND b.deleted='0'
                                AND (DATE(b.date_submitted) >= '{$range_value["start"]}' AND DATE(b.date_submitted) <= '{$range_value["end"]}')
                                {$this->user_query} {$this->created_by_query_sub} {$this->assigned_to}
                            ) AS user_policy_count,
                            (SELECT COUNT(*) FROM users_policy_claims b 
                                WHERE b.status {$this->policy_status} AND b.deleted='0'
                                AND (DATE(b.date_submitted) >= '{$range_value["start"]}' AND DATE(b.date_submitted) <= '{$range_value["end"]}')
                                {$this->user_query} {$this->created_by_query_sub} {$this->assigned_to}
                            ) AS user_policy_claims_count,
                            (SELECT COUNT(*) FROM users_complaints b 
                                WHERE b.status = '{$this->complaint_status}' AND b.deleted='0'
                                AND (DATE(b.date_created) >= '{$range_value["start"]}' AND DATE(b.date_created) <= '{$range_value["end"]}')
                                {$this->user_query} {$this->assigned_to}
                            ) AS user_complaints_count
                        FROM users a 
                        WHERE a.item_id = ? LIMIT 1
                    ");
                    $stmt->execute([$params->userData->user_id]);

                    $summary = $stmt->fetchAll(PDO::FETCH_OBJ);

                    $this->final_report["summary_report"][$range_key]["period"] = $range_value;
                    $this->final_report["summary_report"][$range_key]["data"] = (array) $summary[0];
                    
                }

            }

            // get the clients information if not parsed in the stream
            if(!in_array("clients_report", $params->stream)) {
                // query the clients data
                $this->clients_report($params);
                // get the percentage difference between the current and previous
                $this->array_percentage_diff($this->final_report);
            }

        }

        // append the claims_report if it is in the array list
        if(in_array("clients_report", $params->stream)) {
            // single policy record
            $this->final_report["clients_report"] = $this->clients_report($params);
            // get the percentage difference between the current and previous
            $this->array_percentage_diff($this->final_report);
        }

        // append the policy_reports if it is in the array list
        if(in_array("policy_report", $params->stream)) {
            // single policy record
            $this->policy_id_query = isset($params->label["policy_id"]) && !empty($params->label["policy_id"]) ? " AND a.item_id = '{$params->label["policy_id"]}'" : null;
            $this->policy_id_payment = isset($params->label["policy_id"]) && !empty($params->label["policy_id"]) ? " AND b.record_id = '{$params->label["policy_id"]}'" : null;

            $this->final_report["policy_report"] = $this->policy_reports($params);

            // get the clients information if not parsed in the stream
            if(in_array("summary_report", $params->stream)) {
                // get the percentage difference between the current and previous
                $this->array_percentage_diff($this->final_report);
            }
        }

        // append the claims_report if it is in the array list
        if(in_array("claims_report", $params->stream)) {
            // single policy record
            $this->claim_id_query = isset($params->label["claim_id"]) && !empty($params->label["claim_id"]) ? " AND a.item_id = '{$params->label["claim_id"]}'" : null;
            $this->claim_id_payment = isset($params->label["claim_id"]) && !empty($params->label["claim_id"]) ? " AND b.record_id = '{$params->label["claim_id"]}'" : null;

            $this->final_report["claims_report"] = $this->claims_report($params);

            // get the clients information if not parsed in the stream
            if(in_array("summary_report", $params->stream)) {
                // get the percentage difference between the current and previous
                $this->array_percentage_diff($this->final_report);
            }
        }

        // append the company_policy_report if it is in the array list
        if(in_array("company_policy_report", $params->stream)) {
            $this->final_report["company_policy_report"] = $this->company_policy_report($params);
        }

        // append the users report to the list
        if(in_array("users_report", $params->stream)) {
            $this->final_report["users_report"] = $this->users_report($params);
        }
        
        return $this->final_report;
        
    }

    /**
     * Load Report Filters
     * 
     * Generate additional information to show on the page for the user to use.
     * Depending on the stream value, the addional information such as agents, clients, brokers, policies
     * will be loaded and returned. This will be used to reconstruct additional input/select fields
     * 
     * @return Array
     */
    public function load_report_filters(stdClass $params) {

        global $usersClass;
        
        $result = [];

        /** Return if nothing was set to stream */
        if(!isset($params->label["stream"])) {
            return;
        }

        /** Set the report_types as an array */
        $report_types = ["agents_report", "brokers_report", "clients_report", "insurance_company_user", "admin_report"];

        /** Load the agents list */
        if(in_array($params->label["stream"], $report_types)) {

            /** Quick user type */
            $types = [
                "agents_report" => [
                    "role" => "agent",
                    "title" => "Agent"
                ],
                "brokers_report" => [
                    "role" => "broker",
                    "title" => "Broker"
                ],
                "clients_report" => [
                    "role" => "user,business",
                    "title" => "Client"
                ],
                "admin_report" => [
                    "role" => "admin",
                    "title" => "Admin User's"
                ],
                "insurance_company_user" => [
                    "role" => "insurance_company",
                    "title" => "Admin User"
                ],
            ];

            /** Parameter */
            $client_param = (Object) [
                "userId" => $params->userId,
                "userData" => $params->userData,
                "limit" => 2000,
                "no_permissions" => true,
                "columns" => "name AS title, item_id AS value, client_id AS employee_id",
                "user_type" => $types[$params->label["stream"]]["role"],
                "remote" => true,
            ];
            
            // requests timelines
            $clients_query = $usersClass->list($client_param);
            
            // set the list of users to show
            $the_list = "<select class='form-control selectpicker' name='user_id'><option value=\"\">Please Select {$types[$params->label["stream"]]["title"]}</option>";

            // get the information
            foreach($clients_query["data"] as $eachUser) {
                $the_list .= "<option value='{$eachUser->value}'>{$eachUser->title} ({$eachUser->employee_id})</option>";
            }

            $the_list .= "</select>";

            // populate the list
            $result["load_report_filters"][$params->label["stream"]][] = [
                "list" => $the_list,
                "field" => [
                    "_title" => "Select Agent Name"
                ],
            ];
        }

        /** Claims report */
        elseif(in_array($params->label["stream"], ["policy_report", "claims_report"])) {
            
            // set the list of users to show
            $users_list = "<select class='form-control selectpicker' name='user_id'><option value=\"\">Please Select User ID</option></select>";

            /** Load the policies */
            $policy_param = (object) [
                "userId" => $params->userId,
                "userData" => $params->userData,
                "limit" => 2000,
                "status" => "Enrolled",
                "columns" => "a.item_id, a.name AS policy_name, a.policy_id, a.policy_status"
            ];
            $_policies_list = load_class("company_policy", "controllers")->list($policy_param);
            
            // set the list of users to show
            $policies_list = "<select class='form-control selectpicker' name='policy_type'><option value=\"\">Please Select Policy Type</option>";
            // get the information
            foreach($_policies_list["data"] as $policy) {
                $policies_list .= "<option value='{$policy->item_id}'>{$policy->policy_name}</option>";
            }
            $policies_list .= "</select>";

            // populate the list
            $result["load_report_filters"][$params->label["stream"]][] = [
                "list" => $policies_list,
                "field" => [
                    "_title" => "Select Policy Type"
                ],
            ];

            // populate the list
            $result["load_report_filters"][$params->label["stream"]][] = [
                "list" => $users_list,
                "field" => [
                    "_title" => "Select User"
                ],
            ];
        }

        return $result;
    }

    /**
     * Users Report
     * 
     * This loads through the users list and loads the information that is required
     * 
     * @return Array
     */
    public function users_report(stdClass $params) {

        // initials
        $result = [];
        global $usersClass;

        // user types
        $user_types = [
            "agent" => "agents_list",
            "broker" => "brokers_list",
            "user" => "clients_list",
            "business" => "clients_list"
        ];

        $unique_user_id = isset($params->label["user_id"]) ? $params->label["user_id"] : null;
        $user_type_che = isset($params->label["user_type"]) ? $params->label["user_type"] : null;
        
        // loop through the date ranges for the current and previous
        foreach($this->date_range as $range_key => $range_value) {
            
            /** Parameter */
            $client_param = (Object) [
                "date_range" => "{$range_value["start"]}:{$range_value["end"]}",
                "userId" => $params->userId,
                "limit" => 50,
                "user_type" => $user_type_che,
                "user_id" => $unique_user_id,
                "reporting" => true,
                "remote" => true,
                "remove_user_data" => true,
                "return_where_clause" => true,
                "addition_query" => ", 
                (SELECT COUNT(*) FROM users_policy b WHERE (
                    (b.user_id = a.item_id) OR (b.created_by = a.item_id) OR
                    (b.agent_id = a.item_id) OR (b.broker_id = a.item_id) OR
                    (b.assigned_to = a.item_id)
                ) AND b.policy_status !='Deleted') AS policies_count,
                (SELECT COUNT(*) FROM users_policy_claims b WHERE (b.user_id = a.item_id) OR (b.created_by = a.item_id)) AS claims_count,
                (SELECT COUNT(*) FROM users b WHERE (b.created_by = a.item_id) AND a.deleted='0') AS clients_count,
                (
                    SELECT SUM(b.amount) AS premium_paid
                    FROM users_payments b 
                    LEFT JOIN users_policy c ON c.item_id = b.record_id 
                    WHERE 
                        (b.user_id = a.item_id OR c.created_by = a.item_id) 
                    AND 
                        b.record_type = 'policy' AND b.payment_status='Paid'
                    AND 
                        (
                            DATE(b.payment_date) >= '{$range_value["start"]}' AND 
                            DATE(b.payment_date) <= '{$range_value["end"]}'
                        )
                ) AS premium_paid,
                (
                    SELECT SUM(b.amount) AS premium_paid
                    FROM users_payments b 
                    LEFT JOIN users_policy c ON c.item_id = b.record_id 
                    WHERE 
                        (b.user_id = a.item_id OR c.created_by = a.item_id) 
                    AND 
                        b.record_type = 'claims' AND b.payment_status='Paid'
                    AND 
                        (
                            DATE(b.payment_date) >= '{$range_value["start"]}' AND 
                            DATE(b.payment_date) <= '{$range_value["end"]}'
                        )
                ) AS claims_paid
                "
            ];

            $where_clause = $usersClass->list($client_param);

            /** The policies created for the period */
            $stmt = $this->db->prepare("
                SELECT 
                    a.name, item_id, client_id AS user_id, 
                    a.user_type, a.image
                    {$client_param->addition_query}
                FROM users a 
                WHERE 
                {$where_clause} ORDER BY premium_paid ASC LIMIT {$client_param->limit}
            ");
            $stmt->execute();
            $data = [];
            $row = 0;

            while($result = $stmt->fetch(PDO::FETCH_OBJ)) {
                $row++;
                $result->row_id = $row;
                $result->fullname = "<a title='Click to view user profile' href='{$this->baseUrl}profile/{$result->user_id}'>
                    {$result->name}</a> <br> {$result->user_id}";
                $data[$user_types[$result->user_type]][] = $result;
            }
            
            $result["users_count"][$range_key]["list"] = $data;

        }

        return $result;
        
    }

    /**
     * Clients Report
     * 
     * Loop through the previous and current date ranges to get the records for comparison
     * Also fetch the claims counts and premiums paid for the same period and then get the information
     * replacing dates with no records with the value zero
     * 
     * @return Array
     */
    public function clients_report(stdClass $params) {
        
        try {

            // init
            global $usersClass;
            $result = [];
            $ranger = [];

            // loop through the date ranges for the current and previous
            foreach($this->date_range as $range_key => $range_value) {

                // confirm that the period was in the request
                if(in_array($range_key, $this->query_date_range)) {

                    /** Parameter */
                    $client_param = (Object) [
                        "date_range" => "{$range_value["start"]}:{$range_value["end"]}",
                        "userId" => $params->userId,
                        "userData" => $params->userData,
                        "user_type" => "user,business",
                        "limit" => 200000,
                        "exempt_admin_users" => true,
                        "reporting" => true,
                        "minified" => "reporting_list",
                        "remote" => true,
                        "addition_query" => ", (SELECT COUNT(*) FROM users_policy_claims b WHERE (b.user_id = a.item_id) OR (b.created_by = a.item_id)) AS claims_count,
					    (SELECT COUNT(*) FROM users b WHERE (b.created_by = a.item_id) AND a.deleted='0') AS clients_count"
                    ];

                    // requests timelines
                    $clients_query = $usersClass->list($client_param);
                    $result["users_count"][$range_key] = $clients_query;

                    // if the summary report was requested as well
                    if(in_array("summary_report", $params->stream)) {
                        $this->final_report["summary_report"][$range_key]["data"]["clients_count"] = $clients_query["clients_count"];
                        $this->final_report["summary_report"][$range_key]["data"]["all_users_count"] = $clients_query["total_count"];
                    }

                    // get the where clause for generating the report
                    $client_param->addition_query = null;
                    $client_param->return_where_clause = true;
                    $client_param->user_type = "user,business";
                    $where_clause = $usersClass->list($client_param);

                    /** The policies created for the period */
                    $stmt = $this->db->prepare("
                        SELECT 
                            COUNT(*) AS value, {$this->group_by}(a.date_created) AS value_date
                        FROM users a 
                        WHERE 
                           {$where_clause} 
                        GROUP BY {$this->group_by}(a.date_created) LIMIT {$client_param->limit}
                    ");
                    $stmt->execute();
                    $counter = $stmt->fetchAll(PDO::FETCH_OBJ);
                    $result["users_counter"][$range_key]["data"] = $counter;

                    // get the data to use
                    $the_data = !empty($result["users_counter"][$range_key]) ? $result["users_counter"][$range_key]["data"] : [];

                    // combine the date and sales from the database into one set
                    $combined = array_combine(array_column($the_data, "value_date"), array_column($the_data, "value"));
                    
                    // labels check 
                    $listing = "list-days";
                    if($params->period == "today") {
                        $listing = "hour";
                    }

                    // if the period is a year
                    if(in_array($params->period, ["this_year", "last_6months", "last_year"])) {
                        $listing = "year-to-months";
                    }
                    
                    // replace the empty fields with 0
                    $replaceEmptyField = $this->value_replacer($listing, array_column($the_data, "value_date"), array($range_value["start"], $range_value["end"]));

                    // append the fresh dataset to the old dataset
                    $freshData = array_replace($combined, $replaceEmptyField);
                    ksort($freshData);

                    /** Labels control */
                    $labelArray = array_keys($freshData);
                    $labels = [];
                    // confirm which period we are dealing with
                    if($listing == "list-days") {
                        // change the labels to hours of day
                        foreach($labelArray as $value) {
                            $labels[] = date("jS M", strtotime($value));
                        }
                    } elseif($listing == "hour") {
                        // change the labels to hours of day
                        foreach($labelArray as $value) {
                            $labels[] = $this->convertToPeriod("hour", $value);
                        }
                    } elseif($listing == "year-to-months") {
                        // change the labels to hours of day
                        foreach($labelArray as $value) {
                            $labels[] = $this->convertToPeriod("month", $value, "F");
                        }
                    }

                    // Parse the amount into the chart array data
                    $resultData = [];
                    $resultData["labels"] = $labels;
                    $resultData["data"] = array_values($freshData);

                    $ranger[$range_key]["users_counter"]["data"] = $resultData;
                    $ranger[$range_key]["users_counter"]["period"] = $range_value;
                    

                    // append the period to to the array values
                    $result["users_counter"][$range_key]["period"] = $range_value;

                }

            }

            $result["grouped_list"] = $ranger;

            return $result;

        } catch(PDOException $e) {
            return $e->getMessage();
        }

    }

    /**
     * Claims Report
     * 
     * Loop through the previous and current date ranges to get the records for comparison
     * Also fetch the claims counts and premiums paid for the same period and then get the information
     * replacing dates with no records with the value zero
     * 
     * @return Array
     */
    public function claims_report(stdClass $params) {

        try {

            // init
            $ranger = [];

            // loop through the date ranges for the current and previous
            foreach($this->date_range as $range_key => $range_value) {

                // confirm that the period was in the request
                if(in_array($range_key, $this->query_date_range)) {

                    /** The policies created for the period */
                    $stmt = $this->db->prepare("
                        SELECT 
                            COUNT(*) AS value, {$this->group_by}(a.date_submitted) AS value_date
                        FROM users_policy_claims a 
                        WHERE 
                            a.status = '{$this->claim_status}' AND a.deleted='0'
                            {$this->user_query} {$this->created_by_query_main} {$this->assigned_to} {$this->company_id_main}
                            AND (
                                DATE(a.date_submitted) >= '{$range_value["start"]}' AND DATE(a.date_submitted) <= '{$range_value["end"]}'
                            )
                        GROUP BY {$this->group_by}(a.date_submitted)
                    ");
                    $stmt->execute();
                    $counter = $stmt->fetchAll(PDO::FETCH_OBJ);
                    $result["claims_counter"][$range_key]["data"] = $counter;

                    /** claims paid for the period */
                    $stmt = $this->db->prepare("
                        SELECT {$this->group_by}(b.payment_date) AS value_date, SUM(b.amount) AS value 
                        FROM users_payments b
                        LEFT JOIN users_policy_claims a ON a.item_id = b.record_id
                        WHERE b.payment_status = '{$this->payment_status}' AND b.record_type = 'claims'
                        {$this->company_id_main} {$this->assigned_to} {$this->user_query} 
                        {$this->claim_id_query} {$this->claim_id_payment}
                        AND (
                            DATE(b.payment_date) >= '{$range_value["start"]}' AND DATE(b.payment_date) <= '{$range_value["end"]}'
                        )
                        GROUP BY {$this->group_by}(b.payment_date)
                    ");
                    $stmt->execute();
                    $claims = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    $result["claims_payment"][$range_key]["data"] = $claims;

                    // if the summary report was requested as well
                    if(in_array("summary_report", $params->stream)) {
                        $claims_total_sum = !empty($claims) ? array_sum(array_column($claims, "value")) : 0;
                        $this->final_report["summary_report"][$range_key]["data"]["user_policy_claims_paid"] = $claims_total_sum;
                    }

                    // loop through the results set
                    foreach($result as $the_key => $value) {
                        
                        // get the data to use
                        $the_data = isset($value[$range_key]) ? $value[$range_key]["data"] : [];

                        // combine the date and sales from the database into one set
                        $combined = array_combine(array_column($the_data, "value_date"), array_column($the_data, "value"));
                        
                        // labels check 
                        $listing = "list-days";
                        if($params->period == "today") {
                            $listing = "hour";
                        }

                        // if the period is a year
                        if(in_array($params->period, ["this_year", "last_6months", "last_year"])) {
                            $listing = "year-to-months";
                        }
                        
                        // replace the empty fields with 0
                        $replaceEmptyField = $this->value_replacer($listing, array_column($the_data, "value_date"), array($range_value["start"], $range_value["end"]));

                        // append the fresh dataset to the old dataset
                        $freshData = array_replace($combined, $replaceEmptyField);
                        ksort($freshData);

                        /** Labels control */
                        $labelArray = array_keys($freshData);
                        $labels = [];
                        // confirm which period we are dealing with
                        if($listing == "list-days") {
                            // change the labels to hours of day
                            foreach($labelArray as $value) {
                                $labels[] = date("jS M", strtotime($value));
                            }
                        } elseif($listing == "hour") {
                            // change the labels to hours of day
                            foreach($labelArray as $value) {
                                $labels[] = $this->convertToPeriod("hour", $value);
                            }
                        } elseif($listing == "year-to-months") {
                            // change the labels to hours of day
                            foreach($labelArray as $value) {
                                $labels[] = $this->convertToPeriod("month", $value, "F");
                            }
                        }

                        // Parse the amount into the chart array data
                        $resultData = [];
                        $resultData["labels"] = $labels;
                        $resultData["data"] = array_values($freshData);

                        $ranger[$range_key][$the_key]["data"] = $resultData;
                        $ranger[$range_key][$the_key]["period"] = $range_value;

                    }

                    // append the period to to the array values
                    $result["claims_counter"][$range_key]["period"] = $range_value;
                    $result["claims_payment"][$range_key]["period"] = $range_value;
                
                }

            }

            $result["grouped_list"] = $ranger;
            
            return $result;

        } catch(PDOException $e) {
            return [];
        }

    }

    /**
     * Policy Report
     * 
     * Loop through the previous and current date ranges to get the records for comparison
     * Also fetch the policy counts and premiums paid for the same period and then get the information
     * replacing dates with no records with the value zero
     * 
     * @return Array
     */
    public function policy_reports(stdClass $params) {

        try {

            // init
            $ranger = [];
            $result = [];

            // loop through the date ranges for the current and previous
            foreach($this->date_range as $range_key => $range_value) {

                // confirm that the period was in the request
                if(in_array($range_key, $this->query_date_range)) {

                    /** The policies created for the period */
                    $stmt = $this->db->prepare("
                        SELECT 
                            COUNT(*) AS value, {$this->group_by}(a.date_created) AS value_date
                        FROM users_policy a 
                        WHERE 
                            a.policy_status {$this->policy_status} AND a.deleted='0'
                            {$this->user_query} {$this->created_by_query_main} {$this->assigned_to} {$this->company_id_main}
                            AND (
                                DATE(a.date_submitted) >= '{$range_value["start"]}' AND DATE(a.date_submitted) <= '{$range_value["end"]}'
                            )
                        GROUP BY {$this->group_by}(a.date_created)
                    ");
                    $stmt->execute();
                    $counter = $stmt->fetchAll(PDO::FETCH_OBJ);
                    $result["policy_counter"][$range_key]["data"] = $counter;

                    /** Premiums paid for the period */
                    $stmt = $this->db->prepare("
                        SELECT {$this->group_by}(b.payment_date) AS value_date, SUM(b.amount) AS value 
                        FROM users_payments b
                        LEFT JOIN users_policy a ON a.item_id = b.record_id
                        WHERE b.payment_status = '{$this->payment_status}'  AND b.record_type = 'policy'
                        {$this->created_by_query_main} {$this->company_id_main} {$this->assigned_to} {$this->user_query} 
                        {$this->policy_id_query} {$this->policy_id_payment}
                        AND (
                            DATE(b.payment_date) >= '{$range_value["start"]}' AND DATE(b.payment_date) <= '{$range_value["end"]}'
                        )
                        GROUP BY {$this->group_by}(b.payment_date)
                    ");
                    $stmt->execute();
                    $premiums = $stmt->fetchAll(PDO::FETCH_ASSOC);

                    // append the results
                    $result["premium"][$range_key]["data"] = $premiums;

                    // if the summary report was requested as well
                    if(in_array("summary_report", $params->stream)) {
                        $premiums_total_sum = !empty($premiums) ? array_sum(array_column($premiums, "value")) : 0;
                        $this->final_report["summary_report"][$range_key]["data"]["user_policy_premium_paid"] = $premiums_total_sum;
                    }

                    // loop through the results set
                    foreach($result as $the_key => $value) {

                        // get the data to use
                        $the_data = isset($value[$range_key]) ? $value[$range_key]["data"] : [];

                        // combine the date and sales from the database into one set
                        $combined = array_combine(array_column($the_data, "value_date"), array_column($the_data, "value"));
                        
                        // labels check 
                        $listing = "list-days";
                        if($params->period == "today") {
                            $listing = "hour";
                        }

                        // if the period is a year
                        if(in_array($params->period, ["this_year", "last_6months", "last_year"])) {
                            $listing = "year-to-months";
                        }
                        
                        // replace the empty fields with 0
                        $replaceEmptyField = $this->value_replacer($listing, array_column($the_data, "value_date"), array($range_value["start"], $range_value["end"]));

                        // append the fresh dataset to the old dataset
                        $freshData = array_replace($combined, $replaceEmptyField);
                        ksort($freshData);

                        /** Labels control */
                        $labelArray = array_keys($freshData);
                        $labels = [];
                        // confirm which period we are dealing with
                        if($listing == "list-days") {
                            // change the labels to hours of day
                            foreach($labelArray as $value) {
                                $labels[] = date("jS M", strtotime($value));
                            }
                        } elseif($listing == "hour") {
                            // change the labels to hours of day
                            foreach($labelArray as $value) {
                                $labels[] = $this->convertToPeriod("hour", $value);
                            }
                        } elseif($listing == "year-to-months") {
                            // change the labels to hours of day
                            foreach($labelArray as $value) {
                                $labels[] = $this->convertToPeriod("month", $value, "F");
                            }
                        }

                        // Parse the amount into the chart array data
                        $resultData = [];
                        $resultData["labels"] = $labels;
                        $resultData["data"] = array_values($freshData);

                        $ranger[$range_key][$the_key]["data"] = $resultData;
                        $ranger[$range_key][$the_key]["period"] = $range_value;
                    }

                    // append the period to to the array values
                    $result["premium"][$range_key]["period"] = $range_value;
                    $result["policy_counter"][$range_key]["period"] = $range_value;

                }

            }

            $result["grouped_list"] = $ranger;
            
            return $result;

        } catch(PDOException $e) {
            return $e->getMessage();
        }

    }

    /**
     * Company Policy Report
     * 
     * Loop through the previous and current date ranges to get the records for comparison
     * Also fetch the claims counts and premiums paid for the same period and then get the information
     * replacing dates with no records with the value zero
     * 
     * @return Array
     */
    public function company_policy_report(stdClass $params) {

       try {

            // init
            $ranger = [];
            $result = [];

            // loop through the date ranges for the current and previous
            foreach($this->date_range as $range_key => $range_value) {

                // confirm that the period was in the request
                if(in_array($range_key, $this->query_date_range)) {

                    /** The policies created for the period */
                    $stmt = $this->db->prepare("
                        SELECT 
                            b.name, b.policy_status, (
                                SELECT COUNT(*) FROM users_policy a 
                                WHERE a.policy_type = b.item_id AND a.policy_status {$this->policy_status} AND (
                                    DATE(a.date_submitted) >= '{$range_value["start"]}' AND DATE(a.date_submitted) <= '{$range_value["end"]}'
                                )                                
                            ) AS policy_application_count
                        FROM policy_types b 
                        WHERE b.status = '1' AND b.deleted = '0' AND b.policy_status {$this->company_policy_status_sub}
                    ");
                    $stmt->execute();
                    $counter = $stmt->fetchAll(PDO::FETCH_OBJ);
                    $result["application_counter"][$range_key]["list"] = $counter;

                    // group the list into labels and counter
                    if(!empty($counter) && is_array($counter)) {
                        $result["application_counter"][$range_key]["grouping"]["labels"] = array_column($counter, "name");
                        $result["application_counter"][$range_key]["grouping"]["data"] = array_column($counter, "policy_application_count");
                    }

                    // combine all the policy status count
                    $result["application_counter"][$range_key]["policy_status_count"] = load_class("company_policy", "controllers")->policy_status_array_count($counter);

                    // if the summary report was requested as well
                    if(in_array("summary_report", $params->stream)) {
                        $this->final_report["summary_report"][$range_key]["data"]["company_policy_count"] = count($counter);
                    }

                    // append the period to to the array values
                    $result["application_counter"][$range_key]["period"] = $range_value;

                }

            }

            $result["grouped_list"] = $ranger;

            return $result;

        } catch(PDOException $e) {
            return $e->getMessage();
        }
    }

    /**
     * Preformat the date
     * 
     * This algo formats the dates that have been submitted by the user
     * 
     * @param String $period        This is the date to process
     */
    public function preformat_date($period) {

        /** initial variables */
        $today = date("Y-m-d");
        $explode = explode(":", $period);
        $explode[1] = isset($explode[1]) ? $explode[1] : date("Y-m-d");

        /** Confirm that a valid date was parsed */
        if(!$this->validDate($explode[0])) {
            return "invalid-date";
        }

        /** If the next param was set */
        if(isset($explode[1]) && !$this->validDate($explode[1])) {
            return "invalid-range";
        }

        /** Confirm that the last date is not more than today */
        if(isset($explode[1]) && strtotime($explode[1]) > strtotime($today)) {
            return "exceeds-today";
        }

        /** confirm that the starting date is not greater than the end date */
        if(isset($explode[1]) && strtotime($explode[0]) > strtotime($explode[1])) {
            return "invalid-prevdate";
        }

        /** Confirm valid dates */
        if(!preg_match("/^[0-9-]+$/", $explode[0]) || !preg_match("/^[0-9-]+$/", $explode[1])) {
            return "invalid-range";
        }
        
        /** Check the days difference */
        $days_list = $this->listDays($explode[0], $explode[1]);
        $count = count($days_list);

        /** ensure that the days count does not exceed 90 days */
        if($count > 90) {
            return "exceeds-count";
        }

        $format = "jS M Y";
        $group = "DATE";
        if($count >= 32 && $count <= 60) {
            $group = "MONTH";
            $format = "F";
        }
        
        $this->start_date = $days_list[0];
        $this->end_date = end($days_list);
        $this->group_by = $group;
        $this->current_title = "Past {$count} days";
        $this->previous_title = "Previous {$count} days";
        $this->date_format = $format;
        $this->prevstart_date = date("Y-m-d", strtotime("today -".($count * 2)." days"));
        $this->prevend_date = date("Y-m-d", strtotime("today -{$count} days"));

        return $this;

    }

	/**
     * This formats the correct date range
     *  
     * @param String    $datePeriod      This is the date period that was parsed
     * 
     * @return This     $this->start_date, $this->end_date;
     */
    public function format_date($datePeriod = "this_week") {

        // Check Sales Period
        switch ($datePeriod) {
            case 'this_week':
                $groupBy = "DATE";
                $format = "jS M Y";
                $currentTitle = "This Week";
                $previousTitle = "Last Week";
                $dateFrom = date("Y-m-d", strtotime("today -1 weeks"));
                $dateTo = date("Y-m-d", strtotime("today"));
                $prevFrom = date("Y-m-d", strtotime("today -2 weeks"));
                $prevTo = date("Y-m-d", strtotime("today -1 weeks"));
                break;
            case 'last_week':
                $groupBy = "DATE";
                $format = "jS M Y";
                $currentTitle = "Last Weeks";
                $previousTitle = "Last 2 Weeks";
                $dateFrom = date("Y-m-d", strtotime("-2 weeks"));
                $dateTo = date("Y-m-d", strtotime("-1 weeks"));
                $prevFrom = date("Y-m-d", strtotime("today -3 weeks"));
                $prevTo = date("Y-m-d", strtotime("today -2 weeks"));
                break;
            case 'this_month':
                $groupBy = "DATE";
                $format = "jS M Y";
                $currentTitle = "This Month";
                $previousTitle = "Last Month";
                $dateFrom = date("Y-m-01");
                $dateTo = date("Y-m-t");
                $prevFrom = date("Y-m-01", strtotime("last month"));
                $prevTo = date("Y-m-t", strtotime("last month"));
                break;
            case 'last_month':
                $groupBy = "DATE";
                $format = "jS M Y";
                $currentTitle = "Last Months";
                $previousTitle = "Last 2 Months";
                $dateFrom = date("Y-m-01", strtotime("last month"));
                $dateTo = date("Y-m-t", strtotime("last month"));
                $prevFrom = date("Y-m-01", strtotime("last 2 month"));
                $prevTo = date("Y-m-t", strtotime("last 2 month"));
                break;
            case 'last_14days':
                $groupBy = "DATE";
                $format = "jS M Y";
                $currentTitle = "Last 14 Days";
                $previousTitle = "Previous 14 Days";
                $dateFrom = date("Y-m-d", strtotime("-2 weeks"));
                $dateTo = date("Y-m-d", strtotime("today"));
                $prevFrom = date("Y-m-d", strtotime("-4 weeks"));
                $prevTo = date("Y-m-d", strtotime("-2 weeks"));
                break;
            case 'last_30days':
                $groupBy = "DATE";
                $format = "jS M Y";
                $currentTitle = "Last 30 Days";
                $previousTitle = "Previous 30 Days";
                $dateFrom = date("Y-m-d", strtotime("-30 days"));
                $dateTo = date("Y-m-d", strtotime("today"));
                $prevFrom = date("Y-m-d", strtotime("-60 days"));
                $prevTo = date("Y-m-d", strtotime("-30 days"));
                break;
            case 'last_3months':
                $groupBy = "MONTH";
                $format = "jS M Y";
                $currentTitle = "Last 3 months";
                $previousTitle = "Previous 3 months";
                $dateFrom = date("Y-m-d", strtotime("today -3 months"));
                $dateTo = date("Y-m-d", strtotime("today"));
                $prevFrom = date("Y-m-d", strtotime("today -6 months"));
                $prevTo = date("Y-m-d", strtotime("today -3 months"));
                break;
            case 'last_6months':
                $groupBy = "MONTH";
                $format = "jS M Y";
                $currentTitle = "Last 6 Months";
                $previousTitle = "Previous 6 Months";
                $dateFrom = date("Y-m-d", strtotime("today -6 months"));
                $dateTo = date("Y-m-d", strtotime("today"));
                $prevFrom = date("Y-m-01", strtotime("today -12 months"));
                $prevTo = date("Y-m-t", strtotime("today -6 months"));
                break;
            case 'this_year':
                $groupBy = "MONTH";
                $format = "F";
                $dateFrom = date('Y-01-01');
                $dateTo = date('Y-12-31');
                $currentTitle = "This Year";
                $previousTitle = "Last Year";
                $prevFrom = date("Y-01-01", strtotime("last year"));
                $prevTo = date("Y-12-31", strtotime("last year"));
                break;
            case 'last_year':
                $groupBy = "MONTH";
                $format = "F";
                $currentTitle = "Last Year";
                $previousTitle = "Last Year";
                $dateFrom = date('Y-01-01', strtotime("last year"));
                $dateTo = date('Y-12-31', strtotime("last year"));
                $prevFrom = date('Y-01-01', strtotime("-2 years"));
                $prevTo = date('Y-12-31', strtotime("-2 years"));
                break;
            default:
				$groupBy = "HOUR";
                $format = "jS M Y";
                $currentTitle = "Today";
                $previousTitle = "Yesterday";
                $dateFrom = date("Y-m-d", strtotime("today -1 days"));
                $dateTo = date("Y-m-d", strtotime("today"));
                $prevFrom = date("Y-m-d", strtotime("today -2 days"));
                $prevTo = date("Y-m-d", strtotime("today -1 days"));
                break;
        }

        $this->start_date = $dateFrom;
        $this->end_date = $dateTo;
        $this->prevstart_date = $prevFrom;
        $this->prevend_date = $prevTo;
        $this->current_title = $currentTitle;
        $this->previous_title = $previousTitle;
        $this->group_by = $groupBy;
        $this->date_format = $format;

        return $this;

    }

    /**
     * Replace empty dates with 0 for that day
     * 
     * @param String $rangeSet
     * @param Array $dataSet
     * @param Array $dateRange
     * 
     * @return Array
     */
    public function value_replacer($rangeSet, $dataSet, $dateRange = [], $weekDays = null) {
		// return data set
		$returnDataSet = [];

		if($rangeSet == "list-days") {
			// set the dates
			$dates = array();
			
			// get the list of days
			$datesList = $this->listDays($dateRange[0], $dateRange[1]);
			
			$notFoundDays = array_diff($datesList, $dataSet);
			$dataSet = [];

			foreach($notFoundDays as $value) {
				$dataSet[$value] = "0.00";
			}
			// check the rangeset that has been parsed
		} elseif($rangeSet == "hour") {
			// set the first parameter for the hours
			$hourOfDay = array();

			// get the hours for the day
			$hours = range(0, 23, 1);
			
			// loop through the hours
			foreach ($hours as $hr) {
				$hourOfDay[] = $hr;
			}
			
			// find the difference in the two array set
			$notFoundHours = array_diff($hourOfDay, $dataSet);
			$dataSet = [];
			
			foreach($notFoundHours as $value) {
				$dataSet[$value] = "0.00";
			}
		} elseif($rangeSet == "year-to-months") {
			// set the first parameter for the hours
			$monthsOfYear = array();

			// loop through the hours
			for($i=1; $i <= 12; $i++) {
				$monthsOfYear[] = $i;
			}

			// find the difference in the two array set
			$notFoundMonths = array_diff($monthsOfYear, $dataSet);
			
			$dataSet = [];
			foreach($notFoundMonths as $value) {
				$dataSet[$value] = "0.00";
			}
		}

		return $dataSet;
	}

    /**
     * Convert to string to a valid date form
     * 
     * @param String $timeFrame
     * @param String $period
     * @param String $format
     * 
     * @return String
     */
    public function convertToPeriod($timeFrame, $period, $format='Y-m-01') {
		// Check the time frame hourly
		if($timeFrame == "hour") {
			// get the hours for the day
			$hours = range(0, 23, 1);
			// loop through the hours
			foreach ($hours as $hr) {
				if($hr == $period) {
					return date('hA', strtotime("today +$hr hours"));
					break;
				}
			}
		}
		// Check the time frame monthly
		elseif($timeFrame == "month") {
			// get the months for the day
			$months = range(0, 11, 1);
			// loop through the months
			foreach ($months as $hr) {
				if($hr == ($period-1)) {
					return date($format, strtotime("January +$hr month"));
					break;
				}
			}
		} else {
			//return $period;
		}
	}

    /**
     * Percentage difference
     * 
     * Loop through the array, get the match and find the percentage difference between 
     * the previous and the current values.
     * 
     * @return Array
     */
    public function array_percentage_diff($array_data) {

        // get the array key
        $difference = [];
        $array_key = array_keys($array_data);
        
        // if its an array
        if(is_array($array_data[$array_key[0]])) {

            // loop through the summary information
            foreach($array_data[$array_key[0]] as $key => $value) {
                
                // confirm that the value is an array
                if(is_array($value) && isset($value["data"])) {
                    
                    // loop through the data array
                    foreach($value["data"] as $kkey => $kvalue) {

                        // get the raw_value
                        $raw_value = !is_array($kvalue) ? (isset($difference[$kkey]["value"]) ? $kvalue - $difference[$kkey]["value"] : $kvalue) : null;
                        
                        // confirm if the value already exists
                        if(isset($difference[$kkey]["value"])) {
                            // then the new value is the current one
                            $percentage = $this->percentage_diff($kvalue, $difference[$kkey]["value"]);
                        } else {
                            $percentage = $this->percentage_diff($kvalue, 0);
                        }

                        // find the difference
                        $difference[$kkey] = [
                            "percentage" => $percentage,
                            "value" => $raw_value,
                        ];

                    }                    

                }

            }
        }

        return $this->final_report[$array_key[0]]["difference"] = $difference;

    }
    
}
?>