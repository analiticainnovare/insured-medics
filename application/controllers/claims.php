<?php
// ensure this file is being included by a parent file
if( !defined( 'BASEPATH' ) ) die( 'Restricted access' );

class Claims extends Medics {
	
	# start the construct
	public function __construct() {
		parent::__construct();
	}
	
	/**
	 * Global function to search for item based on the predefined columns and values parsed
	 * 
	 * @param \stdClass $params
	 * @param String $params->user_id
	 * @param String $params->claim_id  
	 * @param String $params->company_id
	 * @param String $params->date
	 * 
	 * @return Object
	 */
	public function list(stdClass $params = null) {

		$params->query = "1";
        $is_permitted = false;

        // boolean value
        $params->remote = (bool) (isset($params->remote) && $params->remote);
        
        // if the user id parameter was not parsed
		if(!isset($params->claim_id)) {

			// perform some checks
			$the_user_type = $params->userData->user_type;
			
            // go ahead
			if(in_array($the_user_type, ["insurance_company"])) {
                $is_permitted = true;
				$params->or_clause = " AND ((a.company_id = '{$params->userData->company_id}' OR a.approved_by='{$params->userId}' OR a.confirmed_by='{$params->userId}') AND a.submit_status='save')";
			}
            // if insurance company user
            elseif(in_array($the_user_type, ["agent", "broker", "bancassurance"])) {
                $params->or_clause = " AND (a.created_by = '{$params->userId}' OR a.assigned_to='{$params->userId}')";
            }
            // user and business
            elseif(in_array($the_user_type, ["user", "business"])) {
                $params->or_clause = " AND (a.user_id = '{$params->userId}')";
            }
            // if the admin is logged in
		}

		// if the field is null
		$params->query .= (isset($params->claim_id) && !empty($params->claim_id)) ? (preg_match("/^[0-9]+$/", $params->claim_id) ? " AND a.id='{$params->claim_id}'" : " AND a.item_id='{$params->claim_id}'") : null;
		$params->query .= (isset($params->status) && !empty($params->status)) ? " AND a.status='{$params->status}'" : null;
        $params->query .= (isset($params->user_id) && !empty($params->user_id)) ? " AND a.user_id='{$params->user_id}'" : null;
        $params->query .= (isset($params->submit_status) && !empty($params->submit_status)) ? " AND a.submit_status='{$params->submit_status}'" : null;
        $params->query .= (isset($params->or_clause) && !empty($params->or_clause)) ? $params->or_clause : null;
        $params->query .= (isset($params->policy_type) && !empty($params->policy_type)) ? " AND a.policy_type='{$params->policy_type}'" : null;
        $params->query .= (isset($params->company_id) && !empty($params->company_id)) ? " AND a.company_id='{$params->company_id}'" : null;
        $params->query .= (isset($params->policy_id) && !empty($params->policy_id)) ? " AND a.policy_id='{$params->policy_id}'" : null;
		$params->query .= (isset($params->date)) ? " AND DATE(a.date_created) ='{$params->date_created}'" : null;
        $params->query .= (isset($params->date_range) && !empty($params->date_range)) ? $this->dateRange($params->date_range, "a", "date_submitted") : null;

		// the number of rows to limit the query
		$params->limit = isset($params->limit) ? $params->limit : $this->global_limit;

        // comments object
        $loadInteractions = isset($params->load_comments) ? true : false;

        // if the loadInteractions variable is true
        if($loadInteractions) {
            $threadInteraction = (object)[
                "userId" => $params->userId,
                "feedback_type" => "comment"
            ];
            $commentsObj = load_class("replies", "controllers");
            $filesObject = load_class("forms", "controllers");
        }

        try {
            // make the request for the record from the model
            $stmt = $this->db->prepare("
                SELECT 
                ".(isset($params->columns) ? $params->columns : "
                    a.*, a.item_id AS claim_id, a.user_id, u.name AS client_name, u.image AS client_image,
                    u.email AS client_email, u.phone_number AS client_contact, u.name AS client_raw_name,
                    (SELECT name FROM users WHERE users.item_id = a.created_by LIMIT 1) AS requested_by,
                    (SELECT image FROM users WHERE users.item_id = a.created_by LIMIT 1) AS requested_by_image,
                    (SELECT name FROM users WHERE users.item_id = a.seen_by LIMIT 1) AS seen_by_name,
                    (SELECT name FROM users WHERE users.item_id = a.approved_by LIMIT 1) AS approved_by_name,
                    (SELECT name FROM companies WHERE companies.item_id = a.company_id LIMIT 1) AS company_name,
                    (SELECT name FROM users WHERE users.item_id = a.assigned_to LIMIT 1) AS assigned_to_name,
                    (SELECT b.description FROM files_attachment b WHERE b.record_id = a.item_id ORDER BY b.id DESC LIMIT 1) AS attachment
                ").", (SELECT b.policy_name FROM users_policy b WHERE b.policy_id = a.policy_id LIMIT 1) AS policy_name
                FROM users_policy_claims a
                LEFT JOIN users u ON u.item_id = a.user_id
                WHERE {$params->query} AND a.deleted='0' ORDER BY a.id DESC LIMIT {$params->limit}
            ");
            $stmt->execute();

            $row = 0;
            $data = [];
            while($result = $stmt->fetch(PDO::FETCH_OBJ)) {
                // unset two parameters from the result set
                unset($result->id);
                unset($result->deleted);

                // attachment
                if(isset($result->attachment)) {
                    $result->attachment = json_decode($result->attachment);
                }

                // if not a remote request
                if(!$params->remote) {

                    // decode the string
                    $result->form_fields = json_decode($result->form_fields);
                    $result->form_answers = json_decode($result->form_answers);
                    $result->policy_details = json_decode($result->policy_details);

                    // the action button to return
                    $result->action = "";

                    // if the status is parsed
                    if(isset($result->status)) {
                        $result->the_status_label = $this->the_status_label($result->status);
                    }

                    // set some variables
                    $result->inline_policy_info = "
                        <p><span class=\"underline cursor\" title=\"Click to view policy details\" onclick=\"return policy_basic_information('{$result->policy_details->policy_type}')\">{$result->policy_details->policy_name}</span></p>
                        <p><strong>Policy ID:</strong> <a href='{$this->baseUrl}policies-view/{$result->policy_details->item_id}'>{$result->policy_details->policy_id}</a></p>
                        <p><strong>Start Date:</strong> {$result->policy_details->policy_start_date}</p>
                        <p><strong>Policy Status:</strong> {$this->the_status_label($result->policy_details->policy_status)}</p>";

                    // view buttons
                    $result->action .= " &nbsp; <a class='btn p-1 btn-outline-success m-0 btn-sm' title='Click to view details of this policy' href='{$this->baseUrl}claims-view/{$result->item_id}'><i class='fa fa-eye'></i></a>";
                    
                    // if this user created the complaint and its still in a draft stage
                    if(($result->submit_status == "draft") && (($result->user_id == $params->userId) || ($result->created_by == $params->userId))) {
                        $result->action .= " &nbsp; <a class='btn btn-outline-danger p-1 m-0 modify-record btn-sm' href='javascript:void(0)' title='Click to delete claim'  onclick='return delete_record(\"users_claims\", \"{$result->item_id}\");'><i class='fa fa-trash-alt'></i></a>";
                    }

                    // make the assigned to clickable
                    if(!empty($result->assigned_to_name)) {
                        $result->assigned_to_name = "<span class=\"underline cursor\" title=\"Click to view the details of {$result->assigned_to_name}\" onclick=\"return user_basic_information('{$result->assigned_to}')\">{$result->assigned_to_name}</span>";
                    }

                    // make the client name clickable
                    if(!empty($result->client_name)) {
                        $result->client_name = "<span class=\"underline cursor\" title=\"Click to view the details of {$result->client_name}\" onclick=\"return user_basic_information('{$result->user_id}')\">{$result->client_name}</span>";
                    }

                    $row++;
                    $result->row_id = $row;
                }

                // load policy comments
                if($loadInteractions) {

                    // create a new object
                    $threadInteraction->resource_id = $result->item_id;
                    $threadInteraction->feedback_type = "comment";
                    $result->comments_list = $commentsObj->list($threadInteraction);

                    // if the user is permitted ie. an insurance company or admin
                    $result->is_permitted = $is_permitted;
                    
                    // load and return the replies list as well
                    if($is_permitted) {
                        $threadInteraction->feedback_type = "reply";
                        $result->replies_list = $commentsObj->list($threadInteraction);
                    }

                    // init the attachment_html
                    $result->attachment_html = "";
                    
                    // if the files is set
                    if(isset($result->attachment->files)) {
                        // format the attachements list
                        $result->attachment_html = $filesObject->list_attachments($result->attachment->files, $result->user_id, "col-lg-6 col-md-6", false, false);
                    } else {
                        $result->attachment = (object) $this->fake_files;
                    }
                }

                $data[] = $result;
            }

            // return the data
            return [
                "data" => $data,
                "code" => !empty($data) ? 200 : 201
            ];
        } catch(PDOException $e) {
            return $e->getMessage();
        }

	}

    /**
     * Add a new complaint
     * 
     * @param \stdClass $params
     * 
     * @return Array
     */
    public function add(stdClass $params) {

        /** Validate the user variables parsed */
        $params->_item_id = random_string("alnum", 32);
        global $noticeClass;
        
        /** Load some basic information needed for processing */
        $policyInfo = $this->pushQuery(
            "policy_id, item_id, company_id, user_id, agent_id, assigned_to, policy_status, broker_id, agent_id, 
                policy_start_date, policy_name, policy_type,
            (SELECT users.name FROM users WHERE users.item_id = users_policy.agent_id LIMIT 1) AS agent_name,
            (SELECT users.name FROM users WHERE users.item_id = users_policy.broker_id LIMIT 1) AS broker_name,
            (SELECT users.name FROM users WHERE users.item_id = users_policy.assigned_to LIMIT 1) AS assigned_to_name,
            (SELECT claims_form FROM policy_types WHERE policy_types.item_id = users_policy.policy_type LIMIT 1) AS claims_form
            ", 
            "users_policy",
            "(
                (user_id='{$params->userId}') OR (agent_id='{$params->userId}' AND user_id='{$params->user_id}') OR
                (broker_id='{$params->userId}' AND user_id='{$params->user_id}') OR 
                (created_by='{$params->userId}' AND user_id='{$params->user_id}') OR 
                (assigned_to='{$params->userId}' AND user_id='{$params->user_id}')
            ) AND policy_status NOT IN ('Deleted') AND (policy_id = '{$params->policy_id}' OR item_id='{$params->policy_id}') LIMIT 1"
        );
        
        /** If the document is empty */
        if(empty($policyInfo)) {
            return ["code" => 203, "data" => "Sorry! You are not permitted to make a claim on this Policy."];
        }

        /** Run some few checks */
        $policyInfo = $policyInfo[0];

        /** Confirm that the policy is not pending */
        if(in_array($policyInfo->policy_status, ["Processing", "Deleted", "Ellapsed"])) {
            // log the user activity
            $this->userLogs("claims", $params->policy_id, $policyInfo, "<strong>{$params->userData->name}</strong> tried to make a claim on this policy. However the status was <strong>{$policyInfo->policy_status}</strong> hence could not be processed.", $params->userId);
            // return an error message
            return ["code" => 203, "data" => "Sorry! Your policy is currently {$policyInfo->policy_status}: hence could not be processed."];
        }

        /** Set the person managing this policy */
        if(!empty($policyInfo->broker_id)) {
            $policyInfo->managed_by = [
                "title" => "Broker",
                "id" => $policyInfo->broker_id,
                "name" => $policyInfo->broker_name
            ];
        }

        // if the agent id is set
        if(!empty($policyInfo->agent_id)) {
            // if the agent is not empty
            $policyInfo->managed_by = [
                "title" => "Agent",
                "id" => $policyInfo->agent_id,
                "name" => $policyInfo->agent_name
            ];
        }

        // if the assigned to is set
        if(!empty($policyInfo->assigned_to)) {
            // if the broker id is not empty
            if(!$policyInfo->broker_id && !$policyInfo->agent_id) {
                $policyInfo->managed_by = [
                    "title" => "Manager",
                    "id" => $policyInfo->assigned_to,
                    "name" => $policyInfo->assigned_to_name
                ];
            }
        }

        // set the claims form
        $claims_form = json_decode($policyInfo->claims_form);

        // unset the claims form from the policy information returned
        unset($policyInfo->claims_form);

        // append the attachments
        $filesObj = load_class("files", "controllers");
        $attachments = $filesObj->prep_attachments("claims_{$params->policy_id}", $params->userData->user_id, $params->_item_id);

        try {

            // begin transaction
            $this->db->beginTransaction();

            // clean the content
            $submit_status = isset($params->the_button) ? strtolower($params->the_button) : "save";

            // insert the information
            $stmt = $this->db->prepare("
                INSERT INTO users_policy_claims SET created_by = ?, item_id = ?, policy_details = ?, form_fields = ?, form_answers = ?
                ".(isset($params->user_id) ? ",user_id='{$params->user_id}'" : null)."
                ".(isset($policyInfo->policy_id) ? ",policy_id='{$policyInfo->policy_id}'" : null)."
                ".(isset($policyInfo->policy_type) ? ",policy_type='{$policyInfo->policy_type}'" : null)."
                ".(isset($policyInfo->company_id) ? ",company_id='{$policyInfo->company_id}'" : null)."
                ".(!empty($submit_status) ? ",submit_status='{$submit_status}'" : null)."
                ".(($submit_status == "save") ? ",date_submitted=now()" : null)."
            ");
            $stmt->execute([$params->userId, $params->_item_id, json_encode($policyInfo), json_encode($claims_form->fields), json_encode($params->field)]);
            
            // insert attachment    
            $files = $this->db->prepare("INSERT INTO files_attachment SET resource= ?, resource_id = ?, description = ?, record_id = ?, created_by = ?, attachment_size = ?");
            $files->execute(["claims", $params->_item_id, json_encode($attachments), "{$params->_item_id}", $params->userId, $attachments["raw_size_mb"]]);

            // log the user activity
            $data = "Your policy claims was successfully ".(($submit_status == "save") ? "submitted" : "saved as draft.");

            // log the user activity
            $this->userLogs("claims", $params->_item_id, null, "<strong>{$params->userData->name}</strong> Submitted a new claim on the policy: {$policyInfo->policy_id}.", $params->userId);

            // notify the user if the claim was done on his/her behalf
            if($params->user_id !== $params->userId) {

                // form the notification parameters
                $notice_param = (object) [
                    '_item_id' => random_string("alnum", 32),
                    'user_id' => $params->user_id,
                    'subject' => "Claims",
                    'username' => $params->userData->username,
                    'remote' => false, 
                    'message' => "<strong>{$params->userData->name}</strong> made a claim on your policy <a title=\"Click to View\" href=\"{{APPURL}}claims-view/{$params->_item_id}\">{$policyInfo->policy_id}</a>.",
                    'notice_type' => 11,
                    'userId' => $params->userId,
                    'initiated_by' => 'system'
                ];
                
                // add a new notification
                $noticeClass->add($notice_param);
            }

            $this->db->commit();

            // return the success response
            return [
                "code" => 200,
                "data" => $data,
                "additional" => [
                    "clear" => true,
                    "reload" => "{$this->baseUrl}claims-view/{$params->_item_id}",
                ]
            ];

        } catch(PDOException $e) {
            $this->db->rollBack();
            return $e->getMessage();
        }
    }

	/**
	 * Update a complaints record
	 * 
	 * @param \stdClass $params
	 * 
	 * @return Array
	 */
	public function update(stdClass $params) {
		
        /** Confirm that this claims does not already exist */
        $claimCheck = $this->pushQuery("a.*, (SELECT b.description FROM files_attachment b WHERE b.record_id = a.item_id ORDER BY a.id DESC LIMIT 1) AS attachment", 
            "users_policy_claims a", "a.item_id='{$params->item_id}' AND a.user_id='{$params->user_id}' AND a.status !='Deleted' LIMIT 1");

        // if the claims check is not empty
        if(empty($claimCheck)) {
            return ["data" => "Sorry! The record was not found"];
        }

        // initialize
        $initial_attachment = [];

        // define the module
        $module = "claims_{$params->item_id}";

        // policy data
        $claimsData = $claimCheck[0];

        /** Confirm that there is an attached document */
        if(!empty($claimsData->attachment)) {
            // decode the json string
            $db_attachments = json_decode($claimsData->attachment);
            // get the files
            if(isset($db_attachments->files)) {
                $initial_attachment = $db_attachments->files;
            }
        }

        // modify the return log
        $return_log = [
            "code" => 200,
            "data" => "The policy has been successfully updated!",
            "additional" => []
        ];
        
        // append the attachments
        $filesObj = load_class("files", "controllers");
        $attachments = $filesObj->prep_attachments($module, $params->userData->user_id, $params->item_id, $initial_attachment);

        // if the approved amount was parsed
        $params->approved_amount = isset($params->approved_amount) ? round(str_replace(",", "", $params->approved_amount)) : null;

        // global variable
        global $noticeClass;

        try {

            $this->db->beginTransaction();

            // approved claims
            $isApproved = (isset($params->status) && $params->status == "Approved") ? true : false;
            $isConfirmed = (isset($params->status) && $params->status == "Confirmed") ? true : false;

            // update the claim request
            $stmt = $this->db->prepare("
                UPDATE users_policy_claims SET last_updated = now()
                    ".(isset($params->field) ? ",form_answers='".json_encode($params->field)."'" : null)."
                    ".($isApproved ? ",approved_by='{$params->userId}'" : null)."
                    ".($isApproved ? ",approved_date=now()" : null)."
                    ".($isConfirmed ? ",confirmed_by='{$params->userId}'" : null)."
                    ".($isConfirmed ? ",confirmed_date=now()" : null)."
                    ".(isset($params->amount_claimed) ? ",amount_claimed='{$params->amount_claimed}'" : null)."
                    ".(!empty($params->approved_amount) ? ",approved_amount='{$params->approved_amount}'" : null)."
                    ".((isset($params->assigned_to) && $params->assigned_to !== "null") ? ",assigned_to='{$params->assigned_to}'" : null)."
                    ".(isset($params->the_button) ? ",submit_status='{$params->the_button}'" : null)."
                    ".((isset($params->the_button) && ($params->the_button == "save")) ? ",date_submitted=now()" : null)."
                    ".(isset($params->status) ? ",status='".ucfirst($params->status)."'" : null)."
                WHERE item_id = ? LIMIT 1
            ");
            $stmt->execute([$params->item_id]);
            
            // update attachment if already existing
            if(isset($db_attachments)) {
                $files = $this->db->prepare("UPDATE files_attachment SET description = ?, attachment_size = ? WHERE record_id = ? LIMIT 1");
                $files->execute([json_encode($attachments), $attachments["raw_size_mb"], $params->item_id]);
            } else {
                // only insert attachment record if there was an attachment to the comment
                if(!empty($attachments["files"])) {
                    // insert the record if not already existing
                    $files = $this->db->prepare("INSERT INTO files_attachment SET resource= ?, resource_id = ?, description = ?, record_id = ?, created_by = ?, attachment_size = ?");
                    $files->execute(["claims", $params->item_id, json_encode($attachments), "{$params->item_id}", $params->userId, $attachments["raw_size_mb"]]);
                }
            }

            // set the notice
            $notice = "{$this->appName} Calculation<br>Property changed by a manual update effected by <strong>{$params->userData->name}</strong>.";

            // if the previous is not the same as the current
            if(isset($params->field) && ($claimsData->form_answers !== json_encode($params->field))) {
                // save the form change information
                $this->userLogs("claims", $params->item_id, $claimsData->form_answers, "Claims form content has been altered.", $params->userId);
                
                // set the return log
                $return_log["additional"]["reload"] = "{$this->baseUrl}claims-view/{$params->item_id}";
            }

            // save the log for the change in amount claimed
            if(isset($params->amount_claimed) && ($claimsData->amount_claimed !== $params->amount_claimed)) {
                // save the status change information
                $this->userLogs("claims", $params->item_id, $claimsData->amount_claimed, "<strong>Amount Claimed</strong> was changed from <strong>{$claimsData->amount_claimed}</strong> to <strong>{$params->amount_claimed}</strong>.", $params->userId);
            }

            // save the log for the change in amount claimed
            if(isset($params->approved_amount) && ($claimsData->approved_amount !== $params->approved_amount)) {
                // save the status change information
                $this->userLogs("claims", $params->item_id, $claimsData->approved_amount, "<strong>Approved Amount</strong> was set to <strong>{$params->approved_amount}</strong>.", $params->userId);
                $return_log["additional"]["reload"] = "{$this->baseUrl}claims-view/{$params->item_id}";
            }
            
            // save the log for the status change
            if(isset($params->status) && ($claimsData->status !== $params->status)) {
                // set the return log
                $return_log["additional"]["reload"] = "{$this->baseUrl}claims-view/{$params->item_id}";
                
                // save the status change information
                $this->userLogs("claims", $params->item_id, $claimsData->status, "<strong>Policy Status</strong> was changed from <strong>{$claimsData->status}</strong> to <strong>{$params->status}</strong>.", $params->userId);
                
                // notify the user of the new status of the claim
                $from = $this->columnValue(
                    "a.name AS client_name, (SELECT b.name FROM users b WHERE a.created_by = b.item_id LIMIT 1) AS created_by_name", 
                    "users a", "a.item_id='{$claimsData->user_id}'"
                );

                // form the notification parameters
                $notice_param = (object) [
                    '_item_id' => random_string("alnum", 32),
                    'user_id' => $claimsData->user_id,
                    'subject' => "Policy Claims",
                    'username' => $params->userData->username,
                    'remote' => false, 
                    'message' => "<strong>Hello {$from->client_name},</strong> your claim request on the policy <a title=\"Click to View\" href=\"{{APPURL}}claims-view/{$params->item_id}\"><strong>{$claimsData->policy_id}</strong></a> is {$params->status}.",
                    'notice_type' => 7,
                    'userId' => $params->userId,
                    'initiated_by' => 'system'
                ];
                // add a new notification
                $noticeClass->add($notice_param);

                // if the one who placed the request is different from the policy holder
                // then notify the agent/broker together with the policy holder
                if($claimsData->created_by !== $claimsData->user_id) {
                    // form the notification parameters
                    $notice_param = (object) [
                        '_item_id' => $params->claim_id,
                        'user_id' => $claimsData->created_by,
                        'subject' => "Policy Claims",
                        'username' => $params->userData->username,
                        'remote' => false, 
                        'message' => "<strong>Hello {$from->created_by_name},</strong> the claim request for the claim on Policy: <a href='{{APPURL}}claims-view/{$params->claim_id}'><strong>{$claimsData->policy_id}</strong></a> has been <strong>{$params->status}</strong>.",
                        'notice_type' => 7,
                        'userId' => $params->userId,
                        'initiated_by' => 'system'
                    ];
                    // add a new notification
                    $noticeClass->add($notice_param);
                }

            }

            // refresh the page
            if(isset($params->the_button) && ($params->the_button == "save")) {
                // set the return log
                $return_log["additional"]["reload"] = "{$this->baseUrl}claims-view/{$params->item_id}";
            }

            // save the log for change in policy start date
            if(isset($params->assigned_to) && ($params->assigned_to !== "null") && ($claimsData->assigned_to !== $params->assigned_to)) {
                
                // name of the one who changed the status, and the name of the person it has been assigned to
                $from = $this->columnValue("a.name AS from_person_name, 
                    (SELECT b.name FROM users b WHERE b.item_id='{$claimsData->user_id}' LIMIT 1) AS client_name,
                    (SELECT b.name FROM users b WHERE b.item_id='{$params->assigned_to}' LIMIT 1) AS to_name", 
                    "users a", "a.item_id='{$claimsData->assigned_to}'"
                );

                // alert the user whom this complaint has been assigned to
                if($params->assigned_to !== $params->userId) {

                    // form the notification parameters
                    $notice_param = (object) [
                        '_item_id' => random_string("alnum", 32),
                        'user_id' => $params->assigned_to,
                        'subject' => "Policy Claims",
                        'username' => $params->userData->username,
                        'remote' => false, 
                        'message' => "<strong>{$params->userData->name}</strong> assigned an Insurance Claim request to you. <a title=\"Click to View\" href=\"{{APPURL}}claims-view/{$params->item_id}\">Click to view</a>.",
                        'notice_type' => 7,
                        'userId' => $params->userId,
                        'initiated_by' => 'system'
                    ];
                    
                    // add a new notification
                    $noticeClass->add($notice_param);
                }

                // if the complaint was initially assigned to someone
                if(isset($from->from_person_name)) {
                    $message = "Policy Claims initially assigned to <strong>{$from->from_person_name}</strong> was reassigned to <strong>{$from->to_name}</strong>.";
                } elseif(isset($from->to_name)) {
                    $message = "Policy Claims was assigned to <strong>{$from->to_name}</strong>.";
                } else {
                    $message = "Policy Claims has been reassigned";
                }

                // log the assigned to change
                $this->userLogs("claims", $params->item_id, $claimsData->assigned_to, $message, $params->userId, $notice);

                // trigger page reload
                $return_log["additional"]["reload"] = "{$this->baseUrl}claims-view/{$params->item_id}";
            }

            // commit the transaction
            $this->db->commit();

            return $return_log;

        } catch(PDOException $e) {
            $this->db->rollBack();
            return $e->getMessage();
        }

	}

    /**
     * Add the claims form
     * 
     * @param \stdClass $params
     * 
     * @return Array
     */
    public function form(stdClass $params) {

        // append the attachments
        $filesObj = load_class("files", "controllers");
        
        // confirm if the policy id really exist
        $policyCheck = $this->pushQuery("id, name", "policy_types", "item_id='{$params->policy_id}' AND deleted='0' AND company_id='{$params->userData->company_id}' LIMIT 1");
        
        // if the policy check is not empty
        if(empty($policyCheck)) {
            return ["data" => "Sorry! There is an existing policy with the same id"];
        }

        // other parameters
        $params->form_footnote = isset($params->form_footnote) ? addslashes(nl2br($params->form_footnote)) : null;
        $params->allow_attachment = isset($params->allow_attachment) ? $params->allow_attachment : "yes";

        /** Convert the form data into an array json */
        $params->form = json_decode($params->form, true);

        /** Policy form data */
        $formData = [
            "fields" => $params->form,
            "form_footnote" => $params->form_footnote,
            "allow_attachment" => $params->allow_attachment
        ];

        // add the claims form documents
        $params->_item_id = "{$params->policy_id}_claims_form";
        $attachments = $filesObj->prep_attachments("company_policy_claims_form_{$params->policy_id}", $params->userData->user_id, $params->_item_id);

        try {

            // insert the record into the database
            $stmt = $this->db->prepare("UPDATE policy_types SET claims_form = ? WHERE item_id = ? LIMIT 1");
            $stmt->execute([json_encode($formData), $params->policy_id]);

            // insert attachment
            $files = $this->db->prepare("INSERT INTO files_attachment SET resource= ?, resource_id = ?, description = ?, record_id = ?, created_by = ?, attachment_size = ?");
            $files->execute(["claims_form", $params->_item_id, json_encode($attachments), "{$params->_item_id}", $params->userId, $attachments["raw_size_mb"]]);

            // log the user activity
            $this->userLogs("company_policy", $params->_item_id, null, "<strong>{$params->userData->name}</strong> updated the claims form for the policy: {$policyCheck[0]->name}.", $params->userId);

            // return the success response
            return [
                "code" => 200,
                "data" => "The policy claims form has been successfully saved!",
                "additional" => [
                    "clear" => true,
                    "reload" => "{$this->baseUrl}policy-claim-form/{$params->policy_id}",
                ]
            ];

        } catch(PDOException $e) {
            return $e->getMessage();
        }
        
    }

    /**
     * Issue Claims Payment
     * 
     * @param String $params->claim_id
     * @param String $params->approved_amount
     * 
     * @return Array
     */
    public function issue_payment(stdClass $params) {
        /** Global variables */
        global $accessObject, $noticeClass;

        /** Permissions check */
        if(!$accessObject->hasAccess("approve", "claims")) {
            return ["code" => 203, "data" => $this->permission_denied];
        }

        /** Confirm that this claims does not already exist */
        $claimsData = $this->pushQuery("a.approved_amount, a.user_id, a.policy_id, a.created_by", 
            "users_policy_claims a", "a.item_id='{$params->claim_id}' AND a.company_id='{$params->userData->company_id}' AND a.status = 'Approved' LIMIT 1");

        // if the claims check is not empty
        if(empty($claimsData)) {
            return ["data" => "Sorry! The record was not found"];
        }

        // create a new payment checkout information 
        try {

            $this->db->beginTransaction();

            // update the claimsa approved amount
            $stmt = $this->db->prepare("UPDATE users_policy_claims 
                SET approved_amount = ?, paid_status = ?, payment_date = now() WHERE status = ? AND item_id = ?");
            $stmt->execute([$params->approved_amount, "Paid", "Approved", $params->claim_id]);

            // create the checkout token information
            $checkout = (object) [
                "item" => "claims", "item_id" => $params->claim_id, 
                "userId" => $params->userId, "initiated" => $params->userId,
                "payment_status" => "Paid"
            ];
            $checkout = load_class("payments", "controllers")->create($checkout);

            // save the user activity log
            $notice = "{$this->appName} Calculation<br>Property changed by a manual update effected by <strong>{$params->userData->name}</strong>.";
            
            // save the log for the change in amount claimed
            if(isset($params->approved_amount) && ($claimsData[0]->approved_amount !== $params->approved_amount)) {
                // save the status change information
                $this->userLogs("claims", $params->claim_id, $claimsData[0]->approved_amount, "<strong>Approved Amount</strong> was set to <strong>{$params->approved_amount}</strong>.", $params->userId, $notice);
                $return_log["additional"]["reload"] = "{$this->baseUrl}claims-view/{$params->claim_id}";
            }

            // notify the user of the payment 
            $from = $this->columnValue(
                "a.name AS client_name, (SELECT b.name FROM users b WHERE a.created_by = b.item_id LIMIT 1) AS created_by_name", 
                "users a", "a.item_id='{$claimsData[0]->user_id}'"
            );

            // if the one who placed the request is different from the policy holder
            // then notify the agent/broker together with the policy holder
            if($claimsData[0]->created_by !== $claimsData[0]->user_id) {
                // form the notification parameters
                $notice_param = (object) [
                    '_item_id' => $params->claim_id,
                    'user_id' => $claimsData[0]->created_by,
                    'subject' => "Policy Claims",
                    'username' => $params->userData->username,
                    'remote' => false, 
                    'message' => "<strong>Hello {$from->created_by_name},</strong> payment has been issued for the claim on Policy: {$claimsData[0]->policy_id}.",
                    'notice_type' => 7,
                    'userId' => $params->userId,
                    'initiated_by' => 'system'
                ];
                // add a new notification
                $noticeClass->add($notice_param);
            }

            // form the notification parameters
            $notice_param = (object) [
                '_item_id' => $params->claim_id,
                'user_id' => $claimsData[0]->user_id,
                'subject' => "Policy Claims",
                'username' => $params->userData->username,
                'remote' => false, 
                'message' => "<strong>Hello {$from->client_name},</strong> payment has been issued for your claim on Policy: {$claimsData[0]->policy_id}.",
                'notice_type' => 7,
                'userId' => $params->userId,
                'initiated_by' => 'system'
            ];
            
            // add a new notification
            $noticeClass->add($notice_param);
            
            // commit the transactions
            $this->db->commit();

            // return success
            return [
                "code" => 200,
                "data" => "Claims payment was successfully issued!",
                "additional" => [
                    "reload" => "{$this->baseUrl}claims-view/{$params->claim_id}"
                ]
            ];

        } catch(PDOException $e) {
            $this->db->rollBack();
        }

    }

}

?>