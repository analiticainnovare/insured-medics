<?php 

class Filters extends Medics {

    public function __construct()
    {
        parent::__construct();
    }

    public function list(stdClass $params) {
        
        /** Confirm that the data is an array */
        if(!is_array($params->data)) {
            return;
        }
        $request = $params->data["request"];

        // if the request for the users list
        if($request === "users_list") {
            /** Assign variables */
            $report_type = isset($params->data["report_type"]) ? $params->data["report_type"] : null;
            $policy_type = isset($params->data["policy_type"]) ? $params->data["policy_type"] : null;
        }

        /** Confirm user record using the email address */
        elseif(in_array($request, ["confirm_user_by_email", "confirm_broker_by_email"])) {
            if(!isset($params->data["email"])) {
                return;
            }
            if(!filter_var($params->data["email"], FILTER_VALIDATE_EMAIL)) {
                return;
            }
            /** Assign the user email address */
            $email_address = $params->data["email"];

            /** Load the user information */
            $users_list = $this->pushQuery("name, item_id, image, email, description, phone_number, user_type", "users", "email='{$email_address}' AND status='1' LIMIT 1");
            
            /**  */
            $name = !empty($users_list) ? $this->the_user_roles[$users_list[0]->user_type]["_role_title"] : "User";

            /** return the results */
            return [
                "additional" => [
                    "color" => !empty($users_list) ? "danger" : "success",
                    "data" => $users_list[0] ?? null
                ],
                "data" => !empty($users_list) ? "There is an existing {$name} with same Email Address <span class='font-weight-bold cursor' title='Click to view details' onclick='return quick_User_View()'>View Details</span>." : "This email address is available."
            ];

        }
        
    }

}
?>