<?php 

class Eduplus extends Medics {


    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Confirm that the user has liked the post
     * 
     * @param \stdClass $likes_list
     * @param String $user_id
     */
    public function is_liked($likes_list, $user_id) {

        if(empty($likes_list)) {
            return 0;
        }

        // convert the list into an array
        $likes_list = (array) $likes_list;

        // return boolean
        return in_array($user_id, $likes_list) ? 1 : 0;
    }

    /**
     * List the Category List
     * 
     * @return Array
     */
    public function category_list(stdClass $params) {
        
        // the number of rows to limit the query
		$params->limit = isset($params->limit) ? $params->limit : $this->global_limit;

        $params->query = "1 ";
        $params->query .= (isset($params->category_id) && !empty($params->category_id)) ? " AND a.item_id='{$params->category_id}'" : null;

        try {

            $stmt = $this->db->prepare("
                SELECT a.*
                FROM eduplus_category a
                WHERE {$params->query} AND a.status = ? ORDER BY a.id LIMIT {$params->limit}
            ");
            $stmt->execute([1]);

            $data = [];
            while($result = $stmt->fetch(PDO::FETCH_OBJ)) {

                $result->description = custom_clean(htmlspecialchars_decode($result->description));

                $data[] = $result;
            }

            return [
                "code" => 200,
                "data" => $data
            ];

        } catch(PDOException $e) {
            return [];
        }

    }

    /**
     * Add Category
     * 
     * @param String    $params->name
     * @param String    $params->message
     * 
     * @return Array
     */
    public function add_category(stdClass $params) {

        try {
            
            // create a new unique item id
            $item_id = random_string("alnum", 32);

            // prepare and execute the statement
            $stmt = $this->db->prepare("INSERT eduplus_category SET item_id = ?, name = ? ".(isset($params->message) ? ", description='{$params->message}'" : null)."");
            $stmt->execute([$item_id, $params->name]);

            // log the user activity
            $this->userLogs("eduplus_subject", $item_id, null, "<strong>{$params->userData->name}</strong> created a new category: {$params->name}.", $params->userId);

            // return the data
            return [
                "data" => "Category was successfully inserted.",
                "additional" => [
                    "clear" => true,
                    "reload" => "{$this->baseUrl}eduplus-manage",
                ],
                "code" => 200
            ];

        } catch(PDOException $e) {
            return [];
        }

    }

    /**
     * Update Category
     * 
     * @param String    $params->name
     * @param String    $params->message
     * @param String    $params->category_id
     * 
     * @return Array
     */
    public function update_category(stdClass $params) {

        /** Load the previous record */
        $prevData = $this->prevData("eduplus_category", $params->category_id);

        // if the param is empty
        if(empty($prevData)) {
            return ["code" => 203, "data" => "Sorry! An invalid category id was parsed"];
        }

        try {

            $stmt = $this->db->prepare("UPDATE eduplus_category SET last_updated = now(), name = ? ".(isset($params->message) ? ",description='{$params->message}'" : null)." WHERE item_id = ? LIMIT 1");
            $stmt->execute([$params->name, $params->category_id]);

            // return the data
            return [
                "data" => "Category was successfully updated.",
                "additional" => [
                    "reload" => "{$this->baseUrl}eduplus-manage",
                ],
                "code" => 200
            ];

        } catch(PDOException $e) {
            return [];
        }

    }

    /**
     * List the Subjects List
     * 
     * @return Array
     */
    public function subject_list(stdClass $params) {
        
        // the number of rows to limit the query
		$params->limit = isset($params->limit) ? $params->limit : $this->global_limit;

        $simpleLoad = isset($params->minified) ? true : false;

        $params->query = "1 ";
        $params->query .= (isset($params->q) && !empty($params->q)) ? " AND a.subject LIKE '%{$params->q}%'" : null;
        $params->query .= (isset($params->subject_id) && !empty($params->subject_id)) ? " AND a.item_id='{$params->subject_id}'" : null;
        $params->query .= (isset($params->category_id) && !empty($params->category_id)) ? " AND a.category_id='{$params->category_id}'" : null;
        $params->query .= (isset($params->exempt) && !empty($params->exempt)) ? " AND a.item_id NOT IN {$this->inList($params->exempt)}" : null;

        // if the search term is parsed
        if(isset($params->q)) {
            // set the search term in a session
            $this->session->set("searchTerm", $params->q);
        }
        
        try {

            $stmt = $this->db->prepare("
                SELECT a.*,
                    (SELECT b.description FROM files_attachment b WHERE b.record_id = a.item_id ORDER BY b.id DESC LIMIT 1) AS attachment,
                    (SELECT CONCAT(name,'|',date_created,'|',description) FROM eduplus_category b WHERE b.item_id = a.category_id LIMIT 1) AS category_info
                FROM eduplus a
                WHERE {$params->query} AND a.status = ? ORDER BY a.id LIMIT {$params->limit}
            ");
            $stmt->execute([1]);

            // create a new object
            $filesObject = load_class("forms", "controllers");

            $data = [];
            while($result = $stmt->fetch(PDO::FETCH_OBJ)) {

                // if not a simple load is requested
                if(!$simpleLoad) {

                    // attachment
                    $result->likes_list = !empty($result->likes_list) ? json_decode($result->likes_list, true) : [];
                    $result->attachment = json_decode($result->attachment);
                    $result->category_info = !empty($result->category_info) ? (object) $this->stringToArray($result->category_info, "|", ["name", "date_created", "description"], true) : (object) [];
                    
                    // convert the category information into an array
                    $result->category_info->description = htmlspecialchars_decode($result->category_info->description);
                    
                    // if the files is set
                    if(isset($result->attachment->files)) {
                        // format the attachements list
                        $result->attachment_html = $filesObject->list_attachments($result->attachment->files, $result->created_by, "col-lg-4 col-md-6", false, false);
                    } else {
                        $result->attachment = (object) $this->fake_files;
                    }    
                }

                // confirm that the user has liked it
                $result->is_liked = $this->is_liked($result->likes_list, $params->userId);
                
                // clean the description
                $result->description = custom_clean(htmlspecialchars_decode($result->description));

                $data[] = $result;
            }

            return [
                "code" => 200,
                "data" => $data
            ];

        } catch(PDOException $e) {
            return [];
        }

    }

    /**
     * Add Subject
     * 
     * @param String    $params->name
     * @param String    $params->message
     * 
     * @return Array
     */
    public function add_subject(stdClass $params) {

        try {

            // create a new unique item id
            $item_id = random_string("alnum", 32);

            // append the attachments
            $filesObj = load_class("files", "controllers");
            $attachments = $filesObj->prep_attachments("eduplus_subject", $params->userData->user_id, $item_id);

            // prepare and execute the statement
            $stmt = $this->db->prepare("INSERT eduplus SET item_id = ?, subject = ?, category_id = ?, created_by = ?
                ".(isset($params->message) ? ", description='{$params->message}'" : null)."
                ".(isset($params->allow_comments) ? ", allow_comments='{$params->allow_comments}'" : null)."
            ");
            $stmt->execute([$item_id, $params->subject, $params->category_id, $params->userId]);

            // insert attachment    
            $files = $this->db->prepare("INSERT INTO files_attachment SET resource= ?, resource_id = ?, description = ?, record_id = ?, created_by = ?, attachment_size = ?");
            $files->execute(["eduplus_subject", $item_id, json_encode($attachments), "{$item_id}", $params->userId, $attachments["raw_size_mb"]]);

            // log the user activity
            $this->userLogs("eduplus_subject", $item_id, null, "<strong>{$params->userData->name}</strong> created a new subject: {$params->subject}.", $params->userId);

            // return the data
            return [
                "data" => "Subject was successfully inserted.",
                "additional" => [
                    "clear" => true,
                    "reload" => "{$this->baseUrl}eduplus-view/{$params->subject_id}",
                ],
                "code" => 200
            ];

        } catch(PDOException $e) {
            return [];
        }

    }

    /**
     * Update Subject
     * 
     * @param String    $params->name
     * @param String    $params->message
     * @param String    $params->subject_id
     * 
     * @return Array
     */
    public function update_subject(stdClass $params) {

        /** param */
        $param = (object) ["subject_id" => $params->subject_id, "userId" => $params->userId,];

        /** Load the previous record */
        $prevData = $this->subject_list($param)["data"];

        // if the param is empty
        if(empty($prevData)) {
            return ["code" => 203, "data" => "Sorry! An invalid subject id was parsed"];
        }
        $prevData = $prevData[0];

        // initialize
        $initial_attachment = [];

        /** Confirm that there is an attached document */
        if(!empty($prevData->attachment)) {
            // decode the json string
            $db_attachments = $prevData->attachment;
            // get the files
            if(isset($db_attachments->files)) {
                $initial_attachment = $db_attachments->files;
            }
        }

        // append the attachments
        $filesObj = load_class("files", "controllers");
        $attachments = $filesObj->prep_attachments("eduplus_subject", $params->userData->user_id, $params->subject_id, $initial_attachment);

        try {

            $stmt = $this->db->prepare("UPDATE eduplus SET last_updated = now(), subject = ? 
                ".(isset($params->message) ? ", description='{$params->message}'" : null)."
                ".(isset($params->allow_comments) ? ", allow_comments='{$params->allow_comments}'" : null)."
                ".(isset($params->category_id) ? ", category_id='{$params->category_id}'" : null)."
                WHERE item_id = ? LIMIT 1
            ");
            $stmt->execute([$params->subject, $params->subject_id]);

            // only insert attachment record if there was an attachment to the comment
            $files = $this->db->prepare("UPDATE files_attachment SET description = ?, attachment_size = ? WHERE record_id = ? LIMIT 1");
            $files->execute([json_encode($attachments), $attachments["raw_size_mb"], $params->subject_id]);

            // return the data
            return [
                "data" => "Subject was successfully updated.",
                "additional" => [
                    "reload" => "{$this->baseUrl}eduplus-view/{$params->subject_id}",
                ],
                "code" => 200
            ];

        } catch(PDOException $e) {
            return [];
        }

    }

    /**
     * Set the user as having viewed the advert
     * 
     * @param String $param->advert_id
     * 
     * @return Array
     */
    public function views(stdClass $params) {
        
        // query the parameter
        $today = date("Y-m-d");
        $param = (object) [
            "limit" => 1,
            "subject_id" => $params->subject_id,
            "userData" => $params->userData,
        ];

        // get the new information
        $data = isset($params->data) ? $params->data : $this->subject_list($param)["data"];
        
        // confirm if the topic id is not empty
        if(empty($data)) {
            return ["code" => 203, "data" => "An invalid topic id was submitted for processing."];
        }
        $data = isset($params->data) ? $params->data : $data[0];

        // seen by information
        $seen_by_list = json_decode($data->seen_by, true);
        
        // append to the list
        if(!in_array($params->userId, $seen_by_list)) {

            // append to the array list
            array_push($seen_by_list, $params->userId);

            // append the clicks and views
            $views = $data->organic_views + 1;

            // update the statistics information
            $stmt = $this->db->prepare("UPDATE eduplus SET organic_views = ?, seen_by = ? WHERE item_id = ? LIMIT 1");
            $stmt->execute([$views, json_encode($seen_by_list), $params->subject_id]);

            // viewed list
            $viewed_list = [
                "{$params->userId}" => [
                    "fullname" => $params->userData->name,
                    "email" => $params->userData->email,
                    "viewed_date" => date("Y-m-d h:iA"),
                    "useragent" => "{$this->browser}|{$this->platform}|{$this->ip_address}"
                ]
            ];

            // if the page statistics is empty
            if(empty($data->statistics)) {
                $stmt = $this->db->prepare("INSERT INTO eduplus_views_log SET subject_id = ?, views_list = ?");
                $stmt->execute([$params->subject_id, json_encode($viewed_list)]);
            }
            // update the list of viewed users
            else {
                // get the record
                $record = $this->pushQuery("views_list, clicks_list", "eduplus_views_log", "subject_id='{$params->subject_id}' LIMIT 1");
                $existing_viewed_list = json_decode($record[0]->views_list, true);

                array_push($existing_viewed_list, $viewed_list);
                $stmt = $this->db->prepare("UPDATE INTO eduplus_views_log SET views_list = ? WHERE subject_id = ? LIMIT 1");
                $stmt->execute([json_encode($viewed_list), $params->subject_id]);
            }
        } else {
            // update the statistics information
            $stmt = $this->db->prepare("UPDATE eduplus SET viral_views = (viral_views+1)  WHERE item_id = ? LIMIT 1");
            $stmt->execute([$params->subject_id]);
        }      

        return [
            "code" => 200,
            "data" => "Topic statistics successfully recorded."
        ];

    }
    
    /**
     * Like/Unlike a Topic
     * 
     * Like the post if not already in the array list, else unlike the post
     * Return the new count back as part of the results
     * 
     * @param String $params->subject_id
     * 
     * @return Array
     */
    public function liked(stdClass $params) {

        /** load the post */
        $likes_list = $this->db->prepare("SELECT likes_list FROM eduplus WHERE item_id = ? AND status = ? LIMIT 1");
        $likes_list->execute([$params->subject_id, 1]);
        $result = $likes_list->fetch(PDO::FETCH_OBJ);

        /** return if empty */
        if(empty($result)) {
            return;
        }

        /** Convert the list into an array */
        $list = !empty($result->likes_list) ? json_decode($result->likes_list, true) : [];

        /** append or remove from list */
        if(!empty($list)) {

            $is_found = false;
            $is_found_key = null;

            // loop through the list
            foreach($list as $key => $value) {
                // if the username is found
                if($value == $params->userId) {
                    // remove the key from the list
                    $is_found = true;
                    $is_found_key = $key;
                    break;
                }
            }
            
            // if the key was found
            if($is_found) {
                
                // remove the value
                unset($list[$is_found_key]);
                
                // likes count
                $likes_count = count($list);

                // update the database table
                $this->db->query("UPDATE eduplus SET likes_list='".json_encode($list)."', likes_count=(likes_count-1) WHERE item_id='{$params->subject_id}' LIMIT 1");
            } else {
                // append the user id
                array_push($list, $params->userId);
                $likes_count = count($list);

                // update the database table
                $this->db->query("UPDATE eduplus SET likes_list='".json_encode($list)."', likes_count=(likes_count+1) WHERE item_id='{$params->subject_id}' LIMIT 1");
            }
        } else {
            // create a new array list
            $list[] = $params->userId;
            $likes_count = 1;

            // update the database table
            $this->db->query("UPDATE eduplus SET likes_list='".json_encode($list)."', likes_count='1' WHERE item_id='{$params->subject_id}' LIMIT 1");
        }

        return [
            "code" => 200,
            "data" => $likes_count
        ];

    }

}