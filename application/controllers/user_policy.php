<?php
// ensure this file is being included by a parent file
if( !defined( 'BASEPATH' ) ) die( 'Restricted access' );

class User_policy extends Medics {
	
	# start the construct
	public function __construct() {
		parent::__construct();
	}
	
	/**
	 * Global function to search for item based on the predefined columns and values parsed
	 * 
	 * @param \stdClass $params
	 * @param String $params->user_id
	 * @param String $params->complaint_id  
	 * @param String $params->company_id
	 * @param String $params->date
	 * 
	 * @return Object
	 */
	public function list(stdClass $params = null) {

		$params->query = "1 ";
        $is_permitted = false;

        $the_user_type = isset($params->userData) ? $params->userData->user_type : "";

        // if the user id parameter was not parsed
		if(!isset($params->user_id) && !isset($params->internal_loop)) {
			// perform some checks
			$the_user_type = $params->userData->user_type;
			// go ahead
			if(in_array($the_user_type, ["insurance_company"])) {
				$params->or_clause = " AND (a.company_id = '{$params->userData->company_id}' AND submit_status='save')";
			}
            // if insurance company user
            elseif(in_array($the_user_type, ["agent", "broker"])) {
                $params->or_clause = " AND (a.{$the_user_type}_id = '{$params->userId}' OR a.assigned_to='{$params->userId}' OR a.created_by='{$params->userId}')";
            }
            // if insurance company user
            elseif(in_array($the_user_type, ["bancassurance"])) {
                $params->or_clause = " AND (a.created_by = '{$params->userId}' OR a.assigned_to='{$params->userId}')";
            }
            // if insurance company user
            elseif(in_array($the_user_type, ["user", "business"])) {
                $params->or_clause = " AND (a.user_id = '{$params->userId}')";
            }
            
            // if insurance company user
            if(isset($params->include_insurance_company)) {
                $params->or_clause = " AND (a.created_by = '{$params->userId}' OR a.assigned_to='{$params->userId}')";
            }
		}

		// if the field is null
		$params->query .= (isset($params->policy_id)) ? " AND (a.policy_id='{$params->policy_id}' OR a.item_id='{$params->policy_id}')" : null;
        $params->query .= (isset($params->user_id) && !empty($params->user_id)) ? " AND a.user_id='{$params->user_id}'" : null;
        $params->query .= (isset($params->the_policy_id) && !empty($params->the_policy_id)) ? " AND a.policy_id='{$params->the_policy_id}'" : null;
		$params->query .= (isset($params->status) && !empty($params->status)) ? " AND a.policy_status='{$params->status}'" : null;
        $params->query .= (isset($params->company_id) && !empty($params->company_id)) ? " AND a.company_id='{$params->company_id}'" : null;
        $params->query .= (isset($params->created_by) && !empty($params->created_by)) ? " AND a.created_by='{$params->created_by}'" : null;
        $params->query .= (isset($params->claim_id) && !empty($params->claim_id)) ? " AND a.claim_id='{$params->claim_id}'" : null;
        $params->query .= (isset($params->policy_type) && !empty($params->policy_type)) ? " AND a.policy_type='{$params->policy_type}'" : null;
        $params->query .= (isset($params->or_clause) && !empty($params->or_clause)) ? $params->or_clause : null;
		$params->query .= (isset($params->date)) ? " AND DATE(a.date_created) ='{$params->date_created}'" : null;
        $params->query .= (isset($params->date_range) && !empty($params->date_range)) ? $this->dateRange($params->date_range, "a", "date_submitted") : null;
        
		// the number of rows to limit the query
		$params->limit = isset($params->limit) ? $params->limit : $this->global_limit;

        try {
            // make the request for the record from the model
            $stmt = $this->db->prepare("
                SELECT 
                    ".(isset($params->columns) ? $params->columns : "a.*, 
                    a.item_id, a.user_id, u.name AS client_name, u.phone_number AS client_contact, 
                    u.email AS client_email, u.image AS client_image, DATE(u.date_created) AS client_joined,
                    (SELECT name FROM users WHERE users.item_id = a.created_by LIMIT 1) AS created_by_name,
                    (SELECT name FROM users WHERE users.item_id = a.broker_id LIMIT 1) AS broker_name,
                    (SELECT b.description FROM files_attachment b WHERE b.record_id = a.item_id ORDER BY b.id DESC LIMIT 1) AS attachment,
                    (SELECT name FROM users WHERE users.item_id = a.assigned_to LIMIT 1) AS assigned_to_name,
                    (SELECT b.id FROM users_items_cancellation_request b WHERE b.record_id = a.policy_id AND b.status='Pending' LIMIT 1) AS pending_cancel_request,
                    (SELECT name FROM users WHERE users.item_id = a.agent_id LIMIT 1) AS agent_name,
                    (SELECT name FROM policy_form WHERE a.payment_option = policy_form.id LIMIT 1) AS payment_option_name,
                    (SELECT name FROM policy_form WHERE a.payment_interval = policy_form.id LIMIT 1) AS payment_interval_name
                ").", (SELECT b.category FROM policy_types b WHERE b.item_id = a.policy_type LIMIT 1) AS policy_category,
                a.company_id, (SELECT name FROM companies WHERE companies.item_id = a.company_id LIMIT 1) AS company_name
                FROM users_policy a
                LEFT JOIN users u ON u.item_id = a.user_id
                WHERE {$params->query} AND a.deleted='0' ORDER BY a.id DESC LIMIT {$params->limit}
            ");
            $stmt->execute();

            // comments object
            $loadInteractions = isset($params->load_comments) ? true : false;

            // if the loadInteractions variable is true
            if($loadInteractions) {
                $threadInteraction = (object)[
                    "userId" => $params->userId,
                    "feedback_type" => "comment"
                ];
                $commentsObj = load_class("replies", "controllers");
                $filesObject = load_class("forms", "controllers");
            }

            $row = 0;
            $data = [];
            while($result = $stmt->fetch(PDO::FETCH_OBJ)) {
                // unset two parameters from the result set
                unset($result->id);

                // if attachment variable was parsed
                if(isset($result->attachment)) {
                    $result->attachment_html = "";
                    $result->attachment = json_decode($result->attachment);
                }

                // if payment plan is attached
                if(isset($result->form_answers)) {
                    $result->payment_plan = json_decode($result->payment_plan);
                    $result->form_fields = json_decode($result->form_fields);
                    $result->form_answers = json_decode($result->form_answers);
                    $result->policy_type_details = json_decode($result->policy_type_details);
                }
                
                // if the agent is not empty
                if(isset($result->client_name)) {
                    $result->managed_by = [
                        "title" => "", 
                        "id" => $result->user_id, 
                        "name" => $result->client_name
                    ];
                }

                // run this section if the policy status has been parsed
                if(isset($result->policy_status)) {

                    // if not a remote request
                    if((isset($params->remote) && !$params->remote) || !isset($params->remote)) {
                        
                        // the action button to return
                        $result->action = "";
                        $result->policy_name_tag = "";

                        // if the broker id was requested
                        if(isset($result->broker_id) && !empty($result->broker_id)) {
                            $result->managed_by = [
                                "title" => " - Broker",
                                "id" => $result->broker_id,
                                "name" => $result->broker_name
                            ];
                        }

                        // if the agent id is set
                        if(isset($result->agent_id) && !empty($result->agent_id)) {
                            // if the agent is not empty
                            $result->managed_by = [
                                "title" => " - Agent",
                                "id" => $result->agent_id,
                                "name" => $result->agent_name
                            ];
                        }

                        // if the assigned to is set
                        if(isset($result->assigned_to) && !empty($result->assigned_to)) {
                            // if the broker id is not empty
                            if(!$result->broker_id && !$result->agent_id) {
                                $result->managed_by = [
                                    "title" => " - Manager",
                                    "id" => $result->assigned_to,
                                    "name" => $result->assigned_to_name
                                ];
                            }
                        }

                        $result->policy_category = ucwords($result->policy_category);

                        // set the label for the policy
                        $result->the_status_label = $this->the_status_label($result->policy_status);
                        
                        // additional tag for insurance companies and agents
                        if(in_array($the_user_type, ["agent", "insurance_company", "admin"])) {
                            // load this section if the client name was parsed in the response
                            if(isset($result->client_name)) {
                                $result->policy_name_tag .= "<p><strong class=\"text-muted\">Client Name:</strong> <span class=\"underline cursor\" onclick=\"return user_basic_information('{$result->user_id}');\">{$result->client_name}</span></p>";
                            }
                        }

                        // the name tag with the submit status
                        $result->policy_name_tag .= "{$result->policy_name} &nbsp;".(($result->submit_status == "draft") ? "<p><span class='badge badge-primary'>Draft</span></p>" : null)."<p><span class='font-weight-bolder'><a href='{$this->baseUrl}policies-view/{$result->item_id}'>{$result->policy_id}</a></span></p>";

                        // additional tags
                        if(!in_array($the_user_type, ["agent", "insurance_company"])) {
                            $result->policy_name_tag .= "<p><strong class=\"text-muted\">Managed by:</strong> <span class=\"underline cursor\" onclick=\"return company_basic_information('{$result->company_id}');\">{$result->company_name}</span></p>";
                        }

                        // display the cancel policy notice
                        if(isset($result->pending_cancel_request)) {
                            $result->policy_name_tag .= "<span class=\"text-danger font-italic\">Cancel policy request pending</span>";
                        }

                        // view buttons
                        $result->action .= " &nbsp; <a class='btn p-1 btn-outline-success m-0 btn-sm' title='Click to view details of this policy' href='{$this->baseUrl}policies-view/{$result->item_id}'><i class='fa fa-eye'></i></a>";
                        
                        // if this user created the complaint and its still in a draft stage
                        if(($result->submit_status == "draft") && (($result->user_id == $params->userId) || ($result->agent_id == $params->userId) || ($result->created_by == $params->userId))) {
                            $result->action .= " &nbsp; <a class='btn btn-outline-danger p-1 m-0 modify-record btn-sm' href='javascript:void(0)' onclick='return delete_record(\"user_policy\",\"{$result->item_id}\");' title='Click to delete policy'><i class='fa fa-trash-alt'></i></a>";
                        }

                    }
                }

                // load policy comments
                if($loadInteractions) {

                    // init the attachment_html
                    $result->attachment_html = "";

                    // create a new object
                    $threadInteraction->resource_id = $result->item_id;
                    $result->comments_list = $commentsObj->list($threadInteraction);
                    
                    // if the user is permitted ie. an insurance company or admin
                    $result->is_permitted = $is_permitted;
                    
                    // load and return the replies list as well
                    if($is_permitted) {
                        $threadInteraction->feedback_type = "reply";
                        $result->replies_list = $commentsObj->list($threadInteraction);
                    }

                    // if the files is set
                    if(isset($result->attachment->files)) {
                        // format the attachements list
                        $result->attachment_html = $filesObject->list_attachments($result->attachment->files, $result->user_id, "col-lg-6 col-md-6", false, false);
                    } else {
                        $result->attachment = (object) [
                            "files" => [],
                            "files_count" => 0,
                            "files_size" => 0,
                            "raw_size_mb" => 0
                        ];
                    }
                }
                
                $row++;
                $result->row_id = $row;
                // append to the array list
                $data[] = $result;
            }

            // return the data
            return [
                "data" => $data,
                "code" => 200
            ];
        } catch(PDOException $e) {
            return $this->unexpected_error;
        }

	}

    /**
     * Add a new user policy
     * 
     * @param \stdClass $params
     * 
     * @return Array
     */
    public function add(stdClass $params) {

        // global variables
        global $noticeClass;

        /** Validate the user variables parsed */
        $params->_item_id = random_string("alnum", 32);

        /** Load the policy information */
        $policyObj = load_class("company_policy", "controllers");

        /** Parameters */
        $param = (object) [
            "policy_type_id" => $params->policy_id,
            "remote" => true,
            "limit" => 1
        ];

        // request
        $policyRecord = $policyObj->list($param);

        // if empty itemFound then return false
        if(empty($policyRecord["data"])) {
            return ["data" => "Sorry! An invalid policy id was parse", "code" => 203];
        }

        // get the policy record
        $policyRecord = $policyRecord["data"][0];
        $submit_status = isset($params->the_button) ? strtolower($params->the_button) : "save";

        // append the attachments
        $filesObj = load_class("files", "controllers");
        $attachments = $filesObj->prep_attachments("user_policy_{$params->policy_id}", $params->userId, $params->_item_id);

        // generate the policy id using the policy code, the count and the year
        $counter = $this->append_zeros(($policyRecord->policies_count + 1), $this->append_zeros);
        $policy_id = $policyRecord->policy_code.$counter.date("Y");

        try {

            // user type here
            if(in_array($params->userData->user_type, ["agent"])) {
                $agent_id = $params->userId;
                $assigned_to = $params->userId;
            }

            // broker
            if(in_array($params->userData->user_type, ["broker"])) {
                $broker_id = $params->userId;
            }

            // get the company id form the policy record
            $stmt = $this->db->prepare("
                INSERT INTO users_policy SET 
                    item_id = ?, date_created = now(), created_by = ?, user_id = ?, company_id = ?, submit_status = ?,
                    form_fields = ?, form_answers = ?, policy_name = ?, policy_type = ?, requirements = ?, policy_type_details = ?
                    ".(isset($agent_id) ? ",agent_id='{$agent_id}'" : null)." 
                    ".(isset($broker_id) ? ",broker_id='{$broker_id}'" : null)."
                    ".(isset($assigned_to) ? ",assigned_to='{$assigned_to}'" : null)."
                    ".(isset($policy_id) ? ",policy_id='{$policy_id}'" : null)."
                    ".(($submit_status == "save") ? ",date_submitted=now()" : null)."
            ");
            $stmt->execute([
                $params->_item_id, $params->userId, $params->user_id, $policyRecord->company_id, $submit_status,
                json_encode($policyRecord->policy_form->fields), json_encode($params->field), $policyRecord->name,
                $policyRecord->item_id, $policyRecord->requirements, $policyRecord->description
            ]);

            // log the user activity
            $this->userLogs("insurance_policy", $params->_item_id, null, "<strong>{$params->userData->name}</strong> applied for a new policy: {$policyRecord->name}.", $params->userId);

            // update the replies count for the resource
            $this->db->query("UPDATE policy_types SET policies_count=(policies_count+1) WHERE item_id='{$params->policy_id}' LIMIT 1");
            
            // set the new Policies count
            $policies_count = $policyRecord->policies_count + 1;
            
            // add the activity for the activity
            $this->userLogs("Policy applications count", $params->policy_id, null, "Number of policy applications is set to {$policies_count}.", $params->userId, "{$this->appName} Calculation<br>Policy application count increased by the creation of a new request by <strong>{$params->userData->name}</strong>.");
            
            // insert attachment
            $files = $this->db->prepare("INSERT INTO files_attachment SET resource= ?, resource_id = ?, description = ?, record_id = ?, created_by = ?, attachment_size = ?");
            $files->execute(["insurance_policy", $params->_item_id, json_encode($attachments), "{$params->_item_id}", $params->userId, $attachments["raw_size_mb"]]);

            // notify the user if the policy was applied on his/her behalf
            if($params->userId !== $params->user_id) {

                // form the notification parameters
                $notice_param = (object) [
                    '_item_id' => $params->_item_id,
                    'user_id' => $params->user_id,
                    'subject' => "Insurance Policy",
                    'username' => $params->userData->username,
                    'remote' => false, 
                    'message' => "<strong>{$params->userData->name}</strong> applied for an insurance policy on your behalf. <a title=\"Click to View\" href=\"{{APPURL}}policies-view/{$params->_item_id}\">Click to view</a>.",
                    'notice_type' => 6,
                    'userId' => $params->userId,
                    'initiated_by' => 'system'
                ];
                
                // add a new notification
                $noticeClass->add($notice_param);
                
            }

            // return the success response
            return [
                "code" => 200,
                "data" => "The policy has been successfully saved!",
                "additional" => [
                    "clear" => true,
                    "reload" => "{$this->baseUrl}policies-view/{$params->_item_id}",
                ]
            ];
            
        } catch(PDOException $e) {
            return $e->getMessage();
        }
    }

	/**
	 * Update a user policy record
	 * 
	 * @param \stdClass $params
	 * 
	 * @return Array
	 */
	public function update(stdClass $params) {
		
        /** Confirm that this policy does not already exist */
        $policyCheck = $this->pushQuery("a.*, (SELECT b.description FROM files_attachment b WHERE b.record_id = a.item_id ORDER BY a.id DESC LIMIT 1) AS attachment", 
            "users_policy a", "a.item_id='{$params->item_id}' AND a.policy_status !='Deleted' LIMIT 1");

        // if the policy check is not empty
        if(empty($policyCheck)) {
            return ["code" => 203, "data" => "Sorry! The policy record was not found"];
        }

        // initialize
        $initial_attachment = [];

        // define the module
        $module = "user_policy_{$params->item_id}";

        // policy data
        $policyData = $policyCheck[0];

        /** Check if the premium has been set */
        if(isset($params->policy_status) && ($params->policy_status === "Approved")) {
            /** Confirm that the premium has been set */
            if(isset($params->premium) && (round($params->premium) < 2)) {
                /** Return an error message */
                return ["code" => 203, "data" => "Sorry! The policy premium must be set and should be at least GHS2.00"];
            } elseif(!isset($params->premium) && (round($policyData->premium) < 2)) {
                /** Return an error message */
                return ["code" => 203, "data" => "Sorry! The policy premium must be set and should be at least GHS2.00"];
            }
        }

        /** Confirm that there is an attached document */
        if(!empty($policyData->attachment)) {
            // decode the json string
            $db_attachments = json_decode($policyData->attachment);
            // get the files
            if(isset($db_attachments->files)) {
                $initial_attachment = $db_attachments->files;
            }
        }

        /** Next repayment date algorithm */
        if(isset($params->policy_start_date) && ($params->policy_start_date !== $policyData->policy_start_date)) {
            $next_repayment_date = date("Y-m-d", strtotime("{$params->policy_start_date} +1 month"));
        }

        // modify the return log
        $return_log = [
            "code" => 200,
            "data" => "The policy has been successfully updated!",
            "additional" => []
        ];

        // append the attachments
        $filesObj = load_class("files", "controllers");
        $attachments = $filesObj->prep_attachments($module, $params->userData->user_id, $params->item_id, $initial_attachment);

        // global variable
        global $noticeClass;
        
        try {

            $this->db->beginTransaction();

            // update the user policy
            $stmt = $this->db->prepare("
                UPDATE users_policy SET last_updated = now()
                    ".(isset($params->field) ? ",form_answers='".json_encode($params->field)."'" : null)."
                    ".(isset($params->agent_id) ? ",agent_id='{$params->agent_id}'" : null)." 
                    ".(isset($params->broker_id) ? ",broker_id='{$params->broker_id}'" : null)."
                    ".(isset($next_repayment_date) ? ",next_repayment_date='{$next_repayment_date}'" : null)."
                    ".(isset($params->policy_id) ? ",policy_id='{$params->policy_id}'" : null)."
                    ".(isset($params->premium) ? ",premium='{$params->premium}'" : null)."
                    ".((isset($params->assigned_to) && $params->assigned_to !== "null") ? ",assigned_to='{$params->assigned_to}'" : null)."
                    ".(isset($params->the_button) ? ",submit_status='{$params->the_button}'" : null)."
                    ".(isset($params->policy_status) ? ",policy_status='".ucfirst($params->policy_status)."'" : null)."
                    ".(isset($params->policy_start_date) ? ",policy_start_date='{$params->policy_start_date}'" : null)."
                    ".((isset($params->the_button) && $params->the_button == "save") ? ",date_submitted=now()" : null)."
                WHERE item_id = ? LIMIT 1
            ");
            $stmt->execute([$params->item_id]);

            // update attachment if already existing
            if(isset($db_attachments)) {
                $files = $this->db->prepare("UPDATE files_attachment SET description = ?, attachment_size = ? WHERE record_id = ? LIMIT 1");
                $files->execute([json_encode($attachments), $attachments["raw_size_mb"], $params->item_id]);
            } else {
                // insert the record if not already existing
                $files = $this->db->prepare("INSERT INTO files_attachment SET resource= ?, resource_id = ?, description = ?, record_id = ?, created_by = ?, attachment_size = ?");
                $files->execute(["insurance_policy", $params->item_id, json_encode($attachments), "{$params->item_id}", $params->userId, $attachments["raw_size_mb"]]);
            }

            // set the notice
            $notice = "{$this->appName} Calculation<br>Property changed by a manual update effected by <strong>{$params->userData->name}</strong>.";

            // if the previous is not the same as the current
            if(isset($params->field) && ($policyData->form_answers !== json_encode($params->field))) {
                // save the form change information
                $this->userLogs("insurance_policy", $params->item_id, $policyData->form_answers, "Policy form content has been altered.", $params->userId);
                // set the return log
                $return_log["additional"]["reload"] = "{$this->baseUrl}policies-view/{$params->item_id}";
            }

            // save the log for change in premium
            if(isset($params->premium) && ($policyData->premium !== $params->premium)) {
                // save the status change information
                $this->userLogs("insurance_policy", $params->item_id, $policyData->premium, "<strong>Policy Premium</strong> was changed from <strong>{$policyData->premium}</strong> to <strong>{$params->premium}</strong>.", $params->userId);
                // set the return log
                $return_log["additional"]["reload"] = "{$this->baseUrl}policies-view/{$params->item_id}";
            }

            // save the log for the status change
            if(isset($params->policy_status) && ($policyData->policy_status !== $params->policy_status)) {
                // set the return log
                $return_log["additional"]["reload"] = "{$this->baseUrl}policies-view/{$params->item_id}";
                
                // save the status change information
                $this->userLogs("insurance_policy", $params->item_id, $policyData->policy_status, "<strong>Policy Status</strong> was changed from <strong>{$policyData->policy_status}</strong> to <strong>{$params->policy_status}</strong>.", $params->userId);
                
                // name of the one who changed the status, and the name of the person it has been assigned to
                $from = $this->columnValue("a.name AS client_name, 
                    (SELECT b.name FROM users b WHERE b.item_id='{$policyData->created_by}' LIMIT 1) AS created_by_name", 
                    "users a", "a.item_id='{$policyData->user_id}'"
                );

                // notify the user of a change in status
                $notice_param = (object) [
                    '_item_id' => random_string("alnum", 32),
                    'user_id' => $policyData->user_id,
                    'subject' => "Insurance Policy",
                    'username' => $params->userData->username,
                    'remote' => false, 
                    'message' => "The Policy: <a title=\"Click to View\" href=\"{{APPURL}}policies-view/{$params->item_id}\"><strong>{$policyData->policy_id}</strong></a> status has been changed to: <strong>{$params->policy_status}</strong>.",
                    'notice_type' => 6,
                    'userId' => $params->userId,
                    'initiated_by' => 'system'
                ];
                // add a new notification
                $noticeClass->add($notice_param);

                // if the user who created it is not the same as the user id
                if($policyData->user_id !== $policyData->created_by) {
                    // notify the user of a change in status
                    $notice_param = (object) [
                        '_item_id' => random_string("alnum", 32),
                        'user_id' => $policyData->created_by,
                        'subject' => "Insurance Policy",
                        'username' => $params->userData->username,
                        'remote' => false, 
                        'message' => "The Policy: <a title=\"Click to View\" href=\"{{APPURL}}policies-view/{$params->item_id}\"><strong>{$policyData->policy_id}</strong></a> status has been changed to: <strong>{$params->policy_status}</strong>.",
                        'notice_type' => 6,
                        'userId' => $params->userId,
                        'initiated_by' => 'system'
                    ];
                    // add a new notification
                    $noticeClass->add($notice_param);
                }
            }

            // refresh the page
            if(isset($params->the_button) && ($params->the_button == "save")) {
                // set the return log
                $return_log["additional"]["reload"] = "{$this->baseUrl}policies-view/{$params->item_id}";
            }

            // save the log for change in policy start date
            if(isset($params->policy_start_date) && ($policyData->policy_start_date !== $params->policy_start_date)) {
                // save the status change information
                $this->userLogs("insurance_policy", $params->item_id, $policyData->policy_start_date, "<strong>Policy Start Date</strong> was changed from <strong>{$policyData->policy_start_date}</strong> to <strong>{$params->policy_start_date}</strong>.", $params->userId);
            }

            // save the log for change in policy start date
            if(isset($params->assigned_to) && ($params->assigned_to !== "null") && ($policyData->assigned_to !== $params->assigned_to)) {
                
                // name of the one who changed the status, and the name of the person it has been assigned to
                $from = $this->columnValue("a.name AS from_person_name, 
                    (SELECT b.name FROM users b WHERE b.item_id='{$policyData->user_id}' LIMIT 1) AS client_name,
                    (SELECT b.name FROM users b WHERE b.item_id='{$params->assigned_to}' LIMIT 1) AS to_name", 
                    "users a", "a.item_id='{$policyData->assigned_to}'"
                );

                // alert the user whom this complaint has been assigned to
                if($params->assigned_to !== $params->userId) {

                    // form the notification parameters
                    $notice_param = (object) [
                        '_item_id' => random_string("alnum", 32),
                        'user_id' => $params->assigned_to,
                        'subject' => "Insurance Policy",
                        'username' => $params->userData->username,
                        'remote' => false, 
                        'message' => "<strong>{$params->userData->name}</strong> assigned an Insurance to you. <a title=\"Click to View\" href=\"{{APPURL}}policies-view/{$params->item_id}\">Click to view</a>.",
                        'notice_type' => 6,
                        'userId' => $params->userId,
                        'initiated_by' => 'system'
                    ];
                    
                    // add a new notification
                    $noticeClass->add($notice_param);
                }

                // if the complaint was initially assigned to someone
                if(isset($from->from_person_name)) {
                    $message = "Insurance Policy initially assigned to <strong>{$from->from_person_name}</strong> was reassigned to <strong>{$from->to_name}</strong>.";
                } elseif(isset($from->to_name)) {
                    $message = "Insurance Policy was assigned to <strong>{$from->to_name}</strong>.";
                } else {
                    $message = "Insurance Policy has been reassigned";
                }

                // log the assigned to change
                $this->userLogs("insurance_policy", $params->item_id, $policyData->assigned_to, $message, $params->userId, $notice);

                // trigger page reload
                $return_log["additional"]["reload"] = "{$this->baseUrl}policies-view/{$params->item_id}";
            }

            // log the user activity
            // $this->userLogs("insurance_policy", $params->item_id, $policyData, "<strong>{$params->userData->name}</strong> triggered an update request to this Policy.", $params->userId);

            // commit the transaction
            $this->db->commit();

            // return the success response
            return $return_log;

        } catch(PDOException $e) {
            $this->db->rollBack();
            return $e->getMessage();
        }

	}

    /**
     * Request for policy cancellation
     * 
     * @param \stdClass $params
     * @param String    $params->policy_id          The id of the policy to cancel
     * @param String    $params->reason             This is the reason for cancelling the policy
     * 
     * @return Array
     */
    public function cancel(stdClass $params) {

        /** Load the policy information */
        $para = (object) [
            "limit" => 1,
            "remote" => true,
            "userId" => $params->userId,
            "userData" => $params->userData,
            "the_policy_id" => $params->policy_id
        ];
        $policyCheck = $this->list($para);

        // if the policy check is not empty
        if(empty($policyCheck["data"])) {
            return ["data" => "Sorry! You are not permitted to perform this action."];
        }

        // check if a cancellation request has already been placed
        $cancelCheck = $this->pushQuery("id", "users_items_cancellation_request", "record_id='{$params->policy_id}' AND status = 'Pending' LIMIT 1");
        
        // if not empty then echo an error
        if(!empty($cancelCheck)) {
            return ["data" => "Sorry! There is a pending cancellation request on this policy."];
        }

        // policy data
        $policyData = $policyCheck["data"][0];
        $params->reason = nl2br($params->reason);

        $slug = random_string("alnum", 5)."_{$policyData->item_id}";

        // format the policy information
        $policy_details = [
            "policy_id" => $policyData->policy_id, "policy_name" => $policyData->policy_name,
            "policy_type" => $policyData->policy_type, "client_id" => $policyData->user_id,
            "company_id" => $policyData->company_id, "company_name" => $policyData->company_name,
            "client_name" => $policyData->client_name, "client_image" => $policyData->client_image,
            "policy_status" => $policyData->policy_status, "enrolled_date" => $policyData->policy_start_date
        ];

        // global variable
        global $noticeClass;

        try {

            $stmt = $this->db->prepare("
                INSERT INTO users_items_cancellation_request SET slug = ?, item_id = ?, record_id = ?, 
                    user_id = ?, company_id = ?,  created_by = ?, record_details = ?, reason = ?
            ");
            $stmt->execute([
                $slug, $policyData->item_id, $params->policy_id, $policyData->user_id, $policyData->company_id, 
                $params->userId, json_encode($policy_details), $params->reason
            ]);

            // form the notification parameters
            $notice_param = (object) [
                '_item_id' => random_string("alnum", 32),
                'user_id' => $policyData->user_id,
                'subject' => "Insurance Policy",
                'username' => $params->userData->username,
                'remote' => false, 
                'message' => "<strong>{$params->userData->name}</strong> has made a request to cancel your policy with ID: <strong>{$params->policy_id}</strong>. You will be notified once the request is approved.",
                'notice_type' => 6,
                'userId' => $params->userId,
                'initiated_by' => 'system'
            ];
            
            // if the cancellation request was done on behalf of the client
            if($policyData->user_id !== $params->userId) {                
                // add a new notification
                $noticeClass->add($notice_param);
                
                // set the user id
                $notice_param->user_id = $params->userId;
                $notice_param->message = "You made a request to cancel the policy with ID: <strong>{$params->policy_id}</strong>. You will be notified once the request is approved.";
                $noticeClass->add($notice_param);
            }
            
            // add a new notification
            $noticeClass->add($notice_param);

            // return the response
            return [
                "code" => 200,
                "data" => "Your request to cancel your policy has been placed.",
                "additional" => [
                    "clear" => true,
                    "reload" => "{$this->baseUrl}policies-view/{$policyData->item_id}"
                ]
            ];

        } catch(PDOException $e) {
            return $e->getMessage();
        }


    }

    /**
     * List the cancel policy requests list
     * 
     * @param \stdClass $params
     * @param String    $params->policy_id          The id of the policy to load the cancel requests list
     */
    public function cancel_list(stdClass $params) {
        // append the policy_id as record_id to the params object
        $params->record_id = $params->policy_id;

        // call the cancel record list method
        return $this->record_cancel_list($params, "user_policy");
    }

    /**
     * Save the user policy payment options
     * 
     * @param String $params->policy_id
     * @param String $params->payment_option
     * 
     * @return Array
     */
    public function save_payment_option(stdClass $params) {

        /** Save the user policy information */
        $policyCheck = $this->pushQuery(
            "a.id, a.payment_option, a.payment_interval, (SELECT b.name FROM policy_form b WHERE b.id='{$params->payment_option}' LIMIT 1) as payment_option_name", 
            "users_policy a", 
            "a.item_id='{$params->policy_id}' AND a.policy_status != 'Deleted' LIMIT 1"
        );

        /** if the policy check is not empty */
        if(empty($policyCheck)) {
            return ["code" => 203, "data" => "Sorry! The policy record was not found"];
        }

        /** Confirm that the payment option really exists */
        if(empty($policyCheck[0]->payment_option_name)) {
            return ["code" => 203, "data" => "Sorry! The payment option was not found"];
        }

        /** If the payment interval was parsed */
        if(isset($params->payment_interval)) {
            /** Save the user policy information */
            $intervalCheck = $this->pushQuery(
                "*", "policy_form", 
                "id='{$params->payment_interval}' LIMIT 1"
            );
        }

        /** Update the policy information */
        try {

            // prepare and execute the statement
            $stmt = $this->db->prepare("UPDATE users_policy SET payment_option = ?, payment_interval = ? WHERE item_id = ?");
            $stmt->execute([$params->payment_option, $params->payment_interval ?? null, $params->policy_id]);

            // save the payment option if changed
            if(($policyCheck[0]->payment_option !== $params->payment_option)) {
                // save the status change information
                $this->userLogs("insurance_policy", $params->policy_id, $policyCheck[0]->payment_option, "<strong>Payment Option</strong> was changed to <strong>{$policyCheck[0]->name}</strong>.", $params->userId);
            }

            // save the payment interval if changed
            if(isset($params->payment_interval) && ($policyCheck[0]->payment_interval !== $params->payment_interval)) {
                // save the status change information
                $this->userLogs("insurance_policy", $params->policy_id, $policyCheck[0]->payment_interval, "<strong>Payment Interval</strong> was changed to <strong>{$intervalCheck[0]->name}</strong>.", $params->userId);
            }

            return ["code" => 200,"data" => "Payment option successfully saved!"];

        } catch(PDOException $e) {}


    }

}

?>