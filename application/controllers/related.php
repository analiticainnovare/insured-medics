<?php
// ensure this file is being included by a parent file
if( !defined( 'BASEPATH' ) ) die( 'Restricted access' );

class Related extends Medics {
    
    public function __construct(){
        parent::__construct();
    }

    /**
     * List the items this is related to
     * 
     * @param \stdClass $params
     * 
     * @return Array
     */
    public function list(stdClass $params) {
        
        /** If related item was not parsed */
        if(!isset($params->related_item)) {
            return "Related variable must be parsed";
        }

        /** Global variables */
        global $accessObject;

        /** Assign some variables */
        $related = $params->related_item;
        $userData = $params->userData;
        $user_id = $userData->user_id;
        $user_type = $userData->user_type;
        $params->module = isset($params->module) ? $params->module : null;

        // if the module is client-policy and the client id was parsed in the related item section
        if($params->module == "client-policy" && !empty($params->related_item)) {
            /** An agent, broker or bancassurance is loading a user policy */
            $policyObj = load_class("user_policy", "controllers");
            
            /** Parameters */
            $param = (object) [
                "columns" => "a.item_id, a.policy_name, a.policy_id, a.policy_status, a.submit_status",
                "user_id" => $params->related_item,
                "remote" => true,
            ];
            /** Switch by users */
            if(in_array($user_type, ["broker"])) {
                // or clause
                $param->or_clause = " AND (a.broker_id = '{$user_id}' OR a.created_by='{$user_id}' OR a.assigned_to='{$user_id}')"; 
            } 
            // if bancassurance
            elseif(in_array($user_type, ["agent"])) {
                // or clause
                $param->or_clause = " AND ((a.company_id = '{$userData->company_id}' AND a.agent_id='{$user_id}') OR (a.company_id = '{$userData->company_id}' AND a.created_by='{$user_id}') OR (a.company_id = '{$userData->company_id}' AND a.assigned_to='{$user_id}'))"; 
            } 
            // if bancassurance
            elseif(in_array($user_type, ["bancassurance", "bank"])) {
                // or clause
                $param->or_clause = " AND ((a.company_id = '{$userData->company_id}' AND a.created_by='{$user_id}') OR (a.company_id = '{$userData->company_id}' AND a.assigned_to='{$user_id}'))"; 
            }
            /** Insurance company */
            elseif(in_array($user_type, ["insurance_company"])) {
                $param->or_clause = " AND (a.company_id = '{$userData->company_id}')"; 
            }

            // make request
            $data = [];
            $request = $policyObj->list($param);
            
            // loop through the results
            if(!empty($request["data"])) {
                foreach($request["data"] as $each) {
                    $data[] = [
                        "item_id" => $each->item_id,
                        "item_name" => "{$each->policy_name} ($each->policy_id): {$each->policy_status}"
                    ];
                }
            }
            // return the result
            return $data;

        }
        
        // insurance policy and the related item is parsed
        elseif($params->module == "company-insurance-policies" && !empty($params->related_item)) {
            
            /** If the related value is insurance-policy */
            $policyObj = load_class("company_policy", "controllers");
            /** Parameters */
            $param = (object) [
                "columns" => "a.item_id, a.name AS item_name, (SELECT name FROM companies WHERE companies.item_id = a.company_id LIMIT 1) AS company_name",
                "userData" => $userData,
                "limited_data" => true
            ];
            /** If the filter is parsed */
            if(isset($params->filter)) {
                // set the filter
                $filter = $params->filter;
                // if the related item is not null
                if($params->related_item !== "null") {
                    // append the query
                    $param->$filter = $params->related_item;
                }
            }
            // make request
            $data = [];
            $request = $policyObj->list($param);
            
            // loop through the results
            if(!empty($request["data"])) {
                foreach($request["data"] as $each) {
                    $data[] = [
                        "item_id" => $each->item_id,
                        "item_short_name" => $each->item_name,
                        "item_name" => "{$each->item_name} - by: {$each->company_name}"
                    ];
                }
            }
            // return the result
            return $data;
        }

        // if the module is claims
        elseif($params->module == "complaints" && $params->related_item == "claims") {
            /** load the users insurance policies */
            $claimsObj = load_class("claims", "controllers");

            /** Parameters */
            $param = (object) [
                "columns" => "a.item_id, a.policy_id, a.status",
                "userData" => $userData,
                "userId" => $user_id,
                "remote" => true,
                "submit_status" => "save",
                "or_clause" => " AND (a.user_id='{$user_id}')"
            ];

            // make request
            $data = [];
            $request = $claimsObj->list($param);

            // loop through the results
            if(!empty($request["data"])) {
                foreach($request["data"] as $each) {
                    $data["data"][] = (object) [
                        "item_id" => $each->item_id,
                        "item_name" => "{$each->policy_name} ($each->policy_id): {$each->status}"
                    ];
                }
            }
            // return the result
            return $data;
        } else {
            
            /** If the related value is client-policy */
            if($related == "client-policy") {
                
                /** load the users insurance policies */
                $policyObj = load_class("user_policy", "controllers");

                /** Parameters */
                $param = (object) [
                    "columns" => "a.item_id, a.policy_name, a.policy_id, a.policy_status, a.submit_status",
                    "userData" => $userData,
                    "userId" => $user_id,
                    "or_clause" => " AND (a.user_id='{$user_id}')"
                ];
                /** Switch by users */
                if(in_array($user_type, ["broker"])) {
                    // or clause
                    $param->userId = $userData->user_id;
                    $param->or_clause = " AND (a.broker_id = '{$user_id}' OR a.created_by='{$user_id}' OR a.assigned_to='{$user_id}')"; 
                } 
                // if bancassurance
                elseif(in_array($user_type, ["agent"])) {
                    // or clause
                    $param->userId = $userData->user_id;
                    $param->or_clause = " AND ((a.company_id = '{$userData->company_id}' AND a.agent_id='{$user_id}') OR (a.company_id = '{$userData->company_id}' AND a.created_by='{$user_id}') OR (a.company_id = '{$userData->company_id}' AND a.assigned_to='{$user_id}'))"; 
                }
                // if bancassurance
                elseif(in_array($user_type, ["bancassurance", "bank"])) {
                    // or clause
                    $param->userId = $userData->user_id;
                    $param->or_clause = " AND ((a.company_id = '{$userData->company_id}' AND a.created_by='{$user_id}') OR (a.company_id = '{$userData->company_id}' AND a.assigned_to='{$user_id}'))"; 
                }
                /** Insurance company */
                elseif(in_array($user_type, ["insurance_company"])) {
                    $param->or_clause = " AND (a.company_id = '{$userData->company_id}')"; 
                }

                // make request
                $data = [];
                $request = $policyObj->list($param);
                
                // loop through the results
                if(!empty($request["data"])) {
                    foreach($request["data"] as $each) {
                        $data[] = [
                            "item_id" => $each->item_id,
                            "item_name" => "{$each->policy_name} ($each->policy_id): {$each->policy_status}"
                        ];
                    }
                }
                // return the result
                return $data;

            }

            /** If the related value is user claims */
            if($related == "claims") {
                
                /** load the users insurance policies */
                $claimsObj = load_class("claims", "controllers");

                /** Parameters */
                $param = (object) [
                    "columns" => "a.item_id, a.policy_id, a.status",
                    "userData" => $userData,
                    "userId" => $user_id,
                    "submit_status" => "save",
                    "remote" => true,
                    "or_clause" => " AND (a.user_id='{$user_id}')"
                ];

                // make request
                $data = [];
                $request = $claimsObj->list($param);
                
                // loop through the results
                if(!empty($request["data"])) {
                    foreach($request["data"] as $each) {
                        $data[] = [
                            "item_id" => $each->item_id,
                            "item_name" => "{$each->policy_name} ($each->policy_id): {$each->status}"
                        ];
                    }
                }
                // return the result
                return $data;

            }

            /** If the related value is company-insurance-policies */
            elseif($related == "company-insurance-policies") {

                /** load the users insurance policies */
                $policyObj = load_class("company_policy", "controllers");

                /** if user type is insurance company then append the policy status */
                $status = ($params->module == "adverts")  ? "Enrolled" : "";

                /** Parameters */
                $param = (object) [
                    "columns" => "a.item_id, a.name AS item_name, a.name AS policy_name, a.company_id,
                        (SELECT name FROM companies WHERE companies.item_id = a.company_id LIMIT 1) AS company_name",
                    "userData" => $userData,
                    "status" => $status
                ];
                // return the result
                return $policyObj->list($param);
            }
            
            /** If the related value is insurance-company */
            elseif($related == "insurance-company") {
                /** load the users insurance policies */
                $companyObj = load_class("company", "controllers");
                /** Parameters */
                $param = (object) [
                    "columns" => "a.item_id, a.name AS item_name"
                ];
                /** Switch by users */
                if(in_array($user_type, ["agent"])) {
                    // or clause
                    $param->or_clause = " AND a.item_id = '{$userData->company_id}'"; 
                }
                // return the result
                return $companyObj->list($param);
            }

            /** If the related value is previous-complaint */
            elseif($related == "previous-complaint") {
                /** load the users insurance policies */
                $complaintObj = load_class("complaints", "controllers");
                /** Parameters */
                $param = (object) [
                    "columns" => "a.item_id, a.subject AS item_name, a.submit_status, a.user_id",
                    "userData" => $userData,
                    "userId" => $user_id,
                    "submit_status" => "save"
                ];

                // return the result
                return $complaintObj->list($param);
            }

            /** If the related value is previous-complaint */
            elseif($related == "system-bug") {
                /** load the users insurance policies */
                $complaintObj = load_class("complaints", "controllers");
                /** Parameters */
                $param = (object) [
                    "columns" => "a.item_id, a.subject AS item_name, a.submit_status, a.user_id",
                    "userId" => $user_id,
                    "userData" => $userData,
                    "related_to" => "system-bug",
                    "submit_status" => "save"
                ];
                // return the result
                return $complaintObj->list($param);
            }

            /** If the related value is license-related */
            elseif($related == "license-related") {
                // if the user has the requisite permissions
                if($accessObject->hasAccess("view", "licenses")) {
                    /** load the users insurance policies */
                    $companyObj = load_class("licenses", "controllers");
                    /** Parameters */
                    $param = (object) [
                        "columns" => "a.item_id, CONCAT(a.license_id, '-', a.status) AS item_name",
                        "company_id" => "{$userData->company_id}",
                        "submit_status" => "save"
                    ];
                    // return the result
                    return $companyObj->list($param);
                }
            }
        }

        return [];

    }

}