<?php 

class Payments extends Medics {

    private $record;
    private $teller_merchant_id = "TTM-00003086";

    public function __construct()
    {
        parent::__construct();

        $this->record = [
            "adverts" => [
                "table" => "adverts",
                "update" => "paid_status = 'Paid', payment_notice='1'",
                "payment" => "amount_payable",
                "page" => "adverts-view",
                "column" => "advert_id, advert_title, image, start_date, end_date, days, status, ad_objective, amount_spent, date_created"
            ],
            "licenses" => [
                "table" => "companies_licenses",
                "payment" => "amount_payable",
                "page" => "licenses-view",
                "update" => "payment_status = 'Paid'",
                "column" => "license_id, start_date, expiry_date, status, amount_payable, payment_status, date_created"
            ],
            "claims" => [
                "table" => "users_policy_claims",
                "payment" => "approved_amount",
                "page" => "claims-view",
                "update" => "paid_status = 'Paid'",
                "column" => "policy_id, user_id, policy_type, amount_claimed, approved_amount, status, date_submitted"
            ],
            "policy" => [
                "table" => "users_policy",
                "payment" => "premium",
                "page" => "policies-view",
                "update" => "last_premium_payment = now(), next_repayment_date = DATE_ADD(next_repayment_date, INTERVAL 1 month)",
                "column" => "policy_id, policy_name, policy_type_details, policy_status, policy_start_date, last_premium_payment, next_repayment_date"
            ]
        ];
    }

    /**
     * Load payment requests
     * 
     * @param StdClass $params
     * 
     * @return Array
     */
    public function list(stdClass $params) {

        $filter = "1";
        $filter .= isset($params->checkout_url) ? " AND a.checkout_url = '{$params->checkout_url}'" : null;
        $filter .= isset($params->initiated_by) ? " AND a.initiated_by = '{$params->initiated_by}'" : null;
        $filter .= isset($params->record_type) && !empty($params->record_type) ? " AND a.record_type = '{$params->record_type}'" : null;
        $filter .= isset($params->record_id) && !empty($params->record_id) ? " AND a.record_id = '{$params->record_id}'" : null;
        $filter .= isset($params->status) ? " AND a.payment_status = '{$params->status}'" : null;
        $filter .= isset($params->user_id) ? " AND a.user_id = '{$params->user_id}'" : null;

        try {
            
            $stmt = $this->db->prepare("
                SELECT a.*,
                    (SELECT CONCAT(name,'|',phone_number,'|',email,'|',image,'|',last_seen,'|',online,'|',user_type) FROM users WHERE users.item_id = a.user_id LIMIT 1) AS created_by_info,
                    (SELECT CONCAT(name,'|',phone_number,'|',email,'|',image,'|',last_seen,'|',online,'|',user_type) FROM users WHERE users.item_id = a.initiated_by LIMIT 1) AS initiated_by_info
                FROM users_payments a
                WHERE {$filter} ORDER BY a.id DESC
            ");
            $stmt->execute();

            $row = 0;
            $data = [];

            while($result = $stmt->fetch(PDO::FETCH_OBJ)) {

                $row++;
                $result->record_details = json_decode($result->record_details, true);
                $result->payment_info = json_decode($result->payment_info);
                $result->created_by_info = (object) $this->stringToArray($result->created_by_info, "|", ["name", "phone_number", "email", "image","last_seen","online","user_type"]);
                $result->initiated_by_info = (object) $this->stringToArray($result->initiated_by_info, "|", ["name", "phone_number", "email", "image","last_seen","online","user_type"]);

                $result->row_id = $row;
                $data[] = $result;
            }

            return [
                "code" => 200,
                "data" => $data
            ];
 
        } catch(PDOException $e) {
            return $this->unexpected_error;
        }

    }

    /**
     * Create a checkout URL
     * 
     * Using the record_type, verify if the record really exist and then get the amount to be paid
     * After generating the url, redirect the user to the payment portal to select medium of payment
     * 
     * @param stdClass $params
     * 
     * @return Array
     */
    public function create(stdClass $params) {

        /** Assign variables */
        $column = $this->record[$params->item]['column'] ?? null;
        $payment = $this->record[$params->item]['payment'] ?? null;
        $table = $this->record[$params->item]['table'] ?? null;

        /** If no table was found */
        if(empty($payment) || empty($table)) {
            return ["code" => 203, "data" => $this->permission_denied];
        }

        /** Existing checkout url */
        $existingCheck = $this->pushQuery("checkout_url", "users_payments", "record_type = '{$params->item}' AND record_id = '{$params->item_id}' AND payment_status = 'Pending'");
        $isFound = (bool) !empty($existingCheck);

        /** Run this section if no pending record is found */
        if(!$isFound) {
        
            /** Get the record */
            $recordCheck = $this->pushQuery("{$payment}, user_id", $table, "item_id='{$params->item_id}' LIMIT 1");
            
            /** End execution if no record was found */
            if(empty($recordCheck)) {
                return ["code" => 203, "data" => $this->permission_denied];
            }
            
            /** Reassign the record */
            $recordData = $recordCheck[0];
            $amount = $recordData->$payment;
            $checkout_url = random_string("alnum", 64);
            $record_details = $this->pushQuery("{$column}", $table, "item_id='{$params->item_id}' LIMIT 1")[0];
            $initiated = isset($params->initiated) ? $params->initiated : $recordData->user_id;
            $initiated_by = isset($params->initiated) ? "system" : "user";
            $transaction_id = "1".$this->append_zeros(($this->lastRowId("users_payments") + 1), 11);

        } else {
            $checkout_url = $existingCheck[0]->checkout_url;
        }

        /** insert the record */
        try {
            
            /** Run this section if no pending record is found */
            if(!$isFound) {

                /** Prepare and execute the statement */
                $stmt = $this->db->prepare("
                    INSERT INTO users_payments SET record_type = ?, record_id = ?, user_id = ?, transaction_id = ?,
                    checkout_url = ?, initiated_by = ?, initiated_medium = ?, amount = ?, record_details = ?
                    ".(isset($params->payment_status) ? ",payment_status='{$params->payment_status}'" : null)."
                    ".(isset($params->payment_status) && ($params->payment_status == "Paid") ? ",payment_option='system'" : null)."
                    ".(isset($params->payment_status) && ($params->payment_status == "Paid") ? ",payment_date=now()" : null)."
                ");
                $stmt->execute([
                    $params->item, $params->item_id, $params->userId, $transaction_id, $checkout_url, 
                    $initiated, $initiated_by, $amount, json_encode($record_details)
                ]);
            }

            return [
                "code" => 200,
                "data" => "Checkout URL successfully generated!",
                "additional" => [
                    "href" => "{$this->baseUrl}payment_checkout/{$checkout_url}"
                ]
            ];

        } catch(PDOException $e) {
            return $this->unexpected_error;
        }

    }

    /**
     * Process the payment request parsed by the user
     * 
     * Validate the information pushed in the payment_info parameter and create a checkout url from the
     * payment aggregator. Once that is done, return the url in order to popup a new window for making the actual payment
     * 
     * @param stdClass $params
     * 
     * @return Array
    */
    public function checkout(stdClass $params) {

        /** Confirm that the payment mediums are in the list */
        if(!in_array($params->payment_mode, ["payswitch", "slydepay", "expresspay"])) {
            return ["code" => 203, "data" => $this->permission_denied];
        }

        /** Convert the payment_info parameter into an object */
        $payment_info = json_decode($params->payment_info);

        /** Confirm that the value is not emty */
        if(empty($payment_info)) {
            return ["code" => 203, "data" => $this->permission_denied];
        }

        /** Confirm that the parameter checkout_url is in the payment_info parameter */
        if(!isset($payment_info->checkout_url) || !isset($payment_info->user_id) || !isset($payment_info->email_address) || !isset($payment_info->amount_payable)) {
            return ["code" => 203, "data" => "The checkout_url, user_id, amount_payable and email_address parameters are all required"];
        }

        /** Existing checkout url */
        $record_data = $this->pushQuery("checkout_url, amount, user_id, record_type, record_id, transaction_id", "users_payments", "checkout_url = '{$payment_info->checkout_url}' AND payment_status = 'Pending' AND user_id = '{$payment_info->user_id}' LIMIT 1");
        
        /** If no record was found then  */
        if(empty($record_data)) {
            return ["code" => 203, "data" => $this->permission_denied];
        }

        /** Payment info */
        $record_data = $record_data[0];
        $payment_info->amount_payable = $record_data->amount;
        $payment_info->record_type = $record_data->record_type;
        $payment_info->checkout_url = $record_data->checkout_url;
        $payment_info->transaction_id = $record_data->transaction_id;
        
        $make_payment = $this->{$params->payment_mode}($payment_info);

        /** Update the payment option selected */
        if(isset($make_payment["checkout_url"])) {
            $this->db->query("
                UPDATE users_payments SET payment_checkout_url = '{$make_payment["checkout_url"]}', 
                payment_option='{$params->payment_mode}' WHERE checkout_url = '{$payment_info->checkout_url}' LIMIT 1
            ");
        }

        /** Results */
        return [
            "code" => 200,
            "data" => $make_payment
        ];

    }

    /**
     * PaySwitch Payment Module
     * 
     * Create a new checkout url from payswitch and redirect to the user
     */
    public function payswitch($payment_info) {

        $teller_username = 'analitica5e8471d08d741';
        $teller_api_key = 'ZjkyYzllZGM4ZWJiMDUzNGNkNjM0YmY1YjQxMDRhY2Q=';
        $teller_endpoint = "https://test.theteller.net/checkout/initiate";

        // Prepare Authentication
        $basic_auth_key =  'Basic ' . base64_encode($teller_username . ':' . $teller_api_key);

        // Convert Amount
        $amount = $payment_info->amount_payable * 100;
        $amount = str_pad($amount, 12, '0', STR_PAD_LEFT);

        // Prepare Payload To Pass To Curl
        $payload = json_encode(
            [
                "amount"        => $amount,
                "merchant_id"   => $this->teller_merchant_id, 
                "desc"          => $payment_info->record_type,
                "transaction_id"=> $payment_info->transaction_id,
                "redirect_url"  => "{$this->baseUrl}payment_checkout/{$payment_info->checkout_url}/verify", 
                "email"         => (empty($payment_info->email_address)) ? "payments@insurehub365.com" : $payment_info->email_address
            ]
        );

        $curl = curl_init();

        curl_setopt_array(
            $curl, 
            array(
                CURLOPT_URL => $teller_endpoint,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_CAINFO => dirname(__FILE__)."\cacert.pem",
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $payload,
                CURLOPT_HTTPHEADER => array(
                    "Authorization: " . $basic_auth_key,
                    "Cache-Control: no-cache",
                    "Content-Type: application/json"
                ),
            )
        );

        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);

        if ($err) {
            $_message = "cURL Error #:" . $err;
        } else {
            $_message = json_decode($response, true);
        }

        return $_message;
        
    }

    /**
     * SlydePay Payment Module
     * 
     * Create a new checkout url from SlydePay and redirect to the user
     */
    public function slydepay($payment_info) {

    }

    /** 
     * Verify payment
     * 
     * @return Array
     */
    public function verify(stdClass $params) {

        /** Begin processing */
        $module = $params->payment_module;
        
        /** Ensure the correct module was used */
        if(!in_array($module, ["payswitch", "slydepay", "expresspay"])) {
            return ["code" => 203, "data" => $this->permission_denied];
        }

        /** Existing checkout url */
        $record_data = $this->pushQuery("checkout_url, payment_checkout_url, amount, user_id, record_type, record_id, transaction_id", "users_payments", "transaction_id = '{$params->transaction_id}' LIMIT 1");
        
        /** If no record was found then  */
        if(empty($record_data)) {
            return ["code" => 203, "data" => $this->permission_denied];
        }

        /** Init */
        $code = 203;
        $data = [
            "message" => "Sorry! Payment could not be verified.",
            "url" => $record_data[0]->payment_checkout_url
        ];

        /** Check which module was parsed */
        if($module == "payswitch") {

            /** make a request to verify payment */
            $request = $this->payswitch_verify($params->transaction_id);
            
            /** Update the dabase information */
            if(isset($request->status) && $request->status == "approved") {
                /** New variables */
                $code = 200;
                $data = [
                    "message" => $request->reason,
                    "url" => "{$this->baseUrl}{$this->record[$record_data[0]->record_type]["page"]}/{$record_data[0]->record_id}"
                ];

                /** Prepare and execute the request statement */
                $stmt = $this->db->prepare("
                    UPDATE users_payments 
                    SET payment_info = ?, momo_medium = ?, payment_status = ?, payment_date = now()
                    WHERE transaction_id = '{$params->transaction_id}' AND payment_status = ? LIMIT 1
                ");
                $stmt->execute([json_encode($request), $request->r_switch, "Paid", "Pending"]);
            }

            /** Update the table of the query made */
            $this->db->query("
                UPDATE {$this->record[$record_data[0]->record_type]["table"]}
                SET {$this->record[$record_data[0]->record_type]["update"]}
                WHERE item_id = '{$record_data[0]->record_id}' LIMIT 1
            ");
            
        }
        elseif($module == "slydepay") {
            /** make a request to verify payment */
            $request = $this->slydepay_verify($params->transaction_id);
        }
        elseif($module == "expresspay") {
            /** make a request to verify payment */
            $request = $this->expresspay_verify($params->transaction_id);
        }

        return [
            "code" => $code,
            "data" => $data
        ];


    }

    /**
     * Verify PaySwitch Transaction
     * 
     * Parse the Transaction ID and confirm the response from the Api Call
     */
    public function payswitch_verify($transactionId) {

        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://test.theteller.net/v1.1/users/transactions/".$transactionId."/status",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_CAINFO => dirname(__FILE__)."\cacert.pem",
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Merchant-Id: ".$this->teller_merchant_id
            )
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $_message = "cURL Error #:" . $err;
        } else {
            $_message = json_decode($response);
        }

        return $_message;

    }

    /**
     * Verify SlydePay Transaction
     * 
     * Parse the Transaction ID and confirm the response from the Api Call
     */
    public function slydepay_verify($transactionId) {

    }

    /**
     * Verify ExpressPay Transaction
     * 
     * Parse the Transaction ID and confirm the response from the Api Call
     */
    public function expresspay_verify($transactionId) {

    }

}