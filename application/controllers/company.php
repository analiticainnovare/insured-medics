<?php
// ensure this file is being included by a parent file
if( !defined( 'BASEPATH' ) ) die( 'Restricted access' );

class Company extends Medics {
	
	# start the construct
	public function __construct() {
		parent::__construct();
	}
	
	/**
	 * Global function to search for item based on the predefined columns and values parsed
	 * 
	 * @param \stdClass $params
	 * @param String $params->user_id
	 * @param String $params->claim_id  
	 * @param String $params->company_id
	 * @param String $params->date
	 * 
	 * @return Object
	 */
	public function list(stdClass $params = null) {

		$params->query = "1 ";

        // boolean value
        $params->remote = (bool) (isset($params->remote) && $params->remote);

		// if the field is null
		$params->query .= (isset($params->company_id)) ? (preg_match("/^[0-9]+$/", $params->company_id) ? " AND a.id='{$params->company_id}'" : " AND (a.item_id='{$params->company_id}' OR a.username='{$params->company_id}')") : null;
		$params->query .= (isset($params->status)) ? " AND a.activated='{$params->status}'" : " AND a.activated='1'";
        $params->query .= (isset($params->username)) ? " AND a.username='{$params->username}'" : null;
        $params->query .= (isset($params->type)) ? " AND a.company_type='{$params->type}'" : " AND a.company_type='insurance'";
		$params->query .= (isset($params->date)) ? " AND DATE(a.date_created) ='{$params->date_created}'" : null;
        $params->query .= (isset($params->date_range)) ? $this->dateRange($params->date_range, "a") : null;

		// the number of rows to limit the query
		$params->limit = isset($params->limit) ? $params->limit : $this->global_limit;

        try {

            // make the request for the record from the model
            $stmt = $this->db->prepare("
                SELECT ".(isset($params->columns) ? "{$params->columns}, a.name" : "a.*").",
                    (SELECT b.description FROM files_attachment b WHERE b.record_id = a.item_id ORDER BY b.id DESC LIMIT 1) AS attachment,
                    (SELECT COUNT(*) FROM policy_types b WHERE a.item_id = b.company_id AND b.policy_status='Enrolled' AND a.deleted='0') 
                    AS number_of_policies
                FROM companies a
                WHERE {$params->query} AND a.deleted = '0' LIMIT {$params->limit}
            ");
            $stmt->execute();

            $row = 0;
            $data = [];

            // if the user also requested for the company insurance policies
            $policy_param = (object) [];

            // loop through the results
            while($result = $stmt->fetch(PDO::FETCH_OBJ)) {
                // unset two parameters from the result set
                unset($result->id);

                // init the action button
                $result->action = "";

                // loop through this array list
				foreach(["preferences", "social_media", "awards", "managers"] as $eachItem) {
					// if isset the value
					if(isset($result->$eachItem)) {
						// convert the string info an array object
						$result->$eachItem = json_decode($result->$eachItem);
					}
				}

                // list insurance policies
                if(isset($params->insurance_policies)) {
                    
                    // load the insurance company policies
                    $result->insurance_policies = $this->pushQuery(
                        "a.item_id, a.category, a.policy_code, a.name, a.description, a.requirements, a.year_enrolled,
                            (SELECT b.description FROM files_attachment b WHERE b.record_id = a.item_id ORDER BY b.id DESC LIMIT 1) AS attachment
                        ", 
                        "policy_types a", 
                        "a.company_id = '{$result->item_id}' AND a.policy_status = 'Enrolled' AND a.deleted='0'"
                    );

                }
                
                // load summary company analytics
                if(isset($params->summary_analytics)) {
                    $params->summary_analytics = $this->summary_analytics($result->item_id);
                }

                // if not a remote request
                if(!$params->remote) {
                    $result->number_of_policies = "<a href='{$this->baseUrl}policy-list?company_id={$result->item_id}' title='View Policies'>{$result->number_of_policies} Policies</a>";
                    $result->the_company_name = "<span class=\"underline cursor\" onclick=\"return company_basic_information('{$result->item_id}')\">{$result->name}</span>";
                    $result->action = "<a class='btn btn-outline-success p-1 m-0 btn-sm' title='Click to view details' href=\"{$this->baseUrl}companies-view/{$result->item_id}\"><i class='fa fa-eye'></i></a>";
                }

                $row++;
                $result->row_id = $row;
                $data[] = $result;
            }

            // return the data
            return [
                "data" => $data,
                "code" => !empty($data) ? 200 : 201
            ];
        } catch(PDOException $e) {
            return [];
        }

	}

    /**
     * Generate summary analytics of the insurance company
     * 
     * @param String $company_id
     * 
     * @return Array 
     */
    public function summary_analytics($company_id) {

    }

    /**
     * List the company notes shared on the page
     * 
     * @param \stdClass $params
     * 
     * @return Array
     */
    public function list_notes($params) {

        // confirm if the user is an admin
        $isAdmin = (bool) (($params->userData->company_id == $params->company_id) && (in_array($params->userData->user_type, ["insurance_company", "admin"])));

        try {

            // load the information fromt the database
            $stmt = $this->db->prepare("SELECT a.item_id, a.title, a.content, a.image, a.date_created, 
                a.created_by, a.views_count, a.tags, a.company_id
            FROM companies_notes a WHERE a.company_id = ? AND a.status = ?");
            $stmt->execute([$params->company_id, 1]);

            $data = [];
            $results = $stmt->fetchAll(PDO::FETCH_OBJ);

            foreach($results as $key => $result) {
                
                // wipe the content
                $result->content = custom_clean(htmlspecialchars_decode($result->content));

                // confirm if the user has the needed permissions
                $result->is_editable = $isAdmin;

                // the link to download this note
                $result->download_link = base64_encode($result->item_id."{$this->underscores}".$result->company_id);

                $data[] = $result;
            }

            return $data;

        } catch(PDOException $e) {
            return $e->getMessage();
        }
    }

    /**
     * Add a new insurance company
     * 
     * @param \stdClass $params
     * 
     * @return Array
     */
    public function add(stdClass $params) {

        /** Validate the user variables parsed */
        $params->_item_id = random_string("alnum", 32);

        try {
            
        } catch(PDOException $e) {
            return $e->getMessage();
        }
    }

	/**
	 * Update a insurance company record
	 * 
	 * @param \stdClass $params
	 * 
	 * @return Array
	 */
	public function update(stdClass $params) {
		
        // load the information
        $companyData = $this->pushQuery(
            "email,name,website,contact,contact_2,license_id,username,address,establishment_date,registration_date,description,logo", 
            "companies", "item_id='{$params->company_id}'"
        );
        
        // if the information is empty
        if(empty($companyData)) {
            return $this->permission_denied;
        }

        // the description
        $companyData = $companyData[0];
        $params->description = isset($params->message) ? custom_clean(htmlspecialchars_decode($params->message)) : null;
        
        // return log
        $return_log = [
            "code" => 200,
            "data" => "Company information successfully updated"
        ];

        // set the upload directory
        $uploadDir = "assets/images/profiles/";

        // if image was parsed
        if(isset($params->logo)) {

            // profile picture 
            $baseName = basename($params->logo["name"]); 
            $targetFilePath = $uploadDir . preg_replace("/[\s]/", "_", $baseName); 
            $fileType = strtolower(pathinfo($targetFilePath, PATHINFO_EXTENSION));

            // Allow certain file formats 
            $allowTypes = ['jpg', 'png', 'jpeg', 'gif', 'webp', 'pjpeg']; 

            // check if its a valid image
            if(!empty($baseName) && in_array($fileType, $allowTypes)){
                
                // set a new filename
                $fileName = $uploadDir . random_string("alnum", 55).".{$fileType}";

                // Upload file to the server 
                if(move_uploaded_file($params->logo["tmp_name"], $fileName)){ 
                    // append the image to the manager information
                    $company_logo = $fileName;
                }

                // refresh when the id is parsed
                $return_log["additional"] = ["reload" => "{$this->baseUrl}companies-view/{$params->company_id}"];
            }

        }

        try {
            
            // prepare and execute the statement to update company information
            $stmt = $this->db->prepare("
                UPDATE companies SET date_updated = now()
                ".(isset($company_logo) ? ", logo='{$company_logo}'" : null)."
                ".(isset($params->name) ? ", name='{$params->name}'" : null)."
                ".(isset($params->email) ? ", email='{$params->email}'" : null)."
                ".(isset($params->website) ? ", website='{$params->website}'" : null)."
                ".(isset($params->address) ? ", address='{$params->address}'" : null)."
                ".(isset($params->license_id) ? ", license_id='{$params->license_id}'" : null)."
                ".(!empty($params->description) ? ", description='{$params->description}'" : null)."
                ".(isset($params->primary_contact) ? ", contact='{$params->primary_contact}'" : null)."
                ".(isset($params->secondary_contact) ? ", contact_2='{$params->secondary_contact}'" : null)."
                ".(isset($params->registration_date) ? ", registration_date='{$params->registration_date}'" : null)."
                ".(isset($params->establishment_date) ? ", establishment_date='{$params->establishment_date}'" : null)."
                WHERE item_id = ? LIMIT 1
            ");
            $stmt->execute([$params->company_id]);

            /** Log the user activity */
            $this->userLogs("company", $params->company_id, (object) $companyData, "<strong>{$params->userData->name}</strong> triggered an update to company information.", $params->userId);

            // save the log for change in registration date
            if(isset($params->registration_date) && ($companyData->registration_date !== $params->registration_date)) {
                // save the status change information
                $this->userLogs("company", $params->company_id, $companyData->registration_date, "<strong>Registration Date</strong> was changed from <strong>{$companyData->registration_date}</strong> to <strong>{$params->registration_date}</strong>.", $params->userId);
            }

            // save the log for change in establishment date
            if(isset($params->establishment_date) && ($companyData->establishment_date !== $params->establishment_date)) {
                // save the status change information
                $this->userLogs("company", $params->company_id, $companyData->establishment_date, "<strong>Date of Incorporation</strong> was changed from <strong>{$companyData->establishment_date}</strong> to <strong>{$params->establishment_date}</strong>.", $params->userId);
            }

            // save the log for change in website
            if(isset($params->website) && ($companyData->website !== $params->website)) {
                // save the status change information
                $this->userLogs("company", $params->company_id, $companyData->website, "<strong>Company Website</strong> was changed from <strong>{$companyData->website}</strong> to <strong>{$params->website}</strong>.", $params->userId);
            }

            // save the log for change in description
            if(isset($params->description) && ($companyData->description !== $params->description)) {
                // save the status change information
                $this->userLogs("company", $params->company_id, $companyData->description, "<strong>Company description</strong> was changed from <strong>{$companyData->description}</strong> to <strong>{$params->description}</strong>.", $params->userId);
                // trigger reload when primary contact is changed
                $return_log["additional"] = ["reload" => "{$this->baseUrl}companies-view/{$params->company_id}"];
            }

            // save the log for change in primary_contact
            if(isset($params->primary_contact) && ($companyData->contact !== $params->primary_contact)) {
                // save the status change information
                $this->userLogs("company", $params->company_id, $companyData->contact, "<strong>Company Primary Contact</strong> was changed from <strong>{$companyData->contact}</strong> to <strong>{$params->primary_contact}</strong>.", $params->userId);
                // trigger reload when primary contact is changed
                $return_log["additional"] = ["reload" => "{$this->baseUrl}companies-view/{$params->company_id}"];
            }

            // save the log for change in secondary contact
            if(isset($params->secondary_contact) && ($companyData->contact_2 !== $params->secondary_contact)) {
                // save the status change information
                $this->userLogs("company", $params->company_id, $companyData->contact_2, "<strong>Company Email</strong> was changed from <strong>{$companyData->contact_2}</strong> to <strong>{$params->secondary_contact}</strong>.", $params->userId);
            }

            // save the log for change in name
            if(isset($params->name) && ($companyData->name !== $params->name)) {
                // save the status change information
                $this->userLogs("company", $params->company_id, $companyData->name, "<strong>Company Name</strong> was changed from <strong>{$companyData->name}</strong> to <strong>{$params->name}</strong>.", $params->userId);
                // trigger reload when primary contact is changed
                $return_log["additional"] = ["reload" => "{$this->baseUrl}companies-view/{$params->company_id}"];
            }

            // save the log for change in email
            if(isset($params->email) && ($companyData->email !== $params->email)) {
                // save the status change information
                $this->userLogs("company", $params->company_id, $companyData->email, "<strong>Company Email</strong> was changed from <strong>{$companyData->email}</strong> to <strong>{$params->email}</strong>.", $params->userId);
            }

            // save the log for change in address
            if(isset($params->address) && ($companyData->address !== $params->address)) {
                // save the status change information
                $this->userLogs("company", $params->company_id, $companyData->address, "<strong>Company Address</strong> was changed from <strong>{$companyData->address}</strong> to <strong>{$params->address}</strong>.", $params->userId);
                // trigger reload when primary contact is changed
                $return_log["additional"] = ["reload" => "{$this->baseUrl}companies-view/{$params->company_id}"];
            }

            /** return the results */
            return $return_log;

        } catch(PDOException $e) {
            return $e->getMessage();
        }

	}

    /**
     * Add Company Manager information
     * 
     * @param \stdClass $params
     * 
     * @return Array
     */
    public function add_manager(stdClass $params) {
        
        // append the description to the array string
        $manager_info = (object) $params->manager_info;
        $manager_info->description = $params->message ?? null;

        // if the user does not have a company_id
        if(empty($params->userData->company_id)) {
            return $this->permission_denied;
        }

        // record id
        $record_id = random_string('alnum', 32);
        $manager_info->manager_id = "{$params->userData->company_id}_{$record_id}";

        // clean the user description
        $manager_info->description = isset($manager_info->description) ? custom_clean(htmlspecialchars_decode($manager_info->description)) : null;

        // set the upload directory
        $uploadDir = "assets/uploads/{$params->userData->company_id}/";

        // create the directory if the company id does not exist
        if(!is_dir("assets/uploads/{$params->userData->company_id}/")) {
            mkdir("assets/uploads/{$params->userData->company_id}/");
        }

        // if image was parsed
        if(isset($params->profile_picture)) {

            // profile picture 
            $baseName = basename($params->profile_picture["name"]); 
            $targetFilePath = $uploadDir . preg_replace("/[\s]/", "_", $baseName); 
            $fileType = strtolower(pathinfo($targetFilePath, PATHINFO_EXTENSION));

            // Allow certain file formats 
            $allowTypes = ['jpg', 'png', 'jpeg', 'gif', 'webp', 'pjpeg']; 

            // check if its a valid image
            if(!empty($baseName) && in_array($fileType, $allowTypes)){
                // set a new filename
                $fileName = $uploadDir . random_string("alnum", 55).".{$fileType}";

                // Upload file to the server 
                if(move_uploaded_file($params->profile_picture["tmp_name"], $fileName)){ 
                    // append the image to the manager information
                    $manager_info->picture = $fileName;
                }
            }

        }

        /** Confirm that the user_id is valid */
        $i_params = (object) ["limit" => 1, "company_id" => $params->userData->company_id, "columns" => "managers, item_id, name"];
        $the_user = $this->list($i_params)["data"];
        
        /** if a record was found */
        if(empty($the_user)) {
            return;
        }

        /** Get the manager information and convert to an array */
        $the_list = (array) $the_user[0]->managers;

        /** append to the list */
        if(empty($existing_info)) {
            $the_list[$record_id] = $manager_info;
        } else {
            array_push($the_list, $manager_info);
        }

        // convert the list into an object
        $managers_list = (object) $the_list;

        try {

            /** Prepare and execute the statement */
            $stmt = $this->db->prepare("UPDATE companies SET managers = ? WHERE item_id = ? LIMIT 1");
            $stmt->execute([json_encode($managers_list), $params->userData->company_id]);

            /** Log the user activity */
            $this->userLogs("company_manager", $manager_info->manager_id, (object) $the_user[0]->managers, "<strong>{$params->userData->name}</strong> added a new manager.", $params->userId);

            /** return the results */
            return [
                "code" => 200,
                "data" => "Manager information successfully added",
                "additional" => [
                    "clear" => true,
                    "reload" => "{$this->baseUrl}companies-view/{$params->userData->company_id}",
                ]
            ];

        } catch(PDOException $e) {
            return [];
        }
        
    }

    /**
     * Update Company Manager information
     * 
     * @param \stdClass $params
     * 
     * @return Array
     */
    public function update_manager(stdClass $params) {
        
        // append the description to the array string
        $manager_info = (object) $params->manager_info;
        $manager_info->description = $params->message ?? null;

        // if the user does not have a company_id
        if(empty($params->userData->company_id)) {
            return $this->permission_denied;
        }

        // if the manager id was not parsed then end the query
        if(!isset($manager_info->manager_id)) {
            return $this->permission_denied;
        }

        // get the company and record ids
        $explode = explode("_", $manager_info->manager_id);
        $company_id = $explode[0];

        // end query if the record id was not set
        if(!isset($explode[1])) {
            return "";
        }

        $record_id = $explode[1];

        // clean the user description
        $manager_info->description = isset($manager_info->description) ? custom_clean(htmlspecialchars_decode($manager_info->description)) : null;

        // set the upload directory
        $uploadDir = "assets/uploads/{$company_id}/";

        // create the directory if the company id does not exist
        if(!is_dir("assets/uploads/{$company_id}/")) {
            mkdir("assets/uploads/{$company_id}/");
        }

        // return log
        $return_log = [
            "code" => 200,
            "data" => "Manager information successfully updated"
        ];

        /** Confirm that the user_id is valid */
        $i_params = (object) ["limit" => 1, "company_id" => $company_id, "columns" => "managers, item_id, name"];
        $the_user = $this->list($i_params)["data"];
        
        /** if a record was found */
        if(empty($the_user)) {
            return;
        }

        /** Get the manager information and convert to an array */
        $the_list = (array) $the_user[0]->managers;

        /** Confirm that the user record was found */
        if(!isset($the_list[$record_id])) {
            return;
        }

        /** append to the list */
        foreach($the_list[$record_id] as $key => $value) {
            if(isset($manager_info->$key)) {
                $the_list[$record_id]->$key = $manager_info->$key;
            }
        }

        // if image was parsed
        if(isset($params->profile_picture)) {

            // profile picture 
            $baseName = basename($params->profile_picture["name"]); 
            $targetFilePath = $uploadDir . preg_replace("/[\s]/", "_", $baseName); 
            $fileType = strtolower(pathinfo($targetFilePath, PATHINFO_EXTENSION));

            // Allow certain file formats 
            $allowTypes = ['jpg', 'png', 'jpeg', 'gif', 'webp', 'pjpeg']; 

            // check if its a valid image
            if(!empty($baseName) && in_array($fileType, $allowTypes)){
                // set a new filename
                $fileName = $uploadDir . random_string("alnum", 55).".{$fileType}";

                // Upload file to the server 
                if(move_uploaded_file($params->profile_picture["tmp_name"], $fileName)){ 
                    // append the image to the manager information
                    $the_list[$record_id]->picture = $fileName;
                }

                // refresh when the id is parsed
                $return_log["additional"] = ["reload" => "{$this->baseUrl}companies-view/{$params->userData->company_id}"];
            }

        }

        // convert the list into an object
        $managers_list = (object) $the_list;

        try {

            /** Prepare and execute the statement */
            $stmt = $this->db->prepare("UPDATE companies SET managers = ? WHERE item_id = ? LIMIT 1");
            $stmt->execute([json_encode($managers_list), $params->userData->company_id]);

            /** Log the user activity */
            $this->userLogs("company_manager", $manager_info->manager_id, (object) $the_user[0]->managers, "<strong>{$params->userData->name}</strong> updated the manager information.", $params->userId);

            /** return the results */
            return $return_log;

        } catch(PDOException $e) {
            return [];
        }
        
    }

    /**
     * Add Company Award  information
     * 
     * @param \stdClass $params
     * 
     * @return Array
     */
    public function add_award(stdClass $params) {
        
        // append the description to the array string
        $award_info = (object) $params->award_info;
        $award_info->description = $params->message ?? null;

        // if the user does not have a company_id
        if(empty($params->userData->company_id)) {
            return $this->permission_denied;
        }

        // record id
        $record_id = random_string('alnum', 32);
        $award_info->award_id = "{$params->userData->company_id}_{$record_id}";

        // clean the user description
        $award_info->description = isset($award_info->description) ? custom_clean(htmlspecialchars_decode($award_info->description)) : null;

        // set the upload directory
        $uploadDir = "assets/uploads/{$params->userData->company_id}/";

        // create the directory if the company id does not exist
        if(!is_dir($uploadDir)) {
            mkdir($uploadDir);
        }

        // if image was parsed
        if(isset($params->award_logo)) {

            // profile picture 
            $baseName = basename($params->award_logo["name"]); 
            $targetFilePath = $uploadDir . preg_replace("/[\s]/", "_", $baseName); 
            $fileType = strtolower(pathinfo($targetFilePath, PATHINFO_EXTENSION));

            // Allow certain file formats 
            $allowTypes = ['jpg', 'png', 'jpeg', 'gif', 'webp', 'pjpeg']; 

            // check if its a valid image
            if(!empty($baseName) && in_array($fileType, $allowTypes)){
                // set a new filename
                $fileName = $uploadDir . random_string("alnum", 55).".{$fileType}";

                // Upload file to the server 
                if(move_uploaded_file($params->award_logo["tmp_name"], $fileName)){ 
                    // append the image to the manager information
                    $award_info->picture = $fileName;
                }
            }

        }

        /** Confirm that the user_id is valid */
        $i_params = (object) ["limit" => 1, "company_id" => $params->userData->company_id, "columns" => "awards, item_id, name"];
        $the_award = $this->list($i_params)["data"];
        
        /** if a record was found */
        if(empty($the_award)) {
            return;
        }

        /** Get the manager information and convert to an array */
        $the_list = (array) $the_award[0]->awards;

        /** append to the list */
        if(empty($existing_info)) {
            $the_list[$record_id] = $award_info;
        } else {
            array_push($the_list, $award_info);
        }

        // convert the list into an object
        $awards_list = (object) $the_list;

        try {

            /** Prepare and execute the statement */
            $stmt = $this->db->prepare("UPDATE companies SET awards = ? WHERE item_id = ? LIMIT 1");
            $stmt->execute([json_encode($awards_list), $params->userData->company_id]);

            /** Log the user activity */
            $this->userLogs("company_award", $award_info->award_id, (object) $the_award[0]->awards, "<strong>{$params->userData->name}</strong> added a new award.", $params->userId);

            /** return the results */
            return [
                "code" => 200,
                "data" => "Company Award information successfully added",
                "additional" => [
                    "clear" => true,
                    "reload" => "{$this->baseUrl}companies-view/{$params->userData->company_id}",
                ]
            ];

        } catch(PDOException $e) {
            return [];
        }
        
    }

    /**
     * Update Company Award information
     * 
     * @param \stdClass $params
     * 
     * @return Array
     */
    public function update_award(stdClass $params) {
        
        // append the description to the array string
        $award_info = (object) $params->award_info;
        $award_info->description = $params->message ?? null;

        // if the user does not have a company_id
        if(empty($params->userData->company_id)) {
            return $this->permission_denied;
        }

        // if the manager id was not parsed then end the query
        if(!isset($award_info->award_id)) {
            return $this->permission_denied;
        }

        // get the company and record ids
        $explode = explode("_", $award_info->award_id);
        $company_id = $explode[0];

        // end query if the record id was not set
        if(!isset($explode[1])) {
            return "";
        }

        $record_id = $explode[1];

        // clean the user description
        $award_info->description = isset($award_info->description) ? custom_clean(htmlspecialchars_decode($award_info->description)) : null;

        // set the upload directory
        $uploadDir = "assets/uploads/{$company_id}/";

        // create the directory if the company id does not exist
        if(!is_dir("assets/uploads/{$company_id}/")) {
            mkdir("assets/uploads/{$company_id}/");
        }

        // return log
        $return_log = [
            "code" => 200,
            "data" => "Award information successfully updated"
        ];

        /** Confirm that the user_id is valid */
        $i_params = (object) ["limit" => 1, "company_id" => $company_id, "columns" => "awards, item_id, name"];
        $the_award = $this->list($i_params)["data"];
        
        /** if a record was found */
        if(empty($the_award)) {
            return;
        }

        /** Get the Award information and convert to an array */
        $the_list = (array) $the_award[0]->awards;

        /** Confirm that the user record was found */
        if(!isset($the_list[$record_id])) {
            return;
        }

        /** append to the list */
        foreach($the_list[$record_id] as $key => $value) {
            if(isset($award_info->$key)) {
                $the_list[$record_id]->$key = $award_info->$key;
            }
        }

        // if image was parsed
        if(isset($params->award_logo)) {

            // profile picture 
            $baseName = basename($params->award_logo["name"]); 
            $targetFilePath = $uploadDir . preg_replace("/[\s]/", "_", $baseName); 
            $fileType = strtolower(pathinfo($targetFilePath, PATHINFO_EXTENSION));

            // Allow certain file formats 
            $allowTypes = ['jpg', 'png', 'jpeg', 'gif', 'webp', 'pjpeg']; 

            // check if its a valid image
            if(!empty($baseName) && in_array($fileType, $allowTypes)){
                // set a new filename
                $fileName = $uploadDir . random_string("alnum", 55).".{$fileType}";

                // Upload file to the server 
                if(move_uploaded_file($params->award_logo["tmp_name"], $fileName)){ 
                    // append the image to the Award information
                    $the_list[$record_id]->picture = $fileName;
                }

                // refresh when the id is parsed
                $return_log["additional"] = ["reload" => "{$this->baseUrl}companies-view/{$params->userData->company_id}"];
            }

        }

        // convert the list into an object
        $awards_list = (object) $the_list;

        try {

            /** Prepare and execute the statement */
            $stmt = $this->db->prepare("UPDATE companies SET awards = ? WHERE item_id = ? LIMIT 1");
            $stmt->execute([json_encode($awards_list), $params->userData->company_id]);

            /** Log the user activity */
            $this->userLogs("company_award", $award_info->award_id, (object) $the_award[0]->awards, "<strong>{$params->userData->name}</strong> updated the company award information.", $params->userId);

            /** return the results */
            return $return_log;

        } catch(PDOException $e) {
            return [];
        }
        
    }

	/**
	 * Save the company's cover picture
	 * 
	 * @param \stdClass $params
	 * 
	 * @return Array
	 */
	public function save_cover_image(stdClass $params) {

		/** Get the session value */
		if(empty($this->session->tempProfilePicture)) {
			return ["code" => 201, "response" => "Sorry! No picture have been uploaded yet."];
		}

        // load the information
        $companyData = $this->pushQuery("preferences", "companies", "item_id='{$params->company_id}' LIMIT 1");
        
        // if the information is empty
        if(empty($companyData)) {
            return $this->permission_denied;
        }

        // the description
        $companyData = json_decode($companyData[0]->preferences);

		/** Save the company information */
		try {
			
			// begin transaction
			$this->db->beginTransaction();
			$init_image = $this->session->tempProfilePicture;
			$exp = explode("/", $init_image);

			// set the user image
			$company_image = "assets/images/profiles/".preg_replace("/[\s]/", "_", $exp[count($exp)-1]);
			$thumbnail = "assets/images/profiles/thumbnail/".preg_replace("/[\s]/", "_", $exp[count($exp)-1]);

			// copy the file to the new destination
			copy($init_image, $company_image);
			create_thumbnail($company_image, $thumbnail);

			// unlink or delete the actual file in temp and the old user image
			unlink($init_image);

			// if previous is not the avatar
			if($companyData->cover_image !== "assets/images/profiles/avatar.jpg") {
				// remove the previous image
				// unlink($params->userData->image);
			}

            // set the new cover image 
            $companyData->cover_image = $company_image;

			// execute the statement
			$stmt = $this->db->prepare("UPDATE companies SET preferences = ? WHERE item_id = ? LIMIT 1");
			$stmt->execute([json_encode($companyData), $params->company_id]);

			// commit all opened transactions
			$this->db->commit();

			// remove the session
			$this->session->remove("tempProfilePicture");

			#record the password change request
            return ["code" => 200, "data" => $company_image ];

		} catch(PDOException $e) {
			$this->db->rollBack();
			return ["code" => 201, "response" => "Sorry! There was an error while processing the request."];
		}

	}

    /**
     * Save the company username
     * 
     * @param String $params->username      The username of the company to save
     * @param String $params->company_id    The unique id of the company to save the record
     * 
     * @return Array
     */
    public function save_username(stdClass $params) {

        // confirm that the user has the required permissions
        if(($params->userData->company_id !== $params->company_id) || !in_array($params->userData->user_type, ["insurance_company", "admin"])) {
            return ["code" => 203, "data" => $this->permission_denied];
        }

        // load the company information
        $companyData = $this->pushQuery("username", "companies", "item_id='{$params->company_id}' LIMIT 1");
        
        // if the information is empty
        if(empty($companyData)) {
            return ["code" => 203, "data" => "Sorry! No record was found"];
        }

        // compare the usernames and run a new search if it doesnt match
        if($companyData[0]->username !== $params->username) {
            // load the company information
            $companyData = $this->pushQuery("username", "companies", "username='{$params->username}' AND item_id != '{$params->company_id}' LIMIT 1");
            // if the information is not empty
            if(!empty($companyData)) {
                return ["code" => 203, "data" => "Sorry! The username already exists"];
            }
        }

        try {

            // save the username parsed
            $stmt = $this->db->prepare("UPDATE companies SET username = ? WHERE item_id = ? LIMIT 1");
            $stmt->execute([$params->username, $params->company_id]);

            // if the information is not empty
            return ["code" => 200, "data" => "Username successfully saved!"];

        } catch(PDOException $e) {}

    }

    /**
     * Manage Comissions
     * 
     * Manage the commission to be received by an agent/broker or to be applied to existing/new users
     * 
     * @param Int       $params->data[base_salary]
     * @param String    $params->data[company_id]
     * @param Int       $params->data[percentage]
     * @param Bool      $params->data[apply_to_all]
     * @param String    $params->data[user_id]
     * 
     * @return Array
     */
    public function manage_commission(stdClass $params) {

        /** Confirm array */
        if(!is_array($params->data)) {
            return ["code" => 203, "data" => "The data parameter must be an array value"];
        }

        $data = (object) $params->data;

        $applyToAll = (bool) isset($data->apply_to_all);
        $isIC = (bool) isset($data->company_id);
        $isUser = (bool) isset($data->user_id);

        if(!$isIC && !$isUser) {
            return ["code" => 203, "data" => "Sorry! You must either submit a User or Company ID"];
        }

        if(!isset($data->base_salary) || !isset($data->percentage)) {
            return ["code" => 203, "data" => "Sorry! The percentage && base_salary parameters are required"];
        }

        if(!preg_match("/^[0-9.]+$/", $data->base_salary) || !preg_match("/^[0-9.]+$/", $data->percentage)) {
            return ["code" => 203, "data" => "Sorry! The base_salary and percentages must be a valid float."];
        }

        if(($data->percentage < 1) || ($data->percentage > 100)) {
            return ["code" => 203, "data" => "Sorry! The percentage must be within the range of 1 to 100"];
        }

        if($isIC && !$isUser) {
            
            // decode the value parsed
            $data->company_id = base64_decode($data->company_id);

            $u_data = $this->pushQuery("preferences", "companies", "item_id='{$data->company_id}' AND activated='1' LIMIT 1");
            if(empty($u_data)) {
                return ["code" => 203, "data" => "Sorry! An invalid company id was parsed."];
            }
            $u_data = json_decode($u_data[0]->preferences);
            $pref = $u_data;

            // run query
            $run_query = false;
            $pref->commission = isset($pref->commission) ? $pref->commission : (object)[];

            $pref->commission->period = $data->period ?? null;

            // do not run the query if its the same as the previous
            if(!isset($pref->commission->percentage) || (isset($pref->commission->percentage) && $pref->commission->percentage !== $data->percentage)) {
                $run_query = true;
                $pref->commission->percentage = $data->percentage;
            }

            if(!isset($pref->commission->base_salary) || (isset($pref->commission->base_salary) && $pref->commission->base_salary !== $data->base_salary)) {
                $run_query = true;
                $pref->commission->base_salary = $data->base_salary;
            }
            $preference = json_encode($pref);

            // if the was an actual change
            if($run_query) {
            
                // run a double update
                $stmt = $this->db->prepare("UPDATE companies SET preferences = ? WHERE item_id = ? LIMIT 1");
                $stmt->execute([$preference, $data->company_id]);

                // if apply to all users
                if($applyToAll) {

                    $stmt = $this->db->prepare("UPDATE users SET base_salary = ?, commission_percentage = ?, commission_period = ? WHERE user_type IN ('broker','agent') AND company_id = ? AND user_status='Active' AND deleted='0'");
                    $stmt->execute([$data->base_salary, $data->percentage, $data->period ?? null, $data->company_id]);
                    
                    /** Log the user activity */
                    $this->userLogs("commission", $data->company_id, $u_data->commission->percentage ?? 1, "<strong>{$params->userData->name}</strong> updated the commission of all brokers and agents to {$data->percentage}.", $params->userId);
                    $this->userLogs("commission", $data->company_id, $u_data->commission->base_salary ?? 0, "<strong>{$params->userData->name}</strong> updated the base salary of all brokers and agents to  {$data->base_salary}.", $params->userId);
                }

                return ["code" => 200, "data" => "Commision data was successfully updated"];

            } else {
                return ["code" => 200, "data" => "The request was successful. However, no changes were effected."];
            }

        }

        elseif($isIC && $isUser) {

            // decode the value parsed
            $data->company_id = base64_decode($data->company_id);
            $data->user_id = base64_decode($data->user_id);

            // load the user information
            $u_data = $this->pushQuery("base_salary, commission_percentage, commission_period", "users", "item_id='{$data->user_id}' AND company_id='{$data->company_id}' AND user_status='Active' AND deleted='0' LIMIT 1");
            if(empty($u_data)) {
                return ["code" => 203, "data" => "Sorry! An invalid user id was parsed."];
            }
            $u_data = $u_data[0];

            // run query
            $run_query = false;

            // do not run the query if its the same as the previous
            if($u_data->commission_percentage !== $data->percentage) {
                $run_query = true;
            }

            if($u_data->base_salary !== $data->base_salary) {
                $run_query = true;
            }

            if($u_data->commission_period !== $data->period) {
                $run_query = true;
            }

            if($run_query) {
                // update user information
                $stmt = $this->db->prepare("UPDATE users SET base_salary = ?, commission_percentage = ?, commission_period = ? WHERE item_id = ? AND company_id = ? AND user_status='Active' AND deleted='0'");
                $stmt->execute([$data->base_salary, $data->percentage, $data->period, $data->user_id, $data->company_id]);

                // log the user activity
                $this->userLogs("commission", $data->user_id, $u_data->commission_percentage, "<strong>{$params->userData->name}</strong> updated the percentage of this user to {$data->percentage}.", $params->userId);
                $this->userLogs("commission", $data->user_id, $u_data->base_salary, "<strong>{$params->userData->name}</strong> updated the base salary of this user to {$data->base_salary}.", $params->userId);

                return ["code" => 200, "data" => "Commision data was successfully updated"];
            } else {
                return ["code" => 200, "data" => "The request was successful. However, no changes were effected."];
            }

        }

    }

}

?>