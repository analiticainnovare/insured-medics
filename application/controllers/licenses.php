<?php
// ensure this file is being included by a parent file
if( !defined( 'BASEPATH' ) ) die( 'Restricted access' );

class Licenses extends Medics {
	
	# start the construct
	public function __construct() {
		parent::__construct();
	}
	
	/**
	 * Global function to search for item based on the predefined columns and values parsed
	 * 
	 * @param \stdClass $params
	 * @param String $params->user_id
	 * @param String $params->claim_id  
	 * @param String $params->company_id
	 * @param String $params->date
	 * 
	 * @return Object
	 */
	public function list(stdClass $params = null) {

        // gloval variables
        global $accessObject;

		$params->query = "1 ";

        // boolean value
        $params->remote = (bool) isset($params->remote);

        // return if the user has no permission to view licenses
        if(!$accessObject->hasAccess("view", "licenses")) {
            return;
        }

        // if the user has permission to approve the license then the status must only be save
        if($accessObject->hasAccess("approve", "licenses")) {
            $params->company_id = null;
            $params->submit_status = "save";
        } else {
            // then the user must only see applications that relates to their company id
            $params->company_id = isset($params->company_id) ? $params->company_id : $params->userData->company_id;
        }

		// if the field is null
		$params->query .= (isset($params->application_id)) ? (preg_match("/^[0-9]+$/", $params->application_id) ? " AND a.id='{$params->application_id}'" : " AND a.item_id='{$params->application_id}'") : null;
		$params->query .= (isset($params->status)) ? " AND a.status='{$params->status}'" : null;
        $params->query .= !empty($params->company_id) ? " AND a.company_id='{$params->company_id}'" : null;
        $params->query .= (isset($params->submit_status) && !empty($params->submit_status)) ? " AND a.submit_status='{$params->submit_status}'" : null;
		$params->query .= (isset($params->date)) ? " AND DATE(a.date_created) ='{$params->date_created}'" : null;
        $params->query .= (isset($params->date_range)) ? $this->dateRange($params->date_range, "a") : null;

		// the number of rows to limit the query
		$params->limit = isset($params->limit) ? $params->limit : $this->global_limit;

        try {

            // make the request for the record from the model
            $stmt = $this->db->prepare("
                SELECT ".(isset($params->columns) ? $params->columns : "a.*, a.item_id AS application_id,
                    (SELECT name FROM companies WHERE companies.item_id = a.company_id LIMIT 1) AS company_name,
                    (SELECT name FROM users WHERE users.item_id = a.user_id LIMIT 1) AS applied_by,
                    (SELECT name FROM users WHERE users.item_id = a.seen_by LIMIT 1) AS seen_by_name,
                    (SELECT checkout_url FROM users_payments up WHERE up.record_id = a.item_id LIMIT 1) AS payment_checkout_url,
                    (SELECT name FROM users WHERE users.item_id = a.approved_by LIMIT 1) AS approved_by_name,
                    (SELECT b.description FROM files_attachment b WHERE b.record_id = a.item_id ORDER BY b.id DESC LIMIT 1) AS attachment
                ").", (SELECT CONCAT(name,'|',email,'|',contact,'|',address,'|',website,'|',logo) FROM companies WHERE companies.item_id = a.company_id LIMIT 1) AS company_info
                FROM companies_licenses a
                WHERE {$params->query} AND a.deleted = '0' ORDER BY a.id DESC LIMIT {$params->limit}
            ");
            $stmt->execute();

            // if the user has permission to approve license
            $approveLicense = $accessObject->hasAccess("approve", "licenses");
            $updateLicense = $accessObject->hasAccess("update", "licenses");

            $row = 0;
            $data = [];

            // loop through the request
            while($result = $stmt->fetch(PDO::FETCH_OBJ)) {
                // unset two parameters from the result set
                unset($result->id);

                // convert the description
                if(isset($result->description)) {
                    $result->description = htmlspecialchars_decode($result->description);
                }

                // attachment
                if(isset($result->attachment)) {
                    $result->attachment = json_decode($result->attachment);
                }

                // status lable
                $result->the_status_label = $this->the_status_label($result->status);
                $result->company_info = (object) $this->stringToArray($result->company_info, "|", ["name", "email", "contact", "address", "website", "logo"], true);

                $result->applied_by = "<span class='underline' onclick='return user_basic_information(\"{$result->user_id}\");'>{$result->applied_by}</span>";

                // action button
                $result->action = "<a href=\"{$this->baseUrl}licenses-view/{$result->item_id}\" class=\"btn btn-sm p-1 btn-outline-success\"><i class='fa fa-eye'></i></a>";

                // cancel request button
                if($updateLicense && $result->submit_status == "draft") {
                    $result->action .= " &nbsp; <a class='btn btn-outline-danger p-1 m-0 modify-record btn-sm' href='javascript:void(0)' data-item='licenses' title='Click to cancel this license application' data-item-id='{$result->item_id}'><i class='fa fa-trash-alt'></i></a>";
                }

                // set the license name
                $payment = (isset($result->payment_status) && $result->payment_status == "Paid") ? "<span class=\"badge badge-success\">{$result->payment_status}</span>" : "<span class=\"badge badge-danger\">{$result->payment_status}</span>";
                $result->license_label = $result->license_id."<br>{$payment}";

                // if the user is an admin
                if($approveLicense) {
                    $result->license_label = "
                        <span onclick='return company_basic_information(\"{$result->company_id}\")' class='underline'>{$result->company_name}</span>
                        <br><strong>{$result->license_id}</strong> <br> {$payment}";
                }

                $row++;
                $result->row_id = $row;
                $data[] = $result;
            }

            // return the data
            return [
                "data" => $data,
                "code" => !empty($data) ? 200 : 201
            ];

        } catch(PDOException $e) {
            return [];
        }

	}

    /**
     * Apply for license renewal
     * 
     * @param \stdClass $params
     * 
     * @return Array
     */
    public function apply(stdClass $params) {

         /** Global variables */
        global $accessObject;

        /** Permission check */
        $addLicense = (bool) $accessObject->hasAccess("apply", "licenses");

        /** End execution if the user does not have at least one of these permissions */
        if(!$addLicense) {
            return $this->permission_denied;
        }

        /** Validate the user variables parsed */
        $item_id = random_string("alnum", 32);

        try {

            // append the attachments
            $filesObj = load_class("files", "controllers");
            $attachments = $filesObj->prep_attachments("licenses", $params->userId, $item_id);

            // insert the record
            $stmt = $this->db->prepare("
                INSERT INTO companies_licenses SET date_created = now(), date_submitted = now()
                ".(isset($params->userId) ? ",user_id='{$params->userId}'" : null)."
                ".(isset($item_id) ? ",item_id='{$item_id}'" : null)."
                ".(isset($params->license_id) ? ",license_id='{$params->license_id}'" : null)."
                ".(isset($params->userData->company_id) ? ",company_id='{$params->userData->company_id}'" : null)."
                ".(isset($params->message) ? ",description='{$params->message}'" : null)."
            ");
            $stmt->execute();

            // log the user activity
            $this->userLogs("licenses", $item_id, null, "<strong>{$params->userData->name}</strong> applied for a License Renewal.", $params->userId);
            
            // insert attachment
            $files = $this->db->prepare("INSERT INTO files_attachment SET resource= ?, resource_id = ?, description = ?, record_id = ?, created_by = ?, attachment_size = ?");
            $files->execute(["licenses", $item_id, json_encode($attachments), "{$item_id}", $params->userId, $attachments["raw_size_mb"]]);

            // return the results
            return [
                "code" => 200,
                "data" => "License renewal application successfully placed.",
                "additional" => [
                    "clear" => true,
                    "reload" => "{$this->baseUrl}licenses-view/{$item_id}",
                ]
            ];

        } catch(PDOException $e) {
            return [];
        }
    }

	/**
	 * Update a license record
	 * 
	 * @param \stdClass $params
	 * 
	 * @return Array
	 */
	public function update(stdClass $params) {
        
        /** Global variables */
        global $accessObject, $noticeClass;

        /** Permission check */
        $updateLicense = (bool) $accessObject->hasAccess("update", "licenses");
        $approveLicense = (bool) $accessObject->hasAccess("approve", "licenses");

        /** End execution if the user does not have at least one of these permissions */
        if(!$updateLicense && !$approveLicense) {
            return $this->permission_denied;
        }

        /** Load the license information */
        $param = (object) [
            "application_id" => $params->application_id,
            "userData" => $params,
            "company_id" => $params->userData->company_id,
            "remote" => false,
            "limit" => 1
        ];

        // return the result
        $recordData = $this->list($param);

        // if the result is empty
        if(empty($recordData["data"])) {
            return $this->permission_denied;
        }
        
        // get the data
        $data = $recordData["data"][0];
        
        // append any existing attachments
        $initial_attachment = [];

        /** Confirm that there is an attached document */
        if(!empty($data->attachment)) {
            // decode the json string
            $db_attachments = $data->attachment;
            // get the files
            if(isset($db_attachments->files)) {
                $initial_attachment = $db_attachments->files;
            }
        }

        // clean the message parsed
        $params->message = isset($params->message) ? custom_clean(htmlspecialchars_decode($params->message)) : null;
        $params->policy_id = isset($params->policy_id) ? strtoupper($params->policy_id) : null;

        // append the attachments
        $filesObj = load_class("files", "controllers");
        $attachments = $filesObj->prep_attachments("licenses", $params->userData->user_id, $params->application_id, $initial_attachment);

        // approved claims
        $isApproved = (isset($params->status) && $params->status == "Approved") ? true : false;
        $isConfirmed = (isset($params->status) && $params->status == "Processing") ? true : false;
        $isRevoked = (isset($params->status) && $params->status == "Revoked") ? true : false;

        // convert all tags into special characters
        $params->message = isset($params->message) ? htmlspecialchars($params->message) : null;

        try {
            // begin transaction
            $this->db->beginTransaction();

            // modify the return log
            $return_log = [
                "code" => 200,
                "data" => "License renewal application has succesfully been updated.",
                "additional" => []
            ];

            // clean the content
            $submit_status = isset($params->the_button) ? strtolower($params->the_button) : "save";

            // update the license application details
            $stmt = $this->db->prepare("UPDATE companies_licenses SET date_updated = now()
                ".(!empty($params->policy_id) ? ",policy_id='{$params->policy_id}'" : null)."
                ".(isset($params->status) ? ",status='{$params->status}'" : null)."
                ".(isset($params->start_date) ? ",start_date='{$params->start_date}'" : null)."
                ".(isset($params->expiry_date) ? ",expiry_date='{$params->expiry_date}'" : null)."
                ".(isset($params->assigned_to) ? ",assigned_to='{$params->assigned_to}'" : null)."
                ".(isset($params->amount_payable) ? ",amount_payable='{$params->amount_payable}'" : null)."
                ".(!empty($params->message) ? ",description='{$params->message}'" : null)."
                ".($isApproved ? ",approved_by='{$params->userId}'" : null)."
                ".(!empty($submit_status) ? ",submit_status='{$submit_status}'" : null)."
                WHERE item_id = ? LIMIT 1
            ");
            $stmt->execute([$params->application_id]);

            // update attachment if already existing
            if(isset($db_attachments)) {
                $files = $this->db->prepare("UPDATE files_attachment SET description = ?, attachment_size = ? WHERE record_id = ? LIMIT 1");
                $files->execute([json_encode($attachments), $attachments["raw_size_mb"], $params->application_id]);
                // set the return log
                $return_log["additional"]["reload"] = "{$this->baseUrl}licenses-view/{$params->application_id}";
            } else {
                // only insert attachment record if there was an attachment to the comment
                if(!empty($attachments["files"])) {
                    // insert the record if not already existing
                    $files = $this->db->prepare("INSERT INTO files_attachment SET resource= ?, resource_id = ?, description = ?, record_id = ?, created_by = ?, attachment_size = ?");
                    $files->execute(["licenses", $params->application_id, json_encode($attachments), "{$params->application_id}", $params->userId, $attachments["raw_size_mb"]]);
                }
            }

            // if the previous is not the same as the current
            if(isset($params->start_date) && ($data->start_date !== $params->start_date)) {
                // save the form change information
                $this->userLogs("licenses", $params->application_id, $data->start_date, "The start date  was changed from <strong>{$data->start_date}</strong> to <strong>{$params->start_date}</strong>", $params->userId);
            }

            // if the previous is not the same as the current
            if(isset($params->expiry_date) && ($data->expiry_date !== $params->expiry_date)) {
                // save the form change information
                $this->userLogs("licenses", $params->application_id, $data->expiry_date, "The end date for the announcement was changed from <strong>{$data->expiry_date}</strong> to <strong>{$params->expiry_date}</strong>", $params->userId);
            }

            // if the previous is not the same as the current
            if(isset($params->amount_payable) && ($data->amount_payable !== $params->amount_payable)) {
                // save the form change information
                $this->userLogs("licenses", $params->application_id, $data->amount_payable, "The amount payable for this license renewal was set to <strong>{$params->amount_payable}</strong>", $params->userId);
            }

            // save the log for the status change
            if(isset($params->status) && ($data->status !== $params->status)) {
                // set the return log
                $return_log["additional"]["reload"] = "{$this->baseUrl}licenses-view/{$params->application_id}";
                
                // save the status change information
                $this->userLogs("licenses", $params->application_id, $data->status, "<strong>The License Renewal Application</strong> was changed from <strong>{$data->status}</strong> to <strong>{$params->status}</strong>.", $params->userId);
                
                // notify the user of the new status of the claim
                $from = $this->columnValue("a.name AS client_name", "users a", "a.item_id='{$data->user_id}'");

                // form the notification parameters
                $notice_param = (object) [
                    '_item_id' => random_string("alnum", 32),
                    'user_id' => $data->user_id,
                    'subject' => "License Renewal Application",
                    'username' => $params->userData->username,
                    'remote' => false, 
                    'message' => "<strong>Hello,</strong> your license renewal application is {$params->status}.<a title=\"Click to View\" href=\"{{APPURL}}licenses-view/{$params->application_id}\">View</a>",
                    'notice_type' => 7,
                    'userId' => $params->userId,
                    'initiated_by' => 'system'
                ];
                
                // add a new notification
                $noticeClass->add($notice_param);
            }

            // commit the transaction
            $this->db->commit();

            // return success
            return $return_log;

        } catch(PDOException $e) {
            $this->db->rollBack();
            return $e->getMessage();
        }

	}

}

?>