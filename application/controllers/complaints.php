<?php
// ensure this file is being included by a parent file
if( !defined( 'BASEPATH' ) ) die( 'Restricted access' );

class Complaints extends Medics {
	
	# start the construct
	public function __construct() {
		parent::__construct();
	}
	
	/**
	 * Global function to search for item based on the predefined columns and values parsed
	 * 
	 * @param \stdClass $params
	 * @param String $params->user_id
	 * @param String $params->complaint_id  
	 * @param String $params->company_id
	 * @param String $params->date
	 * 
	 * @return Object
	 */
	public function list(stdClass $params = null) {

		$params->query = "1 ";
        $params->internal_loop = isset($params->internal_loop) ? $params->internal_loop : false;
        
		// if the user id parameter was not parsed
		if(!isset($params->user_id) && !isset($params->internal_loop) || (isset($params->internal_loop) && !$params->internal_loop) && !isset($params->complaint_id)) {
			// perform some checks
			$the_user_type = $params->userData->user_type;
			// go ahead
			if(!in_array($the_user_type, ["insurance_company", "admin", "nic", "bank"])) {
				$params->or_clause = " AND ( (a.user_id = '{$params->userId}' OR a.assigned_to='{$params->userId}' OR a.updated_by = '{$params->userId}') AND (a.submit_status='save') )";
			}
            // if insurance company user
            elseif(in_array($the_user_type, ["insurance_company"])) {
                $params->or_clause = " AND ( (a.user_id = '{$params->userId}' OR a.assigned_to='{$params->userId}' OR a.updated_by = '{$params->userId}') OR (a.company_id='{$params->userData->company_id}' AND recipient='insurance_company' AND a.submit_status='save') )";
            }
            // if insurance company user
            elseif(in_array($the_user_type, ["bank", "bancassurance"])) {
                $params->or_clause = " AND ( (a.user_id = '{$params->userId}' OR a.assigned_to='{$params->userId}' OR a.updated_by = '{$params->userId}' OR a.recipient='bank') AND (a.submit_status='save') )";
            }
            // if nic or company then it should be related to them
            elseif(in_array($the_user_type, ["nic", "admin"])) {
                $params->or_clause = " AND ( (a.user_id = '{$params->userId}' OR a.assigned_to='{$params->userId}' OR a.updated_by = '{$params->userId}') OR (recipient='{$the_user_type}' AND a.submit_status='save') )";
            }
            // user and business
            elseif(in_array($the_user_type, ["user", "business"])) {
                $params->or_clause = " AND (a.user_id = '{$params->userId}')";
            }
		}

		// if the field is null
		$params->query .= (isset($params->complaint_id)) ? (preg_match("/^[0-9]+$/", $params->complaint_id) ? " AND a.id='{$params->complaint_id}'" : " AND a.item_id='{$params->complaint_id}'") : null;
		$params->query .= (isset($params->user_type) && !empty($params->user_type)) ? " AND a.user_type='{$params->user_type}'" : null;
        $params->query .= (isset($params->user_id) && !empty($params->user_id)) ? " AND a.user_id='{$params->user_id}'" : null;
        $params->query .= (isset($params->or_clause)) ? $params->or_clause : null;
        $params->query .= (isset($params->status) && !empty($params->status)) ? " AND a.status='{$params->status}'" : null;
        $params->query .= (isset($params->recipient) && !empty($params->recipient)) ? " AND a.recipient='{$params->recipient}'" : null;
        $params->query .= (isset($params->parent_id) && !empty($params->parent_id)) ? " AND a.complaint_id='{$params->parent_id}'" : null;
        $params->query .= (isset($params->related_to) && !empty($params->related_to)) ? " AND a.related_to='{$params->related_to}'" : null;
        $params->query .= (isset($params->submit_status) && !empty($params->submit_status)) ? " AND a.submit_status='{$params->submit_status}'" : null;
        $params->query .= (isset($params->complaint_type)) ? " AND a.complaint_type='{$params->complaint_type}'" : " AND a.complaint_type='complaint'";        
		$params->query .= (isset($params->date)) ? " AND DATE(a.date_created) ='{$params->date_created}'" : null;
        $params->query .= (isset($params->date_range) && !empty($params->date_range)) ? $this->dateRange($params->date_range, "c", "date_submitted") : null;
    
		// the number of rows to limit the query
		$params->limit = isset($params->limit) ? $params->limit : $this->global_limit;

        try {
            // make the request for the record from the model
            $stmt = $this->db->prepare("
                SELECT 
                    ".(isset($params->columns) ? $params->columns : "
                        a.*, a.item_id AS complaint_id, a.user_id, 
                        u.name AS complained_by, u.image AS complainant_image, u.email, u.phone_number,
                        a.seen, a.related_to, a.related_to_id, 
                        a.subject, a.status, a.message, 
                        (SELECT b.description FROM files_attachment b WHERE b.record_id = a.item_id ORDER BY b.id DESC LIMIT 1) AS attachment,
                        a.complaint_type, a.user_type, a.date_created, 
                        a.submit_status, a.last_updated, (SELECT name FROM users WHERE users.item_id = a.assigned_to LIMIT 1) AS assigned_to_name,
                        (SELECT name FROM users WHERE users.item_id = a.updated_by LIMIT 1) AS handled_by,
                        (SELECT name FROM users WHERE users.item_id = a.seen_by LIMIT 1) AS seen_by_name
                    ").", a.status
                FROM users_complaints a
                LEFT JOIN users u ON u.item_id = a.user_id
                WHERE {$params->query} ORDER BY a.id DESC LIMIT {$params->limit}
            ");
            $stmt->execute();

            $row = 0;
            $data = [];

            // loop through the results
            while($result = $stmt->fetch(PDO::FETCH_OBJ)) {

                // unset the id
                unset($result->id);

                // if the submit status is not save then no one except the own it must see it
                if(($result->submit_status == "save" && $result->user_id !== $params->userId) || ($result->user_id == $params->userId)) {

                    // if not a minimal load
                    if(!isset($params->minimal_load)) {

                        // related to name
                        $result->related_to_name = isset($result->related_to_name) ? $result->related_to_name : null;
                        
                        // if not a remote request
                        if(!isset($params->remote) || (isset($params->remote) && !$params->remote)) {

                            $result->action = "";
                            
                            // label
                            $result->status_label = $this->the_status_label($result->status);

                            // if complaint id is parsed
                            if(isset($result->complaint_id) && !$params->internal_loop) {
                                // parse the action
                                $result->action = "<a class='btn btn-outline-success p-1 btn-sm' title='Click to view details' href='{$this->baseUrl}complaints-view/{$result->complaint_id}'><i class='fa fa-eye'></i></a>";
                                
                                // if this user created the complaint and its still in a draft stage
                                if(($result->user_id == $params->userData->user_id) && in_array($result->submit_status, ["draft"])) {
                                    $result->action .= " &nbsp; <a class='btn btn-outline-danger p-1 modify-record btn-sm' href='javascript:void(0)' onclick='return delete_record(\"complaints\",\"{$result->complaint_id}\");' title='Click to cancel complaint'><i class='fa fa-trash-alt'></i></a>";
                                }
                            }
                        }

                        // if the attachment object was parsed
                        if(isset($result->last_updated) && isset($result->message)) {

                            // related to details if parsed
                            $result->attachment = isset($result->attachment) ? json_decode($result->attachment) : [];
                            $result->related_to_details = json_decode($result->related_to_details);
                            $result->related_to_name = ucwords(str_replace("-", " ", $result->related_to));
                            
                            // if not an internal loop
                            if(!isset($params->internal_loop) || isset($params->internal_loop) && !$params->internal_loop) {
                                // decode the message
                                $result->message = htmlspecialchars_decode($result->message);
                            }

                            // if not a remote request
                            if(!isset($params->remote) || (isset($params->remote) && !$params->remote)) {
                                // prepare some links
                                $complained_by = "<span class='user-profile-link' onclick=\"return user_basic_information('{$result->user_id}')\"><i style=\"font-size:10px\" class=\"fa fa-user\"></i> {$result->complained_by}</span>";
                                // complaint subject
                                $result->complaint_subject = ($result->submit_status == "draft") ? "{$result->subject} <span class='badge badge-primary pt-1 mt-1'><font>Draft</font></span><br>{$complained_by}" : "{$result->subject} <span class='badge badge-success pt-1 mt-1'><font>Sent</font></span><br>{$complained_by}";
                                $result->complaint_subject_none = ($result->submit_status == "draft") ? "{$result->subject} <span class='badge badge-primary pt-1 mt-1'><font>Draft</font></span><br>" : "{$result->subject} <span class='badge badge-success pt-1 mt-1'><font>Sent</font></span><br>";
                            }

                            $row++;
                            $result->row_id = $row;
                        }

                        // load replies was parsed
                        if(isset($params->load_replies)) {
                            $r_param = (object)[
                                "remote" => true,
                                "internal_loop" => true,
                                "complaint_type" => "reply",
                                "parent_id" => $result->complaint_id,
                            ];
                            $result->replies_list = $this->list($r_param)["data"];
                        }
                    } else {
                        $result->message = limit_words(htmlspecialchars_decode($result->message), 12);
                        $result->date_created = time_diff($result->date_created);
                        $result->status_label = $this->the_status_label($result->status);
                    }

                    $data[] = $result;
                }
            }

            // return the data
            return [
                "data" => $data,
                "code" => !empty($data) ? 200 : 201
            ];
        } catch(PDOException $e) {
            return $e->getMessage();
        }

	}

    /**
	 * This loads the users complaints
	 * 
	 * @param \stdClass $params
	 * @param String $params->user_id
	 * @param String $params->complaint_id  
	 * @param String $params->company_id
	 * @param String $params->date
	 * 
	 * @return Object
	 */
	public function quick_list(stdClass $params = null) {

		$params->query = "1 ";
        $params->internal_loop = isset($params->internal_loop) ? $params->internal_loop : false;

		// if the field is null
		$params->query .= (isset($params->complaint_id)) ? (preg_match("/^[0-9]+$/", $params->complaint_id) ? " AND a.id='{$params->complaint_id}'" : " AND a.item_id='{$params->complaint_id}'") : null;
		$params->query .= (isset($params->user_type) && !empty($params->user_type)) ? " AND a.user_type='{$params->user_type}'" : null;
        $params->query .= (isset($params->user_id) && !empty($params->user_id)) ? " AND a.user_id='{$params->user_id}'" : null;
        $params->query .= (isset($params->or_clause)) ? $params->or_clause : null;
        $params->query .= (isset($params->recipient) && !empty($params->recipient)) ? " AND a.recipient='{$params->recipient}'" : null;
        $params->query .= (isset($params->parent_id) && !empty($params->parent_id)) ? " AND a.complaint_id='{$params->parent_id}'" : null;
        $params->query .= (isset($params->related_to) && !empty($params->related_to)) ? " AND a.related_to='{$params->related_to}'" : null;
        $params->query .= (isset($params->submit_status) && !empty($params->submit_status)) ? " AND a.submit_status='{$params->submit_status}'" : null;
        $params->query .= (isset($params->complaint_type)) ? " AND a.complaint_type='{$params->complaint_type}'" : " AND a.complaint_type='complaint'";        
		$params->query .= (isset($params->date)) ? " AND DATE(a.date_created) ='{$params->date_created}'" : null;
        $params->query .= (isset($params->date_range)) ? $this->dateRange($params->date_range, "c") : null;

		// the number of rows to limit the query
		$params->limit = isset($params->limit) ? $params->limit : $this->global_limit;

        try {
            // make the request for the record from the model
            $stmt = $this->db->prepare("
                SELECT 
                    ".(isset($params->columns) ? $params->columns : "
                        a.*, a.item_id AS complaint_id, a.user_id, 
                        u.name AS complained_by, u.image AS complainant_image, u.email, u.phone_number,
                        a.seen, a.related_to, a.related_to_id, a.subject, a.status, a.message, 
                        (SELECT b.description FROM files_attachment b WHERE b.record_id = a.item_id ORDER BY b.id DESC LIMIT 1) AS attachment,
                        a.complaint_type, a.user_type, a.date_created, 
                        a.submit_status, a.last_updated, (SELECT name FROM users WHERE users.item_id = a.assigned_to LIMIT 1) AS assigned_to_name,
                        (SELECT name FROM users WHERE users.item_id = a.updated_by LIMIT 1) AS handled_by,
                        (SELECT name FROM users WHERE users.item_id = a.seen_by LIMIT 1) AS seen_by_name
                    ").", a.status
                FROM users_complaints a
                LEFT JOIN users u ON u.item_id = a.user_id
                WHERE {$params->query} ORDER BY a.id DESC LIMIT {$params->limit}
            ");
            $stmt->execute();

            $row = 0;
            $data = [];

            // loop through the results
            while($result = $stmt->fetch(PDO::FETCH_OBJ)) {

                // unset the id
                unset($result->id);

                // if the submit status is not save then no one except the own it must see it
                if(($result->submit_status == "save" && $result->user_id !== $params->userId) || ($result->user_id == $params->userId)) {

                    // if not a minimal load
                    if(!isset($params->minimal_load)) {

                        // related to name
                        $result->related_to_name = isset($result->related_to_name) ? $result->related_to_name : null;
                        
                        // if not a remote request
                        if(!isset($params->remote) || (isset($params->remote) && !$params->remote)) {

                            $result->action = "";
                            
                            // label
                            $result->status_label = $this->the_status_label($result->status);

                            // if complaint id is parsed
                            if(isset($result->complaint_id) && !$params->internal_loop) {
                                // parse the action
                                $result->action = "<a class='btn btn-outline-success p-1 btn-sm' title='Click to view details' href='{$this->baseUrl}complaints-view/{$result->complaint_id}'><i class='fa fa-eye'></i></a>";
                                
                                // if this user created the complaint and its still in a draft stage
                                if(($result->user_id == $params->userData->user_id) && in_array($result->submit_status, ["draft"])) {
                                    $result->action .= " &nbsp; <a class='btn btn-outline-danger p-1 modify-record btn-sm' href='javascript:void(0)' title='Click to cancel complaint' onclick='return delete_record(\"complaints\",\"{$result->complaint_id}\");'><i class='fa fa-trash-alt'></i></a>";
                                }
                            }
                        }

                        // if the attachment object was parsed
                        if(isset($result->last_updated) && isset($result->message)) {

                            // related to details if parsed
                            $result->attachment = isset($result->attachment) ? json_decode($result->attachment) : [];
                            $result->related_to_details = json_decode($result->related_to_details);
                            $result->related_to_name = ucwords(str_replace("-", " ", $result->related_to));
                            
                            // if not an internal loop
                            if(!isset($params->internal_loop) || isset($params->internal_loop) && !$params->internal_loop) {
                                // decode the message
                                $result->message = htmlspecialchars_decode($result->message);
                            }

                            // if not a remote request
                            if(!isset($params->remote) || (isset($params->remote) && !$params->remote)) {
                                // prepare some links
                                $complained_by = "<span class='user-profile-link' onclick=\"return user_basic_information('{$result->user_id}')\"><i style=\"font-size:10px\" class=\"fa fa-user\"></i> {$result->complained_by}</span>";
                                // complaint subject
                                $result->complaint_subject = ($result->submit_status == "draft") ? "{$result->subject} <span class='badge badge-primary pt-1 mt-1'><font>Draft</font></span><br>{$complained_by}" : "{$result->subject} <span class='badge badge-success pt-1 mt-1'><font>Sent</font></span><br>{$complained_by}";
                                $result->complaint_subject_none = ($result->submit_status == "draft") ? "{$result->subject} <span class='badge badge-primary pt-1 mt-1'><font>Draft</font></span><br>" : "{$result->subject} <span class='badge badge-success pt-1 mt-1'><font>Sent</font></span><br>";
                            }

                            $row++;
                            $result->row_id = $row;
                        }

                        // load replies was parsed
                        if(isset($params->load_replies)) {
                            $r_param = (object)[
                                "remote" => true,
                                "internal_loop" => true,
                                "complaint_type" => "reply",
                                "parent_id" => $result->complaint_id,
                            ];
                            $result->replies_list = $this->list($r_param)["data"];
                        }
                    } else {
                        $result->message = limit_words(htmlspecialchars_decode($result->message), 12);
                        $result->date_created = time_diff($result->date_created);
                        $result->status_label = $this->the_status_label($result->status);
                    }

                    $data[] = $result;
                }
            }

            // return the data
            return [
                "data" => $data,
                "code" => !empty($data) ? 200 : 201
            ];
        } catch(PDOException $e) {
            return $e->getMessage();
        }

	}

    /**
     * Format the related information and send back
     * 
     * @return String
     */
    public function related_to_data($related, $data) {

        $info = "";
        if($related == "license-related") {
            $info .= "<div class=\"related-label\">License ID: <a class=\"related-value\" data-function=\"quick-view\" title='Quick details view' data-id=\"{$data->license_id}\" href=\"javascript:void(0)\" data-module=\"license\">{$data->license_id}</a></div>";
            $info .= "<p><strong>Status:</strong> {$data->status}</p>";
            $info .= "<p><strong>Expiry Date:</strong> {$data->expiry_date}</p>";
        }
        // if client policy
        elseif($related == "client-policy") {
            $info .= "<div class=\"related-label\">Policy Name: <span class=\"related-value\"><span class=\"user-name-link\" onclick=\"return policy_basic_information('{$data->policy_type}')\">{$data->policy_name}</span></span></div>";
            $info .= "<div class=\"related-label\">Policy ID: <span class=\"related-value\">{$data->policy_id}</span></div>";
            $info .= "<div class=\"related-label\">Policy Holder: <span class=\"related-value\"><span class=\"user-name-link\" onclick=\"return user_basic_information('{$data->policy_holder_id}')\">{$data->policy_holder}</span></span></div>";
            $info .= "<div class=\"related-label\">Start Date: <span class=\"related-value\">{$data->policy_start_date}</span></div>"; 
            $info .= "<div class=\"related-label\">Date Created: <span class=\"related-value\">{$data->date_created}</span></div>";
        } elseif($related == "claims") {
            $info .= "<div class=\"related-label\">Policy Name: <span class=\"related-value\"><span class=\"user-name-link\" onclick=\"return policy_basic_information('{$data->policy_type}')\">{$data->policy_name}</span></span></div>";
            $info .= "<div class=\"related-label\">Policy ID: <span class=\"related-value\">{$data->policy_id}</span></div>";
            $info .= "<div class=\"related-label\">Policy Enrolled Date: <span class=\"related-value\">{$data->policy_enrolled_date}</span></div>";
            $info .= "<div class=\"related-label\">Claim Status: <span class=\"related-value\">{$this->the_status_label($data->claim_status)}</span></div>";
            $info .= "<div class=\"related-label\">Managed By: <span class=\"related-value\"><span class=\"user-name-link\" onclick=\"return user_basic_information('{$data->assigned_to}')\">{$data->assigned_to_name}</span></span></div>";
            $info .= "<div class=\"related-label\"><a class=\"text-primary\" href=\"{$this->baseUrl}claims-view/{$data->item_id}\">Click to view claim</a></div>";
        }
        
        return $info;
    }


    /**
     * Check who must recieve a particular complaint
     * 
     * @param \stdClass
     */
    public function related_to_info(stdClass $params) {

        /** Global */
        global $accessObject;

        /** user type */
        $userData = $params->userData;
        $user_id = $userData->user_id;
        $user_type = $userData->user_type;
        $related_to = create_slug($params->related_to);
        $related_to_id = $params->related_to_id;
        
        // the recipient
        $receive_by = [
            "user" => [
                "system-bug" => "admin",
                "insurance-company" => "nic",
                "client-policy" => "insurance_company",
                "claims" => "insurance_company",
                "general-service-delivery" => "admin",
                "previous-complaint" => "insurance_company",
                "company-insurance-policies" => "insurance_company"
            ],
            "business" => [
                "system-bug" => "admin",
                "insurance-company" => "nic",
                "claims" => "insurance_company",
                "client-policy" => "insurance_company",
                "general-service-delivery" => "admin",
                "previous-complaint" => "insurance_company",
                "company-insurance-policies" => "insurance_company"
            ],
            "broker" => [
                "system-bug" => "admin",
                "insurance-company" => "nic",
                "client-policy" => "insurance_company",
                "claims" => "insurance_company",
                "general-service-delivery" => "admin",
                "previous-complaint" => "admin",
                "company-insurance-policies" => "insurance_company"
            ],
            "agent" => [
                "system-bug" => "admin",
                "insurance-company" => "insurance_company",
                "client-policy" => "insurance_company",
                "claims" => "insurance_company",
                "general-service-delivery" => "admin",
                "previous-complaint" => "insurance_company",
                "company-insurance-policies" => "insurance_company"
            ],
            "bank" => [
                "system-bug" => "admin",
                "insurance-company" => "nic",
                "client-policy" => "insurance_company",
                "claims" => "insurance_company",
                "general-service-delivery" => "admin",
                "previous-complaint" => "admin",
                "company-insurance-policies" => "insurance_company"
            ],
            "bancassurance" => [
                "system-bug" => "admin",
                "insurance-company" => "bank",
                "general-service-delivery" => "admin",
                "previous-complaint" => "bank",
                "claims" => "bank",
                "client-policy" => "insurance_company",
                "company-insurance-policies" => "bank"
            ],
            "insurance_company" => [
                "system-bug" => "admin",
                "insurance-company" => "nic",
                "claims" => "reinsurance",
                "client-policy" => "insurance_company",
                "general-service-delivery" => "admin",
                "previous-complaint" => "admin",
                "company-insurance-policies" => "nic",
                "license-related" => "nic"
            ],
            "admin" => [
                "client-policy" => "insurance_company",
                "system-bug" => "admin",
                "claims" => "insurance_company",
                "insurance-company" => "nic",
                "general-service-delivery" => "admin",
                "previous-complaint" => "admin",
                "company-insurance-policies" => "insurance_company"
            ]
        ];

        $details = null;

        // load summary information about the object
        if($related_to_id !== "null") {
            
            /** If the related value is clients-policy */
            if($related_to == "client-policy") {
                /** load the users insurance policies */
                $policyObj = load_class("user_policy", "controllers");
                /** Parameters */
                $param = (object) [
                    "columns" => "a.item_id, a.policy_name, a.policy_id, a.policy_type,
                        a.policy_type_details, a.policy_start_date, a.premium, 
                        a.first_premium_due_date, a.last_premium_payment, a.next_repayment_date, 
                        a.requirements, a.payment_plan, a.date_created, a.user_id AS policy_holder_id, 
                        (SELECT name FROM users WHERE users.item_id = a.user_id LIMIT 1) AS policy_holder",
                    "internal_loop" => true,
                    "limit" => 1,
                    "policy_id" => $related_to_id
                ];
                // return the result
                $data = $policyObj->list($param)["data"];
                // return the result
                if(!empty($data)) {
                    $details = $data[0];
                }
            }

            /** If the related value is clients-insurance */
            elseif($related_to == "company-insurance-policies") {
                /** load the companies insurance policies */
                $policyObj = load_class("company_policy", "controllers");
                /** Parameters */
                $param = (object) [
                    "columns" => "
                        a.item_id, a.name AS item_name, a.name AS policy_name, a.company_id, a.policy_code,
                        a.year_enrolled, (SELECT name FROM companies WHERE companies.item_id = a.company_id LIMIT 1) AS company_name, a.date_created",
                    "policy_type_id" => $related_to_id,
                    "limited_data" => true,
                    "limit" => 1
                ];
                // return the result
                $data = $policyObj->list($param)["data"];
                // return the result
                if(!empty($data)) {
                    $details = $data[0];
                }
            }

            /** If the related value is insurance-company */
            elseif($related_to == "insurance-company") {
                /** load the insurance companies list */
                $companyObj = load_class("company", "controllers");
                /** Parameters */
                $param = (object) [
                    "columns" => "a.item_id, a.name, a.contact, a.email, a.establishment_date, a.description, a.logo, a.date_created",
                    "company_id" => $related_to_id,
                    "limit" => 1
                ];
                $data = $companyObj->list($param)["data"];
                // return the result
                if(!empty($data)) {
                    $details = $data[0];
                }
            }

            /** If the related value is previous-complaint */
            elseif($related_to == "previous-complaint") {

                /** Parameters */
                $param = (object) [
                    "columns" => "a.item_id AS complaint_id, a.user_id, a.subject, a.related_to_details, a.related_to, a.date_created, a.submit_status",
                    "complaint_id" => $related_to_id,
                    "userId" => $user_id,
                    "limit" => 1,
                    "remote" => true,
                    "internal_loop" => true,
                ];
                // return the result
                $data = $this->list($param)["data"];

                // return the result
                if(!empty($data)) {
                    $details = $data[0];
                }
            }

            /** If the related value is previous-complaint */
            elseif($related_to == "system-bug") {
                /** Parameters */
                $param = (object) [
                    "columns" => "a.item_id AS complaint_id, a.user_id, a.subject, a.related_to_details, a.related_to, a.date_created, a.submit_status",
                    "userId" => $user_id,
                    "limit" => 1,
                    "remote" => true,
                    "complaint_id" => $related_to_id,
                    "internal_loop" => true,
                ];
                // return the result
                $data = $this->list($param)["data"];
                // return the result
                if(!empty($data)) {
                    $details = $data[0];
                }
            }

            /** If the related value is license-related */
            elseif($related_to == "license-related") {
                
                // if the user has the requisite permissions
                if($accessObject->hasAccess("view", "licenses")) {
                    
                    /** load the users insurance policies */
                    $licenseObj = load_class("licenses", "controllers");
                    
                    /** Parameters */
                    $param = (object) [
                        "columns" => "a.item_id, a.license_id, a.description, a.assign_to, a.start_date, a.expiry_date, a.status, a.date_created",
                        "item_id" => $related_to_id,
                        "limit" => 1,
                    ];
                    // return the result
                    
                    $data = $licenseObj->list($param)["data"];
                    
                    // return the result
                    if(!empty($data)) {
                        $details = $data[0];
                    }
                }
            }

            /** If the related value is user claims */
            elseif($related_to == "claims") {
                
                /** load the users insurance policies */
                $claimsObj = load_class("claims", "controllers");

                /** Parameters */
                $param = (object) [
                    "columns" => "
                        a.item_id, a.company_id, a.user_id, a.policy_id, a.amount_claimed, 
                        a.date_created, a.policy_type, a.assigned_to, a.status AS claim_status,
                        (SELECT name FROM users WHERE users.item_id = a.created_by LIMIT 1) AS requested_by,
                        (SELECT name FROM users WHERE users.item_id = a.seen_by LIMIT 1) AS seen_by_name,
                        (SELECT name FROM companies WHERE companies.item_id = a.company_id LIMIT 1) AS company_name,
                        (SELECT name FROM users WHERE users.item_id = a.assigned_to LIMIT 1) AS assigned_to_name,
                        (SELECT b.policy_start_date FROM users_policy b WHERE a.policy_id = b.policy_id LIMIT 1) AS policy_enrolled_date
                    ",
                    "claim_id" => $related_to_id,
                    "userId" => $user_id,
                    "remote" => true,
                ];

                // make request
                $data = $claimsObj->list($param)["data"];

                // return the result
                if(!empty($data)) {

                    $claim_data = $data[0];
                    unset($claim_data->form_fields);
                    
                    $details = $claim_data;
                }

            }
            
        }

        return [
            "recipient" => $receive_by[$user_type][$related_to] ?? "unrelated",
            "details" => !empty($details) ? json_encode($details) : null,
        ];

    }

    /**
     * Add a new complaint
     * 
     * @param \stdClass $params
     * 
     * @return Array
     */
    public function add(stdClass $params) {

        /** Validate the user variables parsed */
        $params->_item_id = random_string("alnum", 32);

        /** Unauthorized */
        if(!isset($params->userData->user_id)) {
            return $this->permission_denied;
        }

        // submission status
        $submit_status = isset($params->the_button) ? strtolower($params->the_button) : "save";
        $params->related_to = (isset($params->related_to) && $params->related_to !== "null") ? $params->related_to : "general-service-delivery";
        $params->related_to_id = (isset($params->related_to_id) && $params->related_to !== "null") ? $params->related_to_id : null;

        // get the recipient of this data set
        $recipient = $this->related_to_info($params)["recipient"];
        $related_to_details = $this->related_to_info($params)["details"];

        // append the attachments
        $filesObj = load_class("files", "controllers");
        $attachments = $filesObj->prep_attachments("complaints", $params->userData->user_id, $params->_item_id);
        
        // catch any errors
        try {

            // begin transaction
            $this->db->beginTransaction();

            // clean the content
            $params->message = addslashes($params->message);

            // insert the record
            $stmt = $this->db->prepare("
                INSERT INTO users_complaints SET date_created = now()
                ".(isset($params->userId) ? ",user_id='{$params->userId}'" : null)."
                ".(isset($params->_item_id) ? ",item_id='{$params->_item_id}'" : null)."
                ".(isset($params->related_to) ? ",related_to='{$params->related_to}'" : null)."
                ".(isset($params->related_to_id) ? ",related_to_id='{$params->related_to_id}'" : null)."
                ".(isset($params->subject) ? ",subject='{$params->subject}'" : null)."
                ".(isset($params->userData->company_id) ? ",company_id='{$params->userData->company_id}'" : null)."
                ".(isset($params->message) ? ",message='{$params->message}'" : null)."
                ".(isset($params->complaint_type) ? ",complaint_type='{$params->complaint_type}'" : null)."
                ".(isset($params->user_type) ? ",user_type='{$params->user_type}'" : null)."
                ".(isset($submit_status) ? ",submit_status='{$submit_status}'" : null)."
                ".(isset($recipient) ? ",recipient='{$recipient}'" : null)."
                ".(isset($related_to_details) ? ",related_to_details='{$related_to_details}'" : null)."
            ");
            // execute the prepared statement
            if($stmt->execute()) {
                
                // only insert attachment record if there was an attachment to the comment
                if(!empty($attachments["files"])) {
                    // insert attachment
                    $files = $this->db->prepare("INSERT INTO files_attachment SET resource= ?, resource_id = ?, description = ?, record_id = ?, created_by = ?, attachment_size = ?");
                    $files->execute(["complaints", $params->_item_id, json_encode($attachments), "{$params->_item_id}", $params->userId, $attachments["raw_size_mb"]]);
                }

                // other parameters
                $code = 200;
                $data = "Your complaint was successfully ".(($submit_status == "save") ? "lodged" : "saved as draft.");

                // log the user activity
                $this->userLogs("complaints", $params->_item_id, null, "<strong>{$params->userData->name}</strong> lodged a new complaint with subject: {$params->subject}.", $params->userId);

            } else {
                $data = "Sorry! There was an error while processing the request.";
            }

            // commit the transaction
            $this->db->commit();

            # append to the response
			return [
                "code" => $code,
                "data" => $data,
                "additional" => [
                    "clear" => true,
                    "reload" => "{$this->baseUrl}complaints-view/{$params->_item_id}",
                ],
                "record_id" => $params->_item_id
            ];
        } catch(PDOException $e) {
            $this->db->rollBack();
            return $e->getMessage();
        }
    }

	/**
	 * Update a complaints record
	 * 
	 * @param \stdClass $params
	 * 
	 * @return Array
	 */
	public function update(stdClass $params) {
		
        $data = "";
        $code = 500;
        $additional = [];

        global $noticeClass;

        // if the complaint_id was not parsed
        if(!isset($params->complaint_id) || empty($params->complaint_id)) {
            return ["code" => 203, "data" => "Sorry! The complaint id cannot be empty."];
        }

        /** Load the previous record */
        $prevData = $this->pushQuery("a.*, (SELECT b.description FROM files_attachment b WHERE b.record_id = a.item_id ORDER BY a.id DESC LIMIT 1) AS attachment", 
            "users_complaints a", "a.item_id='{$params->complaint_id}' LIMIT 1");

        // if the previous data is false
        if(empty($prevData)) {
            return ["code" => 203, "data" => "Sorry! An invalid complaint id was parsed."];
        }

        // get the first item of the array
        $prevData = $prevData[0];

        // if the item is already saved or closed
        if(in_array($prevData->status, ["Solved", "Closed"])) {
            return ["code" => 203, "data" => "Sorry! This complaint cannot be updated."];
        }

        // initialize
        $initial_attachment = [];

        /** Confirm that there is an attached document */
        if(!empty($prevData->attachment)) {
            // decode the json string
            $db_attachments = json_decode($prevData->attachment);
            // get the files
            if(isset($db_attachments->files)) {
                $initial_attachment = $db_attachments->files;
            }
        }

        // append the attachments
        $filesObj = load_class("files", "controllers");
        $attachments = $filesObj->prep_attachments("complaints", $params->userData->user_id, $params->complaint_id, $initial_attachment);

        try {

            // begin transaction
            $this->db->beginTransaction();

            // related to information
            $params->related_to = (isset($params->related_to) && $params->related_to !== "null") ? $params->related_to : null;
            $params->related_to_id = (isset($params->related_to_id) && $params->related_to !== "null") ? $params->related_to_id : null;
            $params->assigned_to = (isset($params->assigned_to) && $params->assigned_to !== "null") ? $params->assigned_to : null;
            
            // get the recipient of this data set
            $recipient = $this->related_to_info($params)["recipient"];
            $related_to_details = $this->related_to_info($params)["details"];

            // prepare the the statement
            $stmt = $this->db->prepare("
                UPDATE users_complaints SET last_updated=now(), updated_by = ?
                ".(isset($params->parent_id) ? ",complaint_id='{$params->parent_id}'" : null)."
                ".(isset($params->subject) ? ",subject='{$params->subject}'" : null)."
                ".(isset($params->related_to) ? ",related_to='{$params->related_to}'" : null)."
                ".(isset($params->related_to_id) ? ",related_to_id='{$params->related_to_id}'" : null)."
                ".(isset($params->message) ? ",message='".addslashes($params->message)."'" : null)."
                ".(!empty($params->assigned_to) ? ",assigned_to='{$params->assigned_to}'" : null)."
                ".(isset($params->status) ? ",status='".ucfirst($params->status)."'" : null)."
                ".(!empty($related_to) ? ",recipient='{$recipient}'" : null)."
                ".(!empty($related_to_details) ? ",related_to_details='{$related_to_details}'" : null)."
                WHERE item_id = ? LIMIT 1
            ");

            // if the update was successful
            if($stmt->execute([$params->userId, $params->complaint_id])) {
                $code = 200;
                $data = "Complaint successfully updated.";

                // only insert attachment record if there was an attachment to the comment
                if(!empty($attachments["files"])) {

                    // update attachment if already existing
                    if(isset($db_attachments)) {
                        $files = $this->db->prepare("UPDATE files_attachment SET description = ?, attachment_size = ? WHERE record_id = ? LIMIT 1");
                        $files->execute([json_encode($attachments), $attachments["raw_size_mb"], $params->complaint_id]);
                        // append to the additional variable
                        $additional["reload"] = "{$this->baseUrl}complaints-view/{$params->complaint_id}";
                    } else {
                        // insert the record if not already existing
                        $files = $this->db->prepare("INSERT INTO files_attachment SET resource= ?, resource_id = ?, description = ?, record_id = ?, created_by = ?, attachment_size = ?");
                        $files->execute(["claims", $params->complaint_id, json_encode($attachments), "{$params->complaint_id}", $params->userId, $attachments["raw_size_mb"]]);

                        // append to the additional variable
                        $additional["reload"] = "{$this->baseUrl}complaints-view/{$params->complaint_id}";
                    }
                }
                
                // set the notice
                $notice = "{$this->appName} Calculation<br>Property changed by a manual update effected by <strong>{$params->userData->name}</strong>.";
                
                // log each individual changes effect
                if(($params->assigned_to !== $prevData->assigned_to)) {
                    
                    // name of the one who changed the status, and the name of the person it has been assigned to
                    $from = $this->columnValue("a.name AS from_person_name, 
                        (SELECT b.name FROM users b WHERE b.item_id='{$prevData->user_id}' LIMIT 1) AS complainant_name,
                        (SELECT b.name FROM users b WHERE b.item_id='{$params->assigned_to}' LIMIT 1) AS to_name", 
                        "users a", "a.item_id='{$prevData->assigned_to}'"
                    );

                    // alert the user whom this complaint has been assigned to
                    if($params->assigned_to !== $params->userId) {

                        // form the notification parameters
                        $notice_param = (object) [
                            '_item_id' => random_string("alnum", 32),
                            'user_id' => $params->assigned_to,
                            'subject' => "Complaint",
                            'username' => $params->userData->username,
                            'remote' => false, 
                            'message' => "<strong>{$params->userData->name}</strong> assigned a complaint to you. <a title=\"Click to View\" href=\"{{APPURL}}complaints-view/{$params->complaint_id}\">Click to view</a>.",
                            'notice_type' => 11,
                            'userId' => $params->userId,
                            'initiated_by' => 'system'
                        ];
                        
                        // add a new notification
                        $noticeClass->add($notice_param);
                    }

                    // if the complaint was initially assigned to someone
                    if(isset($from->from_person_name)) {
                        $message = "Complaint initially assigned to <strong>{$from->from_person_name}</strong> was reassigned to <strong>{$from->to_name}</strong>.";
                    } elseif(isset($from->to_name)) {
                        $message = "Complaint was assigned to <strong>{$from->to_name}</strong>.";
                    } else {
                        $message = "Complaint has been reassigned";
                    }

                    // log the assigned to change
                    $this->userLogs("complaints", $params->complaint_id, $prevData, $message, $params->userId, $notice);
                }

                // if the status was parsed
                if(isset($params->status)) {

                    // the response to return
                    $additional = [
                        "record" => [
                            "record_status" => $this->the_status_label($params->status)
                        ]
                    ];

                    // status changes
                    if(isset($params->status) && ($params->status !== $prevData->status)) {
                        
                        // notify the user if the status has been changed to solved
                        if($params->status == "Solved") {

                            // append to the additional variable
                            $additional["reload"] = $this->session->user_current_url;
                            
                            // form the notification parameters
                            $notice_param = (object) [
                                '_item_id' => random_string("alnum", 32),
                                'user_id' => $prevData->user_id,
                                'subject' => "Complaint",
                                'username' => $params->userData->username,
                                'remote' => false, 
                                'message' => "Your complaint with subject <strong>{$prevData->subject}</strong> has been <strong>Solved</strong> by <strong>{$params->userData->name}</strong>.",
                                'notice_type' => 11,
                                'userId' => $params->userId,
                                'initiated_by' => 'system'
                            ];
                            
                            // add a new notification
                            $noticeClass->add($notice_param);
                        }

                        // log the assigned to change
                        $this->userLogs("complaints", $params->complaint_id, $prevData, "Status was changed from <strong>{$prevData->status}</strong> to <strong>{$params->status}</strong>.", $params->userId, $notice);
                    }

                } else {
                    // log the user activity and save the changes
                    $this->userLogs("complaints", $params->complaint_id, $prevData, "<strong>{$params->userData->name}</strong> updated the complaint: {$prevData->subject}.", $params->userId);
                }
            }

            $this->db->commit();

            // return the response
            return [
                "code" => $code,
                "data" => $data,
                "additional" => $additional
            ];

        } catch(PDOException $e) {
            $this->db->rollBack();
            return $e->getMessage();
        }

	}

}

?>