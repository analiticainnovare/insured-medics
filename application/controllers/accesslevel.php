<?php
// ensure this file is being included by a parent file
if( !defined( 'BASEPATH' ) ) die( 'Restricted access' );

class Accesslevel {

    public $userId;
    public $userPermits = null;

    private $_message = '';

    public function __construct(){
        global $medics, $session;
        
        $this->db = $medics;
        $this->session = $session;
    }

    /**
     * A method to fetch access level details from DB
     *
     * @param String $accessLevel Pass level id to fetch details
     *
     * @return Object $this->_message
     */
    public function getPermissions($accessLevel = false) {
        $this->_message = false;

        // convert to uppercase
        $accessLevel = strtoupper($accessLevel);

        // set the condition for loading the user permission
        $condition = ($accessLevel == false) ? "1" : " (id = '{$accessLevel}' OR name = '{$accessLevel}')";

        // prepare the statement
        $stmt = $this->db->prepare("SELECT * FROM users_types WHERE {$condition}");

        if ($stmt->execute()) {
            $this->_message = $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        return $this->_message;
    }

    /**
     * A method to fetch access level details from DB
     *
     * @param String $accessLevel Pass level user_id to fetch details
     *
     * @return Object $this->_message
     */
    public function getUserPermissions()
    {
        $this->_message = false;

        $stmt = $this->db->prepare("SELECT * FROM users_roles WHERE user_id = '{$this->userId}'");

        if ($stmt->execute()) {
            $this->_message = $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        return $this->_message;
    }

    /**
     * A method to save SMS Into history
     *
     * @param String $message Pass message to send
     * @param String $to      Pass recipients number
     *
     * @return Bool $this->_message
     */
    public function assignUserRole($userID, $accessLevel, $permissions = false) {
        $this->_message = false;

        if ($permissions == false) {

            // Get Default Permissions
            $permissions = ($this->getPermissions($accessLevel) != false) ? 
                $this->getPermissions($accessLevel)[0]->default_permissions : null;

            $stmt = $this->db->prepare("
                INSERT INTO users_roles SET user_id = '{$userID}', permissions = '{$permissions}'
            ");

            if ($stmt->execute()) {
                $this->_message = true;
            }
        } else {

            $stmt = $this->db->prepare("
                UPDATE users_roles SET permissions = '".(is_array($permissions) ? json_encode($permissions) : $permissions)."' WHERE user_id = '{$userID}'
            ");

            if ($stmt->execute()) {
                $this->_message = true;
            }
        }

        return $this->_message;
    }

    /**
     * Confirm that the user has permission
     * 
     * @return Bool
     */
    public function hasAccess($role, $currentPage = null) {
        
        // Check User Roles Table
        $permits = !empty($this->userPermits) ? $this->userPermits : $this->getUserPermissions();
        
        // user permissions
        if ($permits != false) {

            // code the user permissions section
            $permit = empty($this->userPermits) ? json_decode($permits[0]->permissions) : json_decode($this->userPermits);
            $permissions = $permit->permissions;
            
            // confirm that the requested page exists
            if(!isset($permissions->$currentPage)) {
                return false;
            }

            // confirm that the role exists
            if(isset($permissions->$currentPage->$role)) {
                return (bool) ($permissions->$currentPage->$role == 1) ? true : false;
            } else {
                return false;
            }
        }
    }

}