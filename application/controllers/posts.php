<?php 

class Posts extends Medics {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Set the delete button
     * 
     * @param String $item_id
     * @param String $reply_user_id
     * @param String $logged_user_id
     * @param Int $date_created
     * 
     * @return String
     */
    private function is_deletable($item_id, $reply_user_id, $logged_user_id, $date_created) {

        // if the user who shared it is not the same as the person logged in then return
        if($reply_user_id !== $logged_user_id) {
            return "";
        }
        
        // set the delete button
        $delete_button = '<div class="dropdown reply-options" id="reply-option">
                <button class="p-0 btn toggle-reply-options" type="button" id="dropdownMenuButton_'.$item_id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i style="font-size:12px" class="fa fa-bars"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton_'.$item_id.'">
                    <a class="dropdown-item edit-post d-flex align-items-center" data-reply-id="'.$item_id.'" href="javascript:void(0)"><i class="fa fa-edit" class="icon-sm mr-2"></i> &nbsp;<span class="">Edit</span></a>
                    <a class="dropdown-item delete-post d-flex align-items-center" data-reply-id="'.$item_id.'" href="javascript:void(0)"><i class="fa fa-trash" class="icon-sm mr-2"></i> &nbsp;<span class="">Delete</span></a>
                </div>
            </div>';

        // if the post is more than 3 hours then it cannot be deleted
        if(raw_time_diff($date_created) < 2.99) {
            return $delete_button;
        }

        return "";

    }

    /**
     * Confirm that the user has liked the post
     * 
     * @param \stdClass $likes_list
     * @param String $user_id
     */
    public function is_liked($likes_list, $user_id) {

        if(empty($likes_list)) {
            return false;
        }

        // convert the list into an array
        $likes_list = (array) $likes_list;

        // return boolean
        return (bool) in_array($user_id, $likes_list);
    }

    /**
     * List replies for an item
     * 
     * @param \stdClass $params
     * 
     * @return Array
     */
    public function list(stdClass $params) {

        // init the filter
        $where_clause = "";
        $params->query = "1";

        // add some filters
        $params->query .= (isset($params->resource_id)) ? " AND a.resource_id='{$params->resource_id}'" : null;
        $params->query .= (isset($params->shared_by)) ? " AND a.shared_by='{$params->shared_by}'" : null;
        $params->query .= (isset($params->post_id)) ? " AND a.post_id='{$params->post_id}'" : null;
        $params->query .= (isset($params->visibility)) ? " AND a.visibility='{$params->visibility}'" : " AND a.visibility='Public'";
        $params->query .= (isset($params->post_parent_id)) ? " AND a.post_parent_id='{$params->post_parent_id}'" : " AND a.post_parent_id='0'";
        
        // the number of rows to limit the query
		$params->limit = isset($params->limit) ? $params->limit : $this->global_limit;

        // check the last item to display
        $params->last_post_id = isset($params->last_post_id) && !empty($params->last_post_id) ? $params->last_post_id : null;
        if($params->last_post_id) {
            if($params->last_post_id == "no_more_record") {
                $where_clause = " AND a.id < '{$params->last_post_id}'";
            } else {
                $where_clause = " AND a.id < {$params->last_post_id}";
            }
        }

        try {
            // set initial results
            $results = [];
            $filesObject = load_class("forms", "controllers");

            // load the very first record in the query parsed by the user
            $last_one = $this->db->prepare("SELECT a.id AS first_item FROM users_posts a WHERE {$params->query} {$where_clause} AND a.status = '1' ORDER BY a.id ASC LIMIT 1");
            $last_one->execute();
            $first_item = $last_one->fetch(PDO::FETCH_OBJ);

            // prepare the statement
            $stmt = $this->db->prepare("SELECT a.id, a.resource_id, a.post_id,  a.post_content, a.shared_by, 
                    a.likes_count, a.comments_count, a.views_count, a.shares_count, a.likes_list, DATE(a.date_created) AS raw_date, a.post_parent_id,
                    (
                        SELECT b.description FROM files_attachment b WHERE b.record_id = a.post_id ORDER BY b.id DESC LIMIT 1
                    ) AS attachment, a.date_created, a.post_user_agent, a.post_user_device, a.post_mentions
                FROM users_posts a
                WHERE {$params->query} {$where_clause} ORDER BY TIMESTAMP(a.date_created) DESC LIMIT {$params->limit}
            ");
            $stmt->execute();
            $results = $stmt->fetchAll(PDO::FETCH_OBJ);
            
            // init the values
            $data = [];
            $resource = "";
            $last_post_id = "no_more_record";
            // loop through the records
            foreach($results as $key => $result) {
                $key++;
                $result->row_id = $key;

                $result->replied_by = $this->replied_by($result->shared_by);
                $result->post_content = htmlspecialchars_decode($result->post_content);
                $result->post_content = custom_clean($result->post_content);
                $result->likes_list = json_decode($result->likes_list);
                $result->is_liked = $this->is_liked($result->likes_list, $params->userId);
                
                // delete 
                $result->attachment = !empty($result->attachment) ? json_decode($result->attachment) : $this->fake_files;

                $result->time_ago = date("h:iA", strtotime($result->date_created));
                $result->clean_date = date("l, F jS", strtotime($result->date_created));
                
                // if not a remote request
                if((isset($params->remote) && !$params->remote) || !isset($params->remote)) {
                    $result->attachment_html = "";
                    
                    $result->delete_button = $this->is_deletable($result->resource_id, $result->shared_by, $params->userId, $result->date_created);
                    
                    if(!empty($result->attachment->files)) {
                        $result->attachment_html = $filesObject->list_attachments($result->attachment->files, $result->shared_by, "col-lg-6 col-md-6", false, false);
                    }
                }

                $last_post_id = $result->id;

                // unset some variables
                unset($result->deleted);
                unset($result->id);

                $result->modified_date = date("l, F jS, Y \a\\t h:i:sA", strtotime($result->date_created));

                $data[] = $result;
            }

            // if the user wants a clean output 
            if(isset($params->clean_output)) {
                return isset($data[0]) ? $data[0] : [];
            }

            // last reply id
            if(!empty($data)) {
                // $last_id = min(array_column($data, "id"));
                $last_post_id = $last_post_id;
            }
            
            // last row id record
            return [
                "posts_list" => $data,
                "first_post_id" => isset($first_item->first_item) ? $first_item->first_item : 0,
                "last_post_id" => $last_post_id
            ];
        } catch(PDOException $e) {
            return [];
        }
    }

    /**
     * Share a post
     * 
     * @param \stdClass $params
     * @param String $params->post_user_id          The id of the user making the post
     * @param String $params->post_content          The content of the post to share
     * 
     * @return Array
     */
    public function share(stdClass $params) {

        // prepare the post
        $params->_item_id = random_string("alnum", 32);
        $post_content = custom_clean(htmlspecialchars_decode($params->post_content));

        $user_agent = "{$this->platform} | {$this->browser} | {$this->agent} | ". ip_address();
        $params->visibility = isset($params->visibility) ? $params->visibility : "Public";
        
        // prepare the attachments
        $filesObj = load_class("files", "controllers");
        $attachments = $filesObj->prep_attachments("posts_{$params->post_user_id}", $params->userData->user_id, $params->_item_id);

        try {

            // share a post
            $stmt = $this->db->prepare("
                INSERT INTO users_posts SET resource_id = ?, post_content = ?, post_user_agent = ?, 
                    post_user_device = ?, shared_by = ?, user_type = ?, post_id = ?
                ".((isset($params->post_parent_id) ? ",post_parent_id='{$params->post_parent_id}'" : null))."
                ".((isset($params->visibility) ? ",visibility='{$params->visibility}'" : null))."
            ");
            $stmt->execute([$params->post_user_id, htmlspecialchars($post_content), $user_agent, $this->platform, $params->userId, 
                $params->userData->user_type, $params->_item_id]);

            // only insert attachment record if there was an attachment to the post
            if(!empty($attachments["files"])) {
                // prepare and execute the statement
                $files = $this->db->prepare("INSERT INTO files_attachment SET resource= ?, resource_id = ?, description = ?, record_id = ?, created_by = ?, attachment_size = ?");
                $files->execute(["posts", $params->_item_id, json_encode($attachments), "{$params->_item_id}", $params->post_user_id, $attachments["raw_size_mb"]]);
            }
            
            // set the param
            $param = (object) [
                "limit" => 1,
                "clean_output" => true,
                "userId" => $params->userId,
                "item_id" => $params->_item_id
            ];

            // return the results
            return [
                "code" => 200,
                "data" => "Post successfully shared",
                "additional" => [
                    "data" => $this->list($param)
                ]
            ];

        } catch(PDOException $e) { 
            return [];
        }

    }
    
    /**
     * Like/Unlike a post
     * 
     * Like the post if not already in the array list, else unlike the post
     * Return the new count back as part of the results
     * 
     * @param String $params->post_id
     * 
     * @return Array
     */
    public function likes_count(stdClass $params) {

        /** load the post */
        $likes_list = $this->db->prepare("SELECT likes_list FROM users_posts WHERE post_id = ? AND status = ? LIMIT 1");
        $likes_list->execute([$params->post_id, 1]);
        $result = $likes_list->fetch(PDO::FETCH_OBJ);

        /** return if empty */
        if(empty($result)) {
            return;
        }

        /** Convert the list into an array */
        $list = !empty($result->likes_list) ? json_decode($result->likes_list, true) : [];

        /** append or remove from list */
        if(!empty($list)) {

            $is_found = false;
            $is_found_key = null;

            // loop through the list
            foreach($list as $key => $value) {
                // if the username is found
                if($value == $params->userId) {
                    // remove the key from the list
                    $is_found = true;
                    $is_found_key = $key;
                    break;
                }
            }
            
            // if the key was found
            if($is_found) {
                
                // remove the value
                unset($list[$is_found_key]);
                
                // likes count
                $likes_count = count($list);

                // update the database table
                $this->db->query("UPDATE users_posts SET likes_list='".json_encode($list)."', likes_count=(likes_count-1) WHERE post_id='{$params->post_id}' LIMIT 1");
            } else {
                // append the user id
                array_push($list, $params->userId);
                $likes_count = count($list);

                // update the database table
                $this->db->query("UPDATE users_posts SET likes_list='".json_encode($list)."', likes_count=(likes_count+1) WHERE post_id='{$params->post_id}' LIMIT 1");
            }
        } else {
            // create a new array list
            $list[] = $params->userId;
            $likes_count = 1;

            // update the database table
            $this->db->query("UPDATE users_posts SET likes_list='".json_encode($list)."', likes_count='1' WHERE post_id='{$params->post_id}' LIMIT 1");
        }

        return [
            "code" => 200,
            "data" => $likes_count
        ];

    }

}
?>