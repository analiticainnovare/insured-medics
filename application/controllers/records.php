<?php

class Records extends Medics {

    public function __construct()
    {
        parent::__construct();

        
        // get the table
        $this->resource_tables = [
            "messages" => [
                "query" => "(a.receiver_id='{{RECORD_ID}}' AND a.seen_status='0')",
                "column" => "a.message_unique_id, a.date_created, b.name AS sender_name, b.image AS sender_image,
                        c.favicon AS notice_favicon, c.favicon_color AS favicon_color",
                "table" => "users_chat a LEFT JOIN users b ON b.item_id = a.sender_id LEFT JOIN users_notification_types c ON c.id = a.notice_type"
            ],
            "notifications" => [
                "query" => "(a.user_id='{{RECORD_ID}}')",
                "column" => "a.subject, a.message, a.date_created, c.favicon AS notice_favicon, c.favicon_color AS favicon_color",
                "table" => "users_notification a LEFT JOIN users_notification_types c ON c.id = a.notice_type"
            ],
            "replies" => [
                "query" => "(a.resource_id='{{RECORD_ID}}' AND a.feedback_type='reply')",
                "column" => "(SELECT b.name FROM users b WHERE b.item_id = a.user_id LIMIT 1) AS replied_by, a.date_created, a.message",
                "table" => "users_feedback a"
            ],
            "comments" => [
                "query" => "(a.resource_id='{{RECORD_ID}}' AND a.feedback_type='comment')",
                "column" => "(SELECT b.name FROM users b WHERE b.item_id = a.user_id LIMIT 1) AS replied_by, a.date_created, a.message",
                "table" => "users_feedback a"
            ],
            "complaints" => [
                "query" => "a.user_id='{{RECORD_ID}}' AND a.status NOT IN('Solved','Closed')",
                "column" => "(SELECT b.name FROM users b WHERE b.item_id = a.user_id LIMIT 1) AS complaint_by, a.date_created, a.subject",
                "table" => "users_complaints a"
            ],
            "adverts" => [
                "query" => "item_id='{{RECORD_ID}}' AND status NOT IN('Solved','Cancelled','Deleted')",
                "column" => "date_created, advert_title, ad_objective",
                "table" => "adverts"
            ],
            "claims" => [
                "query" => "(item_id='{{RECORD_ID}}' OR assigned_to='{{RECORD_ID}}' OR created_by='{{RECORD_ID}}') AND status NOT IN ('Cancelled')",
                "column" => "(SELECT b.name FROM users b WHERE b.item_id = a.user_id LIMIT 1) AS client_name, a.date_created, a.policy_id, a.amount_claimed, a.approved_amount",
                "table" => "users_policy_claims a",
            ],
            "policies" => [
                "query" => "item_id='{{RECORD_ID}}' AND (user_id='{{USER_ID}}' OR created_by='{{USER_ID}}')",
                "column" => "(SELECT b.name FROM users b WHERE b.item_id = a.user_id LIMIT 1) AS client_name, a.policy_id, a.policy_start_date, a.next_repayment_date",
                "table" => "users_policy a"
            ],
            "payments" => [
                "query" => "(user_id='{{RECORD_ID}}' AND payment_status='Pending')",
                "column" => "a.policy_id, a.initiated_medium, a.created_date, a.amount, a.payment_status",
                "table" => "users_policy_payment a"
            ],
            "cancel_record_request" => [
                "query" => "slug='{{RECORD_ID}}' AND status='Pending'",
                "column" => "*",
                "table" => "users_items_cancellation_request a"
            ],
            "approve_cancel_record_request" => [
                "query" => "slug='{{RECORD_ID}}' AND status='Pending'",
                "column" => "*",
                "table" => "users_items_cancellation_request a"
            ],
            "license_application" => [
                "query" => "item_id='{{RECORD_ID}}' AND status='Pending'",
                "column" => "*",
                "table" => "companies_licenses"
            ]
        ];
    }

    /**
     * Records counter
     * 
     * @param \stdClass $params
     * 
     * @return Array
     */
    public function threads(stdClass $params) {

        /** Do not accept api calls to this endpoint */
        if($params->remote) {
            return ["code" => 203, "data" => "Access denied!"];
        }
        /** Counter parameter check */
        if(!is_array($params->param)) {
            return ["code" => 203, "data" => "The value must be an array"];
        }

        /** data format array variable */
        $list = [];
        $records_count = [];

        /** If the user_id has been set */
        if(isset($params->param["user_id"], $params->param["stream"])) {
                
            /** Convert the stream into an array string */
            $stream = $this->stringToArray($params->param["stream"]);

            /** If complaints is in the array */
            if(in_array("complaints", $stream)) {
                /** Load complaints */
                $param = (object) [
                    "limit" => 8,
                    "remote" => true,
                    "minimal_load" => true,
                    "userId" => $params->param["user_id"],
                    "userData" => $params->userData,
                ];
                $complaints = load_class("complaints", "controllers")->list($param);
                $list["complaints"] = isset($complaints["data"]) ? $complaints["data"] : [];
            }

            /** If user_complaints is in the array */
            if(in_array("user_complaints", $stream)) {
                /** Load user_complaints */
                $param = (object) [
                    "limit" => 8,
                    "remote" => true,
                    "minimal_load" => true,
                    "submit_status" => "save",
                    "internal_loop" => true,
                    "userId" => $params->param["user_id"],
                    "user_id" => $params->param["user_id"],
                    "userData" => $params->userData,
                ];
                $complaints = load_class("complaints", "controllers")->list($param);
                $list["complaints"] = isset($complaints["data"]) ? $complaints["data"] : [];
            }

            /** If policy was requested */
            if(in_array("user_policy", $stream)) {
                /** Load policies */
                $param = (object) [
                    "minimal_load" => true,
                    "load_comments" => true,
                    "userId" => $params->userId,
                    "user_id" => $params->param["user_id"]
                ];
                $policies = load_class("user_policy", "controllers")->list($param);
                $list["policies"] = isset($policies["data"]) ? $policies["data"] : []; 
            }

            /** If claims was requested */
            if(in_array("user_claims", $stream)) {
                /** Load policies */
                $param = (object) [
                    "minimal_load" => true,
                    "load_comments" => true,
                    "userData" => $params->userData,
                    "userId" => $params->userId,
                    "user_id" => $params->param["user_id"]
                ];
                $policies = load_class("claims", "controllers")->list($param);
                $list["claims"] = isset($policies["data"]) ? $policies["data"] : []; 
            }

            /** If company notes was requested */
            if(in_array("company_notes", $stream)) {
                /** Load policies */
                $param = (object) [
                    "minimal_load" => true,
                    "userData" => $params->userData,
                    "company_id" => $params->param["user_id"]
                ];
                $list["notes"] = load_class("company", "controllers")->list_notes($param);
            }

            /** If a request was made for company policies */
            if(in_array("company_policy", $stream)) {
                /** Load policies */
                $param = (object) [
                    "minimal_load" => true,
                    "userId" => $params->userId,
                    "userData" => $params->userData,
                    "company_id" => $params->param["user_id"]
                ];
                $list["company_policy"] = load_class("company_policy", "controllers")->list($param);
            }

            /** If a request was made for profile photos uploaded */
            if(in_array("profile_post_photos", $stream)) {
                /** Load policies */
                $param = (object) [
                    "limit" => 5,
                    "internal_limit" => 12,
                    "is_deletable" => false,
                    "attachment_type" => "jpg,png,gif,jpeg",
                    "created_by" => $params->param["user_id"]
                ];
                $list["profile_post_photos"] = load_class("files", "controllers")->list_attachments($param);
            }

        }

        /** Get the records_count data */
        if(isset($params->param["records_count"])) {

            /** loop through the array list */
            foreach($params->param["records_count"] as $key => $eachItem) {

                // if the record and columns are parsed
                if(isset($eachItem["record_id"], $eachItem["column"])) {
                    
                    // append the columns variable
                    $record_id = $eachItem["record_id"];
                    $return_value = $eachItem["column"];
                    $table_columns = $this->resource_tables[$key]["column"] ??  "a.*";
                    $table_name = $this->resource_tables[$key]["table"];
                    $where_clause = $this->resource_tables[$key]["query"];

                    // catch all pdo errors
                    try {
                        // get the record
                        $query = "SELECT {$table_columns} FROM {$table_name} WHERE {$where_clause} ORDER BY a.id DESC";
                        $query = str_ireplace(["{{RECORD_ID}}"], $record_id, $query);

                        $the_query[] = $query;

                        // execute the query
                        $stmt = $this->db->prepare($query);
                        $stmt->execute();
                        
                        $rows_count = $stmt->rowCount();

                        // if result was found
                        if($rows_count) {
                            
                            // only append the following to the list
                            if(in_array($key, ["notifications", "messages"])) {

                                // fetch the results
                                $results = $stmt->fetchAll(PDO::FETCH_OBJ);
                                $array = [];

                                // loop through the results list
                                foreach($results as $ikey => $each) {
                                    if(isset($each->message)) {
                                        $each->message = limit_words($each->message, 12);
                                        $each->full_message = limit_words($each->message, 50);
                                    }
                                    $each->date_created = time_diff($each->date_created);
                                    $array[] = $each;
                                    
                                    $ikey++;
                                    if($ikey == 4) {
                                        break;
                                    }
                                }
                                // append to the list
                                $list[$key] = $array;
                            }

                        }

                        $records_count[$return_value] = $rows_count;
                    } catch(PDOException $e) {
                        $records_count[$return_value] = $e->getMessage();
                    }
                    
                }
            }
        }

        /** Increment the views count for the company notes */
        if(isset($params->param["company_notes_views_count"])) {
            // clean the parameter
            $item_id = xss_clean($params->param["company_notes_views_count"]);
            // update the database
            $this->db->query("UPDATE companies_notes SET views_count = (views_count+1) WHERE item_id = '{$item_id}' LIMIT 1");
            // return success
            return ["code" => 200, "data" => "Views count increased."];
        }

        /** Fetch the notifications list */
        if(isset($params->param["minimal_load"])) {
            return [
                "data" => [ 
                    "minimal_load" => $list
                ],
                "code" => 200 
            ];
        }

        /** Return the response */
        return [
            "data" => [ 
                "records_count" => $records_count,
                "notifications" => $list
            ],
            "code" => 200 
        ];
        
    }

    /**
     * Save a record
     * 
     * @param \stdClass $params
     * @param String $resource
     * @param String $resource_id
     * 
     * @return Array
     */
    public function save(stdClass $params) {
        
        // global variable
        global $noticeClass;

        // if the resource is in this array list
        if(in_array($params->resource, ["mark_notications_as_read", "mark_messages_as_read"])) {
            // end processing if the resource or resource id was not parsed
            if(!isset($params->resource) || !isset($params->resource_id)) {
                return;
            }

            // call the method to process the user request
            return $this->markAllAsRead($params->resource, $params->resource_id);
        }

        // confirm that the resource contains delete. as value
        $split = explode(".", $params->resource);
        
        // if the first item is delete
        if($split[0] == "delete") {
            // delete the record from the system
            return $this->delete_record($split[1], $params->resource_id, $params->userData);
        }

        // replace any hiphen
        $params->resource = str_ireplace("-", "_", $params->resource);

        // if the table is not found
        if(!isset($this->resource_tables[$params->resource])) {
            return ["code" => 203, "data" => "Access denied!"];
        }

        /** Confirm that all parameters have been parsed */
        try {

            // init
            $notice = "Status successfully changed.";

            // begin transaction
            $this->db->beginTransaction();

            // get the table name
            $table_name = $this->resource_tables[$params->resource]["table"];
            
            // if not in array list
            $table_name = rtrim($table_name, "a");

            // where clause
            $where_clause = $this->resource_tables[$params->resource]["query"];
            
            // load the resource information
            $query = "SELECT * FROM {$table_name} WHERE {$where_clause} LIMIT 1";
            $query = str_ireplace(["{{RECORD_ID}}", "{{USER_ID}}"], [$params->resource_id, $params->userId], $query);

            // execute the query
            $stmt = $this->db->prepare($query);
            $stmt->execute();

            // get the record
            $result = $stmt->fetch(PDO::FETCH_OBJ);

            //return false if no record was found
            if(empty($result)) {
                return ["code" => 203, "data" => "Access denied!"];
            }

            // Only the user who created it has the permission to make the request
            if(in_array($params->resource, ["complaints"])) {
                if($result->user_id !== $params->userId) {
                    return ["code" => 203, "data" => "Access denied!"];
                }
            }

            // if already sent
            if((isset($result->submit_status) && $result->submit_status == 'save') || (isset($result->status) && $result->status == 'Approved')) {
               return ["code" => 203, "data" => "Access denied!"];
            }

            // change the cancel policy status
            if(in_array($params->resource, ["cancel_record_request"])) {
                $this->db->query("UPDATE {$table_name} SET cancelled_by = '{$params->userId}', cancelled_date = now(), status ='Cancelled' WHERE slug = '{$params->resource_id}' LIMIT 1");
            }

            // approve the request to cancel policy
            elseif(in_array($params->resource, ["approve_cancel_record_request"])) {
                $this->db->query("UPDATE {$table_name} SET approved_by = '{$params->userId}', approved_date = now(), status ='Approved' WHERE slug = '{$params->resource_id}' LIMIT 1");   
            }

            // update the submit status for other items
            else {
                $this->db->query("UPDATE {$table_name} SET submit_status = 'save', date_submitted = now() WHERE item_id = '{$params->resource_id}' LIMIT 1");
            }

            // show no notification
            if(in_array($params->resource, ["cancel_record_request", "approve_cancel_record_request"])) {
                
                // get the policy id
                $record_id = explode("_", $params->resource_id)[1];
                $real_name = $result->record_type == "user_policy" ? "Policy" : "Advert";
                $data = ($params->resource == "cancel_record_request") ? "reversed the request to cancel the {$real_name}" : "approved the request to cancel the {$real_name}.";
                
                // log the user activity
                $this->userLogs($params->resource, $record_id, null, "<strong>{$params->userData->name}</strong> {$data}", $params->userId);
                
                // if approved then notify the user
                $notice_text = ($params->resource == "approve_cancel_record_request") ? 
                    "Your request to cancel the {$real_name} with ID: <strong>{$result->record_id}</strong> have been approved." : 
                    "Cancel {$real_name} request has successfully been reversed.";

                // change the policy status
                if(($params->resource == "approve_cancel_record_request")) {
                    // if the record type is policy
                    if($result->record_type == "user_policy") {
                        $this->db->query("UPDATE users_policy SET policy_status = 'Cancelled' WHERE policy_id = '{$result->record_id}' AND user_id = '{$result->user_id}' LIMIT 1");
                    } else {
                        $this->db->query("UPDATE adverts SET status = 'Cancelled' WHERE advert_id = '{$result->record_id}' AND user_id = '{$result->user_id}' LIMIT 1");
                    }
                }

                // notify the user
                $param = (object) [
                    '_item_id' => random_string("alnum", 32),
                    'user_id' => $result->user_id,
                    'subject' => "Cancel {$real_name}",
                    'username' => $params->userData->username,
                    'remote' => false, 
                    'message' => $notice_text,
                    'notice_type' => 6,
                    'userId' => $params->userId,
                    'initiated_by' => 'system'
                ];

                // add a new notification
                $noticeClass->add($param);

                // also notify the agent/broker who created this request to cancel record 
                if($result->user_id !== $result->created_by) {
                    // change the user id
                    $param->user_id = $result->created_by;
                    
                    // add a new notification
                    $noticeClass->add($param);
                }

                $notice = ($params->resource == "approve_cancel_record_request") ? "{$real_name} successfully cancelled." : "Cancel {$real_name} was successfully reverted.";

            } else {
                
                // if the resource is set
                if(isset($this->resource_parameters[$params->resource])) {
                    // key subject
                    $key = $this->resource_parameters[$params->resource]["key"];
                    $subject = $result->$key;

                    // log the user activity
                    $this->userLogs($params->resource, $params->resource_id, $result, "<strong>{$params->userData->name}</strong> changed the status from Draft to Sent.", $params->userId);
                    $this->userLogs($params->resource, $params->userId, $result, "You submitted your {$this->resource_parameters[$params->resource]["message"]} <strong>{$subject}.</strong>", $params->userId);

                    // form the notification parameters
                    $param = (object) [
                        '_item_id' => random_string("alnum", 32),
                        'user_id' => $params->userId,
                        'subject' => "Status Change",
                        'username' => $params->userData->username,
                        'remote' => false, 
                        'message' => "Your {$this->resource_parameters[$params->resource]["message"]} <strong>{$subject}</strong> have been submitted.",
                        'notice_type' => 10,
                        'userId' => $params->userId,
                        'initiated_by' => 'system'
                    ];

                    // add a new notification
                    $noticeClass->add($param);
                }

            }

            // commit the opened transaction
            $this->db->commit();

            // return the success response
            return [
                "code" => 200,
                "data" => $notice,
                "additional" => [
                    "reload" => "{$this->session->user_current_url}"
                ]
            ];

        } catch(PDOException $e) {
            $this->db->rollBack();
            return $e->getMessage();
        }

    }

    /**
     * Mark the resource data as read
     * 
     * @param String $resource
     * @param string $record_id
     * 
     * @return Array
     */
    public function markAllAsRead($resource, $resource_id) {
        
        // process the user request
        if($resource == "mark_notications_as_read") {
            
            try {
                // execute the query
                $stmt = $this->db->prepare("UPDATE users_notification SET confirmed = ? WHERE user_id = ? AND confirmed = ?");
                $stmt->execute([1, $resource_id, 0]);
            } catch(PDOException $e) {}

            // return success response
            return ["data" => "Notifications marked as read"];
        }

    }

    /**
     * Permission Control
     * 
     * @param String $resource
     * @param String $resource_id
     * @param stdClass $userData
     * 
     * @return Array
     */
    private function permission_control($resource, $record_id, $userData) {

        // split the resource id record
        $resource_id = isset($record_id[1]) ? $record_id[1] : $record_id[0];

        // append to the query
        $isCompany = (($record_id[0] == $userData->company_id) && (in_array($userData->user_type, ["insurance_company", "admin"]))) ? "" : " AND item_id='{$userData->company_id}'";
        $isUser = (!in_array($userData->user_type, ["insurance_company", "agent", "broker", "bancassurance", "bank"])) ? "" : " AND ((company_id='{$userData->company_id}') OR (created_by='{$userData->user_id}') OR (assigned_to='{$userData->user_id}'))";
        $isPermissible = (in_array($userData->user_type, ["broker", "agent", "business", "user"])) ? " AND ((user_id='{$userData->user_id}') OR (created_by='{$userData->created_by}'))" : " AND company_id='{$userData->company_id}'";
        $isCompany_2 = " AND (user_id = '{$userData->user_id}' OR company_id='{$userData->company_id}')";
        
        // the list of composite variable to return for each resource
        $resource_list = [
            "complaints" => [
                "table" => "users_complaints",
                "update" => "deleted='1'",
                "query" => "SELECT id FROM users_complaints WHERE item_id='{$resource_id}' AND deleted ='0' AND user_id='{$userData->user_id}' LIMIT 1"
            ],
            "users_claims" => [
                "update" => "status='Deleted'",
                "table" => "users_policy_claims",
                "query" => "SELECT id FROM users_policy_claims WHERE item_id='{$resource_id}' AND status !='Deleted' {$isPermissible} LIMIT 1"
            ],
            "users_policy" => [
                "table" => "users_policy",
                "update" => "policy_status='Deleted'",
                "query" => "SELECT id FROM users_policy WHERE item_id='{$resource_id}' AND policy_status !='Deleted' {$isPermissible} LIMIT 1"
            ],
            "users" => [
                "table" => "users",
                "update" => "deleted='1'",
                "query" => "SELECT id FROM users WHERE item_id='{$resource_id}' AND deleted='0' {$isUser} LIMIT 1"
            ],
            "policy_type" => [
                "table" => "policy_types",
                "update" => "deleted='1', status='0'",
                "query" => "SELECT id FROM policy_types WHERE item_id='{$resource_id}' AND deleted='0' {$isCompany} LIMIT 1"
            ],
            "report_request" => [
                "table" => "reports_request",
                "update" => "state='Cancelled'",
                "query" => "SELECT id FROM reports_request WHERE item_id='{$resource_id}' AND status='1' LIMIT 1"
            ],
            "company" => [
                "table" => "companies",
                "update" => "deleted='1'",
                "query" => "SELECT id FROM companies WHERE item_id='{$record_id[0]}' {$isCompany} AND deleted='0' LIMIT 1"
            ],            
            "adverts" => [
                "table" => "adverts",
                "update" => "status='Deleted'",
                "query" => "SELECT id FROM adverts WHERE item_id='{$record_id[0]}' AND submit_status='draft' AND deleted='0' {$isCompany_2} LIMIT 1"
            ],
            "company_notes" => [
                "table" => "companies_notes",
                "update" => "status='0'",
                "query" => "SELECT id FROM companies_notes WHERE item_id='{$record_id[0]}' AND company_id='{$userData->company_id}' AND status='1' LIMIT 1"
            ],
            "eduplus_category" => [
                "table" => "eduplus_category",
                "update" => "status='0'",
                "query" => "SELECT id FROM eduplus_category WHERE item_id='{$record_id[0]}' AND status='1' LIMIT 1"
            ],
            "eduplus_subject" => [
                "table" => "eduplus",
                "update" => "status='0'",
                "query" => "SELECT id FROM eduplus WHERE item_id='{$record_id[0]}' AND status='1' LIMIT 1"
            ],
            "company_award" => [
                "table" => "companies",
                "query" => "SELECT awards FROM companies WHERE item_id='{$record_id[0]}' {$isCompany} AND deleted='0' LIMIT 1"
            ],
            "company_manager" => [
                "table" => "companies",
                "query" => "SELECT managers FROM companies WHERE item_id='{$record_id[0]}' {$isCompany} AND deleted='0' LIMIT 1"
            ]
        ];

        return $resource_list[$resource] ?? null;

    }

    /**
     * Delete a record from the system
     * 
     * @param String $resource
     * @param String $resource_id
     * @param \stdClass $userData
     * 
     * @return Array
     */
    private function delete_record($resource, $resource_id, $userData) {

        // global variable
        global $accessObject;

        // set the resource id
        $split = explode("_", $resource_id);
        
        $code = 203;
        $additional = [];
        $data = "Error processing request!";

        // get the query to use
        $featured = $this->permission_control($resource, $split, $userData);

        // run the query
        if(!empty($featured)) {

            // try and catch all errors in the statement
            try {
                // perform the query
                $stmt = $this->db->prepare($featured["query"]);
                $stmt->execute();
                $result = $stmt->fetch(PDO::FETCH_OBJ);
            } catch(PDOException $e) {
                // quit the execution of the file
                return ;
            }

            // return if no result was found
            if(empty($result)) {
                return;
            }

            // convert to array if found
            foreach(["awards", "managers"] as $conv) {

                // if the item was found
                if(isset($result->$conv)) {

                    // convert the result into an array
                    $result->$conv = json_decode($result->$conv, true);

                    // set the deleted variable to true and return
                    $result->$conv[$split[1]]["is_deleted"] = 1;
                    $result->$conv[$split[1]]["is_deleted_date"] = date("l, jS F, Y h:iA");
                    $result->$conv[$split[1]]["is_deleted_user_id"] = $userData->user_id;
                    $result->$conv[$split[1]]["is_deleted_username"] = $userData->username;
                    
                    // update the database record
                    $this->db->query("UPDATE {$featured["table"]} SET {$conv}='".json_encode($result->$conv)."' WHERE item_id='{$split[0]}' LIMIT 1");
                    
                    // return the success response
                    $code = 200;
                    $data = "Record set successfully deleted";
                    $additional["remove"][] = $resource_id;
                }

            }

            // if the result is in this list
            if(in_array($resource, [
                "company_notes", "company", "users", "complaints", "report_request",
                "users_policy", "policy_type", "users_claims", "adverts", 
                "eduplus_category", "eduplus_subject"
            ])) {
                // update the database record
                $this->db->query("UPDATE {$featured["table"]} SET {$featured["update"]} WHERE item_id='{$split[0]}' LIMIT 1");
                
                /** Log the user activity */
                $this->userLogs("{$resource}", $split[0], null, "<strong>{$userData->name}</strong> deleted this record from the system.", $userData->user_id);

                // return the success response
                $code = 200;
                $data = "Record set successfully deleted";
                $additional["remove"][] = $resource_id;
            }

            // if a full result was found
            return [
                "code" => $code, "data" => $data, 
                "additional" => $additional
            ];
        }

    }

}
?>