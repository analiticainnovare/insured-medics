<?php 

class Adverts extends Medics {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Filters
     */
    public function list_filter($params, $prefix) {

        $filter = "1";
        $user_type = $params->userData->user_type;
        $filter .= isset($params->status) ? " AND {$prefix}.status='{$params->status}'" : null;
        $filter .= isset($params->user_id) ? " AND {$prefix}.user_id='{$params->user_id}'" : null;
        $filter .= isset($params->advert_id) ? " AND ({$prefix}.item_id='{$params->advert_id}' OR {$prefix}.advert_id='{$params->advert_id}')" : null;
        $filter .= isset($params->company_id) ? " AND {$prefix}.company_id='{$params->company_id}'" : null;

        // listing of adverts per companies / admin
        $filter .= in_array($user_type, ["developer"]) ? ""  : null;
        $filter .= in_array($user_type, ["user", "business", "broker", "agent", "bankassurance"]) ? " AND {$prefix}.status IN ('Running')"  : null;
        $filter .= in_array($user_type, ["admin"]) ? " AND {$prefix}.status NOT IN ('Deleted')"  : null;
        $filter .= in_array($user_type, ["insurance_company"]) ? " AND {$prefix}.status NOT IN ('Deleted') AND {$prefix}.company_id = '{$params->userData->company_id}'"  : null;
        $filter .= in_array($user_type, ["bank", "nic"]) ? " AND {$prefix}.status NOT IN ('Deleted') AND {$prefix}.created_by = '{$params->userData->user_type}'"  : null;
        $filter .= isset($params->where_clause) ? $params->where_clause : null;

        return $filter;
    }

    /**
     * List all adverts
     * 
     * @param stdClass $params
     * 
     * @return Array
     */
    public function list(stdClass $params) {

        global $accessObject;

        $params->show_button = isset($params->show_button) ? $params->show_button : true;

        $filter = $this->list_filter($params, "a");
        $filter_2 = $this->list_filter($params, "b");

        try {

            $updateAdvert = $accessObject->hasAccess("update", "adverts");
            $deleteAdvert = $accessObject->hasAccess("delete", "adverts");
            
            // query the db for the information
            $stmt = $this->db->prepare("
                SELECT a.*, a.item_id,
                    (SELECT SUM(b.budget) FROM adverts b WHERE {$filter_2} ORDER BY id DESC LIMIT {$params->limit}) AS total_budget,
                    (SELECT SUM(b.budget) FROM adverts b WHERE {$filter_2} AND b.paid_status = '1' ORDER BY id DESC LIMIT {$params->limit}) AS total_amount_paid,
                    (SELECT SUM(b.amount_payable) FROM adverts b WHERE {$filter_2} ORDER BY id DESC LIMIT {$params->limit}) AS total_amount_payable,
                    (SELECT name FROM users WHERE users.item_id = a.approved_by LIMIT 1) AS approved_by_name,
                    (SELECT b.id FROM users_items_cancellation_request b WHERE b.record_id = a.advert_id AND b.status='Pending' LIMIT 1) AS pending_cancel_request,
                    (SELECT CONCAT(name,'|',email,'|',contact,'|',address,'|',website,'|',logo) FROM companies WHERE companies.item_id = a.company_id LIMIT 1) AS company_info,
                    (SELECT CONCAT(name,'|',phone_number,'|',email,'|',image,'|',last_seen,'|',online,'|',user_type) FROM users WHERE users.item_id = a.user_id LIMIT 1) AS created_by_info,
                    (SELECT CONCAT(name,'|',phone_number,'|',email,'|',image,'|',last_seen,'|',online,'|',user_type) FROM users WHERE users.item_id = a.assigned_to LIMIT 1) AS assigned_to_info
                FROM adverts a
                WHERE {$filter} ORDER BY id DESC LIMIT {$params->limit}
            ");
            $stmt->execute();

            $row = 0;
            $data = [];

            // loop through the results list
            while($result = $stmt->fetch(PDO::FETCH_OBJ)) {

                $row++;
                $result->row_id = $row;

                // ad statistics
                $result->campaign_statistics = (object) [
                    "limits" => (object) [
                        "views_limit" => $result->views_limit,
                        "clicks_limit" => $result->clicks_limit
                    ],
                    "campaign_stats" => json_decode($result->statistics, true) ?? []
                ];

                // show this content if minimal load was not parsed
                if(!isset($params->minimal_load)) {
                
                    // loop through this array list
                    foreach(["related_to_details", "statistics"] as $eachItem) {
                        // if isset the value
                        if(isset($result->$eachItem)) {
                            // clean the data parsed
                            $result->$eachItem = json_decode($result->$eachItem);
                        }
                    }
                    
                    // convert data into an array string
                    $result->company_info = (object) $this->stringToArray($result->company_info, "|", ["name", "email", "phone_number", "address", "website", "logo"], true);
                    $result->created_by_info = (object) $this->stringToArray($result->created_by_info, "|", ["name", "phone_number", "email", "image","last_seen","online","user_type"]);
                    $result->assigned_to_info = (object) $this->stringToArray($result->assigned_to_info, "|", ["name", "phone_number", "email", "image","last_seen","online","user_type"]);

                    // super summary
                    $result->total_summary = (object) [
                        "total_budget" => $result->total_budget,
                        "total_amount_paid" => $result->total_amount_paid,
                        "total_amount_payable" => $result->total_amount_payable
                    ];

                    // status label
                    $result->status_label = $this->the_status_label($result->status);
                    
                    // other variables
                    $result->date_range = date("jS M", strtotime($result->start_date)) . " - ". date("jS M, Y", strtotime($result->end_date));

                    // if not show buttons
                    if($params->show_button) {

                        // if the user has the required permission to delete the add
                        $result->action = "";

                        // view advert details
                        $result->action .= " &nbsp; <a class='btn btn-outline-success p-1 m-0 modify-record btn-sm' href='{$this->baseUrl}adverts-view/{$result->item_id}' title='Click to view advert details'><i class='fa fa-eye'></i></a>";
                        
                        // if this user created the complaint and its still in a draft stage
                        if(($result->submit_status == "draft") && (($result->user_id == $params->userData->user_id) || ($result->company_id == $params->userData->company_id)) && $deleteAdvert) {
                            $result->action .= " &nbsp; <a class='btn btn-outline-primary p-1 m-0 modify-record btn-sm' href='javascript:void(0)' title='Click to update the ad'  onclick='return quick_form_load(\"post_advertisement\",\"{$result->item_id}\", \"adverts\");'><i class='fa fa-edit'></i></a>";
                            $result->action .= " &nbsp; <a class='btn btn-outline-danger p-1 m-0 modify-record btn-sm' href='javascript:void(0)' title='Click to delete this ad'  onclick='return delete_record(\"adverts\", \"{$result->item_id}\");'><i class='fa fa-trash-alt'></i></a>";
                        }

                    } else {
                        $result->action = "";
                    }

                    // unset the following keys
                    foreach(["total_budget", "seen_by", "total_amount_paid", "id", "total_amount_payable", "views_limit", "clicks_limit", "statistics"] as $eachItem) {
                        // if isset the value
                        unset($result->$eachItem);
                    }
                }

                // append to the array list
                $data[] = $result;
            }

            return [
                "data" => [
                    "ads_list" => $data
                ]
            ];

        } catch(PDOException $e) {
            return $e->getMessage();
        }

    }

    /**
     * Add new advert
     * 
     * Make some checks on the starting and ending dates of the campaign
     * Also check the button that was clicked by the user.
     * 
     * @param stdClass $params
     * 
     * @return Array
     */
    public function add(stdClass $params) {

        global $accessObject;

        /** Confirm the user has the permission to perform this action */
        if(!$accessObject->hasAccess("add", "adverts")) {
            return ["code" => 201, "data" => $this->permission_denied];
        }

        /** Validate the information */
        $item_id = random_string("alnum", 32);
        $advert_id = "AD".$this->append_zeros(($this->lastRowId("adverts") + 1), $this->append_zeros);
        $today = strtotime(date("Y-m-d"));

        /** Valid date check */
        if(!$this->validDate($params->start_date)) {
            return ["code" => 203, "data" => "Sorry! Please enter a valid start date."];
        }

        if(!$this->validDate($params->end_date)) {
            return ["code" => 203, "data" => "Sorry! Please enter a valid end date."];
        }

        /** Days counter */
        $days = count($this->listDays($params->start_date, $params->end_date));

        /** Ad objective */
        if(!in_array($params->ad_objective, array_keys($this->ad_objective_list))) {
            return ["code" => 203, "data" => "Sorry! An invalid campaign objective was parsed."];
        }

        /** The start date must not be less than today */
        if(strtotime($params->start_date) < $today) {
            return ["code" => 203, "data" => "Sorry! The campaign start date must be less than today's date."];
        }

        /** The end date must not be less than the start date */
        if(strtotime($params->end_date) < strtotime($params->start_date)) {
            return ["code" => 203, "data" => "Sorry! The campaign start date must be less than today's date."];
        }

        /** Submit button */
        $submit_status = isset($params->the_button) ? strtolower($params->the_button) : "save";
        $related_to_details = isset($params->related_to_id) ? load_class("complaints", "controllers")->related_to_info($params)["details"] : null;

        // set the upload directory
        $uploadDir = "assets/uploads/adverts/";

        // create the directory if the company id does not exist
        if(!is_dir($uploadDir)) {
            mkdir($uploadDir);
        }

        // if image was parsed
        if(isset($params->ad_image)) {

            // profile picture 
            $baseName = basename($params->ad_image["name"]); 
            $targetFilePath = $uploadDir . preg_replace("/[\s]/", "_", $baseName); 
            $fileType = strtolower(pathinfo($targetFilePath, PATHINFO_EXTENSION));

            // Allow certain file formats 
            $allowTypes = ['jpg', 'png', 'jpeg', 'gif', 'webp', 'pjpeg']; 

            // check if its a valid image
            if(!empty($baseName) && in_array($fileType, $allowTypes)){
                // set a new filename
                $fileName = $uploadDir . random_string("alnum", 55).".{$fileType}";

                // Upload file to the server 
                if(move_uploaded_file($params->ad_image["tmp_name"], $fileName)){ 
                    // append the image to the manager information
                    $ad_image = $fileName;
                }
            }

        }

        try {
            
            // begin the transaction
            $this->db->beginTransaction();

            // insert the record
            $stmt = $this->db->prepare("
                INSERT INTO adverts SET advert_title = ?, company_id = ?, user_id = ?, item_id = ?, budget = ?,
                start_date = ?, end_date = ?, days = ?, ad_context = ?, ad_objective = ?, ad_target = ?, 
                submit_status = ?, created_by = ?, advert_id = ?, seen_by='[\"NULL\"]'
                ".(isset($ad_image) ? ", image = '{$ad_image}'" : null)."
                ".(isset($params->related_to) ? ", related_to = '{$params->related_to}'" : null)."
                ".(isset($params->related_to_id) ? ", related_to_id = '{$params->related_to_id}'" : null)."
                ".(!empty($related_to_details) ? ", related_to_details = '{$related_to_details}'" : null)."
            ");
            $stmt->execute([
                $params->advert_title, $params->userData->company_id, $params->userId, $item_id, $params->budget, 
                $params->start_date, $params->end_date, $days, $params->ad_context, $params->ad_objective, 
                $params->ad_target, $submit_status, $params->userData->user_type, $advert_id
            ]);

            // other parameters
            $code = 200;
            $data = "Your advert was successfully ".(($submit_status == "save") ? "saved" : "posted.");

            // log the user activity
            $this->userLogs("adverts", $item_id, null, "<strong>{$params->userData->name}</strong> posted a new advert with the objective of: {$params->ad_objective}.", $params->userId);

            // commit the transaction
            $this->db->commit();
            
            # append to the response
			return [
                "code" => $code,
                "data" => $data,
                "additional" => [
                    "clear" => true,
                    "reload" => "{$this->baseUrl}adverts-view/{$item_id}",
                ]
            ];

        } catch(PDOException $e) {
            $this->db->rollBack();
            return $e->getMessage();
        }

    }
    
    /**
     * Update new advert
     * 
     * Make some checks on the starting and ending dates of the campaign
     * Also check the button that was clicked by the user.
     * 
     * @param stdClass $params
     * 
     * @return Array
     */
    public function update(stdClass $params) {

        global $accessObject;

        /** Confirm the user has the permission to perform this action */
        if(!$accessObject->hasAccess("update", "adverts")) {
            return ["code" => 201, "data" => $this->permission_denied];
        }

        $reviewAdvert = (bool) $accessObject->hasAccess("approve", "adverts");

        /** Validate the information */
        $item_id = $params->advert_id;
        $today = strtotime(date("Y-m-d"));

        /** Confirm that the user_id is valid */
        $i_params = (object) [
            "limit" => 1, 
            "show_button" => false,
            "advert_id" => $item_id, 
            "userData" => $params->userData
        ];
        $the_advert = load_class("adverts", "controllers")->list($i_params)["data"]["ads_list"];

        /** if a record was found */
        if(empty($the_advert)) {
            return ["code" => 201, "data" => $this->permission_denied];
        }

        /** Assign the manager's data */
        $adverts_data = $the_advert[0];

        /** Valid date check */
        if(isset($params->start_date) && !$this->validDate($params->start_date)) {
            return ["code" => 203, "data" => "Sorry! Please enter a valid start date."];
        }

        if(isset($params->end_date) && !$this->validDate($params->end_date)) {
            return ["code" => 203, "data" => "Sorry! Please enter a valid end date."];
        }

        /** Ad objective */
        if(isset($params->ad_objective) && !in_array($params->ad_objective, array_keys($this->ad_objective_list))) {
            return ["code" => 203, "data" => "Sorry! An invalid campaign objective was parsed."];
        }

        /** Days counter */
        if(isset($params->end_date, $params->end_date)) {
            $days = count($this->listDays($params->start_date, $params->end_date));
        }

        /** The end date must not be less than the start date */
        if(isset($params->end_date, $params->end_date) && (strtotime($params->end_date) < strtotime($params->start_date))) {
            return ["code" => 203, "data" => "Sorry! The campaign start date must be less than today's date."];
        }
        
        // save status
        $submit_status = isset($params->the_button) ? strtolower($params->the_button) : "save";
        $related_to_details = isset($params->related_to_id) ? load_class("complaints", "controllers")->related_to_info($params)["details"] : null;

        // set the upload directory
        $uploadDir = "assets/uploads/adverts/";

        // create the directory if the company id does not exist
        if(!is_dir($uploadDir)) {
            mkdir($uploadDir);
        }

        // if image was parsed
        if(isset($params->ad_image)) {

            // profile picture 
            $baseName = basename($params->ad_image["name"]); 
            $targetFilePath = $uploadDir . preg_replace("/[\s]/", "_", $baseName); 
            $fileType = strtolower(pathinfo($targetFilePath, PATHINFO_EXTENSION));

            // Allow certain file formats 
            $allowTypes = ['jpg', 'png', 'jpeg', 'gif', 'webp', 'pjpeg']; 

            // check if its a valid image
            if(!empty($baseName) && in_array($fileType, $allowTypes)){
                // set a new filename
                $fileName = $uploadDir . random_string("alnum", 55).".{$fileType}";

                // Upload file to the server 
                if(move_uploaded_file($params->ad_image["tmp_name"], $fileName)){ 
                    // append the image to the manager information
                    $ad_image = $fileName;
                }
            }

        }

        // modify the return log
        $return_log = [
            "code" => 200,
            "data" => "The advert has been successfully updated!",
            "additional" => []
        ];

        try {
            
            // begin the transaction
            $this->db->beginTransaction();

            // insert the record
            $stmt = $this->db->prepare("
                UPDATE adverts SET submit_status = ?
                    ".(isset($days) ? ", days = '{$days}'" : null)."
                    ".(isset($ad_image) ? ", image = '{$ad_image}'" : null)."
                    ".(isset($params->budget) ? ", budget = '{$params->budget}'" : null)."
                    ".(isset($params->views_limit) ? ", views_limit = '{$params->views_limit}'" : null)."
                    ".(isset($params->clicks_limit) ? ", clicks_limit = '{$params->clicks_limit}'" : null)."
                    ".(isset($params->start_date) ? ", start_date = '{$params->start_date}'" : null)."
                    ".(isset($params->amount_payable) ? ", amount_payable = '{$params->amount_payable}'" : null)."
                    ".(isset($params->end_date) ? ", end_date = '{$params->end_date}'" : null)."
                    ".(isset($params->ad_target) ? ", ad_target = '{$params->ad_target}'" : null)."
                    ".((isset($params->assigned_to) && $reviewAdvert) ? ", assigned_to = '{$params->assigned_to}'" : null)."
                    ".((isset($params->status) && $reviewAdvert) ? ", status = '{$params->status}'" : null)."
                    ".(isset($params->related_to) ? ", related_to = '{$params->related_to}'" : null)."
                    ".(isset($params->ad_context) ? ", ad_context = '{$params->ad_context}'" : null)."
                    ".(isset($params->advert_title) ? ", advert_title = '{$params->advert_title}'" : null)."
                    ".(isset($params->ad_objective) ? ", ad_objective = '{$params->ad_objective}'" : null)."
                    ".(isset($params->related_to_id) ? ", related_to_id = '{$params->related_to_id}'" : null)."
                    ".(!empty($related_to_details) ? ", related_to_details = '{$related_to_details}'" : null)."
                WHERE item_id = ? LIMIT 1
            ");
            $stmt->execute([$submit_status, $item_id]);

            // other parameters
            $code = 200;
            $data = "Your advert was successfully updated";

            // log the user activity
            $this->userLogs("adverts", $item_id, $adverts_data, "<strong>{$params->userData->name}</strong> updated the advert campaign record with the objective.", $params->userId);

            // save the log for the status change
            if(isset($params->status) && ($adverts_data->status !== $params->status)) {
                // set the return log
                $return_log["additional"]["reload"] = "{$this->baseUrl}adverts-view/{$item_id}";
                // save the status change information
                $this->userLogs("adverts", $item_id, $adverts_data->status, "<strong>Advert Status</strong> was changed from <strong>{$adverts_data->status}</strong> to <strong>{$params->status}</strong>.", $params->userId);
                // alert the user of a status change
                
            }

            // save the log for the advert start date
            if(isset($params->start_date) && ($adverts_data->start_date !== $params->start_date)) {
                // set the return log
                $return_log["additional"]["reload"] = "{$this->baseUrl}adverts-view/{$item_id}";
                // save the status change information
                $this->userLogs("adverts", $item_id, $adverts_data->start_date, "<strong>Start Date</strong> was changed from <strong>{$adverts_data->start_date}</strong> to <strong>{$params->start_date}</strong>.", $params->userId);
            }

            // save the log for the advert Views
            if(isset($params->views_limit) && ($adverts_data->campaign_statistics->limits->views_limit !== $params->views_limit)) {
                // save the status change information
                $this->userLogs("adverts", $item_id, $adverts_data->campaign_statistics->limits->views_limit, "<strong>Views Limit</strong> was changed from <strong>{$adverts_data->campaign_statistics->limits->views_limit}</strong> to <strong>{$params->views_limit}</strong>.", $params->userId);
            }

            // save the log for the advert Clicks
            if(isset($params->clicks_limit) && ($adverts_data->campaign_statistics->limits->clicks_limit !== $params->clicks_limit)) {
                // save the status change information
                $this->userLogs("adverts", $item_id, $adverts_data->campaign_statistics->limits->clicks_limit, "<strong>Clicks Limit</strong> was changed from <strong>{$adverts_data->campaign_statistics->limits->clicks_limit}</strong> to <strong>{$params->clicks_limit}</strong>.", $params->userId);
            }

            // save the log for the advert Views
            if(isset($params->amount_payable) && ($adverts_data->amount_payable !== $params->amount_payable)) {
                // update the checkout created
                $this->db->query("UPDATE users_payments SET amount='{$params->amount_payable}' WHERE record_type='adverts' AND record_id='{$item_id}'");

                // save the status change information
                $this->userLogs("adverts", $item_id, $adverts_data->amount_payable, "<strong>Amount Payable</strong> was changed from <strong>{$adverts_data->amount_payable}</strong> to <strong>{$params->amount_payable}</strong>.", $params->userId);
            }

            // save the log for the advert start date
            if(isset($params->end_date) && ($adverts_data->end_date !== $params->end_date)) {
                // set the return log
                $return_log["additional"]["reload"] = "{$this->baseUrl}adverts-view/{$item_id}";
                // save the status change information
                $this->userLogs("adverts", $item_id, $adverts_data->end_date, "<strong>Start Date</strong> was changed from <strong>{$adverts_data->end_date}</strong> to <strong>{$params->end_date}</strong>.", $params->userId);
            }

            // commit the transaction
            $this->db->commit();
            
            # append to the response
			return $return_log;

        } catch(PDOException $e) {
            $this->db->rollBack();
            return $e->getMessage();
        }

    }

    /**
     * List the cancel adverts requests list
     * 
     * @param \stdClass $params
     * @param String    $params->advert_id          The id of the advert to load the cancel requests list
     */
    public function cancel_list(stdClass $params) {
        // append the advert_id as record_id to the params object
        $params->record_id = $params->advert_id;

        // call the cancel record list method
        return $this->record_cancel_list($params, "adverts");
    }

    /**
     * Request for policy cancellation
     * 
     * @param \stdClass $params
     * @param String    $params->advert_id          The id of the policy to cancel
     * @param String    $params->reason             This is the reason for cancelling the policy
     * 
     * @return Array
     */
    public function cancel(stdClass $params) {

        /** Load the policy information */
        $para = (object) [
            "limit" => 1,
            "show_button" => false,
            "userData" => $params->userData,
            "advert_id" => $params->advert_id
        ];
        $advertCheck = $this->list($para);

        // if the policy check is not empty
        if(empty($advertCheck["data"]["ads_list"])) {
            return ["data" => "Sorry! You are not permitted to perform this action."];
        }

        // check if a cancellation request has already been placed
        $cancelCheck = $this->pushQuery("id", "users_items_cancellation_request", "record_id='{$params->advert_id}' AND status = 'Pending' LIMIT 1");
        
        // if not empty then echo an error
        if(!empty($cancelCheck)) {
            return ["data" => "Sorry! There is a pending cancellation request on this advert."];
        }

        // policy data
        $advertData = $advertCheck["data"]["ads_list"][0];
        $params->reason = nl2br($params->reason);

        $slug = random_string("alnum", 5)."_{$advertData->item_id}";

        // format the policy information
        $advert_details = [
            "item_id" => $advertData->item_id, 
            "advert_id" => $advertData->advert_id, "advert_title" => $advertData->advert_title,
            "start_date" => $advertData->start_date, "end_date" => $advertData->end_date,
            "company_info" => $advertData->company_info, "campaign_statistics" => $advertData->campaign_statistics,
            "ad_context" => $advertData->ad_context, "ad_target" => $advertData->ad_target,
            "budget" => $advertData->budget, "image" => $advertData->image
        ];

        // global variable
        global $noticeClass;

        try {

            $stmt = $this->db->prepare("
                INSERT INTO users_items_cancellation_request SET slug = ?, item_id = ?, record_id = ?, 
                    user_id = ?, company_id = ?,  created_by = ?, record_details = ?, reason = ?, record_type = ?
            ");
            $stmt->execute([
                $slug, $advertData->item_id, $params->advert_id, $advertData->user_id, $advertData->company_id, 
                $params->userId, json_encode($advert_details), $params->reason, "advert"
            ]);

            // form the notification parameters
            $notice_param = (object) [
                '_item_id' => random_string("alnum", 32),
                'user_id' => $advertData->user_id,
                'subject' => "Ad Campaign",
                'username' => $params->userData->username,
                'remote' => false, 
                'message' => "<strong>{$params->userData->name}</strong> has made a request to cancel your Ad Campaign with ID: <strong>{$params->advert_id}</strong>. You will be notified once the request is approved.",
                'notice_type' => 6,
                'userId' => $params->userId,
                'initiated_by' => 'system'
            ];
            
            // if the cancellation request was done on behalf of the client
            if($advertData->user_id !== $params->userId) {                
                // add a new notification
                $noticeClass->add($notice_param);
                
                // set the user id
                $notice_param->user_id = $params->userId;
                $notice_param->message = "You made a request to cancel the Ad Campaign with ID: <strong>{$params->advert_id}</strong>. You will be notified once the request is approved.";
                $noticeClass->add($notice_param);
            }
            
            // add a new notification
            $noticeClass->add($notice_param);

            // return the response
            return [
                "code" => 200,
                "data" => "Your request to cancel your adverts has been placed.",
                "additional" => [
                    "clear" => true,
                    "reload" => "{$this->baseUrl}adverts-view/{$advertData->item_id}"
                ]
            ];

        } catch(PDOException $e) {
            return $e->getMessage();
        }


    }

    /**
     * Show the notification to display
     * One at a time with its function to manage the notification
     * 
     * @param stdClass $userData
     * 
     * @return Object
     */
    public function display(stdClass $userData) {
        
        // query the parameter
        $today = date("Y-m-d");
        $param = (object) [
            "limit" => 1,
            "minimal_load" => true,
            "show_button" => false,
            "where_clause" => " AND DATE(a.end_date) >= '{$today}' AND (a.seen_by NOT LIKE '%{$userData->user_id}%')",
            "userData" => $userData,
        ];

        // get the new information
        $data = $this->list($param);
        
        if(empty($data["data"]["ads_list"])) {
            return [];
        }
        $data = $data["data"]["ads_list"][0];

        $data->modal_function = "advertHandler";
        $data->modal = "
        <div class=\"modal advertModal_{$data->item_id} fade\" data-backdrop=\"static\" data-keyboard=\"false\" id=\"advertModal_{$data->item_id}\" data-advert-id=\"{$data->item_id}\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">
            <div class=\"modal-dialog\" role=\"document\">
                <div class=\"modal-content\">
                    <div class=\"modal-header\">
                        <h4>{$data->advert_title}</h4>
                    </div>
                    <div class=\"modal-body\">
                        <div class=\"notice\">
                            <img src=\"{$this->baseUrl}{$data->image}\" style=\"width:100%;max-height:400px;\">
                        </div>
                    </div>
                    <div class=\"modal-footer text-right\">
                        <button data-advert-id=\"{$data->item_id}\" type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                    </div>
                </div>
            </div>
        </div>";

        // append the function
        $data->modal_function_script = $this->popup_function("advert", "advertHandler", $data->item_id, $userData->user_id);

        return $data;

    }

    /**
     * Set the user as having viewed the advert
     * 
     * @param String $param->advert_id
     * 
     * @return Array
     */
    public function viewed(stdClass $params) {
        
        // query the parameter
        $today = date("Y-m-d");
        $param = (object) [
            "limit" => 1,
            "minimal_load" => true,
            "show_button" => false,
            "show_seenby" => true,
            "advert_id" => $params->advert_id,
            "userData" => $params->userData,
        ];

        // get the new information
        $data = $this->list($param);
        
        // confirm if the ads id is not empty
        if(empty($data["data"]["ads_list"])) {
            return ["code" => 203, "data" => "An invalid ad id was submitted for processing."];
        }
        $data = $data["data"]["ads_list"][0];

        // seen by information
        $seen_by_list = json_decode($data->seen_by, true);
        
        // append to the list
        if(!in_array($params->userId, $seen_by_list)) {

            // append to the array list
            array_push($seen_by_list, $params->userId);

            // append the clicks and views
            $clicks = isset($data->campaign_statistics->campaign_stats->clicks) ? $data->campaign_statistics->campaign_stats->clicks + 1 : 1;
            $views = isset($data->campaign_statistics->campaign_stats->views) ? $data->campaign_statistics->campaign_stats->views + 1 : 1;

            $statistics = [
                "clicks" => $clicks,
                "views" => [
                    "unique_views" => $views
                ]
            ];

            // update the statistics information
            $stmt = $this->db->prepare("UPDATE adverts SET statistics = ?, seen_by = ? WHERE item_id = ? LIMIT 1");
            $stmt->execute([json_encode($statistics), json_encode($seen_by_list), $params->advert_id]);

            // viewed list
            $viewed_list = [
                "{$params->userId}" => [
                    "fullname" => $params->userData->name,
                    "email" => $params->userData->email,
                    "viewed_date" => date("Y-m-d h:iA"),
                    "useragent" => "{$this->browser}|{$this->platform}|{$this->ip_address}"
                ]
            ];

            // if the page statistics is empty
            if(empty($data->statistics)) {
                $stmt = $this->db->prepare("INSERT INTO adverts_log SET advert_id = ?, views_list = ?");
                $stmt->execute([$params->advert_id, json_encode($viewed_list)]);
            }
            // update the list of viewed users
            else {
                // get the record
                $record = $this->pushQuery("views_list, clicks_list", "adverts_log", "advert_id='{$params->advert_id}' LIMIT 1");
                $existing_viewed_list = json_decode($record[0]->views_list, true);

                array_push($existing_viewed_list, $viewed_list);
                $stmt = $this->db->prepare("UPDATE INTO adverts_log SET views_list = ? WHERE advert_id = ? LIMIT 1");
                $stmt->execute([json_encode($viewed_list), $params->advert_id]);
            }
        }        

        return [
            "data" => "Ad statistics successfully recorded."
        ];

    }

}