<?php
/**
 * This class will hold all variables
 */
class Models {

    /** Set the variables */
    public $form_modules;
    public $favicon_array;
    public $resource_tables;
    public $resource_parameters;
    public $permission_denied;
    public $global_limit = 2000;
    public $formPreloader;
    public $the_user_roles;
    public $underscores = "____";

    /** This is used for generation of user ids */
	public $append_zeros = 5;

    /** This is the allowed number of hours which a user can delete an object */
    public $allowed_delete_range = 3;

    /** The allowed maximum attachment size in megabytes */
	public $max_attachment_size = 25;
    public $accepted_attachment_file_types;

    // instantiate the class
    public function __construct() {

        $this->form_modules = [
            "apply_license" => "Apply for License",
            "apply_policy" => "Apply for an Insurance Policy",
            "load_policy_form" => "Insurance Policy Form",
            "load_claims_form" => "Insurance Claims Form",
            "user_account" => "Create User Account",
            "new_proposal" => "Create a Proposal",
            "policy_payment_history" => "Policy Premium Payment History",
            "update_insurance_policy" => "Update Insurance Policy",
            "update_claims" => "Update Claim Request",
            "lodge_complaint" => "Lodge Complaint",
            "make_claim" => "Make a Claim",
            "company_manager" => "Modify Company Managers",
            "company_award" => "Modify Company Awards",
            "manage_company_information" => "Modify Company Information",
            "post_advertisement" => "Ads Management",
            "preview_form" => "Form Preview",
            "company_basic_information" => "Insurance Company Information",
            "policy_basic_information" => "Insurance Policy Details",
			"share_a_reply" => "Share a Reply",
			"view_replies_list" => "Property Feedback Thread",
			"activity_history" => "Activity Logs Thread",
            "preview_file_attachment" => "Preview Attached File",
            "user_notifications" => "User Notifications",
            "user_basic_information" => "User Account Information",
            "ajax_forms_loader" => "Ajax Form HTML",
            "post_announcement" => "Share Announcement",
            "license_application" => "License Renewal Application",
            "request_report" => "Request for Report",
            "eduplus_category" => "InsureEdu Plus: Category",
            "eduplus_subject" => "InsureEdu Plus: Topic"
        ];

		$this->favicon_array = [
			'jpg' => 'fa fa-file-image', 'png' => 'fa fa-file-image',
			'jpeg' => 'fa fa-file-image', 'gif' => 'fa fa-file-image',
            'pjpeg' => 'fa fa-file-image', 'webp' => 'fa fa-file-image',
			'pdf' => 'fa fa-file-pdf', 'doc' => 'fa fa-file-word',
			'docx' => 'fa fa-file-word', 'mp3' => 'fa fa-file-audio',
			'mpeg' => 'fa fa-file-video', 'mpg' => 'fa fa-file-video',
			'mov' => 'fa fa-file-video', 'movie' => 'fa fa-file-video',
			'webm' => 'fa fa-file-video', 'flv' => 'fa fa-file-video',
			'qt' => 'fa fa-file-video', 'zip' => 'fa fa-archive',
			'txt' => 'fa fa-file-alt', 'csv' => 'fa fa-file-csv',
			'rtf' => 'fa fa-file-alt', 'xls' => 'fa fa-file-excel',
			'xlsx' => 'fa fa-file-excel', 'php' => 'fa fa-file-alt',
			'css' => 'fa fa-file-alt', 'ppt' => 'fa fa-file-powerpoint',
			'pptx' => 'fa fa-file-powerpoint', 'sql' => 'fa fa-file-alt',
			'flv' => 'fa fa-file-video', 'json' => 'fa fa-file-alt', 
            'mp4' => 'fa fa-file-alt'
		];

		$this->accepted_attachment_file_types = [
			'jpg', 'png', 'jpeg', 'txt', 'pdf', 'sql', 'docx', 'doc', 'xls', 'xlsx', 'mpeg',
			'ppt', 'pptx', 'php', 'css', 'csv', 'rtf', 'gif', 'pub', 'json', 'zip', 
			'mpg', 'flv', 'webm', 'movie', 'mov', 'qt', 'pjpeg', 'webp', 'mp4'
		];

        $this->resource_parameters = [
            "company_policy" => [
                "table" => "policy_types",
                "message" => "Insurance Policy",
                "key" => "name",
            ],
            "complaint_comments" => [
                "table" => "users_complaints",
                "message" => "Complaint",
                "key" => "subject",
            ],
            "complaints" => [
                "table" => "users_complaints",
                "message" => "Complaint",
                "key" => "subject",
            ],
            "claims" => [
                "table" => "users_policy_claims",
                "message" => "Policy Claim",
                "key" => "policy_id"
            ],
            "policies" => [
                "table" => "users_policy",
                "message" => "Policy",
                "key" => "policy_name"
            ],
            "user_policy" => [
                "table" => "users_policy",
                "message" => "Policy",
                "key" => "policy_name"
            ],
            "licenses" => [
                "table" => "companies_licenses",
                "message" => "License Application",
                "key" => ""
            ],
            "payments" => [
                "table" => "users_policy_payment",
                "message" => "Payment Checkout",
                "key" => ""
            ],
            "announcements" => [
                "table" => "announcements",
                "message" => "Announcement",
                "key" => ""
            ],
            "insurance_company" => [
                "table" => "companies",
                "message" => "",
                "key" => ""
            ],
            "license_application" => [
                "table" => "companies_licenses",
                "message" => "License Renewal application",
                "key" => "license_id"
            ]
        ];

        $this->formPreloader = [
            "users" => [
                "firstname" => "Firstname",
                "lastname" => "Lastname",
                "othername" => "Othernames",
                "name" => "Fullname",
                "date_of_birth" => "Date of Birth",
                "gender" => "Gender",
                "email" => "Email Address",
                "phone_number" => "Primary Contact",
                "phone_number_2" => "Secondary Contact",
                "address" => "Postal Address",
                "residence" => "Residential Address",
                "employer" => "Employer Name",
                "occupation" => "Occupation"
            ],
            "users_policy" => [
                "policy_id" => "Policy ID",
                "policy_start_date" => "Policy Enrollment Date"
            ]
        ];

        $this->fake_files = [
            "files" => [],
            "files_count" => 0,
            "files_size" => 0,
            "raw_size_mb" => 0
        ];

        $this->ad_objective_list = [
            "GENERAL_ADS" => "General Ads",
            "POLICY_VIEWS" => "Policy Views",
            "PAGE_VIEWS" => "Page Views"
        ];

        // permissive users to be created by each access level
        $this->the_user_roles_long = [
            "insurance_company" => [
                "_role_title" => "Insurance Company Admin"
            ],
            "user" => [
                "_role_title" => "Client"
            ],
            "business" => [
                "_role_title" => "Business Client"
            ],
            "agent" => [
                "_role_title" => "Insurance Agent"
            ],
            "broker" => [
                "_role_title" => "Insurance Broker"
            ],
            "bancassurance" => [
                "_role_title" => "Bank Assurance Agent"
            ],
            "bank" => [
                "_role_title" => "Banks"
            ],
            "nic" => [
                "_role_title" => "National Insurance Company"
            ],
            "admin" => [
                "_role_title" => "Admin User"
            ]
        ];

        $this->the_user_roles = [
            "insurance_company" => [
                "_role_title" => "Insurance Company Admin"
            ],
            "user" => [
                "_role_title" => "Client"
            ],
            "business" => [
                "_role_title" => "Business Client"
            ],
            "agent" => [
                "_role_title" => "Agent"
            ],
            "broker" => [
                "_role_title" => "Broker"
            ],
            "bancassurance" => [
                "_role_title" => "Bank Assurance Agent"
            ],
            "bank" => [
                "_role_title" => "Banks"
            ],
            "nic" => [
                "_role_title" => "NIC"
            ],
            "admin" => [
                "_role_title" => "Admin User"
            ]
        ];

        $this->payment_periods = [
            "weekly" => "Weekly",
            "bi-weekly" => "Bi-Weekly",
            "monthly" => "Monthly",
            "quarterly" => "Quarterly"
        ];

		$this->permission_denied = "Sorry! You do not have the required permission to perform this action.";
        $this->unexpected_error = ["code" => 203, "data" => "Sorry! An unexpected error occured.\nPlease contact the admin if problem persists"];
    }
    
    /**
     * User Permissions List
     * 
     * @param String $user_type 
     * 
     * @return Array
     */
    public function permissions_list($user_type) {

		// permissive users to be created by each access level
        $permissions = [
            "insurance_company" => [
                "user" => "Client",
                "business" => "Business Client",
                "agent" => "Insurance Agent",
                "broker" => "Insurance Broker",
                "insurance_company" => "Employee"
            ],
            "agent" => [
                "user" => "Client User",
                "business" => "Business Firm",
            ],
            "broker" => [
                "user" => "Client User",
                "business" => "Business Firm",
            ],
            "bank" => [
                "bancassurance" => "Bancassurance Agent",
                "bank" => "Admin User",
            ],
            "nic" => [
                "nic" => "Admin User",
            ],
            "admin" => [
                "user" => "Client",
                "business" => "Business Client",
                "agent" => "Insurance Agent",
                "broker" => "Insurance Broker",
                "bancassurance" => "Bancassurance Agent",
                "bank" => "Bank Employee",
                "insurance_company" => "Insurance Company Employee",
                "nic" => "NIC Admin User",
                "admin" => "Admin User",
            ]
        ];

		// return the array values
        return $permissions[$user_type] ?? [];
    }

}
?>