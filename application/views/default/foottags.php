        <?php
        // load some global variables
        global $medicsClass, $baseUrl, $session, $userData, $current_url, $loadedJS, $auto_close_modal, $userPrefs, $announcementNotice;
        // init indexdb variable
        $idb_init = (bool) (isset($userPrefs->idb_init->init) && (strtotime($userPrefs->idb_init->idb_next_init) < time()));
        // if the force_cold_boot=1 query parameter has been parsed then set the init to true
        if(isset($_GET["force_cold_boot"]) && ($_GET["force_cold_boot"] == 1)) {
            $idb_init = true;
        }
        ?>
        <!-- <div class="add_new-buttddon"><? //= add_buttons() ?></div> -->
        <footer class="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
            <p class="text-muted text-center text-md-left">Copyright &copy; <?= date("Y") ?> <a href="<?= $baseUrl; ?>"><?= $appName ?></a>. All rights reserved</p>
            <p class="text-muted text-center text-md-left mb-0 d-none d-md-block">Handcrafted With <i class="mb-1 text-primary ml-1 icon-small" data-feather="heart"></i></p>
        </footer>
        <?php if(isset($isCommission_Permitted) && $isCommission_Permitted) { ?>
        <div class="modal fade" id="commissionModal" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-md modal-dialog-top" style="width:100%;" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Manage Commissions Payment</h5>
                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                    </div>
                    <form class="app-data-form" action="<?= $baseUrl ?>api/company/manage_commission" method="POST">
                        <div class="modal-body">
                        <?= form_overlay() ?>
                            <?php 
                            // assign values
                            $in_period = $companyPrefs->commission->period ?? $cUserData->commission_period;
                            $base_salary = $companyPrefs->commission->base_salary ?? $cUserData->base_salary;
                            $percentage = $companyPrefs->commission->percentage ?? $cUserData->commission_percentage;
                            ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="grid-margin">
                                        <div class="card rounded">
                                            <div class="card-body pb-2 pr-2 pl-2">
                                                <div class="row pl-3 pr-2 mb-1 justify-content-between">
                                                    <div><h6 class="card-title mb-1 pb-2">Configure Configuration</h6></div>
                                                    <div><span class="mr-2 cursor text-primary" data-toggle="tooltip" title="These settings affects only new users to be created unless the apply to all field is checked."><i class="fa fa-info"></i></span></div>
                                                </div>
                                                <div>
                                                    <div class="form-group">
                                                        <label for="base_salary">Base Salary <span class="text-primary cursor font-12px" data-toggle="tooltip" title="Can be changed on individual users page"><i class="fa fa-info"></i></span></label>
                                                        <input type="number" value="<?= $base_salary ?>" max="10000" name="data[base_salary]" id="base_salary" placeholder="Base salary for Brokers/Agents" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="commission_percentage">Percentage on Premium <span class="text-primary cursor font-12px" data-toggle="tooltip" title="Can be changed on individual users page"><i class="fa fa-info"></i></span></label>
                                                        <input type="number" min="1" max="100" value="<?= $percentage ?>" name="data[percentage]" id="commission_percentage" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="period">Period for Calculation</label>
                                                        <select name="data[period]" id="period" class="form-control selectpicker" data-width="100%">
                                                            <option value="">Select Payment Period</option>
                                                            <?php foreach($medicsClass->payment_periods as $value => $period) { ?>
                                                                <option <?= ($in_period === $value) ? "selected" : ""; ?> value="<?= $value; ?>"><?= $period; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <?php if(!isset($cUserData)) { ?>
                                                    <div class="form-group">
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="apply_to_all" name="data[apply_to_all]">
                                                                <label class="custom-control-label cursor" for="apply_to_all">Apply to all existing users</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <?php if(isset($cUserData)) { ?>
                            <input type="hidden" name="data[user_id]" value="<?= base64_encode($cUserData->user_id) ?>" readonly>
                            <?php } ?>
                            <input type="hidden" name="data[company_id]" value="<?= base64_encode($userData->company_id) ?>" readonly>
                            <button type="submit" class="btn share-post btn-outline-success">Save</button>
                            <button class="btn cancel-post btn-outline-danger" data-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
    <div class="email-notification" style="display:none">
        <div class="d-flex row justify-content-between">
            <div class="content"></div>
            <div><i class="fa font-18px fa-times-circle"></i></div>
        </div>
    </div>
    <div class="report_generation_controller" data-reriod="<?= $userPrefs->reports->period ?>"></div>
    <?php // below variables can be found in modal_helper.php ?>
    <?= ajax_forms_modal($auto_close_modal); ?>
    <?= replies_modal($auto_close_modal); ?>
    <?= user_preferences_modal($auto_close_modal, $userData->user_type); ?>
    <?= resource_information_modal(); ?>
    <?= ajax_form_button(); ?>
    <?php if(in_array($SITEURL[0], ["policies", "claims"])) { ?>
        <?= discard_form(); ?>
    <?php } ?>
    <?= general_modal(); ?>
    <?= save_form_data(); ?>
    <div id="announcement_preview"></div>

</div>
    <?php if($idb_init) { ?>
    <button id="idb_init" class="btn btn-outline-success btn-sm" hidden></button>
    <?php } ?>
    <?= $announcementNotice->content ?? null ?>
    <?= $advertHandler->modal ?? null ?>
    <script>
    var fieldDefault = {}, thisRowId = 1, thisSelectRow = 1, userAgent = "<?= $medicsClass->agent."||".$medicsClass->platform."||".$medicsClass->browser."||".ip_address(); ?>",
        baseUrl = "<?= $baseUrl ?>",current_url="<?= $user_current_url ?>",
        viewedAs = "<?= (bool) isset($_GET["viewas"]) ?>",
        this_user_unique_key = "persist:imp-client-<?= $session->userId; ?>",
        form_modules = <?= json_encode($medicsClass->form_modules); ?>,
        <?= isset($companyData->awards) ? "company_awards_array=".json_encode($companyData->awards)."," : ""; ?>
        <?= isset($companyData->managers) ? "company_managers_array=".json_encode($companyData->managers)."," : ""; ?>
        <?php if(isset($medicsClass->formPreloader) && in_array($SITEURL[0], ["policy-view", "policy-claim-form"])) { ?>formPreloader = <?= json_encode($medicsClass->formPreloader); ?>,<?php } ?>
        $myPrefs = <?= json_encode($userPrefs); ?>;
    </script>
    <script src="<?= $baseUrl; ?>assets/vendors/core/core.js"></script>
    <script src="<?= $baseUrl; ?>assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="<?= $baseUrl; ?>assets/vendors/sweetalert2/sweetalert2.min.js"></script>
    <script src="<?= $baseUrl; ?>assets/vendors/feather-icons/feather.min.js"></script>
    <script src="<?= $baseUrl; ?>assets/vendors/select2/select2.min.js"></script>
    <script src="<?= $baseUrl; ?>assets/js/script/idb.js"></script>
    <script src="<?= $baseUrl; ?>assets/js/template.js"></script>
    <script src="<?= $baseUrl; ?>assets/vendors/inputmask/jquery.inputmask.min.js"></script>
    <script src="<?= $baseUrl; ?>assets/js/magnify.js"></script>
    <?php if($text_editor == "ckeditor") { ?>
    <script src="<?= $baseUrl; ?>assets/vendors/ckeditor/ckeditor.js"></script>
    <?php } else { ?>
    <script src="<?= $baseUrl; ?>assets/vendors/trix/trix.js"></script>
    <?php } ?>
    <script src="<?= $baseUrl; ?>assets/vendors/datatables.net/jquery.dataTables.js"></script>
    <script src="<?= $baseUrl; ?>assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
    <script src="<?= $baseUrl; ?>assets/js/script/threads.js"></script>
    <script src="<?= $baseUrl; ?>assets/js/script/medics.js"></script>
    <?php if(isset($formToShow)) { ?>
    <script>fieldDefault = <?= json_encode($formToShow) ?>, thisSelectRow = <?= $formData["thisSelectRow"] ?>, thisRowId = <?= $formData["thisRowId"] ?>;</script>
    <?php } ?>
    <?php foreach($loadedJS as $eachJS) { ?>
        <script src="<?= $baseUrl; ?><?= $eachJS ?>"></script>
    <?php } ?>
    <script>
        $(() => {
            <?php if(isset($searchTerm) && $session->searchTerm) { ?>
            quick_Search_Topic();
            $(`input[name="topic_q"]`).focus();
            <?php } ?>
            <?php if(isset($verify_payment)) { ?>
            verify_payment();
            <?php } ?>
            <?php if(!empty($session->tempProfilePicture)) { ?>
            save_profile_picture();
            <?php } ?>
            <?php if(!empty($advertHandler)) { ?>
                $(`div[id="advertModal_<?= $advertHandler->item_id ?>"]`).modal("show");
                <?= $advertHandler->modal_function_script; ?>
                <?= "{$advertHandler->modal_function}();" ?>
            <?php } ?>
            <?php if(!empty($announcementNotice)) { ?>
                <?= $announcementNotice->modal_function_script; ?>
                $(`div[class~="announcementModal_<?= $announcementNotice->item_id ?>"]`).modal("show");
                <?= "{$announcementNotice->modal_function}();" ?>
            <?php } ?>
            <?php if($idb_init) { ?>
            setTimeout(() => { $(`button[id="idb_init"]`).trigger("click"); }, 1000);
            <?php } ?>
            <?php if(isset($_GET["end_id"]) && preg_match("/^[a-z0-9]+$/", $_GET["end_id"])) { ?>
            $(`button[data-function="update"][data-item="<?= $_GET["end_id"] ?>"]`).trigger("click");
            <?php } ?>
        });
    </script>
</body>
</html>