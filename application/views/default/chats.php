<?php
// set the title
$page_title = "Chats";

// require the headtags
require "headtags.php";

// extra js
$loadedJS = [
    "assets/js/script/chats.js"
];

// set the current as as the person logged in
$cUserData = $userData;

// is this the current user?
$isAdmin = $userData->user_type == "admin" ? true : false;

// create a new files object
$formsObj = load_class("forms", "controllers");

/** Set parameters for the data to attach */
$form_params = (object) [
    "module" => "user_chats",
    "userData" => $userData,
    "label" => "list",
    "item_id" => null,
    "no_title" => true
];

// create a new object
// $attachments = load_class("files", "controllers")->attachments($form_params);

// get the attachments list
// $preloaded_attachments = !empty($attachments) && isset($attachments["data"]) ? $attachments["data"]["files"] : null;
?>
<div class="page-content">
    <?php if(empty($cUserData)) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>

        <div class="row chat-wrapper">
			<div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row position-relative">
                            <div class="col-lg-4 chat-aside border-lg-right">
                                <div class="aside-content">
                                    <div class="aside-header">
                                        <div class="d-flex justify-content-between align-items-center pb-2 mb-2">
                                            <div class="d-flex align-items-center">
                                                <figure class="mr-2 mb-0">
                                                    <img src="<?= $baseUrl ?><?= $userData->image ?>" class="img-sm rounded-circle" alt="profile">
                                                    <div class="status online"></div>
                                                </figure>
                                                <div>
                                                    <h6><?= $userData->name ?></h6>
                                                    <p class="text-muted tx-13"><?= $userData->occupation ?></p>
                                                </div>
                                            </div>
                                            <div class="dropdown">
                                                <button class="btn p-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="icon-lg text-muted pb-3px" data-feather="settings" data-toggle="tooltip" title="Settings"></i>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item d-flex align-items-center" href="<?= $baseUrl ?>profile"><i data-feather="eye" class="icon-sm mr-2"></i> <span class="">View Profile</span></a>
                                                    <a class="dropdown-item d-flex align-items-center" data-loader="preferences" href="javascript:void(0)"><i data-feather="edit-2" class="icon-sm mr-2"></i> <span class="">Edit Preferences</span></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-group border rounded-sm">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text border-0 rounded-sm">
                                                    <i data-feather="search" class= "icon-md cursor-pointer"></i>
                                                </div>
                                            </div>
                                            <input type="text" class="form-control border-0 rounded-sm" id="name_search" placeholder="Search here...">
                                        </div>
                                    </div>
                                    <div class="aside-body">
                                        <ul class="nav nav-tabs mt-3" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="chats-tab" data-toggle="tab" href="#chats" role="tab" aria-controls="chats" aria-selected="true">
                                                <div class="d-flex flex-row flex-lg-column flex-xl-row align-items-center">
                                                    <i data-feather="message-square" class="icon-sm mr-sm-2 mr-lg-0 mr-xl-2 mb-md-1 mb-xl-0"></i>
                                                    <p class="d-none d-sm-block">Chats</p>
                                                </div>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="contacts-tab" data-toggle="tab" href="#contacts" role="tab" aria-controls="contacts" aria-selected="false">
                                                <div class="d-flex flex-row flex-lg-column flex-xl-row align-items-center">
                                                    <i data-feather="users" class="icon-sm mr-sm-2 mr-lg-0 mr-xl-2 mb-md-1 mb-xl-0"></i>
                                                    <p class="d-none d-sm-block">Contacts</p>
                                                </div>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content mt-3">
                                            <div class="tab-pane fade show active" id="chats" role="tabpanel" aria-labelledby="chats-tab">
                                                <div>
                                                    <p class="text-muted mb-1">Recent chats</p>
                                                    <ul class="list-unstyled chat-list px-1" id="search_list"></ul>
                                                    <ul class="list-unstyled chat-list px-1" id="chats_list"></ul>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="contacts" role="tabpanel" aria-labelledby="contacts-tab">
                                                <p class="text-muted mb-1">Contacts</p>
                                                <ul class="list-unstyled chat-list px-1" id="contacts_list"></ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 chat-content">
                                <div class="chat-header border-bottom pb-2">
                                    <div class="d-flex justify-content-between">
                                        <div id="current-user" class="d-flex align-items-center"></div>
                                        <div class="d-lg-none d-md-block">
                                            <span class="cursor" id="list_contacts" title="List users"><i class="fa fa-arrow-left"></i></span>
                                        </div>
                                        <div id="current-user-options" class="d-flex hidden align-items-center mr-n1">
                                            <a href="javascript:void(0)">
                                                <i data-feather="video" class="icon-lg text-muted mr-3" data-toggle="tooltip" title="Start video call"></i>
                                            </a>
                                            <a href="javascript:void(0)">
                                                <i data-feather="phone-call" class="icon-lg text-muted mr-0 mr-sm-3" data-toggle="tooltip" title="Start voice call"></i>
                                            </a>
                                            <a href="javascript:void(0)" class="d-none d-sm-block">
                                                <i data-feather="user-plus" class="icon-lg text-muted" data-toggle="tooltip" title="Add to contacts"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="chat-body mb-0">
                                    <div class="default-chat"><div class="text-center">Select recipient for your message.</div></div>
                                    <ul class="messages" style="min-height:400px;margin-top:15px"></ul>
                                </div>
                                <div class="mt-1 mb-1">
                                    <div class="hidden" id="chat_file_attachment_wrapper">
                                        <div class="file_attachment_url" data-url="<?= $baseUrl; ?>api/files/attachments"></div>
                                        <input type="file" name="attachment_file_upload" data-form_module="<?= $form_params->module; ?>" data-form_item_id="temp_attachment" hidden id="attachment_file_upload">
                                        <div class="chat-attachment"><?php //= $preloaded_attachments ?></div>
                                        <div class="file-preview slim-scroll" preview-id="<?= $form_params->module; ?>"></div>
                                    </div>
                                </div>
                                <div class="chat-footer d-flex">
                                    <div class="d-none">
                                        <button disabled type="button" data-request="attach-file" class="btn border btn-icon rounded-circle mr-2" data-toggle="tooltip" title="Attatch files">
                                            <i data-feather="paperclip" class="text-muted"></i>
                                        </button>
                                    </div>
                                    <div class="input-group">
                                        <textarea disabled name="chat_input" id="chat_input" placeholder="Type a message" class="form-control slim-scroll rounded-sm"></textarea>
                                    </div>
                                    <div class="ml-2">
                                        <button disabled data-function="send_chat_message" type="button" class="btn btn-primary btn-icon rounded-circle">
                                            <i data-feather="send"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="mt-2 tx-11 pb-0 mb-0 row">
                                    <div class="text-right col-md-12 pr-3 d-none d-lg-block">
                                        <label for="enter_tosend" class="mr-1 font-italic">Press enter to send</label>
                                        <input type="checkbox" <?= $userPrefs->messages->enter_to_send ? "checked" : null ?> name="enter_tosend" id="enter_tosend" class="enter_tosend">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php } ?>
</div>
<?php require "foottags.php"; ?>