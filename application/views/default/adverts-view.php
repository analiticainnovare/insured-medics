<?php
// set the title
$page_title = "Ad Campaign Management";

// require the headtags
require "headtags.php";

// get some variable
$itemFound = false;
$recordId = confirm_url_id(1) ? xss_clean($SITEURL[1]) : null;

// load the advert information
if($recordId) {

    /** load the users insurance policies */
    $recordObj = load_class("adverts", "controllers");

    /** Parameters */
    $param = (object) [
        "limit" => 1, 
        "show_button" => false,
        "advert_id" => $recordId, 
        "userData" => $userData
    ];
    // return the result
    $recordData = $recordObj->list($param);
    $itemFound = !empty($recordData["data"]["ads_list"]) ? true : false;

    // if the complaint was found
    if($itemFound) {
        
        // extra js
        $loadedJS = [
            "assets/js/script/attachments.js",
            "assets/js/script/comments.js",
            "assets/js/script/adverts.js",
            "assets/js/script/payments.js",
            "assets/vendors/bootstrap-maxlength/bootstrap-maxlength.min.js"
        ];

        // create a new forms object
        $formsObj = load_class("forms", "controllers");
        
        // set the complaint data
        $recordData = $recordData["data"]["ads_list"][0];
        
        // sent state
        $isPaid = (bool) ($recordData->paid_status == "Paid");
        $isActive = (bool) ($recordData->submit_status == "save");
        $hasPermission = ( ($recordData->user_id == $loggedUserId) || ($recordData->created_by == $userData->user_type) ) ? true : false;

        // 'Pending','Cancelled','Approved','Ellapsed','Running','Deleted'
        $isClosed = (bool) (in_array($recordData->status, ["Deleted", "Cancelled", "Ellapsed"]));
        $isApproved = (bool) (in_array($recordData->status, ["Approved"]));
        
    }
}

// review command
$updateAdvert = $accessObject->hasAccess("update", "adverts");
$reviewAdvert = $accessObject->hasAccess("approve", "adverts");

// set the statistics
$statistics = [
    "views_limit" => "Total Views",
    "clicks_limit" => "Total Clicks"
];
?>
<div class="page-content">

    <?php if(!$accessObject->hasAccess("view", "adverts") || !$itemFound) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <a href="<?= $baseUrl ?>adverts-list" type="button" class="btn btn-outline-primary btn-icon-text mb-2 mb-md-0">
                    <i class="btn-icon-prepend" data-feather="list"></i>
                    List Adverts
                </a> &nbsp;
            </div>
        </div>

        <div class="col-lg-12 p-0 col-xl-12 inbox-wrapper">
            <div class="card">
                <div class="card-body">

                    <div class="row">
                        
                        <div class="col-lg-3 email-aside border-lg-right">
                            <div class="aside-content">
                                <div class="aside-header">
                                    <button class="navbar-toggle" data-target=".aside-nav" data-toggle="collapse" type="button"><span class="icon"><i data-feather="chevron-down"></i></span></button>
                                    <span class="title"><h4>Ad Campaign</h4></span>
                                </div>
                                <?php if($reviewAdvert && $isActive && !in_array($recordData->status, ["Pending", "Cancelled", "Deleted", "Ellapsed"])) { ?>
                                <div class="aside-compose" data-toggle="tooltip" title="Update this Advert"><a data-function="load-form" data-resource="adverts" data-module-id="<?= $recordId ?>" data-module="post_advertisement" class="btn btn-primary btn-block" href="javascript:void(0)">Update Advert</a></div>
                                <?php } ?>
                                <?php if($isActive && !$isClosed  && $reviewAdvert) { ?>
                                <div class="aside-compose" data-toggle="tooltip" title="Reply to Advert"><a <?php if($isActive) { ?>data-function="load-form" data-resource="adverts" data-module-id="<?= $recordId ?>" data-module="share_a_reply"<?php } ?> class="btn btn-warning text-white btn-block" href="javascript:void(0)">Reply</a></div>
                                <?php } ?>
                                <?php if(!$isActive) { ?>
                                <div class="aside-compose" data-toggle="tooltip" title="Update this Advert"><a data-function="load-form" data-resource="adverts" data-module-id="<?= $recordId ?>" data-module="post_advertisement" class="btn btn-primary btn-block" href="javascript:void(0)">Update Advert</a></div>
                                <?php } ?>
                                <div class="aside-nav <?= (!$isActive) ? "mt-3" : null ?> collapse">
                                    <?php if($reviewAdvert) { ?>
                                    <ul class="nav">
                                        <li class="active">
                                            <a data-toggle="tooltip" title="View All Replies" <?php if($isActive) { ?>data-function="load-replies-form" data-module="view_replies_list" data-resource="adverts" data-module-id="<?= $recordId ?>"<?php } ?> href="javascript:void(0)">
                                                <span class="icon"><i data-feather="inbox"></i></span>Officials Feedback
                                                <span data-record="replies_count" data-id="<?= $recordId ?>" class="badge badge-danger-muted text-white font-weight-bold float-right">0</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <?php } ?>
                                    <form class="app-data-form" <?= $updateAdvert ? "action=\"{$baseUrl}api/adverts/update\" " : null ?>method="POST">
                                        <?= form_loader(); ?>
                                        <div class="card mb-3 mt-3">
                                            <div class="card-header pb-0 pl-2">
                                                <div class="card-title pb-0 mb-2">Campaign Information</div>
                                            </div>
                                            <div class="card-body p-2">
                                                <p><span class="text-muted">Date Range:</span> <span><?= $recordData->date_range ?></span></p>
                                                <p><span class="text-muted">No of Days:</span> <span><?= $recordData->days ?> Days</span></p>
                                                <p><span class="text-muted">Budget:</span> <span><?= $recordData->budget ?></span></p>
                                                <p><span class="text-muted">Objective:</span> <span><?= $recordData->ad_objective ?></span></p>

                                                <p class="mt-2"><span class="text-muted">Ad Campaign Context:</span><br>
                                                <?php 
                                                // add context
                                                $ad_context = isset($recordData->ad_context) ? $medicsClass->stringToArray($recordData->ad_context) : [];
                                                // loop through the related information
                                                foreach($medicsClass->pushQuery("id, target", "adverts_context") as $each) {
                                                    if(in_array($each->id, $ad_context)) {
                                                        print "<span class='mr-2'> - {$each->target}</span><br>";
                                                    }
                                                }
                                                ?>
                                                </p>

                                                <p class="mt-2"><span class="text-muted">Ad Campaign Context:</span><br>
                                                <?php 
                                                // add context
                                                $ad_target = isset($recordData->ad_target) ? $medicsClass->stringToArray($recordData->ad_target) : [];
                                                // loop through the related information
                                                foreach($medicsClass->pushQuery("id, target", "adverts_target") as $each) {
                                                    if(in_array($each->id, $ad_target)) {
                                                        print "<span class='mr-2 pr-2'> - {$each->target}</span><br>";
                                                    }
                                                }
                                                ?>
                                                </p>
                                                <p class="mt-2"><span class="text-muted">Date Created:</span> <?= $recordData->date_created ?></p>
                                                
                                            </div>
                                        </div>
                                        <?php if(!$reviewAdvert && !empty($recordData->related_to_details)) { ?>
                                        <div class="grid-margin">
                                            <div class="card">
                                                <div class="card-header pb-0 pl-2">
                                                    <div class="card-title pb-0 mb-2">Campaign Related Item Information</div>
                                                </div>
                                                <div class="card-body p-2">
                                                    <?php foreach($recordData->related_to_details as $key => $value) { ?>
                                                        <?php if(!in_array($key, ["company_id", "item_id", "item_name", "row_id", "date_created"])) { ?>
                                                            <p><span class="text-muted"><?= $key ?>:</span> <span><?= $value ?></span></p>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <div class="mt-3 form-group">
                                            <label for="status">Current Status:</label>
                                            <?php if($reviewAdvert) { ?>
                                            <span data-record="record_status"><?= $recordData->status_label ?></span>
                                            <select data-width="100%" name="status" <?= in_array($recordData->status, ["Deleted", "Ellapsed", "Cancelled"]) || !$isActive ? "disabled" : null;  ?> id="status" class="selectpicker">
                                                <option <?= $recordData->status == "Pending" && $recordData->status !== "Processing"  ? "selected" : "disabled" ?> value="Pending">Pending</option>
                                                <option <?= $recordData->status == "Processing" && $recordData->status !== "Approved" ? "selected" : (($recordData->status == "Approved") ? "disabled" : null) ?> value="Processing">Processing</option>
                                                <option <?= $recordData->status == "Cancelled" ? "selected" : null ?> value="Cancelled">Cancelled</option>
                                                <option <?= $recordData->status == "Ellapsed" ? "selected" : null ?> value="Ellapsed">Ellapsed</option>
                                                <option <?= $recordData->status == "Approved" ? "selected" : null ?> <?= $recordData->status == "Approved" ? "disabled" : null ?> value="Approved">Approved</option>
                                            </select>
                                            <?php } else { ?>
                                            <span data-record="record_status"><?= $recordData->status_label ?></span>
                                            <?php } ?>
                                        </div>
                                        <div class="mt-3 form-group">
                                            <label for="assigned_to">Assigned To:</label>
                                            <?php if($reviewAdvert) { ?>
                                            <select data-width="100%" name="assigned_to" <?= in_array($recordData->status, ["Approved", "Deleted", "Ellapsed", "Cancelled"]) || !$isActive ? "disabled" : null;  ?> <?= $recordData->user_id == $loggedUserId ? "disabled" : null;  ?> id="assigned_to" class="selectpicker">
                                                <option value="null">Assign to a Admin</option>
                                                <?php
                                                // load users that this can be assigned to
                                                $query = "SELECT item_id, name FROM users WHERE user_type IN ('admin') AND status = '1' AND deleted='0'";
                                                $stmt = $medics->query($query);
                                                $result = $stmt->fetchAll(PDO::FETCH_OBJ);
                                                // loop through the results
                                                foreach($result as $eachUser) {
                                                    print "<option ".($recordData->assigned_to == $eachUser->item_id ? "selected" :  null)." value=\"{$eachUser->item_id}\">{$eachUser->name}</option>";
                                                }
                                                ?>
                                            </select>
                                            <?php } elseif(!empty($recordData->assigned_to)) { ?>
                                            <span class="user-name-link" onclick="return user_basic_information('<?= $recordData->assigned_to ?>')"><?= $recordData->assigned_to_info->name ?></span>
                                            <?php } ?>
                                        </div>
                                        <?php if($isActive && !$isClosed && $reviewAdvert) { ?>
                                        <div class="mt-3 form-group text-center">
                                            <input type="hidden" name="advert_id" id="advert_id" value="<?= $recordId ?>" class="form-control">
                                            <button type="submit" class="btn btn-outline-success">Save Changes</button>
                                        </div>
                                        <?php } ?>
                                    </form>
                                    <?php if($isActive && $reviewAdvert) { ?>
                                    <div class="mt-5 justify-content-center row form-group">
                                        <button data-toggle="tooltip" title="View summary history" data-function="load-form" data-resource="adverts" data-module-id="<?= $recordId ?>" data-module="activity_history" class="btn btn-outline-secondary">View activity history</button>
                                    </div>
                                    <?php } ?>                                  

                                </div>
                            </div>
                        </div>

                        <div class="col-lg-9 email-content">
                            
                            <div class="email-head">
                                <div class="email-head-subject p-2 mr-0 pr-0">
                                <div class="title d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center">
                                        <span><h4><?= $recordData->user_id == $loggedUserId ? $recordData->advert_title : $recordData->advert_title ?> <small class="text-primary">(<?= $recordData->advert_id ?>)</small></h4></span>
                                    </div>
                                    <?php if($recordData->user_id == $loggedUserId && !$isActive) {  ?>
                                    <div class="text-right">
                                        <?= submit_draft_button("Adverts", $recordData->advert_title, $recordId) ?>
                                    </div>
                                    <?php } ?>
                                    <?php if(!$reviewAdvert && !$isClosed) { ?>
                                        <?php
                                        // if there is a pending request to cancel the policy
                                        if($recordData->pending_cancel_request) {
                                            // show a notice to of a cancel 
                                            print "<button class=\"btn btn-outline-warning\">Pending request to cancel advert</button>";
                                        } elseif($isActive) { ?>
                                            <?= cancel_record_button($recordData->advert_title, $recordData->advert_id, "adverts") ?>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                                </div>
                                <div class="email-head-sender d-flex align-items-center justify-content-between flex-wrap">
                                <div class="d-flex align-items-center mb-2">
                                    <div class="avatar">
                                    <img src="<?= $baseUrl ?><?= $recordData->created_by_info->image ?>" alt="Avatar" class="rounded-circle user-avatar-md">
                                    </div>
                                    <div class="sender d-flex align-items-center">
                                        <p><a href="javascript:void(0)" title='Quick details view' data-function="load-form" data-module="user_basic_information" data-resource="user_information" data-module-id="<?= $recordData->user_id ?>"><?= $recordData->created_by_info->name ?></a></p>
                                        <p class="pl-3"><small><i class="fa fa-envelope"></i> <?= $recordData->created_by_info->email ?></small></p>
                                        <p class="pl-3"><small><i class="fa fa-phone"></i> <?= $recordData->created_by_info->phone_number ?></small></p>
                                    </div>
                                </div>
                                <div class="date"><i class="fa fa-calendar"></i> <?= date("M d Y, h:iA", strtotime($recordData->date_created)) ?></div>
                                </div>
                            </div>
                            
                            <div class="row">

                                <div class="col-lg-8 mb-3">
                                    
                                    <div class="p-0 <?= !$isActive ? "border-top" : null; ?> m-0">

                                        <?php if($isActive) { ?>
                                            <div class="card mb-5">
                                                <div class="card-header p-2">
                                                    <div class="card-title mb-0">Ad Campaign Statistics</div>
                                                </div>
                                                <div class="card-body p-2">
                                                    <?php if($reviewAdvert) { ?>
                                                    <form class="app-data-form" action="<?= $baseUrl ?>api/adverts/update" method="POST">
                                                    <?php } ?>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <h5 class="border-bottom mb-2">Limits</h5>
                                                            <?php foreach($recordData->campaign_statistics->limits as $key => $value) { ?>
                                                                <?php if($reviewAdvert) { ?>
                                                                    <div class="form-group">
                                                                        <label for="<?= $key ?>"><?= $statistics[$key] ?></label>
                                                                        <input <?= $isClosed ? 'disabled' : "name=\"{$key}\" id=\"{$key}\"" ?> class="form-control" type="text" value="<?= $value ?>">
                                                                    </div>
                                                                <?php } else { ?>
                                                                    <p class="mb-2"><span class="text-muted"><?= $statistics[$key] ?></span>: <?= $value ?></p>
                                                                <?php } ?>
                                                            <?php } ?>
                                                            <?php if($reviewAdvert) { ?>
                                                                <div class="form-group">
                                                                    <label for="amount_payable">Amount Payable <?= $medicsClass->the_status_label($recordData->paid_status); ?></label>
                                                                    <input <?= $isPaid || $isClosed ? 'disabled' : 'name="amount_payable" id="amount_payable"' ?> type="number" value="<?= $recordData->amount_payable ?>" class="form-control">
                                                                </div>
                                                                <?php if(!$isClosed) { ?>
                                                                <div class="form-group">
                                                                    <input type="hidden" name="advert_id" id="advert_id" value="<?= $recordId ?>" class="form-control">
                                                                    <button class="btn btn-outline-success">Save Changes</button>
                                                                </div>
                                                                <?php } ?>
                                                            <?php } else { ?>
                                                                <p><span class="text-muted"><?= "Amount Payable" ?></span>: GH&cent; <?= $recordData->amount_payable ?> <?= $medicsClass->the_status_label($recordData->paid_status); ?></p>
                                                                <?php if(!$isPaid && !$isClosed) { ?>
                                                                <button data-item-id="<?= $recordId ?>" data-item="adverts" class="btn create-checkout btn-sm btn-outline-success" type="button">Make Payment</button>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <h5 class="border-bottom mb-2">Statistics</h5>
                                                            <?php foreach($recordData->campaign_statistics->campaign_stats as $key => $value) { ?>
                                                                <p class="mb-2"><span class="text-muted"><?= ucwords($key) ?></span>: <?= json_encode($value) ?></p>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <?php if($reviewAdvert) { ?>
                                                    </form>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <?php if(!$isClosed) { ?>
                                            <?= leave_comments_builder("adverts", $recordId, "Leave a comment") ?>
                                        <?php } ?>

                                        <div id="comments-container" data-autoload="true" data-last-reply-id="0" data-id="<?= $recordId ?>" class="slim-scroll pt-3 mt-3 pr-2 pl-0" style="overflow-y:auto; max-height:850px"></div>
                                        <div class="load-more mt-3 text-center"><button id="load-more-replies" type="button" class="btn btn-outline-secondary">Loading comments</button></div>
                                    </div>

                                </div>

                                <div class="col-lg-4 p-0">

                                    <div class="email-attachments p-0 pt-3">

                                        <div class="grid-margin">
                                            <div class="card">
                                                <div class="card-header pb-0 pl-3">
                                                    <div class="card-title pb-0 mb-2">Advert Image</div>
                                                </div>
                                                <div class="p-0 rs-gallery">
                                                    <div class="attachment-container text-center p-0 pb-2">
                                                        <div class="gallery-item">
                                                            <div class="col-lg-12 attachment-item">
                                                                <img height="100%" width="100%" src="<?= $baseUrl ?><?= $recordData->image ?>"> 
                                                                <div class="gallery-desc">
                                                                    <a class="image-popup" href="<?= $baseUrl ?><?= $recordData->image ?>" title="">
                                                                        <i class="fa fa-search"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="grid-margin">
                                            <div class="card">
                                                <div class="card-header pb-0 pl-3">
                                                    <div class="card-title pb-0 mb-2">Company Information</div>
                                                </div>
                                                <div class="card-body p-2 pt-3">
                                                    <p><span class="text-muted">Company Name:</span> <span class="underline" onclick="return company_basic_information('<?= $recordData->company_id ?>')"><?= $recordData->company_info->name ?></span></p>
                                                    <p><span class="text-muted">Contact Number:</span> <span><?= $recordData->company_info->phone_number ?></span></p>
                                                    <p><span class="text-muted">Email Address:</span> <span><?= $recordData->company_info->email ?></span></p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if($reviewAdvert && !empty($recordData->related_to_details)) { ?>
                                        <div class="grid-margin">
                                            <div class="card">
                                                <div class="card-header pb-0 pl-2">
                                                    <div class="card-title pb-0 mb-2">Campaign Related Item Information</div>
                                                </div>
                                                <div class="card-body p-2">
                                                    <?php foreach($recordData->related_to_details as $key => $value) { ?>
                                                        <?php if(!in_array($key, ["company_id", "item_id", "item_name", "row_id", "date_created"])) { ?>
                                                            <p><span class="text-muted"><?= $key ?>:</span> <span><?= $value ?></span></p>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>

                                        <div class="grid-margin grid-margin-xl-0 stretch-card" id="cancel_existing_request" data-json='<?= json_encode(["container" => "cancel_advert_list", "field" => "advert_id", "url" => "adverts"]); ?>'>
                                            <div class="card">
                                                <div class="card-header card-title mb-0 pl-3">Cancellation Request</div>
                                                <div class="card-body p-2">
                                                    <div class="d-flex flex-column" data-autoload="true" data-policy-id="<?= $recordData->item_id ?>" data-threads="cancel_advert_list">
                                                        <div class="p-0 m-0 font-italic">No pending request</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <?php if(!empty($recordData->attachment)) { ?>
                                            <div class="title">Attachments <span>(<?= count($recordData->attachment["files"]) ?? 0 ?> files, <?= $recordData->attachment["files_size"] ?? 0 ?>)</span></div>
                                            <?= $formsObj->list_attachments($recordData->attachment["files"], $loggedUserId, null, !$isActive); ?>
                                        <?php } ?>

                                    </div>

                                </div>

                            </div>


                        </div>

                    </div>

                </div>
            </div>
        </div>

    <?php } ?>
    
</div>
<?php require "foottags.php"; ?>