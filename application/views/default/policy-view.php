<?php
// set the title
$page_title = "Insurance Policy Update";

// require the headtags
require "headtags.php";

// extra css
$loadedJS = [
    "assets/js/ajax-load.js",
    "assets/js/script/forms.js"
];

// get some variable
$itemFound = false;
$recordId = confirm_url_id(1) ? xss_clean($SITEURL[1]) : null;

// load the complaint information
if($recordId) {
    
    /** create a new object of the company_policy class */
    $recordObj = load_class("company_policy", "controllers");

    /** Parameters */
    $param = (object) [
        "policy_type_id" => $recordId,
        "userData" => $userData,
        "remote" => true,
        "limit" => 1
    ];

    // return the result
    $recordData = $recordObj->list($param);
    $itemFound = !empty($recordData["data"]) ? true : false;

    // if the complaint was found
    if($itemFound) {
        // create a new forms object
        $formsObj = load_class("forms", "controllers");
        // set the complaint data
        $recordData = $recordData["data"][0];
        $recordData->attachment = (array) $recordData->attachment;

        // sent state
        $isSaved = (bool) ($recordData->submit_status == "save");
        
        // get the category 
        $itemCategory = $recordData->category;
        $isEnrolled = (bool) (in_array($recordData->policy_status, ["Pending", "In Review", "Enrolled"]));

        // get the forms data
        $formData = $formsObj->form_enlisting($recordData->policy_form);
        $formToShow = (isset($recordData->policy_form->fields) && !empty($recordData->policy_form->fields)) ? $recordData->policy_form->fields : (object)[];
    }

    /** Set parameters for the data to attach */
    $form_params = (object) [
        "module" => "company_policy_{$recordId}",
        "userData" => $userData,
        "item_id" => null,
        "no_title" => true
    ];
}
?>
<style>
trix-editor {
    min-height: 180px;
    max-height: 180px;
}
</style>
<div class="page-content">
    <?php if(!$accessObject->hasAccess("update", "company_policy") || !$itemFound) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <a href="<?= $baseUrl ?>policy-list/" class="btn btn-primary btn-icon-text mb-2 mb-md-0">
                    <i class="btn-icon-prepend" data-feather="arrow-left"></i>
                    View Insurance Policies
                </a>
            </div>
        </div>
        <div class="col-lg-12 p-0 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <form action="<?= $baseUrl ?>api/company_policy/update" id="jsform-wrapper">
                        <div class="row">
                            <div class="col-lg-11 mb-2 col-md-10">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                <div class="input-group-text">Policy Name &nbsp;<span class="required">*</span></div>
                                                </div>
                                                <input type="text" value="<?= $recordData->name ?>" name="policy_name" id="policy_name" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                <div class="input-group-text">Policy Code &nbsp;<span class="required">*</span></div>
                                                </div>
                                                <input type="text" value="<?= $recordData->policy_id ?>" name="policy_id" id="policy_id" class="form-control text-uppercase">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1 mb-2 col-md-2">
                                <div class="p-0 m-0">
                                    <button type="button" class="btn pl-1 pr-1 btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Options
                                    </button>
                                    <div class="dropdown-menu p-0 m-0">
                                        <a class="dropdown-item" data-module="jsform" data-field="input" href="<?= $baseUrl ?>policy-claim-form/<?= $recordId ?>">Claims Form</a>
                                        <a class="dropdown-item" data-function="load-form" data-module="share_a_reply" data-resource="company_policy" data-module-id="<?= $recordId ?>" href="javascript:void(0)">Add Comments</a>
                                        <a class="dropdown-item" data-function="load-replies-form" data-module="view_replies_list" data-resource="company_policy" data-module-id="<?= $recordId ?>" href="javascript:void(0)">Comments List (<span data-id="<?= $recordId ?>" data-record="replies_count">0</span>)</a>
                                        <a class="dropdown-item" data-function="load-form" data-module="activity_history" data-resource="company_policy" data-module-id="<?= $recordId ?>" data-module="jsform" data-field="email" href="javascript:void(0)">Activity Logs History</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 mb-3">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="policy_description">Policy Description &nbsp;<span class="required">*</span></label>
                                            <?php if($text_editor == "ckeditor") { ?>
                                                <textarea class="form-control" name="policy_description" id="policy_description" rows="7"><?= $recordData->description ?></textarea>
                                            <?php } else { ?>
                                                <input hidden type="text" value="<?= $recordData->description ?>" id="policy_description">
                                                <trix-editor name="policy_description" class="trix-slim-scroll" id="policy_description" input="policy_description"></trix-editor>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                            <div class="col-lg-12 mb-3">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="policy_requirements">Policy Requirements</label>
                                            <?php if($text_editor == "ckeditor") { ?>
                                                <textarea class="form-control" name="policy_requirements" id="policy_requirements" rows="7"><?= $recordData->requirements ?></textarea>
                                            <?php } else { ?>
                                                <input hidden type="text" value="<?= $recordData->requirements ?>" id="policy_requirements">
                                                <trix-editor name="policy_requirements" class="trix-slim-scroll" id="policy_requirements" input="policy_requirements"></trix-editor>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-md-7 mb-3">
                                                <div class="form-group">
                                                    <label>Policy Category</label>
                                                    <select name="policy_category" id="policy_category" class="form-control selectpicker">
                                                        <option value="">Select Policy Category</option>
                                                        <?php
                                                        foreach($medicsClass->pushQuery("*", "policy_form", "type='policy_category' AND status='1'") as $eachCategory) {
                                                            // create a slug
                                                            $value = create_slug($eachCategory->name);
                                                            // list the option
                                                            print "<option ".(($value == $itemCategory) ? "selected" : null)." value=\"{$value}\">{$eachCategory->name}</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label for="status">Current Status:</label>
                                                    <span data-record="record_status"><?= $recordData->the_status_label ?></span>
                                                    <select data-width="100%" name="status" id="status" class="form-control selectpicker">
                                                        <option <?= $recordData->policy_status == "Pending" ? "selected" : "disabled" ?> value="Pending">Pending</option>
                                                        <option <?= $recordData->policy_status == "In Review" && $recordData->policy_status !== "Enrolled" ? "selected" : (($recordData->policy_status == "Enrolled") ? "disabled" : null) ?> value="In Review">In Review</option>
                                                        <option <?= $recordData->policy_status == "Enrolled" ? "selected" : null ?> <?= $recordData->policy_status == "Enrolled" ? "disabled" : null ?> value="Enrolled">Enrolled</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="form-group">
                                                    <label for="allow_attachment">Allow attachment to form by clients</label>
                                                    <select name="allow_attachment" id="allow_attachment" class="form-control selectpicker">
                                                        <option <?= (isset($recordData->policy_form->allow_attachment) && $recordData->policy_form->allow_attachment == "yes") ? "selected" : null ?> value="yes">Yes! Allow file attachments</option>
                                                        <option <?= (isset($recordData->policy_form->allow_attachment) && $recordData->policy_form->allow_attachment == "no") ? "selected" : null ?> value="no">No! Do not allow file attachments</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label for="year_enrolled">Month/Year Enrolled</label>
                                                    <input type="text" class="form-control" id="year_enrolled" name="year_enrolled" value="<?= $recordData->year_enrolled ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="form_footnote">Form Footnote <small>(This will appear at the bottom of the form)</small></label>
                                            <?php if($text_editor == "ckeditor") { ?>
                                                <textarea class="form-control" name="form_footnote" id="form_footnote" rows="7"><?= $recordData->policy_form->form_footnote ?? null ?></textarea>
                                            <?php } else { ?>
                                                <input hidden type="text" value="<?= $recordData->policy_form->form_footnote ?? null ?>" id="form_footnote">
                                                <trix-editor style="max-height:100px;min-height:100px" name="form_footnote" class="trix-slim-scroll" id="form_footnote" input="form_footnote"></trix-editor>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php if(!empty($recordData->attachment)) { ?>
                                                <div class="email-attachments pl-0 border-top-0 pt-0">
                                                    <div class="title">Attachments <span>(<?= count($recordData->attachment["files"]) ?? 0 ?> files, <?= $recordData->attachment["files_size"] ?? 0 ?>)</span></div>
                                                    <?= $formsObj->list_attachments($recordData->attachment["files"], $loggedUserId, "col-lg-6 col-md-6"); ?>
                                                </div>
                                            <?php } ?>
                                            <label for="allow_attachment">Policy Legal Documents <small class="font-italic"> - Attach additional legal document (if any) to this policy</small></label>
                                            <?php 
                                            // load the attachment method
                                            print $formsObj->form_attachment_placeholder($form_params, false);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="border-bottom pb-2">
                                    
                                    <div class="col-lg-12 p-0 m-0 row justify-content-between">
                                        <div><h4>Policy Form</h4></div>
                                        <?= form_manager_options() ?>
                                    </div>

                                </div>
                                <div id="form-pretext" class="text-center font-italic mt-4">The policy form is empty. Add new fields to the set.</div>
                                <div class="pt-2 pl-0 mt-2" id="jsform-container"><?= $formData["form"]; ?></div>
                                <input type="hidden" name="item_id" value="<?= $recordId ?>" class="form-control">
                                <div class="row mt-5">
                                    <div class="col-md-6 text-left">
                                        <button type='button' style='padding:5px' class='btn preview-form hidden btn-sm btn-secondary'><i class='fa fa-eye'></i> Preview Form</button>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <button class="btn btn-outline-success" type="submit"><i class="fa fa-save"></i> Save Update </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    <?php } ?>    
</div>
<?= save_form_button(); ?>
<?= select_field_modal(); ?>
<?php require "foottags.php"; ?>