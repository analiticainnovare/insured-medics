<?php
// set the title
$page_title = "Insurance Company";

// additional css and javascript files
$loadedCSS = [];

// global variables
global $availableQuickLinks;

// extra css
$loadedJS = [
    "assets/js/script/attachments.js",
    "assets/js/script/posts.js"
];

// require the headtags
require "headtags.php";

// company id variable
$companyData = null;
$companyId = confirm_url_id(1) ? xss_clean($SITEURL[1]) : $userData->company_id;

if(!empty($companyId)) {

    // params
    $params = (object) [
        "limit" => 1,
        "company_id" => $companyId
    ];
    // load the company information
    $companyInfo = load_class("company", "controllers")->list($params);

    // if the response is not empty
    if(!empty($companyInfo["data"])) {

        // set the company data
        $companyData = $companyInfo["data"][0];
        $companyId = $companyData->item_id;

        // clean the description
        $companyData->description = custom_clean(htmlspecialchars_decode($companyData->description));
        $isCommission_Permitted = $accessObject->hasAccess("permissions", "users");

        // if the person logged in is an employee
        $isOfficial = (bool) (($userData->company_id == $companyData->item_id) && (in_array($userData->user_type, ["insurance_company", "admin"])));

        // if the viewas parameter was parsed
        if(isset($_GET["viewas"])) {
            $isOfficial = false;
        }

        $companyPrefs = $companyData->preferences;
        
        /** Set parameters for the data to attach */
        $form_params = (object) [
            "module" => "posts_{$companyId}",
            "userData" => $userData,
            "item_id" => $companyId,
            "accept" => "image/*",
            "no_footer" => true
        ];

        // create a new object of the forms class
        $formsObj = load_class("forms", "controllers");
    }
}
?>
<div class="page-content">
    <?php if(empty($companyData)) { ?>
    
        <?= permission_denied() ?>
    
    <?php } else { ?>

        <div class="profile-page tx-13">

            <div class="row">

                <div class="col-lg-12 grid-margin">
                    <div class="profile-header">
                        <div class="cover">
                            <div class="gray-shade"></div>
                            <figure id="company_cover_image">
                                <?php if(isset($companyData->preferences->cover_image) && $companyData->preferences->cover_image) { ?>
                                <img src="<?= $baseUrl ?><?= $companyData->preferences->cover_image ?>"  class="company_cover_image img-fluid" alt="profile cover">
                                <?php } else { ?>
                                <div class="p-6"></div>
                                <?php } ?>
                            </figure> 
                            <div class="cover-body d-flex justify-content-between align-items-center">
                                <div>
                                    <table>
                                        <tr>
                                            <td>
                                                <img title="Change profile picture" data-toggle="tooltip" id="form-control-file" class="cursor company-logo" src="<?= $baseUrl ?><?= $companyData->logo ?>" alt="profile">
                                            </td>
                                            <td class="pl-3">
                                                <span class="profile-name ml-0">
                                                    <?= $companyData->name ?>
                                                    <p class="font-14px text-muted">@<span class="the_username"><?= !$companyData->username ? $companyData->name : $companyData->username ?></span>
                                                        <?php if($isOfficial) { ?>
                                                            <span class="cursor" id="company_username_change" data-toggle="tooltip" title="Edit company username"><i class="fa fa-pen"></i></span>
                                                        <?php } ?>
                                                    </p>
                                                    <p class="font-14px d-none d-md-block"><span class="text-muted">Established on:</span> <?= date("jS M Y", strtotime($companyData->establishment_date)) ?></p>
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                    <?php if($isOfficial) { ?>
                                    <div title="Change profile picture" data-toggle="tooltip" class="mt-2 d-none d-md-block">
                                        <span class="form-control-file font-weight-lighter text-primary cursor"><i class="fa fa-image"></i> Change Cover Picture</span>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php if($isOfficial) { ?>
                                <div class="d-none d-md-block">
                                    <button data-function="load-form" data-module="manage_company_information" data-module-id="<?= $companyId ?>" class="btn btn-outline-primary btn-icon-text btn-edit-profile">
                                        <i data-feather="edit" class="btn-icon-prepend"></i> Update
                                    </button>
                                    <?php if($isCommission_Permitted) { ?>
                                    <button data-toggle="modal" data-target="#commissionModal" class="btn btn-outline-warning btn-icon-text">
                                        <i data-feather="edit" class="btn-icon-prepend"></i> Agent's Commission
                                    </button>
                                    <?php } ?>
                                    <a href="<?= $baseUrl ?>companies-view/<?= $companyId ?>?viewas=true" class="btn p-1 btn-outline-success"><i class="fa fa-eye"></i> View as visitor</a>
                                </div>
                                <?php } ?>
                                <?php if(isset($_GET["viewas"])) { ?>
                                <div class="d-none d-md-block">
                                    <a href="<?= $baseUrl ?>companies-view/<?= $companyId ?>" class="btn p-1 btn-outline-success"><i class="fa fa-eye"></i> Exit View As</a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="header-links">

                        </div>
                    </div>
                </div>

            </div>

            <div class="profile-container">
                
                <div class="row profile-body">

                    <div class="d-md-block mb-3 col-md-6 col-xl-3 right-wrapper">
                        
                        <div class="row">
                            
                            <div class="col-lg-12">
                                <div class="row" id="quick_minimal_load" data-stream="company_policy,profile_post_photos" data-stream_id="<?= $companyData->item_id ?>">
                                    
                                    <?php if($isOfficial) { ?>
                                    
                                    <?php } ?>

                                    <div class="col-lg-12 col-md-6 grid-margin">
                                        <div class="card rounded">
                                            <div class="card-body pb-2 pr-2 pl-2">
                                                <div class="row pl-3 pr-2 mb-1 justify-content-between">
                                                    <div><h6 class="card-title mb-1 pb-2">Managers</h6></div>
                                                    <?php if($isOfficial) { ?>
                                                    <div><span data-function="load-form" data-toggle="tooltip" title="Add new manager" data-module="company_manager" class="text-primary p-1 cursor"><i class="fa fa-user"></i> Add</span></div>
                                                    <?php } ?>
                                                </div>
                                                <div data-threads="company_managers" class="border-top pt-2 slim-scroll" style="max-height:500px; overflow-y:auto">
                                                    <?php if(!$companyData->managers) { ?>
                                                    <p class="font-italic text-center">No defined company managers</p>
                                                    <?php } else { ?>
                                                        <?php foreach($companyData->managers as $eachUser) { ?>
                                                            <?php /** show if not deleted */ if(!isset($eachUser->is_deleted) || (isset($eachUser->is_deleted) && !$eachUser->is_deleted)) { ?>
                                                                <div data-record-setid="<?= $eachUser->manager_id ?>" title="Click to preview the manager information" class="policy_item border-bottom mb-2 pb-2">
                                                                    <div class="d-flex align-items-center hover-pointer">
                                                                        <?php if(isset($eachUser->picture)) { ?>
                                                                        <img data-load-id="<?= $eachUser->manager_id ?>" onclick="return preview_manager_information('<?= explode("_", $eachUser->manager_id)[1] ?>')" class="img-xs cursor rounded-circle" src="<?= $baseUrl ?><?= $eachUser->picture ?>" alt="">
                                                                        <?php } ?>
                                                                        <div data-load-id="<?= $eachUser->manager_id ?>" onclick="return preview_manager_information('<?= explode("_", $eachUser->manager_id)[1] ?>')" class="ml-2">
                                                                            <p class="font-weight-bold"><?= $eachUser->fullname ?></p>
                                                                            <p class="tx-11 text-muted"><i class="fa fa-tags"></i> <?= $eachUser->position ?></p>
                                                                            <p class="tx-11 text-muted"><i class="fa fa-phone"></i> <?= $eachUser->primary_contact ?></p>
                                                                            <?php if(!empty($eachUser->secondary_contact)) { ?>
                                                                            <p class="tx-11 text-muted"><i class="fa fa-phone-square"></i> <?= $eachUser->secondary_contact ?></p>
                                                                            <?php } ?>
                                                                            <p class="tx-11 text-muted"><i class="fa fa-envelope"></i> <a href="mailto:<?= $eachUser->email ?>"><?= $eachUser->email ?></a></p>
                                                                        </div>
                                                                    </div>
                                                                    <?php if($isOfficial) { ?>
                                                                    <div class="d-flex justify-content-between mt-1 border-top pt-2">
                                                                        <span title="Update manager information" data-toggle="tooltip" class="text-primary p-1" data-function="load-form" data-module="company_manager" data-module-id="<?= $eachUser->manager_id ?>"><i class="fa fa-edit"></i> Update</span>
                                                                        <span title="Delete this manager" onclick="delete_record('company_manager', '<?= $eachUser->manager_id ?>')" data-toggle="tooltip" class="text-danger p-1"><i class="fa fa-trash"></i></span>
                                                                    </div>
                                                                    <?php } ?>
                                                                </div>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-6 grid-margin">
                                        <div class="card rounded">
                                            <div class="card-body pb-2 pr-2 pl-2">
                                                <div class="row mb-1 border-bottom mr-1 ml-1 justify-content-between">
                                                    <div><h6 class="card-title mb-1 pb-2">Policies (<span data-record="company_policy_count">0</span>)</h6></div>
                                                    <div><a title="<?= $isOfficial ? "Manage" : "View" ?> Policies" data-toggle="tooltip" href="<?= $baseUrl ?>policy-list<?= !$isOfficial ? "?company_id={$companyId}" : null ?>"><i class="fa fa-list"></i> <?= $isOfficial ? "Manage" : "View" ?> Policies</a></div>
                                                </div>
                                                <div data-threads="company_policy" class="slim-scroll" style="max-height:500px; overflow-y:auto"><p class="font-italic text-center">No enrolled policies</p></div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                        </div>
                        
                    </div>

                    <div class="col-md-6 col-xl-6 mb-3 middle-wrapper">
                        <div class="slim-dscroll" data-style="max-height:1050px; overflow-y:auto">
                            
                            <div class="profile_interaction_wrapper">
                                <?php if($isOfficial) { ?>
                                    <div class="create-post-container mb-4">
                                        <div class="card rounder">
                                            <div class="card-body p-3">
                                                <div class="d-flex align-items-left">
                                                    <p>
                                                        <span class="cursor" onclick="return user_basic_information('<?= $loggedUserId ?>');"><img src="<?= $baseUrl ?><?= $userData->image ?>" class="profile-image" alt=""></span>
                                                    </p>
                                                    <p style="width:100%;" class="ml-3">
                                                        <button data-company-id="<?= $companyId ?>" class="btn create-post-button btn-secondary btn-block">Create Post</button>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div id="profile_posts_list"class="mt-3" data-id="<?= $companyId ?>" data-autoload="true" data-last-post-id=""></div>
                                <div class="load-more mt-3 text-center"><button id="load-more-posts" type="button" class="btn btn-outline-secondary">Loading posts</button></div>
                            </div>

                            <div id="profile_content_loader"></div>
                            <div id="profile_content_display" class="p-0 m-0"></div>

                        </div>
                    </div>

                    <div class="col-md-12 d-xl-block  col-xl-3 left-wrapper">
                        
                        <div class="card rounded d-none d-xl-block mb-3">
                            <div class="card-header pb-0">
                                <div class="row justify-content-between">
                                    <div class="card-title pl-2"><i class="fa fa-award"></i> Awards</div>
                                    <?php if($isOfficial) { ?>
                                        <div><span data-function="load-form" data-toggle="tooltip" title="Add new award" data-module="company_award" class="text-primary p-1 cursor"><i class="fa fa-plus"></i> Add</span></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="card-body p-1">
                                <?php if(!$companyData->awards) { ?>
                                <p class="font-italic text-center">No listed awards</p>
                                <?php } else { ?>
                                    <?php foreach($companyData->awards as $eachAward) { ?>
                                        <?php /** show if not deleted */ if(!isset($eachAward->is_deleted) || (isset($eachAward->is_deleted) && !$eachAward->is_deleted)) { ?>
                                            <div data-record-setid="<?= $eachAward->award_id ?>" title="Click to preview the award information" class="policy_item border-bottom mb-2 pb-2">
                                                <div class="d-flex align-items-center hover-pointer">
                                                    <?php if(isset($eachAward->picture)) { ?>
                                                    <img data-load-id="<?= $eachAward->award_id ?>" onclick="return preview_manager_information('<?= explode("_", $eachAward->award_id)[1] ?>')" class="img-xs cursor rounded-circle" src="<?= $baseUrl ?><?= $eachAward->picture ?>" alt="">
                                                    <?php } ?>
                                                    <div data-load-id="<?= $eachAward->award_id ?>" onclick="return preview_award_information('<?= explode("_", $eachAward->award_id)[1] ?>')" class="ml-2">
                                                        <p class="font-weight-bold"><?= $eachAward->title ?></p>
                                                        <p class="tx-11 text-muted"><i class="fa fa-home"></i> <?= $eachAward->institution ?></p>
                                                        <p class="tx-11 text-muted"><i class="fa fa-calendar-check"></i> <strong>Award Date:</strong> <?= $eachAward->award_date ?></p>
                                                        <p class="tx-11 text-muted"><i class="fa fa-tags"></i> <strong>Category:</strong> <?= $eachAward->category ?></p>
                                                    </div>
                                                </div>
                                                <?php if($isOfficial) { ?>
                                                <div class="d-flex justify-content-between mt-1 border-top pt-2">
                                                    <span title="Update award information" data-toggle="tooltip" class="text-primary p-1" data-function="load-form" data-module="company_award" data-module-id="<?= $eachAward->award_id ?>"><i class="fa fa-edit"></i> Update</span>
                                                    <span title="Delete this award" onclick="delete_record('company_award', '<?= $eachAward->award_id ?>');" data-toggle="tooltip" class="text-danger p-1"><i class="fa fa-trash"></i></span>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="card rounded">
                            <div class="card-body slim-scroll" style="max-height: 700px; overflow-y: auto;">
                                <div class="d-flex align-items-center justify-content-between mb-2">
                                    <h6 class="card-title mb-0">About</h6>
                                </div>
                                <div class="text-muted" style="max-height: 200px; overflow:hidden"><?= $companyData->description ?></div>
                                <div class="mt-3">
                                    <label class="tx-11 font-weight-bold mb-0 text-uppercase">Created:</label>
                                    <p class="text-muted"><?= date("F d, Y", strtotime($companyData->date_created)) ?></p>
                                </div>
                                <div class="mt-3">
                                    <label class="tx-11 font-weight-bold mb-0 text-uppercase">Date of Incorporation:</label>
                                    <p class="text-muted"><?= $companyData->establishment_date ?></p>
                                </div>
                                <div class="mt-3">
                                    <label class="tx-11 font-weight-bold mb-0 text-uppercase">Registration Date:</label>
                                    <p class="text-muted"><?= $companyData->registration_date ?></p>
                                </div>
                                <div class="mt-3">
                                    <label class="tx-11 font-weight-bold mb-0 text-uppercase">License Number:</label>
                                    <p class="text-muted"><?= $companyData->license_id ?></p>
                                </div>
                                <div class="mt-3">
                                    <label class="tx-11 font-weight-bold mb-0 text-uppercase">Address:</label>
                                    <p class="text-muted"><?= $companyData->address ?></p>
                                </div>
                                <div class="mt-3">
                                    <label class="tx-11 font-weight-bold mb-0 text-uppercase">Email:</label>
                                    <p class="text-muted"><a href="mailto:<?= $companyData->email ?>"><?= $companyData->email ?></a></p>
                                </div>
                                <div class="mt-3">
                                    <label class="tx-11 font-weight-bold mb-0 text-uppercase">Contact Number:</label>
                                    <p class="text-muted"><a href="tel:<?= $companyData->contact ?>"><?= $companyData->contact ?></a> <?= !empty($companyData->contact_2) ? " / <a href='tel:{$companyData->contact}'>{$companyData->contact_2}</a>" : null; ?></p>
                                </div>
                                <div class="mt-3">
                                    <label class="tx-11 font-weight-bold mb-0 text-uppercase">Website:</label>
                                    <p class="text-muted"><a href="<?= $companyData->website ?>" target="_blank"><?= $companyData->website ?></a></p>
                                </div>
                                <div class="mt-3 border-top pt-3 d-flex social-links">
                                    <?php 
                                    if(is_object($companyData->social_media)) {
                                        foreach($companyData->social_media as $key => $link) { ?>
                                            <a target="_blank" class="btn d-flex align-items-center justify-content-center border mr-2 btn-icon <?= $key ?>" href="<?= $link ? $link : "javascript:void(0);" ?>" title="<?= ucwords($key) ?>">
                                                <i data-feather="<?= $key ?>" data-toggle="tooltip" title="<?= $link ?>"></i>
                                            </a>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if($isOfficial) { ?>
                                    <p title="Update Social Media Links" data-toggle="tooltip" style="position:absolute;right:20px;">
                                        <span class="text-primary cursor"><i class="fa fa-edit"></i> Edit</span>
                                    </p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class="card rounded d-none d-xl-block mt-3">
                            <div class="card-body pb-2 pr-2 pl-2">
                                <h6 class="card-title mb-1 border-bottom pb-2">Photos</h6>
                                <div data-threads="profile_post_photos" class="slim-scroll"><p class="font-italic text-center">No photos added</p></div>
                            </div>
                        </div>

                    </div>

                </div>
                
            </div>

        </div>
        
        <?php if($isOfficial) { ?>
        <div class="modal fade" id="createPostModal" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-lg modal-dialog-top" style="width:100%;" role="document">
                <div class="modal-content">
                    <?= form_loader() ?>
                    <div class="modal-header">
                        <h5 class="modal-title">Create Post</h5>
                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                    </div>
                    <div class="modal-body" data-scrolling="false" style="text-align:left">
                        <trix-editor id="create-post-content"></trix-editor>
                        <?= $formsObj->comments_form_attachment_placeholder($form_params); ?>
                    </div>
                    <div class="modal-footer">
                        <button class="btn share-post btn-outline-success">Post Comment</button>
                        <button class="btn cancel-post btn-outline-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="changeUsername" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-md modal-dialog-top" style="width:100%;" role="document">
                <div class="modal-content">
                    <?= form_loader() ?>
                    <div class="modal-header">
                        <h5 class="modal-title">Change Username</h5>
                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="username">Company Username</label>
                            <input type="text" value="<?= !$companyData->username ? $companyData->name : $companyData->username ?>" name="username" id="username" class="form-control">
                            <input type="hidden" value="<?= $companyId ?>" name="company_id" disabled id="company_id" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn share-post btn-outline-success">Save</button>
                        <button class="btn cancel-post btn-outline-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="profilePicture" <?= !$auto_close_modal ? null :  'data-backdrop="static" data-keyboard="false"'; ?>>
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Update Cover Photo</h5>
                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-4 text-left">
                                <input type="file" accept="image/*" name="file_upload" id="file_upload">
                            </div>
                            <div class="upload_file_url" data-record-id="<?= $companyId ?>" data-button="save-company-cover-picture" data-url="<?= $baseUrl ?>api/company/manage_commissionfiles/preview"></div>
                            <div class="col-lg-8 text-center">
                                <div class="file-upload-preview">
                                    <?php if(!empty($session->tempProfilePicture)) { ?>
                                        <img style="width:100%" class="img-fluid" src="<?= $baseUrl.$session->tempProfilePicture ?>">
                                        <button class="btn btn-outline-success btn-sm mt-2 btn-block" id="save-company-cover-picture" data-user-id="<?= $companyId ?>">Save Image</button>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>

    <?php } ?>
</div>
<?php require "foottags.php"; ?>