<?php
// set the title
$page_title = "Administrators List";

// require the headtags
require "headtags.php";

// set the item
$session->user_type_to_add = $userData->user_type;
?>
<div class="page-content">
    <?php if(!$accessObject->hasAccess("view", "users")) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        <?= form_loader("fixed"); ?>
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <?php if($accessObject->hasAccess("add", "users")) { ?>
                <a href="javascript:void(0)" data-function="load-form" data-resource="<?= $userData->user_type ?>" data-module="user_account" type="button" class="btn btn-primary btn-icon-text mb-2 mb-md-0">
                    <i class="btn-icon-prepend fa fa-user-plus"></i>
                    Add Admin
                </a>
                <?php } ?>
            </div>
        </div>

        <div class="col-lg-12 p-0 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline mb-2">
                        <h6 class="card-title mb-0"></h6>
                    </div>
                    <div class="table-responsive slim-scroll">
                        <table class="table dataTable table-hover mb-0" data-noinit="datatable" data-user_type="<?= $userData->user_type ?>" id="admins_list">
                            <thead>
                                <tr>
                                    <th width="6%" class="pt-0">#</th>
                                    <th class="pt-0">Name</th>
                                    <th class="pt-0">Contact Details</th>
                                    <th width="6%">Status</th>
                                    <th class="pt-0"></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    
</div>
<?php require "foottags.php"; ?>