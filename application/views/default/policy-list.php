<?php
// set the title
$page_title = "Company Insurance Policies";

// require the headtags
require "headtags.php";
?>
<div class="page-content">
    
    <?php if(!$accessObject->hasAccess("view", "company_policy")) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        <?= form_loader("fixed"); ?>    
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <?php if($accessObject->hasAccess("add", "company_policy")) { ?>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <a href="<?= $baseUrl ?>policy-add" class="btn btn-primary btn-icon-text mb-2 mb-md-0">
                    <i class="btn-icon-prepend" data-feather="folder-plus"></i>
                    Add Policy
                </a>
            </div>
            <?php } ?>
        </div>

        <div class="col-lg-12 p-0 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive slim-scroll">
                        <table data-company-id="<?= isset($_GET["company_id"]) ? xss_clean($_GET["company_id"]) : null ?>" class="table dataTable table-hover mb-0" id="company_policies">
                            <thead>
                                <tr>
                                    <th class="pt-0">#</th>
                                    <th width="30%" class="pt-0">Policy Info</th>
                                    <th class="pt-0">Category</th>
                                    <th class="pt-0">Year Enrolled</th>
                                    <th class="pt-0">Status</th>
                                    <th class="pt-0"></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    
</div>
<?php require "foottags.php"; ?>