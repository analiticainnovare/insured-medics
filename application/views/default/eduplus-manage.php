<?php
// set the title
$page_title = "InsureEdu Plus";

// extra resources
$loadedJS = ["assets/js/script/eduplus.js"];

$eduClass = load_class("eduplus", "controllers");

// require the headtags
require "headtags.php";

// set the user id
$param = (object) [
    "userData" => $userData,
    "userId" => $loggedUserId
];
$params = (object) [];

// unset all admin session
$session->is_Admin_View = true;

$topics = 0;
$categories = 0;
$organic_views = 0;
$viral_views = 0;

// set the subject id
$topicFound = false;
?>
<div class="page-content">
    <?php if(!$accessObject->hasAccess("eduplus", "control")) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <a href="<?= $baseUrl ?>eduplus" class="btn btn-primary btn-icon-text mb-2 mb-md-0">
                    <i class="btn-icon-prepend" data-feather="book-open"></i> InsuredEdu Plus
                </a>
            </div>
            
        </div>
        <div class="row">

            <div class="col-lg-9 col-md-8">

                <div class="d-flex mb-2 justify-content-between">
                    <div><h6 class="card-title">Category Management</h6></div>
                    <div><button data-function="load-form" data-module="eduplus_category" title="Add new Category" class="btn p-1 btn-sm btn-outline-primary"><i class="fa fa-plus"></i> Add Category</button></div>
                </div>
                <div>
                    
                    <div id="accordion" class="accordion" role="tablist">
                        <?php foreach($eduClass->category_list($params)["data"] as $key => $category) { ?>
                            <?php
                            // increment
                            $categories++;
                            // set the category id to the parameter
                            $param->category_id = $category->item_id;
                            // load the topics for each category
                            $topics_list = $eduClass->subject_list($param)["data"];
                            ?>
                            <div class="card mb-3 eduplus_category_list" data-row_id="<?= $key ?>" data-record-setid="<?= $category->item_id ?>">
                                <div class="card-header" role="tab" id="heading_<?= $category->item_id ?>">
                                    <h6 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" href="#collapse_<?= $category->item_id ?>" aria-expanded="false" aria-controls="collapse_<?= $category->item_id ?>">
                                            <?= $category->name ?>
                                        </a>
                                    </h6>
                                </div>
                                <div id="collapse_<?= $category->item_id ?>" class="collapse" role="tabpanel" aria-labelledby="heading_<?= $category->item_id ?>" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="d-flex justify-content-between">
                                            <div style="width:60%">
                                                <table class="border-0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="font-weight-bolder">Last Updated:</td>
                                                            <td><?= $category->last_updated ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div>
                                                <button data-function="load-form" data-module="eduplus_category" data-module-id="<?= $category->item_id ?>" title="Update this Category" class="btn btn-sm btn-outline-success"><i style="font-size:12px;" class="fa fa-edit"></i> Edit</button>
                                                <button data-function="load-form" data-module="eduplus_subject" data-module-id="<?= $category->item_id ?>" class="btn btn-sm btn-outline-primary"><i style="font-size:12px;" class="fa fa-plus"></i> New Topic</button>
                                                <button onclick="return delete_record('eduplus_category','<?= $category->item_id ?>');" title="Delete this Category" class="btn btn-sm btn-outline-danger"><i style="font-size:12px;" class="fa fa-trash"></i></button>
                                            </div>
                                        </div>
                                        <?= !empty($category->description) ? "<div class=\"mt-3 pt-3 border-top\">{$category->description}</div>" : ""; ?>
                                        <div class="mt-3">
                                            <h5 class="text-uppercase border-bottom mb-3">Category Topics</h5>
                                            <div class="row mt-3">
                                                <?php
                                                // get the items count
                                                if(!empty($topics_list)) {
                                                    // loop through the items
                                                    foreach($topics_list as $ikey => $subject) {
                                                        // increment
                                                        $topics++;
                                                        $organic_views += $subject->organic_views;
                                                        $viral_views += $subject->viral_views;
                                                        ?>
                                                        <div class="col-lg-4 col-md-6 mb-3" data-row_id="<?= $ikey ?>" data-record-setid="<?= $subject->item_id ?>">
                                                            <div class="card">
                                                                <div class="card-header p-2">
                                                                    <?= str_word_count($subject->subject) > 4 ? "<span  title='{$subject->subject}' data-toggle='tooltip'>".limit_words($subject->subject, 4). "...</span>" : $subject->subject ?>
                                                                </div>
                                                                <div class="card-body p-2 mb-2 slim-scroll" style="min-height:120px; max-height:120px; overflow-y:auto">
                                                                    <?= limit_words($subject->description, 50) ?>
                                                                </div>
                                                                <div class="card-footer p-1">
                                                                    <div class="d-flex justify-content-between">
                                                                        <div>
                                                                            <a href="<?= $baseUrl ?>eduplus-view/<?= $subject->item_id ?>" class="btn btn-sm btn-outline-primary"><i style="font-size:12px" class="fa fa-eye"></i> View</a>
                                                                        </div>
                                                                        <div>
                                                                            <button data-function="load-form" data-module="eduplus_subject" data-module-id="<?= $category->item_id ?>_<?= $subject->item_id ?>" class="btn btn-sm btn-outline-success"><i style="font-size:12px;" class="fa fa-edit"></i> Edit</button>
                                                                            <button onclick="return delete_record('eduplus_subject','<?= $subject->item_id ?>');" title="Delete this Topic" class="btn btn-sm btn-outline-danger"><i style="font-size:12px;" class="fa fa-trash"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php }
                                                } else { ?>
                                                <div class="col-lg-12 font-italic">Sorry! No topics have been uploaded yet.</div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        
                    </div>

                </div>

            </div>

            <div class="col-lg-3 col-md-4">

                <div class="card">
                    <div class="card-header">
                        <h5 class="text-uppercase">Statistics</h5>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <tr>
                                <td>Categories</td>
                                <td><?= $categories ?></td>
                            </tr>
                            <tr>
                                <td>Topics</td>
                                <td><?= $topics ?></td>
                            </tr>
                            <tr>
                                <td>Organic Views</td>
                                <td><?= $organic_views ?></td>
                            </tr>
                            <tr>
                                <td>Inorganic Views</td>
                                <td><?= $viral_views ?></td>
                            </tr>
                        </table>
                    </div>
                </div>

            </div>

        </div>
    <?php } ?>

</div>
<?php require "foottags.php"; ?>