<?php
// set the title
$page_title = "Report Request";

// require the headtags
require "headtags.php";

// extra css
$loadedJS = [
    "assets/js/ajax-load.js",
    "assets/js/script/forms.js"
];

// get some variable
$itemFound = false;
$recordId = confirm_url_id(1) ? xss_clean($SITEURL[1]) : null;

// load the complaint information
if($recordId) {
    
    /** create a new object of the company_policy class */
    $recordObj = load_class("reports", "controllers");

    /** Parameters */
    $param = (object) [
        "request_id" => $recordId,
        "userData" => $userData,
        "userId" => $userData->user_id,
        "limit" => 1
    ];

    // return the result
    $recordData = $recordObj->requests_list($param);
    
    $itemFound = !empty($recordData["data"]) ? true : false;

    // if the complaint was found
    if($itemFound) {
        // extra js
        $loadedJS = [
            "assets/js/script/comments.js",
            "assets/js/script/attachments.js",
        ];

        // create a new forms object
        $formsObj = load_class("forms", "controllers");

        // set the complaint data
        $isClosed = false;
        $recordData = $recordData["data"][0];
        // $recordData->attachment = (array) $recordData->attachment;

        // get the category 
        $isApproved = (bool) (in_array($recordData->state, ["Approved", "Generated"]));
        $isGenerated = (bool) (in_array($recordData->state, ["Generated"]));
 
    }

}
// review command
$isPermitted = $accessObject->hasAccess("generate", "reports");
?>

<div class="page-content">
<?php if(!$accessObject->hasAccess("view", "reports") || !$itemFound) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <a href="<?= $baseUrl ?>reports-request/" class="btn btn-primary btn-icon-text mb-2 mb-md-0">
                    <i class="btn-icon-prepend" data-feather="arrow-left"></i>
                    View Request List
                </a>
            </div>
        </div>
        <div class="col-lg-12 p-0 col-xl-12 inbox-wrapper">
            <div class="card">
                <div class="card-body">

                    <div class="row">

                        <div class="col-lg-3 email-aside border-lg-right">
                            <div class="aside-content">
                                <div class="aside-header">
                                    <button class="navbar-toggle" data-target=".aside-nav" data-toggle="collapse" type="button"><span class="icon"><i data-feather="chevron-down"></i></span></button>
                                    <span class="title"><h4>Request Details</h4></span>
                                </div>
                                <?php /** If the user has the permission to review policies */ if($isPermitted) { ?>
                                    <div class="aside-compose" data-toggle="tooltip" title="Reply to Client Policy"><a data-function="load-form" data-resource="user_policy" data-module-id="<?= $recordId ?>" data-module="share_a_reply" class="btn btn-primary btn-block" href="javascript:void(0)">Add Feedback</a></div>
                                <?php } ?>
                                <div class="aside-nav collapse">
                                    <?php if($isPermitted) { ?>
                                    <div class="mb-4">
                                        <ul class="nav mb-3">
                                            <li class="active">
                                                <a data-toggle="tooltip" title="View All Replies" data-function="load-replies-form" data-module="view_replies_list" data-resource="user_policy" data-module-id="<?= $recordId ?>" href="javascript:void(0)">
                                                    <span class="icon"><i data-feather="inbox"></i></span>Officials Feedback &nbsp;<span class="smaller-font"> (Admin only)</span>
                                                    <span data-record="replies_count" data-id="<?= $recordId ?>" class="badge badge-danger-muted text-white font-weight-bold float-right">0</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <?php } ?>
                                    <div class="col-lg-12 border-bottom mb-2 mt-3 pl-0 pb-1">
                                        <h6><strong>INSURANCE COMPANY INFO</strong></h6>
                                    </div>
                                    <div class="d-flex border-bottom mb-2 pb-3 justify-content-start">
                                        <div>
                                            <p><span class="underline" onclick="return company_basic_information('<?= $recordData->company_id ?>')"><?= $recordData->company_info->name ?></span></p>
                                            <p><i class="fa fa-phone"></i> <?= $recordData->company_info->contact ?></p>
                                            <p><i class="fa fa-envelope"></i> <?= $recordData->company_info->email ?></p>
                                            <p><i class="fa fa-globe"></i> <?= $recordData->company_info->website ?></p>
                                        </div>
                                    </div>
                                    <div>
                                        <p><i class="fa fa-globe"></i> <?= strtoupper($recordData->request_type) ?></p>
                                        <p><strong>Start Date</strong>: <i class="fa fa-calendar"></i> <?= $recordData->start_date ?></p>
                                        <p><strong>End Date</strong>: <i class="fa fa-calendar-check"></i> <?= $recordData->end_date ?></p>
                                        <p><strong>Status: </strong><?= $recordData->the_status_label ?></p>
                                        <div class="border-bottom mt-1 mb-1"></div>
                                        <p><strong>Expected Date</strong>: <?= $recordData->expected_date ?></p>
                                    </div>
                                    <div class="card mt-3 mb-3">
                                        <div class="card-body p-2">
                                            <?= $recordData->description ?>
                                        </div>
                                    </div>
                                    <?php if($isGenerated) { ?>
                                        <div class="mt-3 justify-content-center row form-group">
                                            <a data-toggle="tooltip" href="<?= $recordData->download_url ?>" target="_blank" title="Click to download generated report" data-resource="reports_request" data-module-id="<?= $recordId ?>" class="btn btn-outline-success"><i class="fa fa-download"></i> Download Report</a>
                                        </div>
                                    <?php } ?>
                                    <?php if($isPermitted) { ?>
                                        <div class="mt-3 justify-content-center row form-group">
                                            <button data-toggle="tooltip" title="View summary history" data-function="load-form" data-resource="reports_request" data-module-id="<?= $recordId ?>" data-module="activity_history" class="btn btn-outline-secondary">View activity history</button>
                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>

                        <div class="col-lg-9 email-content">

                            <div class="email-head">
                                <div class="email-head-subject p-2 mr-0 pr-0">
                                    <div class="title d-flex align-items-center justify-content-between">
                                        <div class="d-flex align-items-center">
                                            <span><h4><?= strtoupper($recordData->subject) ?></h4></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row slim-scroll">

                                <div class="col-lg-8 mb-3">

                                    <div class="p-0 m-0">
                                        <?php if(!$isApproved) { ?>
                                            <?= leave_comments_builder("report_requests", $recordId, "Leave a comment below") ?>
                                        <?php } ?>
                                        <div id="comments-container" data-autoload="true" data-last-reply-id="0" data-id="<?= $recordId ?>" class="slim-scroll pt-3 mt-3 pr-2 pl-0" style="overflow-y:auto; max-height:850px"></div>
                                        <div class="load-more mt-3 text-center"><button id="load-more-replies" type="button" class="btn btn-outline-secondary">Loading comments</button></div>
                                    </div>

                                </div>
                                

                                <div class="col-lg-4 p-0">

                                    <div class="email-attachments border-top-0 p-0 pt-3">

                                        <div class="grid-margin mb-2 pb-3">
                                            
                                            <form class="app-data-form" autocomplete="Off" <?= $isPermitted && !$isClosed ? "action=\"{$baseUrl}api/reports/update_request\" " : null ?>method="POST">
                                                
                                                <div class="col-lg-12 border-bottom mb-2 pl-0 pb-1">
                                                    <h6><strong>REQUEST PLACED BY</strong></h6>
                                                </div>
                                                <div class="d-flex border-bottom mb-2 pb-3 justify-content-start">
                                                    <div class="mr-2">
                                                        <img class="user_image cursor" onclick="return user_basic_information('<?= $recordData->created_by ?>')" src="<?= $baseUrl ?><?= $recordData->requested_by_info->image ?>">
                                                    </div>
                                                    <div>
                                                        <p><span class="underline" onclick="return user_basic_information('<?= $recordData->created_by ?>')"><?= $recordData->requested_by_info->name ?></span></p>
                                                        <p><span class="text-muted">Contact:</span> <i class="fa fa-phone"></i> <?= $recordData->requested_by_info->contact ?></p>
                                                        <p><span class="text-muted">Email:</span> <i class="fa fa-envelope"></i> <?= $recordData->requested_by_info->email ?></p>
                                                        <p><span class="text-muted">Date:</span> <i class="fa fa-calendar-check"></i> <?= $recordData->date_created ?></p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 border-bottom mb-2 pt-1 pl-0 pb-1">
                                                    <h6><strong>REPORT GENERATED</strong></h6>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label for="">Start Date</label>
                                                            <input <?= !$isPermitted || $isApproved ? "disabled" : 'name="start_date" id="start_date"' ?> value="<?= $recordData->start_date ?>" type="text" class="form-control datepicker">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label for="">End Date</label>
                                                            <input value="<?= $recordData->end_date ?>" type="text" <?= !$isPermitted || $isApproved ? "disabled" : 'name="end_date" id="end_date"' ?> class="form-control datepicker">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label for="export_type">Export Format</label>
                                                            <select <?= !$isPermitted || $isApproved ? "disabled" : 'name="export_type" id="export_type"' ?> class="selectpicker">
                                                                <option value="pdf">PDF Format</option>
                                                                <option value="excel">Excel Format</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <?php if($isPermitted) { ?>
                                                    <div class="col-lg-12">
                                                        <div class="form-group" data-record_id="<?= $recordData->record_id ?>" data-company_id="<?= $recordData->company_id ?>" data-request_item="<?= $recordData->request_type ?>" data-request_type="report_request"></div>
                                                    </div>
                                                    <?php } ?>
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label for="state">Current Status <?php if($isPermitted) { ?><small class="required">(Once approved, form cannot be updated)</small><?php } ?></label>
                                                            <select data-width="100%" <?= !$isPermitted || $isApproved ? "disabled" : 'name="state" id="state"' ?> class="selectpicker">
                                                                <option <?= $recordData->state == "Pending" && $recordData->state !== "In Review"  ? "selected" : "disabled" ?> value="Pending">Pending</option>
                                                                <option <?= $recordData->state == "In Review" && $recordData->state !== "Approved" ? "selected" : (($recordData->state == "Approved") ? "disabled" : null) ?> value="In Review">In Review</option>
                                                                <option <?= $recordData->state == "Approved" ? "selected" : null ?> <?= $recordData->state == "Approved" ? "disabled" : null ?> value="Approved">Approved</option>
                                                                <option value="Generated" disabled>Generated</option>
                                                                <option disabled <?= $recordData->state == "Cancelled" ? "selected" : null ?> value="Cancelled">Request Cancelled</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <?php if($isPermitted && !$isApproved) { ?>
                                                    <div class="col-lg-12">
                                                        <div class="form-group text-right">
                                                            <input type="hidden" name="request_id" id="request_id" value="<?= $recordId ?>" class="form-control">
                                                            <button type="submit" class="btn btn-outline-success">Save Changes</button>
                                                        </div>
                                                    </div>
                                                    <?php } ?>

                                                </div>

                                            </form>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                    </div>                    
                    
                </div>
            </div>
        </div>

    <?php } ?>    
</div>
<?php require "foottags.php"; ?>