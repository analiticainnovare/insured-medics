<?php
// set the title
$page_title = "404 - Page Not Found";

// additional css and javascript files
$loadedCSS = [];

// global variables
global $availableQuickLinks;

// require the headtags
require "headtags.php";

// set the current as as the person logged in
$cUserData = $userData;

// is this the current user?
$isAdmin = $userData->user_type == "admin" ? true : false;
?>
<div class="page-content">
    <?php if(empty($cUserData)) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        <div class="row">
            <div class="col-md-12 text-center">
                <img width="350px" src="<?= $baseUrl ?>assets/images/404.svg" class="img-fluid mb-2" alt="404">
            </div>
            <div class="col-md-12 mt-4 text-center border-top pt-4">
                <h6 class="text-muted mb-3 text-center">Oopps!! The page you were looking for doesn't exist or is under construction.</h6>
                <a href="<?= $baseUrl ?>dashboard" class="btn btn-primary">Back to home</a>
            </div>
        </div>
    <?php } ?>
</div>
<?php require "foottags.php"; ?>