<?php 
// base url
$baseUrl = config_item("base_url");
// if the user is loggedin then redirect
if($usersClass->loggedIn()) {
    // redirect the page
    redirect( "{$baseUrl}dashboard", "refresh:10");
	exit(-1);
}

// set the access token
$accessToken = "null";

// create new object
$auth = load_class("auth", "controllers");
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Create Account - <?= config_item('site_name') ?></title>
	<link rel="stylesheet" href="<?= $baseUrl ?>assets/vendors/core/core.css">
	<link rel="stylesheet" href="<?= $baseUrl ?>assets/fonts/feather-font/css/iconfont.css">
	<link rel="stylesheet" href="<?= $baseUrl ?>assets/vendors/flag-icon-css/css/flag-icon.min.css">
	<link rel="stylesheet" href="<?= $baseUrl ?>assets/css/style/light-theme.css">
    <link href="<?= $baseUrl ?>assets/css/fontawesome.css"  rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?= $baseUrl ?>assets/css/custom.css">
    <link id="current_url" name="current_url" value="<?= current_url(); ?>">
    <link rel="shortcut icon" href="<?= $baseUrl ?>assets/images/favicon.png" />
    <style>
        .page-content {
            background-image: url("<?= $baseUrl ?>assets/images/carousel/img61.jpg");
            background-attachment: fixed;
            background-position: center;
            background-size: cover;
        }
    </style>
</head>
<body>
	<div class="main-wrapper">
		<div class="page-wrapper full-page">
			<div class="page-content d-flex align-items-center justify-content-center">                
                <div class="row w-100 mx-0 auth-page">
					<div class="col-md-8 col-xl-6 mx-auto">
						<div class="card">
							<div class="row">
                                <div class="col-md-3 pr-md-0">
                                    <div class="auth-left-wrapper">

                                    </div>
                                </div>
                                <div class="col-md-9 pl-md-0">
                                <div class="auth-form-wrapper px-4 py-5">
                                    <?= form_loader(); ?>
                                    <a href="<?= $baseUrl ?>" class="noble-ui-logo d-block mb-2">Insuretech <span>Hub</span></a>
                                    <h5 class="text-muted font-weight-normal mb-4">Create an account.</h5>
                                    <form method="POST" autocomplete="Off" action="<?= $baseUrl ?>api/auth" class="forms-sample">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="firstname">Firstname</label>
                                                    <input maxlength="64" required="required" type="text" class="form-control" name="firstname" id="firstname" placeholder="Enter firstname">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="lastname">Lastname</label>
                                                    <input maxlength="64" type="text" class="form-control" name="lastname" id="lastname" placeholder="Enter lastname">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="email">Email</label>
                                                    <input maxlength="64" type="email" class="form-control" name="email" id="email" placeholder="Enter email">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="phone">Contact</label>
                                                    <input maxlength="64" type="text" class="form-control" name="phone" id="phone" placeholder="Enter contact number">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="password">Password</label>
                                                    <input maxlength="32" type="password" class="form-control" name="password" id="password" autocomplete="current-password" placeholder="Password">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="password_2">Confirm Password</label>
                                                    <input maxlength="32" type="password" class="form-control" name="password_2" id="password_2" autocomplete="current-password" placeholder="Password">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mt-3">
                                            <button type="submit" class="btn btn-primary mr-2 mb-2 mb-md-0 text-white">Create Account</button>
                                        </div>
                                        <div class="mt-3">
                                            <a href="<?= $baseUrl ?>login" class="mt-3 text-muted">Already have an account? Login Here.</a>
                                        </div>
                                        <div class="form-results"></div>
                                    </form>
                                </div>
                                </div>
                            </div>
						</div>
					</div>
				</div>
            </div>
		</div>
	</div>
    <script>var baseUrl = "<?= $baseUrl ?>";</script>
	<script src="<?= $baseUrl ?>assets/vendors/core/core.js"></script>
	<script src="<?= $baseUrl ?>assets/vendors/feather-icons/feather.min.js"></script>
	<script src="<?= $baseUrl ?>assets/js/template.js"></script>
    <script src="<?= $baseUrl ?>assets/js/script/auth.js"></script>
</body>
</html>
