<?php
// set the title
$page_title = "Profile";

// global variables
global $availableQuickLinks;

// require the headtags
require "headtags.php";

// set the current as as the person logged in
$cUserData = $userData;

// if the user in view is not the same as the person logged in
if($cur_user_id !== $session->userId) {

    // the query parameter to load the user information
    $i_params = (object) ["limit" => 1, "user_id" => $cur_user_id];

    // append to the search filter
    if(!in_array($userData->user_type, ["insurance_company", "admin"])) {
        $i_params->created_by = $loggedUserId;
    }

    // if the user is with an insurance company
    if(in_array($userData->user_type, ["insurance_company"])) {
        $i_params->company_id = $userData->company_id;
    }

    // get the user data
    $cUserData = $usersClass->list($i_params)["data"];
    // if the current user data is not empty
    if(!empty($cUserData)) {
        $cUserData = $usersClass->list($i_params)["data"][0];
    } else {
        $cUserData = $userData;
    }
}

$cUserPrefs = $cUserData->preferences;

$user_types = [
    "admin" => "admin",
    "user" => "clients",
    "business" => "clients",
    "broker" => "brokers",
    "agent" => "agents",
    "insurance_company" => "users",
    "bank" => "users",
    "nic" => "users"
];
// is this the current user?
$current_user = ($cUserData->user_id == $userData->user_id);
$isAdmin = $userData->user_type == "admin" ? true : false;
$hasPermission = $isCommission_Permitted = $accessObject->hasAccess("permissions", "users");
$canUpdate = $accessObject->hasAccess("update", $user_types[$cUserData->user_type]);
// load the permissions javascript if user has the rights
if($hasPermission) {
    $loadedJS = [
        "assets/js/script/permissions.js",
    ];
}
?>
<div class="page-content">
    <?php if(empty($cUserData)) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>

        <div class="profile-page tx-13">
            
            <div class="row">
                <div class="col-12 grid-margin">
                    <div class="profile-header">
                        <div class="cover">
                            <div class="gray-shade"></div>
                            <figure>
                                <?php if(isset($cUserData->preferences->cover_image) && $cUserData->preferences->cover_image) { ?>
                                <img src="<?= $baseUrl ?><?= $cUserData->preferences->cover_image ?>" class="img-fluid" alt="profile cover">
                                <?php } else { ?>
                                <div class="p-5"></div>
                                <?php } ?>
                            </figure> 
                            <div class="cover-body d-flex justify-content-between align-items-center">
                                <div>
                                    <table>
                                        <tr>
                                            <td>
                                                <img title="Change profile picture" data-toggle="tooltip" id="form-control-file" class="cursor profile-pic" src="<?= $baseUrl ?><?= $cUserData->image ?>" alt="profile">
                                            </td>
                                            <td class="pl-3">
                                                <span class="profile-name ml-0">
                                                    <?= $cUserData->name ?><br>
                                                    <small class="small-font">(<?= strtoupper($cUserData->user_type_description) ?>)</small>
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                    <?php if($current_user || (!$current_user && $canUpdate)) { ?>
                                    <div title="Change profile picture" data-toggle="tooltip" class="mt-2">
                                        <span class="form-control-file font-weight-lighter text-primary cursor"><i class="fa fa-image"></i> Change Picture</span>
                                    </div>
                                    <?php } ?>
                                </div>
                                <div class="d-none d-md-block">
                                    <?php if($current_user || (!$current_user && $canUpdate)) { ?>
                                    <button class="btn btn-outline-primary btn-icon-text btn-edit-profile">
                                        <i data-feather="edit" class="btn-icon-prepend"></i> Edit profile
                                    </button>
                                    <?php } ?>
                                    <?php if($hasPermission && !$current_user) { ?>
                                        <button class="btn btn-outline-success btn-icon-text btn-user-permissions" data-user_id="<?= $cur_user_id ?>" data-user_access_level="<?= $cUserData->user_type ?>">
                                            <i class="fa fa-sitemap"></i> User Permissions
                                        </button>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="header-links">
                            <ul class="links d-flex align-items-center mt-3 mt-md-0">
                                <li class="header-link-item d-flex align-items-center">
                                    <i class="mr-1 icon-md" data-feather="columns"></i>
                                    <a data-function="load-form" data-resource="users" data-module-id="<?= $cUserData->user_id ?>" data-module="activity_history" class="pt-1px d-none d-md-block" data-loader="timeline" href="javascript:;">Activity Logs</a>
                                </li>
                                <li class="header-link-item ml-3 pl-3 border-left d-flex align-items-center">
                                    <i class="mr-1 icon-md fa-1x fa fa-weight-hanging"></i>
                                    <a class="pt-1px d-none d-md-block" data-loader="claims" href="javascript:;">Claims <span class="text-muted tx-12" data-id="<?= $cUserData->user_id ?>" data-record="claims_count">0</span></a>
                                </li>
                                <li class="header-link-item ml-3 pl-3 border-left d-flex align-items-center">
                                    <i class="mr-1 icon-md" data-feather="columns"></i>
                                    <a class="pt-1px d-none d-md-block" data-loader="policies" href="javascript:;">Policies <span class="text-muted tx-12" data-id="<?= $cUserData->user_id ?>" data-record="policies_count">0</span></a>
                                </li>
                                <?php if($current_user || $isAdmin) { ?>
                                <li class="header-link-item ml-3 pl-3 border-left d-flex align-items-center">
                                    <i class="mr-1 icon-md" data-feather="bell"></i>
                                    <a class="pt-1px d-none d-md-block" data-resource="notifications" data-function="load-form" data-module-id="<?= $cUserData->user_id ?>" data-module="user_notifications" data-loader="about" href="javascript:;">Notifications <span class="text-muted tx-12" data-id="<?= $cUserData->user_id ?>" data-record="notices_count">0</span></a>
                                </li>
                                <?php } ?>
                                <li class="header-link-item ml-3 pl-3 border-left d-flex align-items-center">
                                    <i class="mr-1 icon-md" data-feather="columns"></i>
                                    <a class="pt-1px d-none d-md-block" data-loader="complaints" href="javascript:;">Complaints <span class="text-muted tx-12" data-id="<?= $cUserData->user_id ?>" data-record="complaints_count">0</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="profile-container">
                
                <div class="row profile-body">
                    
                    <div class="d-md-block mb-3 col-md-4 col-xl-3 left-wrapper">
                        <div class="card rounded mb-3">
                            <div class="card-body slim-scroll" style="max-height: 600px; overflow-y: auto;">
                                <div class="d-flex align-items-center justify-content-between mb-2">
                                    <h6 class="card-title mb-0">About</h6>
                                    <div class="dropdown">
                                        <button class="btn p-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="icon-lg text-muted pb-3px" data-feather="more-horizontal"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item d-flex align-items-center update-profile" href="javascript:;"><i data-feather="edit" class="icon-sm mr-2"></i> <span class="">Edit</span></a>
                                            <a class="dropdown-item d-flex align-items-center update-profile" href="<?= $baseUrl ?>browse-files/user_profile/<?= $cUserData->user_id ?>"><i data-feather="file" class="icon-sm mr-2"></i> <span class="">View Files</span></a>
                                            <?php if($cur_user_id !== $loggedUserId) { ?>
                                                <a class="dropdown-item d-flex align-items-center send-message" href="javascript:;"><i class="icon-sm mr-2 fa fa-envelope"></i> <span class="">Send Message</span></a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <p><?= $cUserData->description ?></p>
                                <div class="mt-3">
                                    <label class="tx-11 font-weight-bold mb-0 text-uppercase">Created:</label>
                                    <p class="text-muted"><?= date("F d, Y", strtotime($cUserData->date_created)) ?></p>
                                </div>
                                <?php if($cur_user_id !== $loggedUserId) { ?>
                                <div class="mt-3">
                                    <label class="tx-11 font-weight-bold mb-0 text-uppercase">Current Status</label> <?= $medicsClass->the_status_label($cUserData->user_status) ?>
                                    <p class="text-muted"></p>
                                </div>
                                <?php } ?>
                                <div class="mt-3">
                                    <label class="tx-11 font-weight-bold mb-0 text-uppercase">Gender:</label>
                                    <p class="text-muted"><?= $cUserData->gender ?></p>
                                </div>
                                <div class="mt-3">
                                    <label class="tx-11 font-weight-bold mb-0 text-uppercase">Date of Birth:</label>
                                    <p class="text-muted"><?= $cUserData->date_of_birth ?></p>
                                </div>
                                <div class="mt-3">
                                    <label class="tx-11 font-weight-bold mb-0 text-uppercase">Lives:</label>
                                    <p class="text-muted"><?= !empty($cUserData->residence) ? "{$cUserData->residence}, " : null ?> <?= $cUserData->country_name ?></p>
                                </div>
                                <div class="mt-3">
                                    <label class="tx-11 font-weight-bold mb-0 text-uppercase">Address:</label>
                                    <p class="text-muted"><?= $cUserData->address ?></p>
                                </div>
                                <div class="mt-3">
                                    <label class="tx-11 font-weight-bold mb-0 text-uppercase">Nationality:</label>
                                    <p class="text-muted"><?= $cUserData->nationality ?></p>
                                </div>
                                <div class="mt-3">
                                    <label class="tx-11 font-weight-bold mb-0 text-uppercase">Email:</label>
                                    <p class="text-muted"><?= $cUserData->email ?></p>
                                </div>
                                <div class="mt-3">
                                    <label class="tx-11 font-weight-bold mb-0 text-uppercase">Contact Number:</label>
                                    <p class="text-muted"><?= $cUserData->phone_number ?></p>
                                </div>
                            </div>
                        </div>

                        <?php if(in_array($cUserData->user_type, ["agent", "broker", "bancassurance"])) { ?>
                        <div class="card rounded" id="user_performance_load" data-base_salary="<?= $cUserData->base_salary ?>" data-user_percentage="<?= round(($cUserData->commission_percentage/100), 2) ?>" data-user_period="<?= $cUserData->commission_period ?>" data-user_type="<?= $cUserData->user_type ?>" data-user_id="<?= $cur_user_id ?>">
                            <div class="d-flex justify-content-between p-2 pb-0">
                                <div class="text-uppercase pt-2">
                                    <h5>Commission</h5>
                                </div>
                                <div class="text-uppercase">
                                    <button class="btn p-2 btn-outline-success mb-0" data-toggle="modal" data-target="#commissionModal">Manage</button>
                                </div>                        
                            </div>
                            <div class="p-2">
                                <table class="table border-0">
                                    <tbody>
                                        <tr>
                                            <td><strong>Base Salary</strong></td>
                                            <td><span class="base_salary"><?= $cUserData->base_salary ?></span></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Premium Paid</strong></td>
                                            <td><span class="premium_paid_by_clients">0.00</span></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Commission</strong></td>
                                            <td><span class="commission_receivable">0.00</span></td>
                                        </tr>                                        
                                        <tr>
                                            <td><strong>Total Earnings</strong></td>
                                            <td><span class="total_amount_receivable">0.00</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php } ?>

                    </div>

                    <div class="col-md-8 col-xl-6 p-0 middle-wrapper">                        
                        <div class="slim-scroll" style="max-height:700px; overflow-y:auto">

                            <?php if(in_array($cUserData->user_type, ["agent", "broker", "bancassurance", "insurance_company"])) { ?>
                                <div class="card">
                                    <div class="card-header p-2 m-0">
                                        <div class="text-uppercase font-weight-bolder">Policies Managed</div>
                                    </div>
                                    <div class="card-body p-2">
                                        <?php 
                                        $p_params = (object) [];
                                        $p_params->include_insurance_company = true;
                                        $p_params->company_id = $cUserData->company_id;
                                        $p_params->userData = $cUserData;
                                        $p_params->userId = $cur_user_id;
                                        $p_params->remote = true;
                                        // get the user data
                                        $managed_policies = load_class("user_policy", "controllers")->list($p_params);
                                        
                                        // loop through the list
                                        ?>
                                        <table class="table table-condensed dataTable">
                                            <thead>
                                                <th>Client Name</th>
                                                <th>Policy Name</th>
                                                <th width="6%">Status</th>
                                            </thead>
                                            <tbody>
                                                <?php foreach($managed_policies["data"] as $eachPolicy) { ?>
                                                <tr>
                                                    <td><span title="Click to view summary information of <?= $eachPolicy->client_name ?>" class="underline" onclick="return user_basic_information('<?= $eachPolicy->user_id ?>')"><?= $eachPolicy->client_name ?></span></td>
                                                    <td>
                                                        <p><a href="<?= $baseUrl ?>policies-view/<?= $eachPolicy->item_id ?>" title="Click to view the details of this Policy"><?= $eachPolicy->policy_name ?></a></p>
                                                        <p><i class="fa fa-calendar-check"></i> <?= $eachPolicy->policy_start_date ?></p>
                                                    </td>
                                                    <td><?= $medicsClass->the_status_label($eachPolicy->policy_status) ?></td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php } ?>
                            
                            <div class="user_interaction_wrapper">
                                <div class="slim-scroll" id="user_complaints_list" data-threads="user_complaints_list"></div>
                            </div>

                            <div id="profile_content_loader"></div>
                            <div id="user_policy_display"></div>

                        </div>
                    </div>

                    <div class="col-md-12 d-xl-block col-xl-3 right-wrapper">
                        <div class="row">
                            <?php if(in_array($cUserData->user_type, ["user", "business"])) { ?>
                            <div class="col-lg-12">
                                <div class="row slim-scroll user_resources_list" style="max-height: 700px; overflow-y:hidden" id="quick_minimal_load" data-stream="user_policy,user_claims,user_complaints" data-stream_id="<?= $cUserData->user_id ?>">
                                    <div class="col-lg-12 col-md-6 grid-margin">
                                        <div class="card rounded">
                                            <div class="card-body pb-2 pr-2 pl-2">
                                                <h6 class="card-title mb-1 border-bottom pb-2">Policies (<span class="text-muted tx-12" data-id="<?= $cUserData->user_id ?>" data-record="policies_count">0</span>)</h6>
                                                <div data-threads="user_policy_list"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-6 grid-margin">
                                        <div class="card rounded">
                                            <div class="card-body pb-2 pr-2 pl-2">
                                                <h6 class="card-title mb-1 border-bottom pb-2">Claims (<span class="text-muted tx-12" data-id="<?= $cUserData->user_id ?>" data-record="claims_count">0</span>)</h6>
                                                <div class="user_claims_list" data-threads="user_claims_list"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>

                            <div class="col-md-12 grid-margin">
                                <?php if(in_array($cUserData->user_type, ["agent", "broker", "bancassurance", "insurance_company"])) { ?>
                                <div class="card rounded">
                                    <div class="card-body p-2">
                                        <h6 class="card-title">Managed Clients List</h6>
                                        <div class="slim-scroll pr-1" style="max-height:600px; overflow-y:auto">
                                            <?php
                                            // reset the variables
                                            unset($i_params->user_id);
                                            $i_params->query = "";
                                            $i_params->company_id = "";
                                            $i_params->limit = "100";
                                            $i_params->remote = true;
                                            $i_params->or_clause = " AND (a.created_by='{$cUserData->user_id}' OR a.assigned_to='{$cUserData->user_id}')";
                                            $i_params->columns = "a.item_id AS user_id, a.name, a.image, a.phone_number, a.phone_number_2, a.email";
                                            
                                            // get the user data
                                            $managed_accounts = $usersClass->list($i_params);
                                            
                                            // loop through the array 
                                            if(isset($managed_accounts["data"])) {
                                                foreach($managed_accounts["data"] as $eachUser) { ?>
                                                <div class="d-flex justify-content-between mb-2 pb-2">
                                                    <div class="d-flex align-items-center hover-pointer">
                                                        <img class="img-xs rounded-circle" src="<?= $baseUrl ?><?= $eachUser->image ?>" alt="">
                                                        <div class="ml-2">
                                                            <p><?= $eachUser->name ?></p>
                                                            <p class="tx-11 text-muted"><?= $eachUser->phone_number ?></p>
                                                            <p class="tx-11 text-muted"><a href="mailto:<?= $eachUser->email ?>"><?= $eachUser->email ?></a></p>
                                                        </div>
                                                    </div>
                                                    <button style="padding:5px; height:;" onclick="return user_basic_information('<?= $eachUser->user_id ?>', 'policies')" data-toggle="tooltip" title="Click to view summary details of <?= $eachUser->name ?>" class="btn btn-icon text-primary btn-sm cursor">
                                                        <i class="fa fa-eye"></i>
                                                    </button>
                                                </div>
                                                <?php
                                                }
                                            }                                    
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>

                        </div>
                    </div>
                    
                </div>

            </div>

            <?php if(in_array($cUserData->user_type, ["user", "business"])) { ?>
            <div class="row mt-3">
                <div class="col-12 grid-margin">
                    <div class="card">
                        <form action="<?= $baseUrl; ?>api/users/preference" method="POST" class="app-data-form">
                            
                            <div class="card-header">
                                <div class="d-flex justify-content-between">
                                    <div><h6 class="card-title mb-0">Payment Option</h6></div>
                                    <div><button type="submit" class="btn btn-outline-success">Save Options</button></div>
                                </div>
                            </div>
                            <div class="card-body">
                            
                                <div class="row">
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label for="default_payment">Default Payment Option</label>
                                            <select data-width="100%" name="label[payments][default_payment]" class="form-control selectpicker">
                                                <option value="null">Please select option</option>
                                                <?php foreach($medicsClass->pushQuery("*", "policy_form", "type='payment_options'") as $eachOption) { ?>
                                                    <option <?= (!empty($cUserPrefs->payments->default_payment) && ($cUserPrefs->payments->default_payment == $eachOption->id)) ? "selected" : null ?> value="<?= $eachOption->id ?>"><?= $eachOption->name ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="payment_interval">Default Payment Interval</label>
                                            <select data-width="100%" name="label[payments][payment_interval]" class="form-control selectpicker">
                                                <option value="null">Please select option</option>
                                                <?php foreach($medicsClass->pushQuery("*", "policy_form", "type='payment_interval'") as $eachOption) { ?>
                                                    <option <?= (!empty($cUserPrefs->payments->payment_interval) && ($cUserPrefs->payments->payment_interval == $eachOption->id)) ? "selected" : null ?> value="<?= $eachOption->id ?>"><?= $eachOption->name ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="mb-3 border-bottom"><h6 class="card-title mb-0">Mobile Money</h6></div>
                                        <div class="col-lg-12 p-0">
                                            <div class="form-group">
                                                <label for="default_network">Network</label>
                                                <select data-width="100%" name="label[payments][default_network]" id="default_network" class="form-control selectpicker">
                                                    <option value="null">Select Network</option>
                                                    <?php foreach($medicsClass->pushQuery("*", "policy_form", "type='mobile_network'") as $eachOption) { ?>
                                                        <option <?= (!empty($cUserPrefs->payments->default_network) && $cUserPrefs->payments->default_network == $eachOption->id) ? "selected" : null ?> value="<?= $eachOption->id ?>"><?= $eachOption->name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 p-0">
                                            <div class="form-group">
                                                <label for="default_contact">Contact Number</label>
                                                <input type="text" value="<?= $cUserPrefs->payments->default_contact ?? null ?>" data-inputmask-alias="(+999) 999-999-999" inputmode="text" name="label[payments][default_contact]" id="default_contact" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-lg-12 pr-0 text-right">
                                            <span class="badge badge-danger">Not Verified</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="mb-3 border-bottom"><h6 class="card-title mb-0">Bank Account</h6></div>
                                        <div class="col-lg-12 p-0">
                                            <div class="form-group">
                                                <label for="bank_name">Bank Name</label>
                                                <select data-width="100%" name="label[payments][bank_name]" id="bank_name" class="form-control selectpicker">
                                                    <option value="null">Select Bank</option>
                                                    <?php foreach($medicsClass->pushQuery("item_id, name,contact,email", "companies", "company_type='bank' AND deleted='0' AND deactivated='0'") as $eachBank) { ?>
                                                    <option value="<?= $eachBank->item_id ?>"><?= $eachBank->name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="bank_branch">Banch Branch</label>
                                                <input type="text" value="<?= $cUserPrefs->payments->bank_branch ?? null ?>" name="label[payments][bank_branch]" id="bank_branch" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="account_number">Account Number</label>
                                                <input type="text" value="<?= $cUserPrefs->payments->account_number ?? null ?>" name="label[payments][account_number]" id="account_number" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-lg-12 pr-0 text-right">
                                            <span class="badge badge-danger">Not Verified</span>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="the_user_id" id="the_user_id" value="<?= $cur_user_id ?>">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <?php } ?>

        </div>
        <?php if($current_user || (!$current_user && $canUpdate)) { ?>
        <div class="modal fade" id="profilePicture" <?= !$auto_close_modal ? null :  'data-backdrop="static" data-keyboard="false"'; ?>>
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Update Profile Picture</h5>
                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-8 text-center">
                                <input type="file" accept="image/*" name="file_upload" id="file_upload">
                            </div>
                            <div class="upload_file_url" data-record-id="<?= $cUserData->user_id ?>" data-button="save-profile-picture" data-url="<?= $baseUrl ?>api/files/preview"></div>
                            <div class="col-md-4 text-center">
                                <div class="file-upload-preview">
                                    <?php if(!empty($session->tempProfilePicture)) { ?>
                                        <img style="width:100px" class="img-fluid" src="<?= $baseUrl.$session->tempProfilePicture ?>">
                                        <button class="btn btn-outline-success btn-sm mt-2 btn-block" id="save-profile-picture" data-user-id="<?= $cUserData->user_id ?>">Save Image</button>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade modal-dialog-right right" id="profileModal" <?= !$auto_close_modal ? null :  'data-backdrop="static" data-keyboard="false"'; ?>>
            <div class="modal-dialog modal-dialog-centered modal-lg" style="height:100%;" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Update Profile</h5>
                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?= $baseUrl ?>api/users/update" class="app-data-form">
                            <?= form_overlay() ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <div class="input-group-text">UNIQUE USER ID</div>
                                            </div>
                                            <input type="text" disabled value="<?= $cUserData->client_id ?>" name="client_id" id="client_id" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 mb-2"><div class="form-group mb-1"><h5>BIO DATA</h5></div></div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <div class="input-group-text">Firstname &nbsp;<span class='required'>*</span></div>
                                            </div>
                                            <input type="text" value="<?= $cUserData->firstname ?>" name="firstname" id="firstname" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <div class="input-group-text">Lastname &nbsp;<span class='required'>*</span></div>
                                            </div>
                                            <input type="text" value="<?= $cUserData->lastname ?>" name="lastname" id="lastname" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <div class="input-group-text">Othernames</div>
                                            </div>
                                            <input type="text" value="<?= $cUserData->othername ?>" name="othername" id="othername" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="gender">Gender</label>
                                        <select name="gender" id="gender" data-width="100%" class="selectpicker">
                                            <option value="null">Select Gender</option>
                                            <?php foreach($medicsClass->pushQuery("id, name", "policy_form", "type='gender'") as $each) { ?>
                                                <option <?= $each->name == $cUserData->gender ? "selected" : null; ?> value="<?= $each->name ?>"><?= $each->name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="date_of_birth">Date of Birth</label>
                                        <input type="text" value="<?= $cUserData->date_of_birth ?>" name="date_of_birth" id="date_of_birth" class="form-control datepicker">
                                    </div>
                                </div>
                                <div class="col-lg-12 mb-2 mt-2"><div class="form-group mb-1"><h5>CONTACT DETAILS</h5></div></div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <div class="input-group-text">Email Address<span class='required'>*</span></div>
                                            </div>
                                            <input name="email" value="<?= $cUserData->email ?>" id="email" title="email" class="form-control mb-4 mb-md-0" data-inputmask="'alias': 'email'" inputmode="email">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="phone">Primary Number <span class='required'>*</span></label>
                                        <input type="text" value="<?= $cUserData->phone_number ?>" data-inputmask-alias="(+999) 999-999-999" inputmode="text" name="phone" id="phone" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="phone_2">Secondary Contact</label>
                                        <input type="text" value="<?= $cUserData->phone_number_2 ?>" data-inputmask-alias="(+999) 999-999-999" inputmode="text" name="phone_2" id="phone_2" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <div class="input-group-text">Postal Address</div>
                                            </div>
                                            <input type="text" value="<?= $cUserData->address ?>" name="address" id="address" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <div class="input-group-text">Residential Address</div>
                                            </div>
                                            <input type="text" value="<?= $cUserData->residence ?>" name="residence" id="residence" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="nationality">Nationality</label>
                                        <input type="text" value="<?= $cUserData->nationality ?>" name="nationality" id="nationality" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="country">Country</label>
                                        <select name="country" id="country" data-width="100%" class="selectpicker w-100">
                                            <option value="null">Select Country</option>
                                            <?php foreach($medicsClass->pushQuery("id, country_name", "country", "'1'") as $each) { ?>
                                                <option <?= $each->id == $cUserData->country ? "selected" : null; ?> value="<?= $each->id ?>"><?= $each->country_name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12 mb-2 mt-2"><div class="form-group mb-1"><h5>OTHER INFORMATION</h5></div></div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <div class="input-group-text">Employer</div>
                                            </div>
                                            <input type="text" value="<?= $cUserData->employer ?>" name="employer" id="employer" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <div class="input-group-text">Occupation</div>
                                            </div>
                                            <input type="text" value="<?= $cUserData->occupation ?>" name="occupation" id="occupation" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <div class="input-group-text">Position</div>
                                            </div>
                                            <input type="text" value="<?= $cUserData->position ?>" name="position" id="position" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea name="description" id="description" rows="3" class="form-control"><?= $cUserData->description ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6 text-left">
                                    <button class="btn btn-outline-danger" type="button" data-dismiss="modal">Close</button>
                                </div>
                                <div class="col-md-6 text-right">
                                    <input type="hidden" name="user_id" required id="user_id" value="<?= $cUserData->user_id ?>" hidden class="form-control">
                                    <button class="btn btn-outline-success btn-sm" type="submit">Save Record</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>

        <?php if($hasPermission && !$current_user) { ?>
            <div class="modal fade" id="permissionsModal" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-dialog-top modal-lg" style="width:100%;" role="document">
                    <div class="modal-content">
                        <?= form_loader() ?>
                        <div class="modal-header">
                            <h5 class="modal-title">Manage User Permissions</h5>
                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                        </div>
                        <div class="modal-body" data-scrolling="false" style="text-align:left"></div>
                        <div class="modal-footer">
                            <button class="btn btn-outline-success">Save Permissions</button>
                            <button class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>

    <?php } ?>
</div>
<?php require "foottags.php"; ?>