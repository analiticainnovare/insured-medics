<?php
// set the title
$page_title = "Complaint Review";

// require the headtags
require "headtags.php";

// get some variable
$itemFound = false;
$recordId = confirm_url_id(1) ? xss_clean($SITEURL[1]) : null;

// load the complaint information
if($recordId) {
    /** load the users insurance policies */
    $recordObj = load_class("complaints", "controllers");
    
    /** Parameters */
    $param = (object) [
        "limit" => 1,
        "complaint_id" => $recordId,
        "userData" => $userData,
        "remote" => false,
        "submit_status" => false,
        "userId" => $loggedUserId,
    ];
    
    // return the result
    $recordData = $recordObj->list($param);
    $itemFound = !empty($recordData["data"]) ? true : false;

    // if the complaint was found
    if($itemFound) {

        // extra js
        $loadedJS = [
            "assets/js/script/comments.js",
            "assets/js/script/attachments.js",
        ];

        // create a new forms object
        $formsObj = load_class("forms", "controllers");
        // set the complaint data
        $recordData = $recordData["data"][0];
        $recordData->attachment = (array) $recordData->attachment;
        // sent state
        $isActive = (bool) ($recordData->submit_status == "save");
        $isClosed = (bool) (in_array($recordData->status, ["Solved", "Closed", "Cancelled"]));
        // set the seen date and who saw it
        if(!$recordData->seen && ($loggedUserId != $recordData->user_id) && $isActive) {
            $medics->query("UPDATE users_complaints SET seen = '1', seen_date = now(), seen_by = '{$loggedUserId}' WHERE item_id = '{$recordId}' AND user_id != '{$loggedUserId}' LIMIT 1");
        }
    }
}

$reviewComplaint = (bool) $accessObject->hasAccess("review", "complaints");
?>
<div class="page-content">
    <?php if(!$accessObject->hasAccess("view", "complaints") || !$itemFound) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <a href="<?= $baseUrl ?>complaints" type="button" class="btn btn-outline-primary btn-icon-text mb-2 mb-md-0">
                    <i class="btn-icon-prepend" data-feather="list"></i>
                    List Complaints
                </a> &nbsp;
            </div>
        </div>

        <div class="col-lg-12 p-0 col-xl-12 inbox-wrapper">
            <div class="card">
                <div class="card-body">

                    <div class="row">
                        
                        <div class="col-lg-3 email-aside border-lg-right">
                            <div class="aside-content">
                                <div class="aside-header">
                                    <button class="navbar-toggle" data-target=".aside-nav" data-toggle="collapse" type="button"><span class="icon"><i data-feather="chevron-down"></i></span></button>
                                    <span class="title"><h4><small class="font-weight-bold small-font">Related To:</small> <?= $recordData->related_to_name ?></h4></span>
                                    <?php if(!empty($recordData->related_to_id)) { ?>
                                        <div class="description"><?= $recordObj->related_to_data($recordData->related_to, $recordData->related_to_details) ?></div>
                                    <?php } ?>
                                </div>
                                <?php if($isActive && $reviewComplaint) { ?>
                                <div class="aside-compose" data-toggle="tooltip" title="Reply to Complaint"><a <?php if($isActive) { ?>data-function="load-form" data-resource="complaints" data-module-id="<?= $recordId ?>" data-module="share_a_reply"<?php } ?> class="btn btn-primary btn-block" href="javascript:void(0)">Reply</a></div>
                                <?php } ?>
                                <?php if(!$isActive) { ?>
                                <div class="aside-compose" data-toggle="tooltip" title="Update this Complaint"><a data-function="load-form" data-resource="complaints" data-module-id="<?= $recordId ?>" data-module="lodge_complaint" class="btn btn-primary btn-block" href="javascript:void(0)">Update Complaint</a></div>
                                <?php } ?>
                                <div class="aside-nav <?= (!$isActive) ? "mt-3" : null ?> collapse">
                                    <?php if($isActive && $reviewComplaint) { ?>
                                    <ul class="nav">
                                        <li class="active">
                                            <a data-toggle="tooltip" title="View All Replies" <?php if($isActive) { ?>data-function="load-replies-form" data-module="view_replies_list" data-resource="complaints" data-module-id="<?= $recordId ?>"<?php } ?> href="javascript:void(0)">
                                                <span class="icon"><i data-feather="inbox"></i></span>Officials Feedback &nbsp;<span class="smaller-font"> (Admin only)</span>
                                                <span data-record="replies_count" data-id="<?= $recordId ?>" class="badge badge-danger-muted text-white font-weight-bold float-right">0</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <?php } else { ?>
                                    <div class="border-bottom mt-3"></div>
                                    <?php } ?>
                                    <div class="mt-3 mb-2">
                                        <div class="date mb-2">
                                            <label for="">Last Reply Date</label>
                                            <?php if($recordData->seen_date) { ?>
                                            <i class="fa fa-calendar"></i> <?= date("M d Y, h:iA", strtotime($recordData->last_updated)) ?>
                                            <?php } else { ?>
                                            ---
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php if($recordData->seen_date) { ?>
                                    <div class="mt-3 mb-2">
                                        <div class="date mb-0 m-0 p-0" title="Complaint seen date">
                                            <label class="mb-0">First seen by:</label><br><span class="user-name-link" onclick="return user_basic_information('<?= $recordData->seen_by ?>')"><?= $recordData->seen_by_name ?></span>
                                            <br> <span class="text-muted"><i class="fa fa-calendar-check"></i> <?= date("M d Y, h:iA", strtotime($recordData->seen_date)) ?> </span>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <form class="app-data-form" <?= $reviewComplaint ? "action=\"{$baseUrl}api/complaints/update\" " : null ?>method="POST">
                                        <?= form_loader(); ?>
                                        <div class="mt-3 form-group">
                                            <label for="status">Current Status:</label>
                                            <?php if($reviewComplaint) { ?>
                                            <span data-record="record_status"><?= $recordData->status_label ?></span>
                                            <select data-width="100%" name="status" <?= in_array($recordData->status, ["Solved", "Closed"]) || !$isActive ? "disabled" : null;  ?> id="status" class="selectpicker">
                                                <option <?= $recordData->status == "Pending" && $recordData->status !== "Processing"  ? "selected" : "disabled" ?> value="Pending">Pending</option>
                                                <option <?= $recordData->status == "Processing" && $recordData->status !== "Solved" ? "selected" : (($recordData->status == "Solved") ? "disabled" : null) ?> value="Processing">Processing</option>
                                                <option <?= $recordData->status == "Solved" ? "selected" : null ?> <?= $recordData->status == "Solved" ? "disabled" : null ?> value="Solved">Solved</option>
                                            </select>
                                            <?php } else { ?>
                                            <span data-record="record_status"><?= $recordData->status_label ?></span>
                                            <?php } ?>
                                        </div>
                                        <div class="mt-3 form-group">
                                            <label for="assigned_to">Assigned To:</label>
                                            <?php if($reviewComplaint) { ?>
                                            <select data-width="100%" name="assigned_to" <?= in_array($recordData->status, ["Solved", "Closed"]) || !$isActive ? "disabled" : null;  ?> <?= $recordData->user_id == $loggedUserId ? "disabled" : null;  ?> id="assigned_to" class="selectpicker">
                                                <option value="null">Assign Complaint to a Member</option>
                                                <?php
                                                // load users that this can be assigned to
                                                if($userData->user_type == "insurance_company") {
                                                    $query = "SELECT item_id, name FROM users WHERE user_type IN ('agent', 'insurance_company') AND status = '1' AND deleted='0'";
                                                } elseif($userData->user_type == "nic") {
                                                    $query = "SELECT item_id, name FROM users WHERE user_type IN ('nic') AND status = '1' AND deleted='0'";
                                                } elseif(in_array($userData->user_type, ["bank", "bancassurance"])) {
                                                    $query = "SELECT item_id, name FROM users WHERE user_type IN ('bank','bancassurance') AND status = '1' AND deleted='0'";
                                                } else {
                                                    $query = "SELECT item_id, name FROM users WHERE user_type IN ('admin') AND status = '1' AND deleted='0'";
                                                }
                                                $stmt = $medics->query($query);
                                                $result = $stmt->fetchAll(PDO::FETCH_OBJ);
                                                // loop through the results
                                                foreach($result as $eachUser) {
                                                    print "<option ".($recordData->assigned_to == $eachUser->item_id ? "selected" :  null)." value=\"{$eachUser->item_id}\">{$eachUser->name}</option>";
                                                }
                                                ?>
                                            </select>
                                            <?php } elseif(!empty($recordData->assigned_to)) { ?>
                                            <span class="user-name-link" onclick="return user_basic_information('<?= $recordData->assigned_to ?>')"><?= $recordData->assigned_to_name ?></span>
                                            <?php } ?>
                                        </div>
                                        <?php if($isActive && !$isClosed && $reviewComplaint) { ?>
                                        <div class="mt-3 form-group text-center">
                                            <input type="hidden" name="complaint_id" id="complaint_id" value="<?= $recordId ?>" class="form-control">
                                            <button type="submit" class="btn btn-outline-success">Save Changes</button>
                                        </div>
                                        <?php } ?>
                                    </form>
                                    <div class="mt-5 justify-content-center row form-group">
                                        <button data-toggle="tooltip" title="View summary history" data-function="load-form" data-resource="complaints" data-module-id="<?= $recordId ?>" data-module="activity_history" class="btn btn-outline-secondary">View activity history</button>
                                    </div>                                    

                                </div>
                            </div>
                        </div>

                        <div class="col-lg-9 email-content">
                            <div class="email-head">
                                <div class="email-head-subject p-2 mr-0 pr-0">
                                <div class="title d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center">
                                        <span><h4><?= $recordData->user_id == $loggedUserId ? $recordData->complaint_subject_none : $recordData->subject ?></h4></span>
                                    </div>
                                    <?php if($recordData->user_id == $loggedUserId && !$isActive) {  ?>
                                    <div class="text-right">
                                        <?= submit_draft_button("Complaints", $recordData->subject, $recordId) ?>
                                    </div>
                                    <?php } ?>
                                </div>
                                </div>
                                <div class="email-head-sender d-flex align-items-center justify-content-between flex-wrap">
                                <div class="d-flex align-items-center mb-2">
                                    <div class="avatar">
                                    <img src="<?= $baseUrl ?><?= $recordData->complainant_image ?>" alt="Avatar" class="rounded-circle user-avatar-md">
                                    </div>
                                    <div class="sender d-flex align-items-center">
                                        <p><a href="javascript:void(0)" title='Quick details view' data-function="load-form" data-module="user_basic_information" data-resource="user_information" data-module-id="<?= $recordData->user_id ?>"><?= $recordData->complained_by ?></a></p>
                                        <p class="pl-3"><small><i class="fa fa-envelope"></i> <?= $recordData->email ?></small></p>
                                        <p class="pl-3"><small><i class="fa fa-phone"></i> <?= $recordData->phone_number ?></small></p>
                                    </div>
                                </div>
                                <div class="date"><i class="fa fa-calendar"></i> <?= date("M d Y, h:iA", strtotime($recordData->date_created)) ?></div>
                                </div>
                            </div>
                            
                            <div class="row slim-scroll">

                                <div class="col-lg-7 mb-3">
                                    <div class="border-top p-0 m-0">
                                        <?php $comment = !$reviewComplaint ? "Leave a comment for the <strong>Admin</strong>" : null; ?>
                                        <?= leave_comments_builder("complaint_comments", $recordId, $comment) ?>
                                        <div id="comments-container" data-autoload="true" data-last-reply-id="0" data-id="<?= $recordId ?>" class="slim-scroll pt-3 mt-3 pr-2 pl-0" style="overflow-y:auto; max-height:850px"></div>
                                        <div class="load-more mt-3 text-center"><button id="load-more-replies" type="button" class="btn btn-outline-secondary">Loading comments</button></div>
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="email-attachments p-0 pt-3">
                                        <?= stripslashes($recordData->message) ?>

                                        <?php if(!empty($recordData->attachment)) { ?>
                                            <div class="email-attachments mt-4">
                                                <div class="title">Attachments <span>(<?= count($recordData->attachment["files"]) ?? 0 ?> files, <?= $recordData->attachment["files_size"] ?? 0 ?>)</span></div>
                                                <?= $formsObj->list_attachments($recordData->attachment["files"], $loggedUserId, null, !$isActive); ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>

    <?php } ?>
    
</div>
<?php require "foottags.php"; ?>