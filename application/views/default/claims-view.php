<?php
// set the title
$page_title = "Insurance Policy Claim Request";

// require the headtags
require "headtags.php";

// get some variable
$itemFound = false;
$recordId = confirm_url_id(1) ? xss_clean($SITEURL[1]) : null;

// load the complaint information
if($recordId) {

    /** load the users insurance policies */
    $recordObj = load_class("claims", "controllers");

    /** Parameters */
    $param = (object) [
        "claim_id" => $recordId,
        "remote" => false,
        "limit" => 1,
        "userId" => $loggedUserId,
        "submit_status" => false,
    ];
    // return the result
    $recordData = $recordObj->list($param);
    $itemFound = !empty($recordData["data"]) ? true : false;

    // if the complaint was found
    if($itemFound) {

        // extra js
        $loadedJS = [
            "assets/js/script/attachments.js",
            "assets/js/script/comments.js",
        ];

        // create a new forms object
        $formsObj = load_class("forms", "controllers");
        
        // set the complaint data
        $recordData = $recordData["data"][0];
        $recordData->attachment = (array) $recordData->attachment;
        $policyInfo = $recordData->policy_details;
        
        // sent state
        $isActive = (bool) ($recordData->submit_status == "save");
        $hasPermission = ( ($recordData->user_id == $loggedUserId) || ($recordData->created_by == $loggedUserId) ) ? true : false;

        //'Pending','Approved','Confirmed','Cancelled','Rejected','Deleted'
        $isClosed = (bool) (in_array($recordData->status, ["Deleted", "Cancelled"]));
        $isApproved = (bool) (in_array($recordData->status, ["Approved"]));
        $isPaid = (bool) ($recordData->paid_status == "Paid");
        
        // set the seen date and who saw it
        if(!$recordData->seen && ($loggedUserId != $recordData->created_by) && ($loggedUserId != $recordData->user_id) && $isActive) {
            $medics->query("UPDATE users_policy_claims SET seen = '1', seen_date = now(), seen_by = '{$loggedUserId}' WHERE item_id = '{$recordId}' AND user_id != '{$loggedUserId}'");
        }
    }
}

// review command
$reviewClaim = (bool) $accessObject->hasAccess("review", "claims");
$approveClaim = (bool) $accessObject->hasAccess("approve", "claims");
$additionInfo = $reviewClaim ? "policies" : null;
?>
<div class="page-content">
    <?php if(!$accessObject->hasAccess("view", "claims") || !$itemFound) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>

                
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <a href="<?= $baseUrl ?>claims-list" type="button" class="btn btn-outline-primary btn-icon-text mb-2 mb-md-0">
                    <i class="btn-icon-prepend" data-feather="list"></i>
                    List Claims
                </a> &nbsp;
            </div>
        </div>

        <div class="col-lg-12 p-0 col-xl-12 inbox-wrapper">
            <div class="card">
                <div class="card-body">

                    <form class="app-data-form" autocomplete="Off" <?= $reviewClaim ? "action=\"{$baseUrl}api/claims/update\" " : null ?>method="POST">

                    <div class="row">
                        
                        <div class="col-lg-3 email-aside border-lg-right">
                            <div class="aside-content">
                                <div class="aside-header">
                                    <button class="navbar-toggle" data-target=".aside-nav" data-toggle="collapse" type="button"><span class="icon"><i data-feather="chevron-down"></i></span></button>
                                    <span class="title"><h4>Policy Information
                                </div>
                            
                                <?php /** If the form has not been submitted yet and a draft */ if(!$isActive) { ?>

                                        <div class="aside-compose" data-toggle="tooltip" title="Update the Claim Form"><a data-function="load-form" data-resource="claims" data-module-id="<?= $recordId ?>" data-module="update_claims" class="btn btn-primary btn-block" href="javascript:void(0)">Update Claim</a></div>                                    

                                <?php } else { /** View the details of the submitted form */ ?>
                                    
                                    <div class="aside-compose mb-0 pb-0" data-toggle="tooltip" title="View the claim application form"><a data-function="load-form" data-resource="view_claims" data-module-id="<?= $recordId ?>" data-module="update_claims" class="btn btn-outline-success btn-block" href="javascript:void(0)">View Claim Application Form</a></div>

                                <?php } ?>

                                <?php /** If the user has the permission to review policies */ if($reviewClaim) { ?>
                                    
                                    <?php /** if the policy has been submitted */ if($isActive) { ?>
                                        
                                        <div class="aside-compose" data-toggle="tooltip" title="Reply to Client Policy"><a <?php if($isActive) { ?>data-function="load-form" data-resource="claims" data-module-id="<?= $recordId ?>" data-module="share_a_reply"<?php } ?> class="btn btn-primary btn-block" href="javascript:void(0)">Add Feedback</a></div>
                                    
                                    <?php } ?>

                                <?php } ?>

                                <div class="aside-nav <?= (!$isActive) ? "mt-2" : null ?> collapse">
                                    <?php if($reviewClaim) { ?>
                                    <ul class="nav">
                                        <li class="active">
                                            <a data-toggle="tooltip" title="View All Replies" <?php if($isActive) { ?>data-function="load-replies-form" data-module="view_replies_list" data-resource="claims" data-module-id="<?= $recordId ?>"<?php } ?> href="javascript:void(0)">
                                                <span class="icon"><i data-feather="inbox"></i></span>Officials Feedback &nbsp;<span class="smaller-font"> (Admin only)</span>
                                                <span data-record="replies_count" data-id="<?= $recordId ?>" class="badge badge-danger-muted text-white font-weight-bold float-right">0</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <?php } ?>
                                    <?php if($recordData->seen_date) { ?>
                                    <div class="mt-3 mb-1">
                                        <div class="date mb-1" title="Complaint Seen Date">
                                            <label>First seen by:</label><br> <?= date("M d Y, h:iA", strtotime($recordData->seen_date)) ?> 
                                            <br><span class="user-name-link" onclick="return user_basic_information('<?= $recordData->seen_by ?>')"><?= $recordData->seen_by_name ?></span></div>
                                    </div>
                                    <?php } ?>

                                    <div class="mt-3 border-bottom">
                                        <div class="date">
                                            <label class="mb-0">Policy Name:</label><br><span class="font-18px"><?= $policyInfo->policy_name; ?></span>
                                            <span title="Click to learn more about <?= $policyInfo->policy_name ?>" data-toggle="tooltip" class="user-name-link text-primary small-font cursor" onclick="return policy_basic_information('<?= $recordData->policy_type ?>')">Learn more</span></h4></span>
                                        </div>
                                    </div>
                                    <div class="mt-2 border-bottom">
                                        <div class="date">
                                            <label class="m-0" for="">Policy ID:</label>
                                            <p><span class="font-18px"><a title="Click to View Policy Details" href="<?= $baseUrl ?>policies-view/<?= $policyInfo->item_id; ?>"><?= $recordData->policy_id; ?></a></span></p>
                                        </div>
                                    </div>
                                    <div class="mt-2 border-bottom">
                                        <div class="date">
                                            <label for="">Policy Status:</label> <?= $medicsClass->the_status_label($policyInfo->policy_status); ?>
                                        </div>
                                    </div>
                                    <div class="mb-2 mt-2 pb-2 border-bottom">
                                        <div class="date">
                                            <label class="mb-0">Date Enrolled:</label><br>
                                            <span class="font-italic"><?= date("l, jS M Y", strtotime($policyInfo->policy_start_date)) ?></span>
                                        </div>
                                    </div>
                                    <div class="mb-0 mt-2">
                                        <div class="date">
                                            <label for="">Agent Name:</label> <span title="Click to view summary information about <?= $policyInfo->assigned_to_name ?>" data-toggle="tooltip" class="user-name-link" data-function="load-form" data-module="user_basic_information" data-resource="user_information" data-module-id="<?= $policyInfo->assigned_to ?>"><?= $policyInfo->assigned_to_name ?></span>
                                        </div>
                                    </div>

                                    <?php if($policyInfo->policy_status !== "Pending") { ?>
                                    <div class="aside-compose mb-0 pb-0" data-toggle="tooltip" title="View the policy premium payment history"><a data-function="load-form" data-resource="view_payment_history" data-module-id="<?= $recordData->policy_id ?>" data-module="policy_payment_history" class="btn btn-outline-success btn-block" href="javascript:void(0)">View premium payment history</a></div>
                                    <?php } ?>
                                    
                                    <div class="mb-4 border-bottom"></div>
                                    
                                    <div class="mt-3 form-group">
                                        <label for="status" class="text-muted">Claim Status:</label>
                                        <?php if($reviewClaim) { ?>
                                        <span data-record="status"><?= $recordData->the_status_label ?></span>
                                        <select data-width="100%" name="status" <?= in_array($recordData->status, ["Cancelled", "Deleted"]) || !$isActive ? "disabled" : null;  ?> id="status" class="selectpicker">
                                            <option <?= $recordData->status == "Pending" && $recordData->status !== "Confirmed"  ? "selected" : "disabled" ?> value="Pending">Pending</option>
                                            <option <?= $recordData->status == "Confirmed" && $recordData->status !== "Approved" ? "selected" : (($recordData->status == "Approved") ? "disabled" : null) ?> value="Confirmed">Confirmed</option>
                                            
                                            <?php if($approveClaim) { ?>
                                            <option <?= $recordData->status == "Approved" ? "selected" : null ?> <?= $recordData->status == "Approved" ? "disabled" : null ?> value="Approved">Approved</option>
                                            <?php } ?>
                                            
                                            <option <?= $recordData->status == "Rejected" ? "selected" : (($recordData->status == "Approved") ? "disabled" : null) ?> value="Rejected">Rejected</option>
                                            <option <?= $recordData->status == "Cancelled" ? "selected" : null ?> value="Cancelled">Cancel</option>
                                        </select>

                                        <?php } else { ?>
                                        <span data-record="status"><?= $recordData->the_status_label ?></span>
                                        <?php } ?>
                                    </div>
                                    <div class="form-group">
                                        <?php if($isApproved && $reviewClaim) { ?>

                                            <p><label for="approved_by" class="text-muted">Approved By: </label> <span class="user-name-link" title="Click to view summary information" data-toggle="tooltip" data-function="load-form" data-module="user_basic_information" data-resource="user_information" data-module-id="<?= $recordData->approved_by ?>"><?= $recordData->approved_by_name ?></span></p>
                                            <p><label for="approved_date" class="text-muted">Approved Date: </label> <span><?= $recordData->approved_date ?></span></p>

                                        <?php } ?>
                                    </div>
                                    <div class="mt-1 form-group">
                                        
                                        <?php if($reviewClaim) { ?>

                                            <label for="assigned_to" class="text-muted">Assigned To:</label>

                                            <select data-width="100%" name="assigned_to" <?= (in_array($recordData->status, ["Deleted", "Cancelled"]) || !$isActive) ? "disabled" : (($recordData->status == "Approved") ? "disabled" : null);  ?> <?= $recordData->user_id == $loggedUserId ? "disabled" : null;  ?> id="assigned_to" class="selectpicker">
                                                <option value="null">Assign this Policy</option>
                                                <?php
                                                // load users that this can be assigned to
                                                if($userData->user_type == "insurance_company") {
                                                    $query = "SELECT item_id, name FROM users WHERE user_type IN ('agent', 'insurance_company') AND status = '1' AND deleted='0'";
                                                } elseif($userData->user_type == "nic") {
                                                    $query = "SELECT item_id, name FROM users WHERE user_type IN ('nic') AND status = '1' AND deleted='0'";
                                                } elseif(in_array($userData->user_type, ["bank", "bancassurance"])) {
                                                    $query = "SELECT item_id, name FROM users WHERE user_type IN ('bank','bancassurance') AND status = '1' AND deleted='0'";
                                                } else {
                                                    $query = "SELECT item_id, name FROM users WHERE user_type IN ('admin') AND status = '1' AND deleted='0'";
                                                }
                                                $stmt = $medics->query($query);
                                                $result = $stmt->fetchAll(PDO::FETCH_OBJ);
                                                // loop through the results
                                                foreach($result as $eachUser) {
                                                    print "<option ".($recordData->assigned_to == $eachUser->item_id ? "selected" :  null)." value=\"{$eachUser->item_id}\">{$eachUser->name}</option>";
                                                }
                                                ?>
                                            </select>

                                            <?php if(!empty($recordData->assigned_to)) { ?>
                                            <div class="mt-2 text-right">
                                                <span class="user-name-link" title="Click to view summary information" data-toggle="tooltip" data-function="load-form" data-module="user_basic_information" data-resource="user_information" data-module-id="<?= $recordData->assigned_to ?>">View details</span>
                                            </div>
                                            <?php } ?>

                                        <?php } ?>

                                        <?php if(!$reviewClaim) { ?>
                                            <div class="mt-0">
                                                <label class="p-0 m-0 text-muted" for="managed_by">Insurance Company:</label> <span title="Click to learn more about <?= $recordData->company_name ?>" data-toggle="tooltip" class="user-name-link cursor" onclick="return company_basic_information('<?= $recordData->company_id ?>')"><?= $recordData->company_name ?></span>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php if(($isActive && !$isClosed && $reviewClaim) || ($isApproved && empty($recordData->approved_amount))) { ?>
                                        <div class="mt-1 form-group text-center">
                                            <input type="hidden" name="item_id" id="item_id" value="<?= $recordId ?>" class="form-control">
                                            <input type="hidden" name="user_id" id="user_id" value="<?= $recordData->user_id ?>" class="form-control">
                                            <button type="submit" class="btn hidden save-changes btn-outline-success">Save Changes</button>
                                        </div>
                                    <?php } ?>
                                    
                                    <?php if($reviewClaim) { ?>
                                    <div class="mt-4 pt-4 justify-content-center border-top row form-group">
                                        <button data-toggle="tooltip" title="View summary history" data-function="load-form" data-resource="complaints" data-module-id="<?= $recordId ?>" data-module="activity_history" class="btn btn-outline-secondary">View activity history</button>
                                    </div>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>

                        <div class="col-lg-9 email-content">

                            <div class="email-head">
                                <div class="email-head-subject p-2 mr-0 pr-0">
                                    <div class="title d-flex align-items-center justify-content-between">
                                        <div class="d-flex align-items-center">
                                            <span><h4>Claims Request</h4></span>
                                        </div>
                                        <?php if($hasPermission && !$isActive) {  ?>
                                        <?= submit_draft_button("claims", $policyInfo->policy_name, $recordId) ?>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="email-head-sender d-flex align-items-center justify-content-between flex-wrap">
                                    <div class="d-flex align-items-center mb-2">
                                        <div class="avatar">
                                        <img src="<?= $baseUrl ?><?= $recordData->client_image ?>" alt="Avatar" class="rounded-circle user-avatar-md">
                                        </div>
                                        <div class="sender d-flex align-items-center">
                                            <p><a href="javascript:void(0)" data-toggle="tooltip" title='Click to view details of' onclick="return user_basic_information('<?= $recordData->user_id ?>', '<?= $additionInfo ?>')"><?= $recordData->client_raw_name ?></a></p>
                                            <p class="pl-3"><small><i class="fa fa-envelope"></i> <?= $recordData->client_email ?></small></p>
                                            <p class="pl-3"><small><i class="fa fa-phone"></i> <?= $recordData->client_contact ?></small></p>
                                        </div>
                                    </div>
                                    <div class="date"><i class="fa fa-calendar"></i> 
                                        <?php 
                                        // date to show
                                        $date = !empty($recordData->date_submitted) ? $recordData->date_submitted : $recordData->date_created;
                                        print date("M d Y, h:iA", strtotime($date))
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-lg-8">

                                    <div class="border-top p-0 m-0">
                                        <?php $comment = !$reviewClaim ? "Leave a comment for the <strong>Insurance Company</strong>" : null; ?>
                                        <?= leave_comments_builder("claim_comments", $recordId, $comment) ?>
                                        <div id="comments-container" data-autoload="true" data-last-reply-id="0" data-id="<?= $recordId ?>" class="slim-scroll pt-3 pr-2 pl-0" style="overflow-y:auto; max-height:850px"></div>
                                        <div class="load-more mt-3 text-center"><button id="load-more-replies" type="button" class="btn btn-outline-secondary">Loading comments</button></div>
                                    </div>

                                </div>

                                <div class="col-lg-4 p-0">
                                    <div class="email-attachments">
                                        <div class="grid-margin border-bottom pb-3">
                                            <p><span class="text-muted">Requested By:</span> <span class="underline" onclick="return user_basic_information('<?= $recordData->created_by ?>')"><?= $recordData->requested_by ?></span></p>
                                            <p><span class="text-muted">Date Created:</span> <?= $recordData->date_created ?></p>
                                        </div>
                                        <div class="row">  
                                            <div class="col-lg-12 col-md-6">
                                                <div class="form-group">
                                                    <label for="amount_claimed">Amount Claimed</label>
                                                    <input type="text" <?= !$reviewClaim ? "disabled" : ($recordData->status == "Approved" ? "disabled" : "name=\"amount_claimed\"") ?> value="<?= !$reviewClaim ? number_format($recordData->amount_claimed, 2) : ($recordData->status == "Approved" ? number_format($recordData->amount_claimed, 2) : $recordData->amount_claimed) ?>" onkeyup="this.value = this.value.replace(/[^\d.,]+/g, '');" maxlength="15" class="form-control font-18px">
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-6">
                                                <div class="form-group">
                                                    <label for="approved_amount">Approved Amount <?= $isPaid ? "<span class='badge badge-success'>Paid</span>" : "<span class='badge badge-danger'>Not Paid</span>" ?>
                                                        <?= (($recordData->status == "Approved") && empty($recordData->approved_amount) && $approveClaim) ? "<br><span class=\"small-font text-danger\">(The approved amount must be set)</span>" : null ?>
                                                    </label>
                                                    <!-- onkeyup="this.value = this.value.replace(/[^\d.,]+/g, '');"-->
                                                    <input <?= (!$approveClaim || $recordData->status !== "Approved") ? "disabled" : "" ?> <?= ($recordData->status == "Approved" && $isPaid) ? "disabled" : "name=\"approved_amount\"" ?> maxlength="15" type="text" value="<?= !empty($recordData->approved_amount) ? number_format($recordData->approved_amount, 2) : "" ?>" class="form-control font-18px">
                                                </div>
                                                <?php if($recordData->status == "Approved" && !$isPaid && $reviewClaim) { ?>
                                                <div class="mt-2 pb-2 text-right"><button type="button" data-function="pay-claim" data-claim_id="<?= $recordId ?>" class="btn btn-outline-success">Issue Payment</button></div>
                                                <?php } ?>
                                                <?php if($isPaid) { ?>
                                                <div>
                                                    <label class="p-0 mb-0">Payment Date</label>
                                                    <p class="p-0 m-0"><i class="fa fa-calendar-check"></i> <?= $recordData->payment_date ?></p>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>

                                        <?php if(!empty($recordData->attachment)) { ?>

                                            <div class="border-top pt-3">
                                                <div class="title">Attachments <span>(<?= count($recordData->attachment["files"]) ?? 0 ?> files, <?= $recordData->attachment["files_size"] ?? 0 ?>)</span></div>
                                                <?= $formsObj->list_attachments($recordData->attachment["files"], $loggedUserId, "col-lg-12 col-md-6", !$isActive); ?>
                                            </div>

                                        <?php } ?>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    </form>

                </div>
            </div>
        </div>

    <?php } ?>
    
</div>
<?php require "foottags.php"; ?>