<?php
// set the title
$page_title = "Complaints";

// require the headtags
require "headtags.php";
?>
<div class="page-content">
    <?php if(!$accessObject->hasAccess("view", "complaints")) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        <?= form_loader("fixed"); ?>
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <?php if($accessObject->hasAccess("lodge", "complaints")) { ?>
                <a href="javascript:void(0)" data-function="load-form" data-module="lodge_complaint" type="button" class="btn btn-primary btn-icon-text mb-2 mb-md-0">
                    <i class="btn-icon-prepend" data-feather="folder-plus"></i>
                    Lodge Complaint
                </a>
                <?php } ?>
            </div>
        </div>

        <div class="col-lg-12 p-0 col-xl-12">
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="related_to">Related To</label>
                        <select name="related_to" id="related_to" class="form-control selectpicker">
                            <option value="">Select Relation</option>
                            <?php foreach($medicsClass->pushQuery("name, id", "policy_form", "status='1' AND type='complaints'") as $related) { ?>
                            <option value="<?= $related->id ?>"><?= $related->name ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="status">Complaint Status</label>
                        <select name="status" id="status" class="form-control selectpicker">
                            <option value="">Select Status</option>
                            <option value="Pending">Pending</option>
                            <option value="Processing">Processing</option>
                            <option value="Answered">Answered</option>
                            <option value="Solved">Solved</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="d-flex justify-content-between">
                        <div class="form-group">
                            <label for="start_date">Submitted Start Date</label>
                            <input type="text" name="start_date" id="start_date" class="datepicker form-control">
                        </div>
                        <div class="form-group">
                            <label for="end_date">Submitted End Date</label>
                            <input type="text" name="end_date" id="end_date" class="datepicker form-control">
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <label for="filter_complaints">&nbsp;</label>
                    <button onclick="return filter_complaints()" class="btn btn-outline-primary btn-block" id="filter_complaints" name="filter"><i class="fa fa-filter"></i> Filter</button>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive slim-scroll">
                        <table class="table dataTable table-hover mb-0" data-status="" data-noinit="datatable" id="user_complaints">
                            <thead>
                                <tr>
                                    <th width="7%" class="pt-0">#</th>
                                    <th width="35%" class="pt-0">Subject</th>
                                    <th class="pt-0">Related To</th>
                                    <th width="18%" class="pt-0">Date Submitted</th>
                                    <th width="10%" class="pt-0">Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    
</div>
<?php require "foottags.php"; ?>