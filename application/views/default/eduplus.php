<?php
// set the title
$page_title = "InsureEdu Plus";

// extra resources
$loadedJS = ["assets/js/script/eduplus.js"];

$eduClass = load_class("eduplus", "controllers");
$params = (object) [];

// require the headtags
require "headtags.php";

// set the user id
$param = (object) [
    "userData" => $userData,
    "userId" => $loggedUserId
];
$params = (object) [];

// set the subject id
$topicFound = false;

// unset all admin session
$session->is_Admin_View = false;

// load the search term
$searchTerm = $session->searchTerm;
?>
<div class="page-content">
    
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
        </div>
        <?php if($accessObject->hasAccess("eduplus", "control")) { ?>
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            <a href="<?= $baseUrl ?>eduplus-manage" class="btn btn-primary btn-icon-text mb-2 mb-md-0">
                <i class="btn-icon-prepend" data-feather="box"></i> Manage
            </a>
        </div>
        <?php } ?>
    </div>
    <div class="row" id="eduplus_content">
        <div class="col-lg-4 mb-3 col-md-5">
            <div class="card mb-3">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline mb-2">
                        <h6 class="card-title mb-0"></h6>
                    </div>
                    <div class="form-group">
                        <label for="eduplus_category">Search Topic</label>
                        <div class="input-group date timepicker" data-target-input="nearest">
                            <input type="text" class="form-control" value="<?= $searchTerm ?>" name="topic_q" placeholder="Search for topic">
                            <div class="input-group-append">
                                <div class="input-group-text cursor" data-toggle="tooltip" title="Click to Search" onclick="return quick_Search_Topic()">
                                    <i class="fa fa-filter"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="eduplus_subject_loader"></div>
                    <div id="eduplus_subject_list"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-8 col-md-7">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline mb-2">
                        <h6 class="card-title mb-0"></h6>
                    </div>
                    <div id="eduplus_content">
                        <div>
                            <div id="accordion" class="accordion" role="tablist">
                                <?php foreach($eduClass->category_list($params)["data"] as $key => $category) { ?>
                                    <?php
                                    // set the category id to the parameter
                                    $param->category_id = $category->item_id;
                                    // load the topics for each category
                                    $topics_list = $eduClass->subject_list($param)["data"];
                                    ?>
                                    <div class="card mb-3 eduplus_category_list" data-row_id="<?= $key ?>" data-record-setid="<?= $category->item_id ?>">
                                        <div class="card-header" role="tab" id="heading_<?= $category->item_id ?>">
                                            <h6 class="mb-0">
                                                <a class="collapsed" data-toggle="collapse" href="#collapse_<?= $category->item_id ?>" aria-expanded="false" aria-controls="collapse_<?= $category->item_id ?>">
                                                    <?= $category->name ?>
                                                </a>
                                            </h6>
                                        </div>
                                        <div id="collapse_<?= $category->item_id ?>" class="collapse" role="tabpanel" aria-labelledby="heading_<?= $category->item_id ?>" data-parent="#accordion">
                                            <div class="card-body">
                                                <?= !empty($category->description) ? "<div>{$category->description}</div>" : ""; ?>
                                                <div class="mt-3">
                                                    <h5 class="text-uppercase border-bottom mb-3">Category Topics</h5>
                                                    <div class="row mt-3">
                                                        <?php
                                                        // get the items count
                                                        if(!empty($topics_list)) {
                                                            // loop through the items
                                                            foreach($topics_list as $ikey => $subject) {
                                                                ?>
                                                                <div class="col-lg-6 col-md-12 mb-3" data-row_id="<?= $ikey ?>" data-record-setid="<?= $subject->item_id ?>">
                                                                    <div class="card">
                                                                        <div class="card-header p-2">
                                                                            <?= str_word_count($subject->subject) > 4 ? "<span  title='{$subject->subject}' data-toggle='tooltip'>".limit_words($subject->subject, 4). "...</span>" : $subject->subject ?>
                                                                        </div>
                                                                        <div class="card-body p-2 mb-2 slim-scroll" style="min-height:120px; max-height:120px; overflow-y:auto">
                                                                            <?= limit_words($subject->description, 50) ?>
                                                                        </div>
                                                                        <div class="card-footer p-1">
                                                                            <div class="d-flex justify-content-between">
                                                                                <div>
                                                                                    <a href="<?= $baseUrl ?>eduplus-view/<?= $subject->item_id ?>" class="btn btn-sm p-1 btn-outline-primary"><i style="font-size:12px" class="fa fa-eye"></i> Read Topic</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php }
                                                        } else { ?>
                                                        <div class="col-lg-12 font-italic">Sorry! No topics have been uploaded yet.</div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
</div>
<?php require "foottags.php"; ?>