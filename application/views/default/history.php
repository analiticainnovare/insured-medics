<?php
// set the title
$page_title = "Property History";

// get some variable
$itemFound = false;
$recordId = confirm_url_id(1) ? xss_clean($SITEURL[1]) : null;

// load the activity logs on a single item
if($recordId) {

    /** load the users insurance policies */
    $recordObj = load_class("forms", "controllers");

    // return the result
    $recordData = $recordObj->property_activity_logs($recordId, 1000, true);
    $itemFound = !empty($recordData) ? true : false;

    // if the complaint was found
    if($itemFound) {
       
    }
}

// require the headtags
require "headtags.php";
?>
<div class="page-content">
    <?php if(!$itemFound) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        <?= form_loader("fixed"); ?>
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <a href="javascript:history.back()" type="button" class="btn btn-outline-primary btn-icon-text mb-2 mb-md-0">
                    <i class="fa fa-arrow-left"></i>
                    Go Back
                </a> &nbsp;
            </div>
        </div>

        <div class="col-lg-12 p-0 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline mb-2">
                        <h6 class="card-title mb-0"></h6>
                    </div>
                    <div class="table-responsive slim-scroll">
                        <table class="table dataTable table-hover nowrap mb-0" id="history_list">
                            <thead>
                                <tr>
                                    <th width="6%" class="pt-0">#</th>
                                    <th class="pt-0">Property</th>
                                    <th width="15%" class="pt-0">Date Created</th>
                                    <th>Source</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($recordData as $key => $eachItem) { ?>
                                    <tr>
                                        <td><?= $key+1 ?></td>
                                        <td><?= $eachItem->description ?></td>
                                        <td>
                                            <?= $eachItem->date_recorded ?>
                                            <p>by: <?= $eachItem->fullname ?></p>
                                        </td>
                                        <td width="50%"><?= $eachItem->source ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    
</div>
<?php require "foottags.php"; ?>