<?php
// set the title
$page_title = "Add Insurance Policy";

// extra css
$loadedJS = [
    "assets/js/ajax-load.js",
    "assets/js/script/forms.js"
];

// require the headtags
require "headtags.php";

// create a new files object
$formsObj = load_class("forms", "controllers");

/** Set parameters for the data to attach */
$form_params = (object) [
    "module" => "company_policy",
    "userData" => $userData,
    "item_id" => null,
    "no_title" => true
];
?>
<style>
trix-editor {
    min-height: 100px;
    max-height: 100px;
}
</style>
<div class="page-content">
    <?php if(!$accessObject->hasAccess("add", "company_policy")) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <?php if($accessObject->hasAccess("view", "company_policy")) { ?>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <a href="<?= $baseUrl ?>policy-list" class="btn btn-primary btn-icon-text mb-2 mb-md-0">
                    <i class="btn-icon-prepend" data-feather="arrow-left"></i>
                    Insurance Policies
                </a>
            </div>
            <?php } ?>
        </div>
        
        <div class="col-lg-12 p-0 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <form action="<?= $baseUrl ?>api/company_policy/add" id="jsform-wrapper">
                        <div class="row mb-2">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                <div class="input-group-text">Policy Name &nbsp;<span class="required">*</span></div>
                                                </div>
                                                <input type="text" name="policy_name" id="policy_name" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                <div class="input-group-text">Policy Code &nbsp;<span class="required">*</span></div>
                                                </div>
                                                <input type="text" name="policy_id" id="policy_id" class="form-control text-uppercase">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                <div class="input-group-text">Policy Category</div>
                                                </div>
                                                <select name="policy_category" id="policy_category" class="form-control selectpicker">
                                                    <option value="">Select Policy Category</option>
                                                    <?php
                                                    foreach($medicsClass->pushQuery("*", "policy_form", "type='policy_category' AND status='1'") as $eachCategory) {
                                                        print "<option value=\"".create_slug($eachCategory->name)."\">{$eachCategory->name}</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="policy_description">Policy Description &nbsp;<span class="required">*</span></label>
                                            <?php if($text_editor == "ckeditor") { ?>
                                                <textarea class="form-control" name="policy_description" id="policy_description" rows="7"></textarea>
                                            <?php } else { ?>
                                                <trix-editor name="policy_description" class="trix-slim-scroll" id="policy_description"></trix-editor>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 mb-3">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="policy_requirements">Policy Requirements</label>
                                                    <?php if($text_editor == "ckeditor") { ?>
                                                        <textarea class="form-control" name="policy_requirements" id="policy_requirements" rows="7"></textarea>
                                                    <?php } else { ?>
                                                        <trix-editor name="policy_requirements" class="trix-slim-scroll" id="policy_requirements" input="policy_requirements"></trix-editor>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-7">
                                                        <div class="form-group">
                                                            <label for="allow_attachment">Allow attachment to form by clients</label>
                                                            <select name="allow_attachment" id="allow_attachment" class="form-control selectpicker">
                                                                <option value="yes">Yes! Allow file attachments</option>
                                                                <option value="no">No! Do not allow file attachments</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                            <label for="year_enrolled">Month/Year Enrolled</label>
                                                            <input type="text" class="form-control" id="year_enrolled" name="year_enrolled">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="form_footnote">Form Footnote <small>(This will appear at the bottom of the form)</small></label>
                                                    <textarea name="form_footnote" placeholder="Enter form footnote" id="form_footnote" cols="30" rows="5   " class="form-control"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <label for="allow_attachment">Policy Legal Documents <small class="font-italic"> - Attach additional legal document (if any) to this policy</small></label>
                                                    <?php 
                                                    // load the attachment method
                                                    print $formsObj->form_attachment_placeholder($form_params);
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-12">
                                <div class="row mt-4">
                                    <div class="col-lg-12">
                                        <div class="border-bottom pb-2">
                                            <div class="col-lg-12 p-0 m-0 row justify-content-between">
                                                <div><h4>Form Content</h4></div>
                                                <?= form_manager_options() ?>
                                            </div>
                                        </div>
                                        <div id="form-pretext" class="text-center font-italic mt-2">The policy form is empty. Add new fields to the set.</div>
                                        <div class="pt-2 pl-0 mt-2" id="jsform-container"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="row mt-3">
                                    <div class="col-md-6 text-left">
                                        <button type='button' style='padding:5px' class='btn preview-form hidden btn-sm btn-secondary'><i class='fa fa-eye'></i> Preview Form</button>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <button class="btn btn-outline-success" type="submit"><i class="fa fa-save"></i> Save Policy & Add Claim Form </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    <?php } ?>
    
</div>
<?= save_form_button(); ?>
<?= select_field_modal(); ?>
<?php require "foottags.php"; ?>