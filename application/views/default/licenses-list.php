<?php
// set the title
$page_title = "Licenses";

// require the headtags
require "headtags.php";
?>
<div class="page-content">
    <?php if(!$accessObject->hasAccess("view", "licenses")) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <?php if($accessObject->hasAccess("apply", "licenses")) { ?>
                <a href="javascript:void(0)" data-function="load-form" data-module="license_application" type="button" class="btn btn-primary btn-icon-text mb-2 mb-md-0">
                    <i class="btn-icon-prepend" data-feather="folder-plus"></i>
                    Apply for License
                </a>
                <?php } ?>
            </div>
        </div>

        <div class="col-lg-12 p-0 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline mb-2">
                        <h6 class="card-title mb-0"></h6>
                    </div>
                    <div class="table-responsive slim-scroll">
                        <table class="table dataTable table-hover mb-0" data-status="" id="licenses_list">
                            <thead>
                                <tr>
                                    <th class="pt-0">#</th>
                                    <th class="pt-0">License ID</th>
                                    <th class="pt-0">Date of Expiry</th>
                                    <th class="pt-0">Date of Application</th>
                                    <th>Applied By</th>
                                    <th class="pt-0">Status</th>
                                    <th class="pt-0"></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    
</div>
<?php require "foottags.php"; ?>