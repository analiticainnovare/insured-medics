<?php
// set the title
$page_title = "Insurance Agents";

// require the headtags
require "headtags.php";

// set the item
$session->user_type_to_add = "agent";
?>
<div class="page-content">
    <?php if(!$accessObject->hasAccess("view", "user_policy")) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        <?= form_loader("fixed"); ?>
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <?php if($accessObject->hasAccess("add", "agents")) { ?>
                <a href="javascript:void(0)" data-function="load-form" data-module="user_account" data-resource="agent" type="button" class="btn btn-primary btn-icon-text mb-2 mb-md-0">
                    <i class="btn-icon-prepend fa fa-user-plus"></i>
                    Add Agent
                </a>
                <?php } ?>
            </div>
        </div>

        <div class="col-lg-12 p-0 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline mb-2">
                        <h6 class="card-title mb-0"></h6>
                    </div>
                    <div class="table-responsive">
                        <table class="table dataTable table-hover mb-0" data-noinit="datatable" id="agents_list">
                            <thead>
                                <tr>
                                    <th class="pt-0">#</th>
                                    <th class="pt-0">Name</th>
                                    <th class="pt-0">Contact Details</th>
                                    <th class="pt-0">Number of Policies</th>
                                    <th class="pt-0">Assigned Claims</th>
                                    <th class="pt-0">Managed Clients</th>
                                    <th class="pt-0">Status</th>
                                    <th class="pt-0"></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    
</div>
<?php require "foottags.php"; ?>