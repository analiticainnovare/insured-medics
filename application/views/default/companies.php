<?php
// set the title
$page_title = "Insurance Companies";

// require the headtags
require "headtags.php";

// set the item
$session->user_type_to_add = "brokers";
?>
<div class="page-content">
    <?php if(!$accessObject->hasAccess("view", "insurance_company")) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        <?= form_loader() ?>
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
        </div>
        <div class="col-lg-12 p-0 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table dataTable table-hover" id="insurance_companies">
                            <thead>
                                <tr>
                                    <th width="5%" class="pt-0">#</th>
                                    <th width="20%" class="pt-0">Name</th>
                                    <th class="pt-0">Contact</th>
                                    <th class="pt-0">Address</th>
                                    <th class="pt-0">Email</th>
                                    <th width="10%">Number of Policies</th>
                                    <th width="6%" class="pt-0"></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<?php require "foottags.php"; ?>