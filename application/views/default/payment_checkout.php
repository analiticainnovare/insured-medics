<?php
// set the title
$page_title = "Payment";

// additional css and javascript files
$loadedJS = [
    "assets/js/script/payments.js",
];

// require the headtags
require "headtags.php";

// variables
$itemFound = false;

// load payment information
$recordId = confirm_url_id(1) ? xss_clean($SITEURL[1]) : null;

// load the complaint information
if($recordId) {

    /** Parameters */
    $param = (object) [
        "limit" => 1,
        "checkout_url" => $recordId, 
        "userData" => $userData
    ];
    
    // Create new object
    $paymentObj = load_class("payments", "controllers");

    // return the result
    $recordData = $paymentObj->list($param);
    $itemFound = !empty($recordData["data"]) ? true : false;

    // if the complaint was found
    if($itemFound) {
        $recordData = $recordData["data"][0];
        $recordData->payment_details  = (object)[];
        $recordData->payment_details->user_id = $recordData->user_id;
        $recordData->payment_details->amount_payable = $recordData->amount;
        $recordData->payment_details->checkout_url = $recordData->checkout_url;
        $recordData->payment_details->transaction_id = $recordData->transaction_id;
        $recordData->payment_details->fullname = $recordData->created_by_info->name;
        $recordData->payment_details->email_address = $recordData->created_by_info->email;
        $recordData->payment_details->amount_payable_formated = $medicsClass->append_zeros(($recordData->amount * 100), 12);
    }
}
?>
<div class="page-content">
    <?php if(!$itemFound) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>

        <?= form_loader("fixed"); ?>

        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <a href="javascript:history.back()" type="button" class="btn btn-outline-primary btn-icon-text mb-2 mb-md-0">
                    <i class="fa fa-arrow-left"></i>
                    Go Back
                </a> &nbsp;
            </div>
        </div>

        <?php if(!confirm_url_id(2, "verify")) { ?>
        <div class="col-lg-12 p-0 stretch-card">
            <div class="card">
                <div class="card-body p-3">
                    <div class="row">
                        <div class="col-md-6 col-lg-5">
                            <h5 class="card-title">Item Details</h5>
                            <div class="table-responsive slim-scroll">
                                <table class="table table-hover">
                                    <tbody>
                                        <?php foreach($recordData->record_details as $key => $value) { ?>
                                            <tr>
                                                <td><?= strtoupper(str_replace(["_"], " ", $key)) ?></td>
                                                <td>
                                                    <?php if($key == "image") { ?>
                                                    <img src="<?= $baseUrl ?><?= $value ?>" width="100px" style="border-radius:0px; width: 200px; height: 150px;" alt="">
                                                    <?php } else { ?>
                                                        <?= $value ?>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="amount_payable">Amount Payable</label>
                                <input type="text" readonly name="amount_payable" style="font-size:25px" value="<?= $recordData->amount ?>" id="amount_payable" class="form-control">
                            </div>
                            <?php if($recordData->payment_status !== "Paid") { ?>
                            <div class="form-group">
                                <label for="payment_medium">Payment Medium</label>
                                <select data-transaction_info='<?= json_encode($recordData->payment_details) ?>' name="payment_medium" id="payment_medium" class="form-control selectpicker">
                                    <option value="null">Select Payment Medium</option>
                                    <option value="payswitch">PaySwitch</option>
                                    <option value="slydepay">SlydePay</option>
                                </select>
                            </div>
                            <div class="form-group text-right">
                                <button class="btn btn-outline-success hidden make-payment">Process Payment</button>
                            </div>
                            <?php } else { ?>

                                <p class="mb-2"><span class="badge badge-success">Amount Paid</span></p>
                                <div class="border-bottom font-weight-bolder">PAYMENT SUMMARY</div>
                                <p><strong>Payment Date: <i class="fa fa-calendar-check"></i> </strong><?= ucwords($recordData->payment_date) ?></p>
                                <p><strong>Payment Option: </strong><?= ucwords($recordData->payment_option) ?></p>
                                <?php foreach($recordData->payment_info as $key => $value) { ?>
                                    <p><strong><?= ucwords($key) ?></strong>: <?= $value ?></p>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } else { ?>
            <?php 
            /** Process the payment request pushed */
            if($recordData->payment_option == "payswitch") {

                /** Confirm the parameters parsed in the url */
                if(isset($_GET["code"], $_GET["status"], $_GET["transaction_id"])) {
                    
                    /** If the payment was declined */
                    if($_GET["code"] == "020" && $_GET["status"] == "Declined") {
                        
                        /** Get the reason */
                        $reason = xss_clean($_GET["reason"]);

                        /** We can try again */
                        print "<div class='text-danger mb-3 text-center'>{$reason}</div>";
                        print "<div class='text-center'>
                            <a class=\"btn btn-outline-success\" href=\"{$recordData->payment_checkout_url}\">Try Again</a>
                        </div>";
                    }
                    /** If redirection is required */
                    elseif ($_GET["status"] == "vbv required" && isset($_GET["reason"])) {
                        // No VBV Required (Redirect the user to the)
                        print "<script>window.location.href='{$_GET["reason"]}'</script>";
                    }
                    /** If the payment was successful */
                    elseif($_GET["code"] == "000" && $_GET["status"] == "approved") {
                        
                        /** Get the reason */
                        $reason = xss_clean($_GET["reason"]);
                        $verify_payment = true;

                        /** We can try again */
                        print "<div data-payment_module=\"payswitch\" data-transaction_id=\"$recordData->transaction_id\" class='verify_payment mb-3 text-center'>
                            Payment verification <i class=\"fa fa-spin fa-spinner\"></i>
                        </div>";                        
                    }
                }

            }
            ?>
        <?php } ?>

        <div class="default-variables"></div>
        <div class="payment_check" data-value="1"></div>
        <div class="payment-backdrop hidden text-center">
            <div class="payment-buttons text-center">
                <button class="btn btn-success return-to-payment-window">View Payment Window</button>
                <button class="btn btn-danger cancel-ongoing-payment-activity">Cancel</button>
            </div>
        </div>
    <?php } ?>
</div>
<?php require "foottags.php"; ?>
