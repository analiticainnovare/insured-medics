<?php 
// base url
$baseUrl = config_item("base_url");
// verify header
$page_title = "Account Verification";
$content = "Verify your Account";

// if password was set
if(isset($_GET["password"])) {
    $page_title = "Reset Password";
    $content = "Recover Password";
}
// set the token variable
$token = (isset($_GET["token"]) && strlen($_GET["token"]) > 40) ? xss_clean($_GET["token"]) : null; 
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?= $page_title ?> - <?= config_item('site_name') ?></title>
	<link rel="stylesheet" href="<?= $baseUrl ?>assets/vendors/core/core.css">
	<link rel="stylesheet" href="<?= $baseUrl ?>assets/fonts/feather-font/css/iconfont.css">
	<link rel="stylesheet" href="<?= $baseUrl ?>assets/vendors/flag-icon-css/css/flag-icon.min.css">
	<link rel="stylesheet" href="<?= $baseUrl ?>assets/css/style/light-theme.css">
    <link href="<?= $baseUrl ?>assets/css/fontawesome.css"  rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?= $baseUrl ?>assets/css/custom.css">
    <link rel="shortcut icon" href="<?= $baseUrl ?>assets/images/favicon.png" />
    <style>
        .page-content {
            background-image: url("<?= $baseUrl ?>assets/images/carousel/img61.jpg");
            background-attachment: fixed;
            background-position: center;
            background-size: cover;
        }
    </style>
</head>
<body>
	<div class="main-wrapper">
		<div class="page-wrapper full-page">
			<div class="page-content d-flex align-items-center justify-content-center">                
                <div class="row w-100 mx-0 auth-page">
					<div class="col-md-8 col-xl-6 mx-auto">
						<div class="card">
							<div class="row">
                                <div class="col-md-3 pr-md-0">
                                    <div class="auth-left-wrapper">

                                    </div>
                                </div>
                                <div class="col-md-9 pl-md-0">
                                <div class="auth-form-wrapper px-4 py-5">
                                    <a href="<?= $baseUrl ?>" class="noble-ui-logo d-block mb-2">Insuretech <span>Hub</span></a>
                                    <h5 class="text-muted font-weight-normal mb-4"><?= $content ?></h5>
                                    <div class="form-group">
                                        <?php if($token && ($page_title == "Reset Password")) { ?>
                                            <?php
                                            // reset the expiry
                                            $expiry_time = 60*60*6;
                                            // verify user account
                                            $stmt = $medics->prepare("SELECT * FROM users_reset_request WHERE request_token=? AND token_status=? LIMIT 1");
                                            $stmt->execute([$token, 'PENDING']);
                                            // failed
                                            $count = $stmt->rowCount();
                                            // if not empty
                                            if($count) {
                                                // get the results
                                                $data = $stmt->fetch(PDO::FETCH_OBJ);
                                                // time check
                                                $expiry_time = $data->expiry_time;
                                                $username = $data->username;
                                                $user_id = $data->user_id;
                                            }
                                            // confirm that the token hasnt yet expired
                                            if(!$count || ($expiry_time < time())) {
                                                print "<div class='alert alert-danger'>Sorry! The token could not be authenticated or has expired. <a href='{$baseUrl}'>Go Back</a></div>";
                                            }
                                            // present the user with the form to reset password
                                            if($count) {
                                            ?>
                                            <form method="POST" autocomplete="Off" action="<?= $baseUrl ?>api/auth" class="forms-sample">
                                                <?= form_loader(); ?>
                                                <div class="form-group">
                                                    <label for="password">Password</label>
                                                    <input type="password" class="form-control" name="password" id="password" placeholder="Enter password">
                                                </div>
                                                <div class="form-group">
                                                    <label for="password_2">Confirm Password</label>
                                                    <input type="password" class="form-control" name="password_2" id="password_2" placeholder="Confirm password">
                                                </div>
                                                <div class="mt-3">
                                                    <button type="submit" class="btn btn-primary mr-2 mb-2 mb-md-0 text-white">Recover</button>
                                                    <input type="hidden" name="reset_token" value="<?= $token ?>" id="reset_token" hidden>
                                                </div>
                                                <div class="mt-3">
                                                    <a href="<?= $baseUrl ?>" class="mt-3 text-muted">Go Back</a>
                                                </div>
                                                <div class="form-results"></div>
                                            </form>
                                            <?php } ?>
                                        <?php } elseif($token && ($page_title == "Account Verification")) { ?>
                                            <?php 
                                            // reset the expiry
                                            $expiry_time = 60*60*6;
                                            // verify user account
                                            $stmt = $medics->prepare("SELECT name AS fullname, email, item_id AS user_id, username, token_expiry FROM users WHERE verify_token=? LIMIT 1");
                                            $stmt->execute([$token]);
                                            // failed
                                            $count = $stmt->rowCount();
                                            // if not empty
                                            if($count) {
                                                // get the results
                                                $data = $stmt->fetch(PDO::FETCH_OBJ);
                                                // time check
                                                $data->verify_token = $token;
                                                $expiry_time = $data->token_expiry;
                                                $username = $data->username;
                                                $user_id = $data->user_id;
                                            }
                                            // confirm that the token hasnt yet expired
                                            if($count) {
                                                // if the expiry time is less than the current time
                                                if($expiry_time < time()) {
                                                    // confirm if the user has selected to resend the verify link
                                                    if(isset($_GET["resend"]) && ($_GET["resend"] == "token")) {
                                                        // generate the token
                                                        $query = $usersClass->resend_token($data);
                                                        // sent notification
                                                        if($query["code"] == 200) {
                                                            print "<div class='alert alert-success'>The verification link has been resent via email.</div>";
                                                        }
                                                    } else {
                                                        // print the error message
                                                        print "<div class='alert alert-danger'>Sorry! The verify token has expired. Click <a href='{$baseUrl}verify?account&token={$token}&resend=token'>here</a> to resend.</div>";
                                                    }
                                                } else {
                                                    // activate the user account
                                                    $query = $usersClass->activate_account($data);
                                                    // print the success message
                                                    print "<div class='alert alert-success'>Congrats! Your account has successfully been verified. <a href='{$baseUrl}login'>Login</a> to continue.</div>";
                                                }
                                            } else {
                                                print "<div class='alert alert-danger'>Sorry! The token could not be authenticated or has expired <a href='{$baseUrl}'>Go Back</a>.</div>";
                                            }
                                            ?>

                                        <?php } else { ?>
                                        <?= "<div class='alert alert-danger'>Sorry! The token could not be authenticated or has expired <a href='{$baseUrl}'>Go Back</a>.</div>"; ?>
                                        <?php } ?>
                                    </div>
                                    
                                </div>
                                </div>
                            </div>
						</div>
					</div>
				</div>
            </div>
		</div>
	</div>
    <script>var baseUrl = "<?= $baseUrl ?>";</script>
	<script src="<?= $baseUrl ?>assets/vendors/core/core.js"></script>
	<script src="<?= $baseUrl ?>assets/vendors/feather-icons/feather.min.js"></script>
	<script src="<?= $baseUrl ?>assets/js/template.js"></script>
    <script src="<?= $baseUrl ?>assets/js/script/auth.js"></script>
</body>
</html>
