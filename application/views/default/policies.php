<?php
// set the title
$page_title = "Insurance Policies";

// require the headtags
require "headtags.php";
?>
<div class="page-content">
    <?php if(!$accessObject->hasAccess("view", "user_policy")) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        <?= form_loader("fixed"); ?>
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <?php if($accessObject->hasAccess("signup", "company_policy")) { ?>
                <a href="javascript:void(0)" data-function="load-form" data-module="apply_policy" type="button" class="btn btn-primary btn-icon-text mb-2 mb-md-0">
                    <i class="btn-icon-prepend" data-feather="folder-plus"></i>
                    Apply for Policy
                </a>
                <?php } ?>
            </div>
        </div>

        <div class="col-lg-12 p-0 col-xl-12">
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="policy_type">Policy Type</label>
                        <select name="policy_type" id="policy_type" class="form-control selectpicker">
                            <option value="">Select Policy Type</option>
                            <?php foreach($medicsClass->pushQuery("name, policy_id, item_id", "policy_types", "policy_status ='Enrolled' AND status='1'") as $policy) { ?>
                            <option value="<?= $policy->item_id ?>"><?= $policy->name ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="status">Policy Status</label>
                        <select name="status" id="status" class="form-control selectpicker">
                            <option value="">Select Status</option>
                            <option value="Pending">Pending</option>
                            <option value="Processing">Processing</option>
                            <option value="Approved">Approved</option>
                            <option value="Ellapsed">Ellapsed</option>
                            <option value="Cancelled">Policy Cancelled</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="d-flex justify-content-between">
                        <div class="form-group">
                            <label for="start_date">Start Date</label>
                            <input type="text" name="start_date" id="start_date" class="datepicker form-control">
                        </div>
                        <div class="form-group">
                            <label for="end_date">End Date</label>
                            <input type="text" name="end_date" id="end_date" class="datepicker form-control">
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <label for="filter_policy">&nbsp;</label>
                    <button onclick="return filter_policy()" class="btn btn-outline-primary btn-block" id="filter_policy" name="filter"><i class="fa fa-filter"></i> Filter</button>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline mb-2">
                        <h6 class="card-title mb-0"></h6>
                    </div>
                    <div class="table-responsive slim-scroll">
                        <table class="table dataTable table-hover mb-0" data-noinit="datatable" data-status="" id="user_policies">
                            <thead>
                                <tr>
                                    <th width="6%" class="pt-0">#</th>
                                    <th class="pt-0">Policy Info</th>
                                    <th class="pt-0">Start Date</th>
                                    <th class="pt-0">Premium</th>
                                    <th class="pt-0">Next Due Date</th>
                                    <th width="6%">Status</th>
                                    <th class="pt-0">Signup Date</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    
</div>
<?php require "foottags.php"; ?>