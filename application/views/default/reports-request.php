<?php
// set the title
$page_title = "Report Requests";

// require the headtags
require "headtags.php";

?>
<div class="page-content">
    <?php if(!$accessObject->hasAccess("view", "reports")) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        <?= form_loader("fixed"); ?>

        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <?php if($accessObject->hasAccess("request", "reports")) { ?>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <a href="javascript:void(0)" data-function="load-form" data-module="request_report" type="button" class="btn btn-primary btn-icon-text mb-2 mb-md-0">
                    <i class="btn-icon-prepend" data-feather="folder-plus"></i>
                    Place a New Request
                </a>
            </div>
            <?php } ?>
        </div>

        <div class="col-lg-12 p-0 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline mb-2">
                        <h6 class="card-title mb-0"></h6>
                    </div>
                    <div class="table-responsive slim-scroll">
                        <table class="table dataTable table-hover mb-0" data-noinit="datatable" id="report_requests">
                            <thead>
                                <tr>
                                    <th width="6%">#</th>
                                    <th>Subject</th>
                                    <th width="20%">Report Type</th>
                                    <th width="15%">Date Created</th>
                                    <th width="15%">Expected Date</th>
                                    <th width="9%">Status</th>
                                    <th width="7%"></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    <?php } ?>
</div>
<?php require "foottags.php"; ?>