<?php
// set the title
$page_title = "Emails";

// additional css and javascript files
$loadedJS = [
    "assets/js/script/suggestion.js",
    "assets/js/script/attachments.js",
    "assets/js/script/emails.js"
];

$loadedCSS = [
    "assets/css/suggestion.css"
];

// require the headtags
require "headtags.php";

// create a new object of the forms class
$formsObj = load_class("forms", "controllers");

// set the current as as the person logged in
$viewEmail = $accessObject->hasAccess("view_email", "communication");
$sendEmail = $accessObject->hasAccess("send_email", "communication");

// is this the current user?
$isAdmin = $userData->user_type == "admin" ? true : false;

/** Set parameters for the data to attach */
$form_params = (object) [
    "module" => "emails",
    "userData" => $userData,
    "item_id" => "emails",
    "no_footer" => true,
    "no_padding" => true
];
?>
<style>
trix-editor {
    border: 1px solid #bbb;
    border-radius: 3px;
    margin: 0;
    padding: 0.4em 0.6em;
    min-height: 270px;
    outline: none;
    max-height: 270px;
    overflow-y: auto;
}
</style>
<div class="page-content">
    <?php if(empty($viewEmail)) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>

        
        <div class="row inbox-wrapper">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-3 email-aside border-lg-right">
                    <div class="aside-content">
                      <div class="aside-header">
                        <button class="navbar-toggle" data-target=".aside-nav" data-toggle="collapse" type="button"><span class="icon"><i data-feather="chevron-down"></i></span></button><span class="title">Mail Service</span>
                        <p class="description"><?= $userData->email ?></p>
                      </div>
                      <?php if($sendEmail) { ?>
                      <div class="aside-compose"><a class="btn btn-primary btn-block" id="send_mail" href="javascript:void(0)">Compose Email</a></div>
                      <?php } ?>
                      <div class="aside-nav <?= !$sendEmail ? "mt-3" : "" ?> collapse">
                        <ul class="nav" data-email-duty="list">
                          <li class="active"><a data-email-duty="list" data-email-value="inbox" href="javascript:void(0)"><span class="icon"><i data-feather="inbox"></i></span>Inbox<span class="badge badge-danger-muted text-white font-weight-bold float-right" data-mails-count="inbox">0</span></a></li>
                          <li><a data-email-duty="list" data-email-value="sent" href="javascript:void(0)"><span class="icon"><i data-feather="mail"></i></span data-email-label-count="sent">Sent Mail</a></li>
                          <li><a data-email-duty="list" data-email-value="important" href="javascript:void(0)"><span class="icon"><i data-feather="briefcase"></i></span>Important</a></li>
                          <?php if($sendEmail) { ?>
                          <li><a data-email-duty="list" data-email-value="draft" href="javascript:void(0)"><span class="icon"><i data-feather="file"></i></span>Drafts <span class="badge badge-primary text-white font-weight-bold float-right" data-mails-count="draft">0</span></a></li>
                          <?php } ?>
                          <li><a data-email-duty="list" data-email-value="favorite" href="javascript:void(0)"><span class="icon"><i data-feather="star"></i></span>Favorite <span class="badge badge-warning text-white font-weight-bold float-right" data-mails-count="favorite">0</span></a></li>
                          <li><a data-email-duty="list" data-email-value="trash" href="javascript:void(0)"><span class="icon"><i data-feather="trash"></i></span>Trash </a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-9 email-content">
                    <div class="email-inbox-header">
                      <div class="row align-items-center">
                        <div class="col-lg-6">
                          <div id="email-content-filters" class="email-title mb-2 mb-md-0"><span class="icon"><i data-feather="inbox"></i></span> Inbox <span class="new-messages">(<span data-mails-count="unread_count">0</span> new messages)</span> </div>
                        </div>
                        <div class="d-none d-md-block col-lg-6">
                          <div class="email-search">
                            <div class="input-group input-search">
                              <input class="form-control email-search-item" name="search" type="text" placeholder="Search mail..."><span class="input-group-btn">
                              <button class="btn btn-outline-secondary email-search-btn" type="button"><i data-feather="search"></i></button></span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div id="email-content-filters" class="email-filters d-flex align-items-center justify-content-between flex-wrap">
                      
                      <div class="email-filters-left flex-wrap d-none d-md-flex">
                        <div class="form-check form-check-flat form-check-primary">
                          <label class="form-check-label">
                            <input type="checkbox" class="data-email-checkall form-check-input">
                          </label>
                        </div>
                        <div class="btn-group ml-3">
                          <button class="btn btn-outline-primary dropdown-toggle" data-toggle="dropdown" type="button"> With selected <span class="caret"></span></button>
                          <div class="dropdown-menu" role="menu">
                            <button class="dropdown-item" data-email-change="state" data-email-value="mark_as_read" href="javascript:void(0)">Mark as read</button>
                            <button class="dropdown-item font-weight-bolder" data-email-change="state" data-email-value="mark_as_unread" href="javascript:void(0)">Mark as unread</button>
                            <a class="dropdown-item text-warning" data-email-change="state" data-email-value="favorite" href="javascript:void(0)">Set as Favorite</a>
                            <div class="dropdown-divider"></div>
                            <button class="dropdown-item text-success" data-email-change="state" data-email-value="important" href="javascript:void(0)">Mark as Important</button>
                            <div class="dropdown-divider"></div>
                            <button class="dropdown-item hidden" data-email-change="state" data-email-value="inbox" href="javascript:void(0)">Move to Inbox</button>
                            <button class="dropdown-item" data-email-change="state" data-email-value="trash" href="javascript:void(0)">Move to Trash</button>
                            <button class="dropdown-item text-danger" data-email-change="state" data-email-value="delete" href="javascript:void(0)">Delete</button>
                          </div>
                        </div>
                      </div>
                      <div class="email-filters-right"><span class="email-pagination-indicator"><span data-pagination="start_point">0</span>-<span data-pagination="end_point">0</span> of <span data-pagination="total_count">0</span></span>
                        <div class="btn-group email-pagination-nav">
                          <button class="btn new-email-list btn-outline-secondary btn-icon" type="button"><i data-feather="chevron-left"></i></button>
                          <button class="btn old-email-list btn-outline-secondary btn-icon" type="button"><i data-feather="chevron-right"></i></button>
                        </div>
                      </div>

                    </div>
                    <div class="email-list">
                        <?= absolute_loader() ?>
                        <div id="emails-content-listing" class="emails-content-listing slim-scroll"></div>
                        <div id="emails-content-display"></div>
                    </div>

                    <?php if($sendEmail) { ?>
                    <div class="send-email-content hidden">
                        <div class="email-head">
                            <div class="email-head-title d-flex align-items-center">
                                <span data-feather="edit" class="icon-md mr-2"></span>
                                New message
                            </div>
                        </div>
                        <div class="email-compose-fields">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Subject</div>
                                    </div>
                                    <input id="mail_subject" name="mail_subject" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">To</div>
                                    </div>
                                    <div id="recipients_list" data-toggle="suggestions" name="recipients_list" class="form-control"></div>
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Cc</div>
                                    </div>
                                    <div id="cc_list" name="cc_list" data-toggle="suggestions" class="form-control"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="row">

                                            <div class="col-lg-12 mb-3">
                                                <trix-editor name="email_content slim-scroll mb-3" id="email_content"></trix-editor>                                                
                                            </div>

                                            <div class="col-sm-9 pl-2 p-0">
                                                <div class="d-flex">
                                                    <div class="btn-group dropup">
                                                        <button class="btn btn-primary send-button" data-request="inbox">Send</button>
                                                        <button type="button" class="btn p-2 btn-primary btn-icon-text dropdown-toggle" data-toggle="dropdown"></button>
                                                        <div class="dropdown-menu" data-function="email" x-placement="bottom-start">
                                                            <button class="dropdown-item hidden" data-action="schedule_now" href="javascript:void(0)">Schedule Now</button>
                                                            <button class="dropdown-item" data-action="schedule_send" href="javascript:void(0)">Schedule Send</button>
                                                        </div>
                                                    </div>
                                                    <div class="ml-2">
                                                      <input type="hidden" name="mail_request" id="mail_request" value="send_now">
                                                      <input type="datetime-local" value="<?= date("Y-m-d\TH:i:s") ?>" name="schedule_date" id="schedule_date" class="form-control hidden">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 text-right p-0">
                                                <button type="discard" class="btn discard-button"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 p-0">
                                        <?= $formsObj->comments_form_attachment_placeholder($form_params) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                  </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>
        
        <?= discard_form("discard_email_content", "center-top"); ?>
    <?php } ?>
</div>
<?php require "foottags.php"; ?>