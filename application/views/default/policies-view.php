<?php
// set the title
$page_title = "Insurance Policy";

// require the headtags
require "headtags.php";

// get some variable
$itemFound = false;
$recordId = confirm_url_id(1) ? xss_clean($SITEURL[1]) : null;

// load the complaint information
if($recordId) {

    /** load the users insurance policies */
    $recordObj = load_class("user_policy", "controllers");

    /** Parameters */
    $param = (object) [
        "policy_id" => $recordId,
        "userData" => $userData,
        "remote" => false,
        "limit" => 1,
        "userId" => $loggedUserId,
        "submit_status" => false,
    ];
    // return the result
    $recordData = $recordObj->list($param);
    $itemFound = !empty($recordData["data"]) ? true : false;

    // if the complaint was found
    if($itemFound) {

        // extra js
        $loadedJS = [
            "assets/js/script/comments.js",
            "assets/js/script/attachments.js",
            "assets/js/script/payments.js",
            "assets/vendors/bootstrap-maxlength/bootstrap-maxlength.min.js"
        ];

        // create a new forms object
        $formsObj = load_class("forms", "controllers");
        
        // set the complaint data
        $recordData = $recordData["data"][0];
        $recordData->attachment = (array) $recordData->attachment;
        
        // sent state
        $isActive = (bool) ($recordData->submit_status == "save");
        $hasPermission = ( ($recordData->user_id == $loggedUserId) || ($recordData->agent_id == $loggedUserId) || ($recordData->broker_id == $loggedUserId) ) ? true : false;

        $isClosed = (bool) (in_array($recordData->policy_status, ["Deleted", "Cancelled"]));

        $paymentIsDue = (bool) (strtotime($recordData->next_repayment_date) <= strtotime(date("Y-m-d")));
        
    }
}

// review command
$reviewPolicy = $accessObject->hasAccess("approve", "user_policy");
$additionInfo = $reviewPolicy ? "policies" : null;
?>
<div class="page-content">
    <?php if(!$accessObject->hasAccess("view", "complaints") || !$itemFound) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
                        
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <a href="<?= $baseUrl ?>policies" type="button" class="btn btn-outline-primary btn-icon-text mb-2 mb-md-0">
                    <i class="btn-icon-prepend" data-feather="list"></i>
                    List Policies
                </a> &nbsp;
            </div>
        </div>

        <div class="col-lg-12 p-0 col-xl-12 inbox-wrapper">
            <div class="card">
                <div class="card-body">

                    <div class="row">
                        
                        <div class="col-lg-3 email-aside border-lg-right">
                            <div class="aside-content">
                                <div class="aside-header">
                                    <button class="navbar-toggle" data-target=".aside-nav" data-toggle="collapse" type="button"><span class="icon"><i data-feather="chevron-down"></i></span></button>
                                    <span class="title"><h4>Policy Information <span title="Click to learn more about <?= $recordData->policy_name ?>" data-toggle="tooltip" class="user-name-link text-primary small-font cursor" onclick="return policy_basic_information('<?= $recordData->policy_type ?>')">Learn more</span></h4></span>
                                </div>
                            
                                <?php /** If the form has not been submitted yet and a draft */ if(!$isActive) { ?>

                                        <div class="aside-compose" data-toggle="tooltip" title="Update this Client Policy"><a data-function="load-form" data-resource="user_policy" data-module-id="<?= $recordId ?>" data-module="update_insurance_policy" class="btn btn-primary btn-block" href="javascript:void(0)">Update Policy</a></div>                                    

                                <?php } else { /** View the details of the submitted form */ ?>
                                    
                                    <div class="aside-compose mb-0 pb-0" data-toggle="tooltip" title="Update this Client Policy"><a data-function="load-form" data-resource="view_insurance_policy" data-module-id="<?= $recordId ?>" data-module="update_insurance_policy" class="btn btn-outline-primary btn-block" href="javascript:void(0)">View Policy Application Form</a></div>

                                <?php } ?>

                                <?php /** If the user has the permission to review policies */ if($reviewPolicy) { ?>
                                    
                                    <?php /** if the policy has been submitted */ if($isActive) { ?>
                                        
                                        <div class="aside-compose" data-toggle="tooltip" title="Reply to Client Policy"><a <?php if($isActive) { ?>data-function="load-form" data-resource="user_policy" data-module-id="<?= $recordId ?>" data-module="share_a_reply"<?php } ?> class="btn btn-primary btn-block" href="javascript:void(0)">Add Feedback</a></div>
                                    
                                    <?php } ?>

                                <?php } ?>

                                <div class="aside-nav <?= (!$isActive) ? "mt-3" : null ?> collapse">
                                 <?php if($reviewPolicy) { ?>
                                    <ul class="nav mb-2">
                                        <li class="active">
                                            <a data-toggle="tooltip" title="View All Replies" <?php if($isActive) { ?>data-function="load-replies-form" data-module="view_replies_list" data-resource="user_policy" data-module-id="<?= $recordId ?>"<?php } ?> href="javascript:void(0)">
                                                <span class="icon"><i data-feather="inbox"></i></span>Officials Feedback &nbsp;<span class="smaller-font"> (Admin only)</span>
                                                <span data-record="replies_count" data-id="<?= $recordId ?>" class="badge badge-danger-muted text-white font-weight-bold float-right">0</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <?php } ?>
                                    <?php if($hasPermission) { ?>
                                    <div class="mt-3 mb-3">
                                        <div class="card p-0">
                                            <div class="card-header p-2">
                                                <div class="text-uppercase">Payment Option</div>
                                            </div>
                                            <div class="card-body p-2">
                                                <div class="form-group">
                                                    <div class="d-flex justify-content-between">
                                                        <div><label for="payment_method">Default Payment Option</label></div>
                                                        <div><i class="fa cursor font-14px fa-question" data-toggle="tooltip" title="Go to your profile page to update the payment information"></i></div>
                                                    </div>
                                                    <select data-width="100%" <?= !$isClosed ? 'id="payment_method" name="payments[payment_method]"' : "disabled"; ?> class="form-control selectpicker">
                                                        <option value="null">Please select option</option>
                                                        <?php foreach($medicsClass->pushQuery("*", "policy_form", "type='payment_options'") as $eachOption) { ?>
                                                            <option <?= $recordData->payment_option == $eachOption->id ? "selected" : null ?> value="<?= $eachOption->id ?>"><?= $eachOption->name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <div class="d-flex justify-content-between">
                                                        <div><label for="payment_interval">Payment Interval</label></div>
                                                    </div>
                                                    <select data-width="100%" <?= !$isClosed ? 'id="payment_interval" name="payments[payment_interval]"' : "disabled"; ?> class="form-control selectpicker">
                                                        <option value="null">Please select option</option>
                                                        <?php foreach($medicsClass->pushQuery("*", "policy_form", "type='payment_interval'") as $eachOption) { ?>
                                                            <option <?= $recordData->payment_interval == $eachOption->id ? "selected" : null ?> value="<?= $eachOption->id ?>"><?= $eachOption->name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <?php if(!$isClosed) { ?>
                                                <div class="form-group text-right mb-1">
                                                    <button data-policy_id="<?= $recordId ?>" data-button="save-policy_payment_option" class="btn btn-outline-success">Save</button>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>

                                    <?php if($isActive) { ?>
                                        <div class="text-center mb-3 mt-2 border-bottom pb-3" data-toggle="tooltip" title="View the policy premium payment history"><a data-function="load-form" data-resource="view_payment_history" data-module-id="<?= $recordId ?>" data-module="policy_payment_history" class="btn btn-outline-success pt-2 pb-2" href="javascript:void(0)">View premium payment history</a></div>
                                    <?php } ?>

                                    <form class="app-data-form" autocomplete="Off" <?= $reviewPolicy && !$isClosed ? "action=\"{$baseUrl}api/user_policy/update\" " : null ?>method="POST">
                                        <?php if(!$hasPermission) { ?>
                                        <div class="mt-3">
                                            <div class="date mb-2">
                                                <label for="">Payment Option:</label>
                                                <span class="font-18px"><?= $recordData->payment_option_name; ?></span>
                                            </div>
                                            <div class="date mb-2">
                                                <label for="">Payment Interval:</label>
                                                <span class="font-18px"><?= $recordData->payment_interval_name; ?></span>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <div class="mt-2 mb-2">
                                            <div class="date mb-2">
                                                <label for="">Policy ID:</label>
                                                <?php if($reviewPolicy) { ?>
                                                    <input type="text" readonly value="<?= $recordData->policy_id; ?>" id="policy_id" class="form-control font-weight-bold text-uppercase">
                                                <?php } else { ?>
                                                    <?php if($recordData->policy_id) { ?> <span class="font-18px"><?= $recordData->policy_id; ?></span> <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php //= form_loader(); ?>
                                        <div class="mt-3 form-group">
                                            <label for="status">Current Status:</label>
                                            <?php if($reviewPolicy) { ?>
                                            <span data-record="policy_status"><?= $recordData->the_status_label ?></span>
                                            <select data-width="100%" name="policy_status" <?= in_array($recordData->policy_status, ["Cancelled", "Deleted"]) || !$isActive ? "disabled" : null;  ?> id="policy_status" class="selectpicker">
                                                <option <?= $recordData->policy_status == "Pending" && $recordData->policy_status !== "Processing"  ? "selected" : "disabled" ?> value="Pending">Pending</option>
                                                <option <?= $recordData->policy_status == "Processing" && $recordData->policy_status !== "Approved" ? "selected" : (($recordData->policy_status == "Approved") ? "disabled" : null) ?> value="Processing">Processing</option>
                                                <option <?= $recordData->policy_status == "Approved" ? "selected" : null ?> <?= $recordData->policy_status == "Approved" ? "disabled" : null ?> value="Approved">Approved</option>
                                                <option <?= $recordData->policy_status == "Ellapsed" ? "selected" : null ?> value="Ellapsed">Ellapsed</option>
                                                <option disabled <?= $recordData->policy_status == "Cancelled" ? "selected" : null ?> value="Cancelled">Policy Cancelled</option>
                                            </select>
                                            <?php } else { ?>
                                            <span data-record="policy_status"><?= $recordData->the_status_label ?></span>
                                            <?php } ?>
                                        </div>
                                        <div class="mt-3 mb-3">
                                            <div class="date mb-0">
                                                <label class="p-0 m-0">Policy Start Date:</label>
                                                <?php if($reviewPolicy) { ?>
                                                    <input type="text" <?= !$isClosed ? 'name="policy_start_date"' : "disabled"; ?> value="<?= $recordData->policy_start_date; ?>" id="policy_start_date" class="form-control datepicker">
                                                <?php } else { ?>
                                                    <?php if($recordData->policy_start_date) { ?>
                                                    <br><i class="fa fa-calendar mb-0 pb-0"></i> <?= date("l, jS M Y", strtotime($recordData->policy_start_date)) ?>
                                                    <?php } else { ?>
                                                    Not Set
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="mt-2 form-group">
                                            <label for="assigned_to">Assigned To:</label>
                                            
                                            <?php if($reviewPolicy) { ?>

                                            <select data-width="100%" name="assigned_to" <?= in_array($recordData->policy_status, ["Deleted", "Cancelled"]) || !$isActive ? "disabled" : null;  ?> <?= $recordData->user_id == $loggedUserId ? "disabled" : null;  ?> id="assigned_to" class="selectpicker">
                                                <option value="null">Assign this Policy</option>
                                                <?php
                                                // load users that this can be assigned to
                                                if($userData->user_type == "insurance_company") {
                                                    $query = "SELECT item_id, name FROM users WHERE user_type IN ('agent', 'insurance_company') AND status = '1' AND deleted='0'";
                                                } elseif($userData->user_type == "nic") {
                                                    $query = "SELECT item_id, name FROM users WHERE user_type IN ('nic') AND status = '1' AND deleted='0'";
                                                } elseif(in_array($userData->user_type, ["bank", "bancassurance"])) {
                                                    $query = "SELECT item_id, name FROM users WHERE user_type IN ('bank','bancassurance') AND status = '1' AND deleted='0'";
                                                } else {
                                                    $query = "SELECT item_id, name FROM users WHERE user_type IN ('admin') AND status = '1' AND deleted='0'";
                                                }
                                                $stmt = $medics->query($query);
                                                $result = $stmt->fetchAll(PDO::FETCH_OBJ);
                                                // loop through the results
                                                foreach($result as $eachUser) {
                                                    print "<option ".($recordData->assigned_to == $eachUser->item_id ? "selected" :  null)." value=\"{$eachUser->item_id}\">{$eachUser->name}</option>";
                                                }
                                                ?>
                                            </select>

                                            <div class="mt-2 text-right">
                                                <span class="user-name-link" data-function="load-form" data-module="user_basic_information" data-resource="user_information" data-module-id="<?= $recordData->assigned_to ?>">View details</span>
                                            </div>

                                            <?php } elseif(!empty($recordData->assigned_to)) { ?>
                                                
                                                <span title="Click to learn more about this agent" data-toggle="tooltip" class="user-name-link text-primary" data-function="load-form" data-module="user_basic_information" data-resource="user_information" data-module-id="<?= $recordData->assigned_to ?>"><?= $recordData->assigned_to_name ?></span>
                                            
                                            <div class="mt-2">
                                                <label for="managed_by">Managed By:</label> 
                                                <span title="Click to learn more about <?= $recordData->company_name ?>" data-toggle="tooltip" class="user-name-link text-primary cursor" onclick="return company_basic_information('<?= $recordData->company_id ?>')"><?= $recordData->company_name ?></span>
                                            </div>

                                            <?php } ?>

                                            <?php if($reviewPolicy) { ?>
                                            <label for="premium">Premium Payable</label>
                                            <input type="number" value="<?= $recordData->premium ?>" name="premium" id="premium" class="form-control">
                                            <?php } ?>
                                            
                                        </div>
                                        
                                        <?php if($isActive && !$isClosed && $reviewPolicy) { ?>
                                            <div class="mt-3 mb-3 form-group text-center">
                                                <input type="hidden" name="item_id" id="item_id" value="<?= $recordId ?>" class="form-control">
                                                <input type="hidden" name="user_id" id="user_id" value="<?= $recordData->user_id ?>" class="form-control">
                                                <button type="submit" class="btn btn-outline-success">Save Changes</button>
                                            </div>
                                        <?php } ?>

                                    </form>

                                    <div class="form-group">
                                        <div class="border-bottom mb-2"><label>Next Payment Details</label></div>
                                        <p>Date: &nbsp; <i class="fa fa-calendar-check"></i> <?= $recordData->next_repayment_date ?></p>
                                        <p>Amount: GH&cent; <span class="font-18px"><?= $recordData->premium ?></span> </p>
                                        <?php if($hasPermission && $paymentIsDue) { ?>
                                            <button data-item-id="<?= $recordId ?>" data-item="policy" class="btn create-checkout btn-sm btn-outline-success" type="button">Make Payment</button>
                                        <?php } ?>
                                    </div>
                                    
                                    <?php if($reviewPolicy) { ?>
                                    <div class="mt-5 justify-content-center row form-group">
                                        <button data-toggle="tooltip" title="View summary history" data-function="load-form" data-resource="complaints" data-module-id="<?= $recordId ?>" data-module="activity_history" class="btn btn-outline-secondary">View activity history</button>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>  

                        <div class="col-lg-9 email-content">

                            <div class="email-head">
                                <div class="email-head-subject p-2 mr-0 pr-0">
                                    <div class="title d-flex align-items-center justify-content-between">
                                        <div class="d-flex align-items-center">
                                            <span><h4><?= $recordData->user_id == $loggedUserId ? $recordData->policy_name : $recordData->policy_name ?></h4></span>
                                        </div>
                                        <?php if($hasPermission && !$isActive) {  ?>
                                        <?= submit_draft_button("policies", $recordData->policy_name, $recordId) ?>
                                        <?php } ?>
                                        <?php if(!$reviewPolicy && !$isClosed && $hasPermission) { ?>
                                            <?php
                                            // if there is a pending request to cancel the policy
                                            if($recordData->pending_cancel_request) {
                                                // show a notice to of a cancel 
                                                print "<button class=\"btn btn-outline-warning\">Pending request to cancel policy</button>";
                                            } elseif($isActive) { ?>
                                                <?= cancel_record_button($recordData->policy_name, $recordData->policy_id, "user_policy") ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="email-head-sender d-flex align-items-center justify-content-between flex-wrap">
                                    <div class="d-flex align-items-center mb-2">
                                        <div class="avatar">
                                        <img src="<?= $baseUrl ?><?= $recordData->client_image ?>" alt="Avatar" class="rounded-circle user-avatar-md">
                                        </div>
                                        <div class="sender d-flex align-items-center">
                                            <p><a href="javascript:void(0)" data-toggle="tooltip" title='Click to view details of <?= $recordData->client_name ?>' onclick="return user_basic_information('<?= $recordData->user_id ?>', '<?= $additionInfo ?>')"><?= $recordData->client_name ?></a></p>
                                            <p class="pl-3"><small><i class="fa fa-envelope"></i> <?= $recordData->client_email ?></small></p>
                                            <p class="pl-3"><small><i class="fa fa-phone"></i> <?= $recordData->client_contact ?></small></p>
                                        </div>
                                    </div>
                                    <div class="date"><i class="fa fa-calendar-check"></i> <?= date("M d Y, h:iA", strtotime($recordData->date_submitted)) ?></div>
                                </div>
                            </div>

                            <div class="row slim-scroll">

                                <div class="col-lg-8 mb-3">

                                    <div class="border-top p-0 m-0">
                                        <?php if($reviewPolicy && !$isClosed) { ?>
                                            <?= leave_comments_builder("policy_comments", $recordId) ?>
                                        <?php } ?>

                                        <div id="comments-container" data-autoload="true" data-last-reply-id="0" data-id="<?= $recordId ?>" class="slim-scroll pt-3 mt-3 pr-2 pl-0" style="overflow-y:auto; max-height:850px"></div>
                                        <div class="load-more mt-3 text-center"><button id="load-more-replies" type="button" class="btn btn-outline-secondary">Loading comments</button></div>
                                    </div>

                                </div>

                                <div class="col-lg-4 p-0">

                                    <div class="email-attachments p-0 pt-3">
                                        <div class="grid-margin mb-2 border-bottom pb-3">
                                            <p><span class="text-muted">Created By:</span> <span class="underline text-primary" onclick="return user_basic_information('<?= $recordData->managed_by["id"] ?>')"><?= $recordData->managed_by["name"] ?></span></p>
                                            <p><span class="text-muted">Date Created:</span> <span class="text-primary"><?= $recordData->date_created ?></span></p>
                                        </div>

                                        <div class="grid-margin grid-margin-xl-0 stretch-card" id="cancel_existing_request" data-json='<?= json_encode(["container" => "cancel_policy_list", "field" => "policy_id", "url" => "user_policy"]); ?>'>
                                            <div class="card">
                                                <div class="card-header p-2 card-title mb-0 pb-1">Cancellation Request</div>
                                                <div class="card-body p-2">
                                                    <div class="d-flex flex-column" data-autoload="true" data-policy-id="<?= $recordData->policy_id ?>" data-threads="cancel_policy_list">
                                                        <div class="p-0 m-0 font-italic">No pending request</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <?php if(!empty($recordData->attachment)) { ?>
                                            <?php if(!empty($recordData->attachment["files"])) { ?>
                                                <div class="title mt-3">Attachments <span>(<?= count($recordData->attachment["files"]) ?? 0 ?> files, <?= $recordData->attachment["files_size"] ?? 0 ?>)</span></div>
                                                <?= $formsObj->list_attachments($recordData->attachment["files"], $loggedUserId, "col-lg-12 col-md-6", !$isActive); ?>
                                            <?php } ?>
                                        <?php } ?>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>

    <?php } ?>
    
</div>
<?php require "foottags.php"; ?>