<?php
// set the title
$page_title = "Analitics";

// additional css and javascript files
$loadedJS = [
    "assets/vendors/chartjs/Chart.min.js",
	"assets/js/script/reports.js"
];

// additional period query
$periods = [
    "today" => "Today",
    "last_week" => "Last Week",
    "this_week" => "This Week",
    "last_14days" => "Last 14 Days",
    // "last_30days" => "Last 30 Days",
    "last_month" => "Last Month",
    "this_month" => "This Month",
    "last_3months" => "Last 3 Months",
    "last_6months" => "Last 6 Months",
    "this_year" => "This Year",
    // "last_year" => "Last Year",
];

// require the headtags
require "headtags.php";
?>
<div class="page-content">
    
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
		<div>
			<h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
		</div>
	</div>
	<div class="row" id="report_generation_init" data-stream_period="current" data-report_stream="clients_report,summary_report,policy_report,claims_report<?= $isIC ? ",company_policy_report,users_report" : null ?>">
        <?= form_loader("fixed"); ?>
        <div class="col-md-12 mb-4">
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <label for="reporting_type">Report Type</label>
                    <select name="reporting_type" id="reporting_type" class="form-control selectpicker">
                        <option value="summary_report">Summary Report</option>
                        <option value="policy_report">Policy Reports</option>
                        <option value="clients_report">Clients Reports</option>
                        <option value="agents_report">Insurance Agents Reports</option>
                        <option value="brokers_report">Insurance Brokers Reports</option>
                        <option value="claims_report">Claims Reports</option>
                        <option value="adverts_report">Adverts Reports</option>
                        <?php if($isIC) { ?>
                        <option value="company_policy_report">Insurance Policies Reports</option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-lg-9 col-md-8" id="reporting_type_filters"></div>
            </div>
        </div>
        <div class="col-md-12 mb-4 slim-scroll table-responsive">
            <div class="btn-group" role="group" aria-label="period_selector">
                <?= report_period_buttons(true, $periods) ?>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">

                        <div class="col-lg-12 mb-3">
                            <div class="card">
                                <div class="card-header p-0">
                                    <div class="card-title mb-0 p-2">Premium Payments</div>
                                </div>
                                <div class="card-body">
                                    <div class="policy_premium_chart-wrapper">
                                        <canvas height="375px" id="policy_premium_chart"></canvas>
                                    </div>
                                </div>
                            </div>        
                        </div>
                        <div class="col-lg-6 mb-3">
                            <div class="card">
                                <div class="card-header p-0">
                                    <div class="card-title mb-0 p-2">Policy Application Distribution</div>
                                </div>
                                <div class="card-body" style="height:140px">
                                    <div class="simple_policy_chart-wrapper">
                                        <canvas style="height:70px; width:100%" id="simple_policy_chart"></canvas>
                                    </div>
                                </div>
                            </div>        
                        </div>
                        <div class="col-lg-6 mb-3">
                            <div class="card">
                                <div class="card-header p-0">
                                    <div class="card-title mb-0 p-2">Claims Application Distribution</div>
                                </div>
                                <div class="card-body" style="height:140px">
                                    <div class="simple_claims_chart-wrapper">
                                        <canvas style="height:70px; width:100%" id="simple_claims_chart"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if($isIC) { ?>
                        <div class="col-lg-12 mb-3">
                            <div class="card">
                                <div class="card-header p-0">
                                    <div class="card-title mb-0 p-2">Company Policy Distribution</div>
                                </div>
                                <div class="card-body">
                                    <div class="company_policy-wrapper">
                                        <canvas style="height:230px; width:100%" id="company_policy_chart"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="row">
                        <div class="col-lg-6 mb-3">
                            <div class="card">
                                <div class="card-header p-0">
                                    <div class="card-title mb-0 p-2">Policies Count</div>
                                </div>
                                <div class="card-body pr-1" style="height:70px">
                                    <div class="d-flex justify-content-between">
                                        <div><h3 class="mb-2" data-record="user_policy_count">0</h3></div>
                                        <div class="d-flex align-items-baseline">
                                            <p data-item="summary_percent" data-record="user_policy_count">
                                                <span>0.00%</span>
                                                <i data-feather="arrow-up" class="icon-sm mb-1"></i>
                                            </p>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-3">
                            <div class="card">
                                <div class="card-header p-0">
                                    <div class="card-title mb-0 p-2">Claims Count</div>
                                </div>
                                <div class="card-body pr-1" style="height:70px">
                                    <div class="d-flex justify-content-between">
                                        <h3 class="mb-2" data-record="user_policy_claims_count">0</h3>
                                        <div class="d-flex align-items-baseline">
                                            <p data-item="summary_percent" data-record="user_policy_claims_count">
                                                <span>0.00%</span>
                                                <i data-feather="arrow-up" class="icon-sm mb-1"></i>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-3">
                            <div class="card">
                                <div class="card-header p-0">
                                    <div class="card-title mb-0 p-2">Premium Paid</div>
                                </div>
                                <div class="card-body pr-1" style="height:70px">
                                    <div class="d-flex justify-content-between">
                                        <h3 class="mb-2" data-record="user_policy_premium_paid">0</h3>
                                        <div class="d-flex align-items-baseline">
                                            <p data-item="summary_percent" data-record="user_policy_premium_paid">
                                                <span>0.00%</span>
                                                <i data-feather="arrow-up" class="icon-sm mb-1"></i>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-3">
                            <div class="card">
                                <div class="card-header p-0">
                                    <div class="card-title mb-0 p-2">Claims Payment</div>
                                </div>
                                <div class="card-body pr-1" style="height:70px">
                                    <div class="d-flex justify-content-between">
                                        <h3 class="mb-2" data-record="user_policy_claims_paid">0</h3>
                                        <div class="d-flex align-items-baseline">
                                            <p data-item="summary_percent" data-record="user_policy_claims_paid">
                                                <span>0.00%</span>
                                                <i data-feather="arrow-up" class="icon-sm mb-1"></i>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 mb-3">
                            <div class="card">
                                <div class="card-header p-0">
                                    <div class="card-title mb-0 p-2">Clients Distribution</div>
                                </div>
                                <div class="card-body">
                                    <div class="simple_clients_chart-wrapper">
                                        <canvas height="100px" id="simple_clients_chart"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if($isAdmin || $isIC) { ?>
                        <div class="col-lg-12 mb-3">
                            <div class="card">
                                <div class="card-header p-0">
                                    <div class="card-title mb-0 p-2">Clients Distribution</div>
                                </div>
                                <div class="card-body">
                                    <div class="users_group_chart-wrapper">
                                        <canvas height="400px" id="users_group_chart"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>

                        <div class="col-lg-6 mb-3">
                            <div class="card">
                                <div class="card-header p-0">
                                    <div class="card-title mb-0 p-2">Clients Count</div>
                                </div>
                                <div class="card-body" style="height:70px">
                                    <div class="d-flex justify-content-between">
                                        <h3 class="mb-2" data-record="clients_count">0</h3>
                                        <div class="d-flex align-items-baseline">
                                            <p data-item="summary_percent" data-record="clients_count">
                                                <span>0.00%</span>
                                                <i data-feather="arrow-up" class="icon-sm mb-1"></i>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-3">
                            <div class="card">
                                <div class="card-header p-0">
                                    <div class="card-title mb-0 p-2">Complaints Count</div>
                                </div>
                                <div class="card-body" style="height:70px">
                                    <div class="d-flex justify-content-between">
                                        <h3 class="mb-2" data-record="user_complaints_count">0</h3>
                                        <div class="d-flex align-items-baseline">
                                            <p data-item="summary_percent" data-record="user_complaints_count">
                                                <span>0.00%</span>
                                                <i data-feather="arrow-up" class="icon-sm mb-1"></i>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-lg-12 mb-3">
                    <div class="card">
                        <div class="card-header p-0">
                            <div class="card-title mb-0 p-2">Claims Payment vs. Premium Payment Distribution</div>
                        </div>
                        <div class="card-body" style="height:430px">
                            <div class="claims_against_premium-wrapper">
                                <canvas height="300px" id="claims_against_premium"></canvas>
                            </div>
                        </div>
                    </div>        
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="row">
                <?php if($isIC || $isAdmin || $isNIC) { ?>
                <div class="col-lg-7 col-md-12">
                    <div data-report_stream="agent,user,business,broker" data-report_content="user_type" class="card">
                        <div class="card-header p-0">
                            <div class="card-title mb-0 p-2">Most Performing Agents</div>
                        </div>
                        <div class="card-body">
                            <div class="table-reponsive slim-scroll">
                                <table id="agents_performance_list" class="table dataTable">
                                    <thead>
                                        <th width="40%">Agent</th>
                                        <th>Clients</th>
                                        <th>Claims Applied</th>
                                        <th>Policies</th>
                                        <th>Premium</th>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>

                <div class="col-lg-5 col-md-12">
                    <div class="row">
                        <?php if($accessObject->hasAccess("add", "adverts") && isset($that)) { ?>
                        <div class="col-lg-6 mb-3">
                            <div class="card">
                                <div class="card-header p-0">
                                    <div class="card-title mb-0 p-2">Ads Count</div>
                                </div>
                                <div class="card-body" style="height:70px"></div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="col-lg-12 mb-3">
                            <div class="card">
                                <div class="card-header p-0">
                                    <div class="card-title mb-0 p-2">Clients Performance</div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive slim-scroll">
                                        <table id="clients_performance_list" class="table dataTable">
                                            <thead>
                                                <th width="50%">Client Name</th>
                                                <th>Policies</th>
                                                <th>Premium Paid</th>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        
	</div>


</div>
<?php require "foottags.php"; ?>