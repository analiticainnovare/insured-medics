<?php
// set the title
$page_title = "Licenses";

// require the headtags
require "headtags.php";

// get some variable
$itemFound = false;
$recordId = confirm_url_id(1) ? xss_clean($SITEURL[1]) : null;

// permissions
$updateLicense = (bool) $accessObject->hasAccess("update", "licenses");
$approveLicense = (bool) $accessObject->hasAccess("approve", "licenses");
$canPay = $accessObject->hasAccess("payment", "licenses");

// load the complaint information
if($recordId) {

    /** load the company licenses */
    $recordObj = load_class("licenses", "controllers");

    /** Parameters */
    $param = (object) [
        "application_id" => $recordId,
        "remote" => false,
        "limit" => 1,
        "company_id" => $userData->company_id
    ];

    // return the result
    $recordData = $recordObj->list($param);
    $itemFound = !empty($recordData["data"]) ? true : false;

    
    // if the complaint was found
    if($itemFound) {
        
        // extra js
        $loadedJS = [
            "assets/js/script/attachments.js",
            "assets/js/script/comments.js"
        ];
        
        // append the payment script if the user has the permission to make payment
        if($canPay) {
            $loadedJS[] = "assets/js/script/payments.js";
        }

        // create a new forms object
        $formsObj = load_class("forms", "controllers");
        
        // set the complaint data
        $recordData = $recordData["data"][0];
        $recordData->attachment = (array) $recordData->attachment;
        
        // is active
        $isActive = (bool) ($recordData->submit_status == "save");
        $isClosed = (bool) (in_array($recordData->status, ["Approved", "Revoked", "Cancelled"]));
        $hasPaid = (bool) ($recordData->payment_status == "Paid");

        // set the seen date and who saw it
        if(!$recordData->seen && $approveLicense && $isActive) {
            $medics->query("UPDATE companies_licenses SET seen = '1', seen_date = now(), seen_by = '{$loggedUserId}' WHERE item_id = '{$recordId}' LIMIT 1");
        }
    }
}
?>
<div class="page-content">
    <?php if(!$accessObject->hasAccess("view", "licenses") || !$itemFound) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>

        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <a href="<?= $baseUrl ?>licenses-list" type="button" class="btn btn-outline-primary btn-icon-text mb-2 mb-md-0">
                    <i class="btn-icon-prepend" data-feather="list"></i>
                    List Licenses
                </a> &nbsp;
            </div>
        </div>
        <div class="col-lg-12 p-0 col-xl-12 inbox-wrapper">
            <div class="card">
                <div class="card-body">

                    <div class="row">

                        <div class="col-lg-3 email-aside border-lg-right">
                            
                            <form class="app-data-form" autocomplete="Off" <?= $approveLicense && !$isClosed ? "action=\"{$baseUrl}api/licenses/update\" " : null ?>method="POST">

                                <div class="aside-content">
                                    <div class="aside-header">
                                        <button class="navbar-toggle" data-target=".aside-nav" data-toggle="collapse" type="button"><span class="icon"><i data-feather="chevron-down"></i></span></button>
                                        <span class="title"><h4>Application Information
                                    </div>
                                    <?php /** If the form has not been submitted yet and a draft */ if(!$isActive) { ?>

                                            <div class="aside-compose" data-toggle="tooltip" title="Update the license application Form"><a data-function="load-form" data-resource="licenses" data-module-id="<?= $recordId ?>" data-module="license_application" class="btn btn-primary btn-block" href="javascript:void(0)">Update Application</a></div>                                    

                                    <?php } ?>

                                    <?php /** If the user has the permission to approve licenses */ if($approveLicense) { ?>
                                        
                                        <?php /** if the policy has been submitted */ if($isActive) { ?>
                                            
                                            <div class="aside-compose" data-toggle="tooltip" title="Reply to Client Policy"><a <?php if($isActive) { ?>data-function="load-form" data-resource="licenses" data-module-id="<?= $recordId ?>" data-module="share_a_reply"<?php } ?> class="btn btn-primary btn-block" href="javascript:void(0)">Add Feedback</a></div>
                                        
                                        <?php } ?>

                                    <?php } ?>

                                    <div class="aside-nav <?= (!$isActive) ? "mt-2" : null ?> collapse">
                                        
                                        <?php if($approveLicense) { ?>
                                        <ul class="nav">
                                            <li class="active">
                                                <a data-toggle="tooltip" title="View All Replies" <?php if($isActive) { ?>data-function="load-replies-form" data-module="view_replies_list" data-resource="licenses" data-module-id="<?= $recordId ?>"<?php } ?> href="javascript:void(0)">
                                                    <span class="icon"><i data-feather="inbox"></i></span>Officials Feedback &nbsp;<span class="smaller-font"> (Admin only)</span>
                                                    <span data-record="replies_count" data-id="<?= $recordId ?>" class="badge badge-danger-muted text-white font-weight-bold float-right">0</span>
                                                </a>
                                            </li>
                                        </ul>
                                        <?php } ?>
                                        
                                        <?php if($approveLicense && $recordData->seen_date) { ?>
                                        <div class="mt-3 mb-2 pb-2 border-bottom">
                                            <div class="date mb-0 m-0 p-0" title="License application seen date">
                                                <label class="mb-0">First seen by:</label><br><span class="user-name-link" onclick="return user_basic_information('<?= $recordData->seen_by ?>')"><?= $recordData->seen_by_name ?></span>
                                                <br> <span class="text-muted"><i class="fa fa-calendar-check"></i> <?= date("M d Y, h:iA", strtotime($recordData->seen_date)) ?> </span>
                                            </div>
                                        </div>
                                        <?php } ?>

                                        <div class="mt-0 mb-0 pt-0">
                                            <p>
                                                <label for="license_id">License ID: </label> 
                                                <?php if($approveLicense) { ?>
                                                <input type="text" readonly name="license_id" id="license_id" value="<?= $recordData->license_id ?>" class="form-control pl-1 font-18px mb-2">
                                                <?php } else { ?>
                                                <span class="font-weight-bolder font-18px"><?= $recordData->license_id; ?></span>
                                                <?php } ?>
                                            </p>
                                            
                                            <?php
                                            // show the input fields if the user has permission to approve the application
                                            if($approveLicense) { ?>

                                                <div class="form-group mt-1 border-top pt-2">
                                                    <label for="start_date" class="mb-0">Start Date</label> 
                                                    <input type="text" <?= $isClosed ? "disabled" : null ?> name="start_date" id="start_date" value="<?= $recordData->start_date ?>" class="datepicker form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label for="expiry_date" class="mb-0">Expiry Date</label> 
                                                    <input type="text" <?= $isClosed ? "disabled" : null ?> name="expiry_date" id="expiry_date" value="<?= $recordData->expiry_date ?>" class="datepicker form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label for="amount_payable" class="mb-0">Amount Due</label> 
                                                    <input type="text" <?= $isClosed && $hasPaid ? "disabled" : null ?> name="amount_payable" id="amount_payable" value="<?= $recordData->amount_payable ?>" class="form-control">
                                                </div>

                                                <p><label for="end_date">Payment Status: </label> <span><?= $medicsClass->the_status_label($recordData->payment_status) ?></span></p>

                                            <?php }
                                            // show the start and expiry dates if the application has been submitted
                                            elseif($isActive) { ?>

                                                <p><label for="start_date">Start Date: </label> <span><?= $recordData->start_date ?></span></p>
                                                <p><label for="end_date">Expiry Date: </label> <span><?= $recordData->expiry_date ?></span></p>
                                                <p>
                                                    <label for="end_date">Amount Payable: </label>
                                                    <span class="font-18px">GH&cent;<?= $recordData->amount_payable ?></span>
                                                    <?php if(!$hasPaid && $canPay) { ?>
                                                        <button data-item-id="<?= $recordId ?>" data-item="licenses" type="button" class="btn btn-outline-success create-checkout btn-sm p-1">Make Payment</button>
                                                    <?php } ?>
                                                </p>
                                            
                                            <?php } ?>

                                            <?php if($isActive) {  ?>

                                                <div class="mt-2 mb-0 border-top pt-3 form-group">

                                                    <label for="status">Application Status:</label>
                                                    <?php if($approveLicense) { ?>
                                                        <span data-record="status"><?= $recordData->the_status_label ?></span>
                                                        <select data-width="100%" name="status" <?= in_array($recordData->status, ["Cancelled", "Revoke"]) || !$isActive ? "disabled" : null;  ?> id="status" class="selectpicker">
                                                            <option <?= $recordData->status == "Pending" && $recordData->status !== "Processing"  ? "selected" : "disabled" ?> value="Pending">Pending</option>
                                                            <option <?= $recordData->status == "Processing" && $recordData->status !== "Approved" ? "selected" : (($recordData->status == "Approved") ? "disabled" : null) ?> value="Processing">Processing</option>
                                                            <option <?= $recordData->status == "Approved" ? "selected" : null ?> <?= $recordData->status == "Approved" ? "disabled" : null ?> value="Approved">Approved</option>
                                                            <option disabled <?= $recordData->status == "Cancelled" ? "selected" : null ?> value="Cancelled">Cancel Application</option>
                                                            <option disabled <?= $recordData->status == "Revoke" ? "selected" : null ?> value="Revoke">Revoke License</option>
                                                        </select>
                                                    <?php } else { ?>
                                                        <span data-record="status"><?= $recordData->the_status_label ?></span>
                                                    <?php } ?>

                                                </div>

                                            <?php } ?>

                                        </div>

                                        <?php if(!$approveLicense) { ?>
                                        <div class="card p-0 m-0">
                                            <div class="card-body">
                                                <?= $recordData->description ?>
                                            </div>
                                        </div>
                                        <?php } ?>

                                    </div>

                                </div>
                                <?php if($isActive && $approveLicense && !$hasPaid) { ?>
                                    <div class="mt-3 form-group text-center">
                                        <input type="hidden" name="application_id" id="application_id" value="<?= $recordId ?>" class="form-control">
                                        <button type="submit" class="btn btn-outline-success">Save Changes</button>
                                    </div>
                                <?php } ?>

                            </form>

                            <?php if($approveLicense) { ?>
                            <div class="mt-5 justify-content-center row form-group">
                                <button data-toggle="tooltip" title="View summary history" data-function="load-form" data-resource="licenses" data-module-id="<?= $recordId ?>" data-module="activity_history" class="btn btn-outline-secondary">View activity history</button>
                            </div>
                            <?php } ?>

                        </div>

                        <div class="col-lg-9 email-content">

                            <div class="email-head">
                                <div class="email-head-subject p-2 mr-0 pr-0">
                                    <div class="title d-flex align-items-center justify-content-between">
                                        <div class="d-flex align-items-center">
                                            <h4 class="text-muted">
                                                License Application: 
                                                <span class="text-black font-weight-bolder font-18px"><?= $recordData->license_id ?></span>
                                                <?php if($updateLicense && !$isActive) {  ?>
                                                    <small class="badge badge-primary p-1 tx-11">Draft</small>
                                                <?php } ?>
                                            </h4>
                                        </div>
                                        <?php if($updateLicense && !$isActive) {  ?>
                                        <?= submit_draft_button("License Application", $recordData->license_id, $recordId) ?>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="email-head-sender d-flex align-items-center justify-content-between flex-wrap">
                                    <div class="d-flex align-items-center mb-2">
                                        <div class="mr-3">
                                            <img width="80px" src="<?= $baseUrl ?><?= $recordData->company_info->logo ?>" alt="" class="user-avatar-md">
                                        </div>
                                        <div class="sender d-flex align-items-center">
                                            <p><a href="javascript:void(0)" data-toggle="tooltip" title='Click to view details of' onclick="return company_basic_information('<?= $recordData->company_id ?>')"><?= $recordData->company_name ?></a></p>
                                            <p class="pl-3"><small><i class="fa fa-envelope"></i> <?= $recordData->company_info->email ?></small></p>
                                            <p class="pl-3"><small><i class="fa fa-phone"></i> <?= $recordData->company_info->contact ?></small></p>
                                        </div>
                                    </div>
                                    <div class="date"><i class="fa fa-calendar"></i> 
                                        <?php 
                                        // date to show
                                        $date = !empty($recordData->date_submitted) ? $recordData->date_submitted : $recordData->date_created;
                                        print date("M d Y, h:iA", strtotime($date))
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-lg-8 mb-3">
                                    <div class="p-0 m-0">
                                        <?php if($approveLicense) { ?>
                                        <div class="card mb-3">
                                            <div class="card-body">
                                                <?= $recordData->description ?>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        
                                        <?php if($updateLicense) { ?>
                                            <?php $comment = !$approveLicense ? "Leave a comment for the <strong>NIC</strong>" : null; ?>
                                            <div class="border-bottom"></div>
                                            <?= leave_comments_builder("licenses_comments", $recordId, $comment) ?>
                                        <?php } ?>

                                        <?php 
                                        /** List of comments on this application */
                                        if($isActive) {  ?>
                                            <div id="comments-container" data-autoload="true" data-last-reply-id="0" data-id="<?= $recordId ?>" class="slim-scroll pt-3 pr-1 pl-0" style="overflow-y:auto; max-height:850px"></div>
                                            <div class="load-more mt-3 text-center"><button id="load-more-replies" type="button" class="btn btn-outline-secondary">Loading comments</button></div>
                                        <?php } ?>

                                    </div>
                                </div>
                                <div class="col-lg-4 p-0">
                                    
                                    <div class="email-attachments p-0 pt-3">

                                        <div class="grid-margin pb-0">
                                            <p><span class="text-muted">Applied By:</span> <?= $recordData->applied_by ?></p>
                                            <p><span class="text-muted">Date Created:</span> <?= $recordData->date_created ?></p>
                                        </div>

                                        <?php if(!empty($recordData->attachment)) { ?>
                                            <div class="border-top mt-0 pt-3">
                                                <div class="title">Attachments <span>(<?= count($recordData->attachment["files"]) ?? 0 ?> files, <?= $recordData->attachment["files_size"] ?? 0 ?>)</span></div>
                                                <?= $formsObj->list_attachments($recordData->attachment["files"], $loggedUserId, "col-lg-12 col-md-6", !$isActive); ?>
                                            </div>
                                        <?php } ?>

                                        <?php 
                                        /** History of the license application for this License ID */
                                        if($isActive) {  ?>
                                            <div class="grid-margin mt-2 grid-margin-xl-0 stretch-card" id="company_policy_licenses">
                                                <div class="card">
                                                    <div class="card-header card-title mb-0 pb-1">License Renewal History</div>
                                                    <div class="card-body">
                                                        <div class="d-flex flex-column" data-autoload="true" data-policy-id="<?= $recordData->license_id ?>" data-threads="company_license_list">
                                                            <div class="p-0 m-0 font-italic">No available records</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>


                </div>
            </div>
        </div>

    <?php } ?>
</div>
<?php require "foottags.php"; ?>