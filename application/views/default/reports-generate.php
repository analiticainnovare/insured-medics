<?php
// set the title
$page_title = "Generate Report";

// require the headtags
require "headtags.php";
?>
<div class="page-content">
    <?php if(!$accessObject->hasAccess("generate", "reports")) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        <?= form_loader("fixed"); ?>
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
        </div>

    <?php } ?>
</div>
<?php require "foottags.php"; ?>