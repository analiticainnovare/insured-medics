<?php
// set the title
$page_title = "Notifications";

// require the headtags
require "headtags.php";
?>
<div class="page-content">
    <?php if(!$accessObject->hasAccess("view", "clients")) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        <?= form_loader("fixed"); ?>
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
        </div>

        <div class="col-lg-12 p-0 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline mb-2">
                        <h6 class="card-title mb-0"></h6>
                    </div>
                    <div class="table-responsive slim-scroll">
                        <table class="table dataTable table-hover nowrap mb-0" id="notifications_list">
                            <thead>
                                <tr>
                                    <th width="6%" class="pt-0">#</th>
                                    <th class="pt-0">Subject</th>
                                    <th class="pt-0">Message</th>
                                    <th width="15%" class="pt-0">Date Created</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    
</div>
<?php require "foottags.php"; ?>