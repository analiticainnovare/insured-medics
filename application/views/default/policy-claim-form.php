<?php
// set the title
$page_title = "Insurance Policy Claims Form";

// require the headtags
require "headtags.php";

// extra css
$loadedJS = [
    "assets/js/ajax-load.js",
    "assets/js/script/forms.js"
];

// get some variable
$itemFound = false;
$recordId = confirm_url_id(1) ? xss_clean($SITEURL[1]) : null;

// load the complaint information
if($recordId) {
    
    /** load the users insurance policies */
    $recordObj = load_class("company_policy", "controllers");

    /** Parameters */
    $param = (object) [
        "policy_type_id" => $recordId,
        "userData" => $userData,
        "remote" => true,
        "limit" => 1
    ];

    // return the result
    $recordData = $recordObj->list($param);
    $itemFound = !empty($recordData["data"]) ? true : false;

    // if the complaint was found
    if($itemFound) {
        // create a new forms object
        $formsObj = load_class("forms", "controllers");
        // set the complaint data
        $recordData = $recordData["data"][0];
        $recordData->attachment = (array) $recordData->attachment;

        // sent state
        $isSaved = (bool) ($recordData->submit_status == "save");
        $isEnrolled = (bool) (in_array($recordData->policy_status, ["Pending", "In Review", "Enrolled"]));

        // get the forms data
        $formData = $formsObj->form_enlisting($recordData->claims_form);
        $formToShow = (isset($recordData->claims_form->fields) && !empty($recordData->claims_form->fields)) ? $recordData->claims_form->fields : (object)[];
    }
    // claims form id
    $formId = "{$recordId}_claims_form";

    /** Set parameters for the data to attach */
    $form_params = (object) [
        "module" => "company_policy_claims_form_{$recordId}",
        "userData" => $userData,
        "item_id" => null,
        "no_title" => true
    ];
}
?>
<style>
trix-editor {
    min-height: 100px;
    max-height: 100px;
}
</style>
<div class="page-content">
    <?php if(!$accessObject->hasAccess("update", "company_policy") && !$accessObject->hasAccess("add", "company_policy") || !$itemFound) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <a href="<?= $baseUrl ?>policy-view/<?= $recordId ?>" class="btn btn-primary btn-icon-text mb-2 mb-md-0">
                    <i class="btn-icon-prepend" data-feather="arrow-left"></i>
                    View Policy
                </a>
            </div>
        </div>
        <div class="col-lg-12 p-0 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body"> 
                    <div class="row">
                        <div class="col-lg-11 mb-2 col-md-10">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <td class="font-weight-bold text-uppercase">Policy Name</td>
                                        <td class="font-weight-bold text-uppercase">Policy ID</td>
                                        <td class="font-weight-bold text-uppercase">Category</td>
                                        <td class="font-weight-bold text-uppercase">Policy Status</td>
                                        <td class="font-weight-bold text-uppercase">Date Created</td>
                                    </tr>
                                    <tr>
                                        <td width="30%"><?= $recordData->name ?></td>
                                        <td width=""><?= $recordData->policy_id ?></td>
                                        <td width=""><?= str_replace("\"", "", $recordData->category); ?></td>
                                        <td width=""><?= $recordData->the_status_label ?></td>
                                        <td width=""><?= $recordData->date_created ?></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="col-lg-1 mb-2 col-md-2">
                            <div class="p-0 m-0">
                                <button type="button" class="btn pl-1 pr-1 btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Options
                                </button>
                                <div class="dropdown-menu p-0 m-0">
                                    <a class="dropdown-item" data-function="load-form" data-module="share_a_reply" data-resource="company_policy" data-module-id="<?= $formId ?>" href="javascript:void(0)">Add Comments</a>
                                    <a class="dropdown-item" data-function="load-replies-form" data-module="view_replies_list" data-resource="company_policy" data-module-id="<?= $formId ?>" href="javascript:void(0)">Comments List (<span data-id="<?= $formId ?>" data-record="replies_count">0</span>)</a>
                                    <a class="dropdown-item" data-function="load-form" data-module="activity_history" data-resource="company_policy" data-module-id="<?= $formId ?>" data-module="jsform" data-field="email" href="javascript:void(0)">Activity Logs History</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 mt-4">
                            <form action="<?= $baseUrl ?>api/claims/form" id="jsform-wrapper">
                                <div class="border-bottom pb-2">
                                    <div class="col-lg-12 p-0 mb-3">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="form_footnote">Form Footnote <small>(This will appear at the bottom of the form)</small></label>
                                                    <?php if($text_editor == "ckeditor") { ?>
                                                        <textarea class="form-control" name="form_footnote" id="form_footnote" rows="7"><?= $recordData->claims_form->form_footnote ?? null ?></textarea>
                                                    <?php } else { ?>
                                                        <input hidden type="text" value="<?= $recordData->claims_form->form_footnote ?? null ?>" id="form_footnote">
                                                        <trix-editor name="form_footnote" class="trix-slim-scroll" id="form_footnote" input="form_footnote"></trix-editor>
                                                    <?php } ?>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-7">
                                                        <div class="form-group">
                                                            <label for="allow_attachment">Allow attachment to form by clients</label>
                                                            <select name="allow_attachment" id="allow_attachment" class="form-control selectpicker">
                                                                <option value="yes">Yes! Allow file attachments</option>
                                                                <option value="no">No! Do not allow file attachments</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <label for="allow_attachment">Policy Claims Legal Documents <small class="font-italic"> - Attach additional legal document (if any) to this policy</small></label>
                                                    <?php 
                                                    // load the attachment method
                                                    print $formsObj->form_attachment_placeholder($form_params);
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 p-0 m-0 row justify-content-between">
                                        <div><h4>Policy Claim Form</h4></div>
                                        <div>
                                            <div class="btn-group dropdown">
                                                <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i style="font-size:10px;" class="fa fa-plus"></i> Add New
                                                </button>
                                                <div class="dropdown-menu" data-function="jsform-module">
                                                    <a class="dropdown-item" data-module="jsform" data-field="input" href="javascript:void(0)">Text Input Field</a>
                                                    <a class="dropdown-item" data-module="jsform" data-field="date" href="javascript:void(0)">Date Input Field</a>
                                                    <a class="dropdown-item" data-module="jsform" data-field="email" href="javascript:void(0)">Email Field</a>
                                                    <a class="dropdown-item" data-module="jsform" data-field="textarea" href="javascript:void(0)">Textarea</a>
                                                    <a class="dropdown-item" data-module="jsform" data-field="select" href="javascript:void(0)">Select Options</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="form-pretext" class="text-center font-italic mt-4">The policy claim form is empty. Add new fields to the set.</div>
                                <div class="pt-2 pl-0 mt-2" id="jsform-container"><?= $formData["form"]; ?></div>
                                <input type="hidden" name="policy_id" value="<?= $recordId ?>" class="form-control">
                                
                                <div class="row mt-5">
                                    <div class="col-md-6 text-left">
                                        <button type='button' style='padding:5px' class='btn preview-form hidden btn-sm btn-secondary'><i class='fa fa-eye'></i> Preview Form</button>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <button class="btn btn-outline-success" type="submit"><i class="fa fa-save"></i> Save Claims Form </button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <?php } ?>    
</div>
<?= save_form_button(); ?>
<?= select_field_modal(); ?>
<?php require "foottags.php"; ?>