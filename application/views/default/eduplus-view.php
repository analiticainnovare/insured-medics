<?php
// set the title
$page_title = "InsureEdu Plus";

// extra resources
$loadedCSS = ["assets/vendors/datatables.net-bs4/dataTables.bootstrap4.css"];
$loadedJS = ["assets/js/script/eduplus.js","assets/js/script/comments.js"];

$eduClass = load_class("eduplus", "controllers");

// require the headtags
require "headtags.php";

// set the user id
$param = (object) [
    "userData" => $userData,
    "userId" => $loggedUserId
];
$params = (object) [];

$topics = 0;
$categories = 0;
$organic_views = 0;
$viral_views = 0;

// set the subject id
$topicFound = false;
$subjectId = isset($SITEURL[1]) ? xss_clean($SITEURL[1]) : null;

// confirm that the user is permitted
$isPermitted = $accessObject->hasAccess("eduplus", "control");

// view only 
if(isset($_GET["viewonly"]) || !$session->is_Admin_View) {
    $isPermitted = false;
}

// if the subject id is not null
if(!empty($subjectId)) {

    // append some additional parameters
    $params->limit = 1;
    $params->subject_id = $subjectId;
    $params->userId = $userPrefs->userId;

    // load the topic information
    $topic_info = $eduClass->subject_list($params)["data"];

    // if the information is not empty then set the found variable to true
    if(!empty($topic_info)) {
        $topicFound = true;
        $topic_info = $topic_info[0];

        // create a new forms object
        $formsObj = load_class("forms", "controllers");

        $topic_info->attachment = (array) $topic_info->attachment;

        // append the user data
        $params->userData = $userData;

        // append the data
        $params->data = $topic_info;
        
        // increment the statistics count of this item
        $eduClass->views($params);

        // get similar topics
        $params->category_id = $topic_info->category_id;
        $params->exempt = $subjectId;
        $params->subject_id = null;
        $params->minified = true;
        $params->limit = 5;

        // load the topics
        $similar_topics = $eduClass->subject_list($params)["data"];
    }
}
?>
<style>
.leave-comment-wrapper trix-editor {
    min-height: 60px;
    max-height: 60px;
}
</style>
<div class="page-content">
    <?php if(!$topicFound) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <a href="<?= $baseUrl ?><?= $isPermitted ? "eduplus-manage": "eduplus"; ?>" class="btn btn-primary btn-icon-text mb-2 mb-md-0">
                    <i class="btn-icon-prepend" data-feather="arrow-left"></i> Go Back
                </a>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-9 col-md-8" id="eduplus_content">
            
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between">
                            <div><h5><?= $topic_info->subject ?></h5></div>
                            <div>
                                <i class="fa fa-calendar-check"></i> <?= $topic_info->date_created ?>
                                <?php if($isPermitted) { ?>
                                    <button data-function="load-form" data-module="eduplus_subject" data-module-id="<?= $topic_info->category_id ?>_<?= $topic_info->item_id ?>" class="btn btn-sm mr-2 ml-2 btn-outline-success"><i style="font-size:12px;" class="fa fa-edit"></i> Edit</button>
                                    <button onclick="return delete_record('eduplus_subject','<?= $topic_info->item_id ?>','<?= $baseUrl ?>eduplus-manage');" title="Delete this Topic" class="btn btn-sm btn-outline-danger"><i style="font-size:12px;" class="fa fa-trash"></i></button>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <?= $topic_info->description ?>

                        <?php if(!empty($topic_info->attachment)) { ?>
                            <div class="border-top mt-4 pt-3">
                                <div class="title">Attachments <span>(<?= count($topic_info->attachment["files"]) ?? 0 ?> files, <?= $topic_info->attachment["files_size"] ?? 0 ?>)</span></div>
                                <?= $formsObj->list_attachments($topic_info->attachment["files"], $loggedUserId, "col-lg-4 col-md-6", $isPermitted, false); ?>
                            </div>
                        <?php } ?>

                    </div>
                    <div class="card-footer">
                        <div class="d-flex post-actions">
                            <a href="javascript:void(0);" data-post-is-liked="<?= $topic_info->is_liked ?>" data-post-id="<?= $subjectId ?>" class="d-flex like-post align-items-center text-muted mr-4">
                                <i data-post-id="<?= $subjectId ?>" class="fa <?= $topic_info->is_liked === 1 ? "text-primary" : "" ?> fa-heart"></i>
                                <p class="d-none d-md-block <?= $topic_info->is_liked === 1 ? "text-primary" : "" ?> ml-2 " data-likes-count="<?= count($topic_info->likes_list) ?>">Likes (<?= count($topic_info->likes_list) ?>)</p>
                            </a>
                            <a href="javascript:void(0);" data-post-id="<?= $subjectId ?>" class="d-flex comment-on-post align-items-center text-muted mr-4">
                                <i class="fa fa-comment-alt mr-2"></i>
                                 Comments (<p class="d-none d-md-block" data-id="<?= $subjectId ?>" data-record="comments_count" data-comments-count="<?= $topic_info->comments_count ?>">0</p>)
                            </a>
                        </div>
                    </div>
                </div>

                <div class="border-top p-0 m-0">
                    <?php if($topic_info->allow_comments) { ?>
                        <?php $comment = "Leave a comment on this Topic"; ?>
                        <?= leave_comments_builder("eduplustopic", $subjectId, $comment, false) ?>
                    <?php } else { ?>
                        <div class="alert alert-warning mt-3">Comments are closed for this Topic</div>
                    <?php } ?>
                    <div id="comments-container" data-autoload="true" data-last-reply-id="0" data-id="<?= $subjectId ?>" class="slim-scroll pt-3 pr-2 pl-0" style="overflow-y:auto; max-height:850px"></div>
                    <div class="load-more mt-2 text-center"><button id="load-more-replies" type="button" class="btn btn-outline-secondary">Loading comments</button></div>
                </div>

            </div>

            <div class="col-lg-3 col-md-4">

                <div class="card mb-3">
                    <div class="card-header">
                        <h5 class="text-uppercase">Statistics</h5>
                    </div>
                    <div class="card-body pb-0">
                        <table class="table">
                            <tr>
                                <td>Organic Views</td>
                                <td><?= $topic_info->organic_views ?></td>
                            </tr>
                            <tr>
                                <td>Inorganic Views</td>
                                <td><?= $topic_info->viral_views ?></td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="card mb-3">
                    <div class="card-header">
                        <h5 class="text-uppercase"><?= !$topicFound ? "Statistics" : "Category Details" ?></h5>
                    </div>
                    <div class="card-body pb-0">
                        <div class="form-group border-bottom pb-2">
                            <?= $topic_info->category_info->name ?>
                        </div>
                        <div class="form-group">
                            <?= $topic_info->category_info->description ?>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h5 class="text-uppercase">Other Related Topics</h5>
                    </div>
                    <div class="card-body p-2">
                        <ul class="list-group">
                            <?php foreach($similar_topics as $topic) { ?>
                                <li class="list-group-item pl-2 pr-2">
                                    <a href="<?= $baseUrl ?>eduplus-view/<?= $topic->item_id ?>"><?= $topic->subject ?></a><br>
                                    <span class="mr-2"><i class="fa fa-calendar-check"></i> <?= $topic->date_created ?></span>
                                    <span><i class="fa fa-eye"></i> <?= $topic->viral_views ?></span>
                                </li>
                            <?php } ?>
                        </ul>                        
                    </div>
                </div>

            </div>

        </div>
    <?php } ?>

</div>
<?php require "foottags.php"; ?>