<?php
// global variables
global $usersClass, $accessObject, $medicsClass;

// base url
$baseUrl = config_item("base_url");
$appName = config_item("site_name");

// if the user is not loggedin then show the login form
if(!$usersClass->loggedIn()) { require "login.php"; exit(-1); }

// confirm that user id has been parsed
$loggedUserId = $session->userId;
$cur_user_id = (confirm_url_id(1)) ? xss_clean($SITEURL[1]) : $loggedUserId;

// the query parameter to load the user information
$i_params = (object) ["limit" => 1, "user_id" => $loggedUserId];

// get the user data
$userData = $usersClass->list($i_params)["data"][0];

// get the variables for the accessobject
$accessObject->userId = $loggedUserId;
$accessObject->userPermits = $userData->user_permissions;
$userPrefs = $userData->preferences;
$userPrefs->userId = $loggedUserId;

// user sidebar preference
$sidebar_pref = $userPrefs->sidebar_nav ?? null;
$theme_color = $userPrefs->theme_color ?? null;

// quick links
$quick_links = is_object($userPrefs->quick_links) ? (array) $userPrefs->quick_links : $userPrefs->quick_links;
$my_quick_links = is_array($quick_links) ? array_keys($quick_links) : $quick_links;

// auto close modal options
$auto_close_modal = (isset($userPrefs->auto_close_modal) && ($userPrefs->auto_close_modal == "allow")) ? false : true;
$text_editor = (isset($userPrefs->text_editor) && ($userPrefs->text_editor == "trix")) ? "trix" : "ckeditor";

// user notifications
$userNotifications = [];

// set the current url in session
$user_current_url = current_url();
$session->user_current_url = $user_current_url;

// notification handler
$announcementNotice = $announcementClass->notice($userData);

// adverts handler
$advertHandler = load_class("adverts", "controllers")->display($userData);

// is this the current user?
$isAdmin = $userData->user_type == "admin" ? true : false;
$isNIC = $userData->user_type == "nic" ? true : false;
$isIC = $userData->user_type == "insurance_company" ? true : false;
$isAgent = $userData->user_type == "agent" ? true : false;
$isClient = $userData->user_type == "user" || $userData->user_type == "business" ? true : false;

// chat preferences
if(!isset($userPrefs->messages)) {
    $userPrefs->messages = (object)[
        "enter_to_send" => 1,
        "hide_online" => 0,
        "status" => ""
    ];
}

// extra css
$loadedCSS[] = "assets/vendors/datatables.net-bs4/dataTables.bootstrap4.css";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $page_title ?? "Dashboard" ?> - <?= $appName ?></title>
    <link rel="stylesheet" href="<?= $baseUrl ?>assets/vendors/select2/select2.min.css">
    <link rel="stylesheet" href="<?= $baseUrl ?>assets/vendors/core/core.css">
    <link rel="stylesheet" href="<?= $baseUrl ?>assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="<?= $baseUrl ?>assets/fonts/feather-font/css/iconfont.css">
    <link rel="stylesheet" href="<?= $baseUrl ?>assets/vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?= $baseUrl ?>assets/vendors/sweetalert2/sweetalert2.min.css">
    <link rel="stylesheet" style="template-theme" href="<?= $baseUrl ?>assets/css/style/<?= ($theme_color == "sidebar-dark") ? "dark-theme.css" : "light-theme.css" ?>">
    <link href="<?= $baseUrl ?>assets/css/fontawesome.css"  rel="stylesheet" type="text/css" />
    <?php if($text_editor == "trix") { ?>
    <link rel="stylesheet" href="<?= $baseUrl ?>assets/vendors/trix/trix.css">
    <?php } ?>
    <link rel="stylesheet" href="<?= $baseUrl ?>assets/css/custom.css">
    <link rel="stylesheet" href="<?= $baseUrl ?>assets/css/gallery.css">
    <link rel="shortcut icon" href="<?= $baseUrl ?>assets/images/favicon.png" />
    <?php foreach($loadedCSS as $eachCSS) { ?>
        <link rel="stylesheet" href="<?= $baseUrl ?><?= $eachCSS ?>">
    <?php } ?>
    <link id="user_current_url" name="user_current_url" value="<?= $user_current_url ?>">
</head>
<body class="<?= $sidebar_pref ?> <?=  $theme_color ?>">
	<div class="main-wrapper">
        
        <nav class="sidebar">
            <div class="sidebar-header">
                <a href="<?= $baseUrl ?>dashboard" class="sidebar-brand">
                Insuretech<span> Hub</span>
                </a>
                <div class="sidebar-toggler <?= ($sidebar_pref) ? "active" : "not-active"; ?>">
                <span></span>
                <span></span>
                <span></span>
                </div>
            </div>
            <div class="sidebar-body">
                <ul class="nav">
                <li class="nav-item nav-category">Main</li>
                <li class="nav-item">
                    <a href="<?= $baseUrl ?>dashboard" class="nav-link">
                    <i class="link-icon" data-feather="box"></i>
                    <span class="link-title">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item nav-category">Nav Menu</li>
                <?php if($accessObject->hasAccess("view", "user_policy")) { ?>
                <li class="nav-item">
                    <a href="<?= $baseUrl ?>policies" class="nav-link">
                    <i class="link-icon" data-feather="calendar"></i>
                    <span class="link-title">View Policies</span>
                    </a>
                </li>
                <?php } ?>
                <?php if($accessObject->hasAccess("view", "insurance_company")) { ?>
                <li class="nav-item">
                    <a href="<?= $baseUrl ?>companies" class="nav-link">
                    <i class="link-icon fa fa-quidditch"></i>
                    <span class="link-title">Insurance Company</span>
                    </a>
                </li>
                <?php } ?>
                <?php if($accessObject->hasAccess("view", "clients")) { ?>
                <li class="nav-item">
                    <a href="<?= $baseUrl ?>clients-list" class="nav-link">
                    <i class="link-icon" data-feather="user-plus"></i>
                    <span class="link-title">Manage Clients</span>
                    </a>
                </li>
                <?php } ?>
                <?php if($accessObject->hasAccess("view", "agents")) { ?>
                <li class="nav-item">
                    <a href="<?= $baseUrl ?>agents-list" class="nav-link">
                    <i class="link-icon fa fa-user-shield"></i>
                    <span class="link-title">Manage Agents</span>
                    </a>
                </li>
                <?php } ?>
                <?php if($accessObject->hasAccess("view", "brokers")) { ?>
                <li class="nav-item">
                    <a href="<?= $baseUrl ?>brokers-list" class="nav-link">
                    <i class="link-icon fa fa-user-secret"></i>
                    <span class="link-title">Insurance Brokers</span>
                    </a>
                </li>
                <?php } ?>
                <?php if($accessObject->hasAccess("view", "claims")) { ?>
                <li class="nav-item">
                    <a href="<?= $baseUrl ?>claims-list" class="nav-link">
                    <i class="link-icon fa fa-weight-hanging"></i>
                    <span class="link-title">View Claims</span>
                    </a>
                </li>
                <?php } ?>
                <!--
                <?php if($accessObject->hasAccess("view", "proposals")) { ?>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#proposalsComponents" role="button" aria-expanded="false" aria-controls="proposalsComponents">
                    <i class="link-icon fa fa-vr-cardboard"></i>
                    <span class="link-title">Proposals</span>
                    <i class="link-arrow" data-feather="chevron-down"></i>
                    </a>
                    <div class="collapse" id="proposalsComponents">
                    <ul class="nav sub-menu">
                        <li class="nav-item"><a href="<?= $baseUrl ?>proposals-list" nav-label="proposals-list" class="nav-link">View Proposals</a></li>
                        <?php if($accessObject->hasAccess("create", "proposals")) { ?>
                        <li class="nav-item"><a href="<?= $baseUrl ?>proposals-create" nav-label="proposals-create" class="nav-link">Generate Proposal</a></li>
                        <?php } ?>
                        <li class="nav-item"><a href="<?= $baseUrl ?>proposals-send" nav-label="proposals-send" class="nav-link">Forward Proposals</a></li>
                    </ul>
                    </div>
                </li>
                <?php } ?>
                -->
                
                <li class="nav-item nav-category">Other Features</li>
                
                <?php if($accessObject->hasAccess("view", "company_policy")) { ?>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#uiComponents" role="button" aria-expanded="false" aria-controls="uiComponents">
                    <i class="link-icon" data-feather="tablet"></i>
                    <span class="link-title">Company Policies</span>
                    <i class="link-arrow" data-feather="chevron-down"></i>
                    </a>
                    <div class="collapse" id="uiComponents">
                    <ul class="nav sub-menu">
                        <li class="nav-item"><a href="<?= $baseUrl ?>policy-list" nav-label="policy-list" class="nav-link">View Policies</a></li>
                        <?php if($accessObject->hasAccess("add", "company_policy")) { ?>
                        <li class="nav-item"><a href="<?= $baseUrl ?>policy-add" nav-label="policy-add" class="nav-link">New Policy</a></li>
                        <?php } ?>
                    </ul>
                    </div>
                </li>
                <?php } ?>
                <?php if($accessObject->hasAccess("view", "licenses")) { ?>
                <li class="nav-item">
                    <a href="<?= $baseUrl ?>licenses-list" class="nav-link">
                    <i class="link-icon" data-feather="feather"></i>
                    <span class="link-title">License Renewals</span>
                    </a>
                </li>
                <?php } ?>
                <?php if($accessObject->hasAccess("view", "complaints")) { ?>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#advancedUI" role="button" aria-expanded="false" aria-controls="advancedUI">
                    <i class="link-icon" data-feather="anchor"></i>
                    <span class="link-title">Complaints</span>
                    <i class="link-arrow" data-feather="chevron-down"></i>
                    </a>
                    <div class="collapse" id="advancedUI">
                    <ul class="nav sub-menu">
                        <li class="nav-item"><a href="<?= $baseUrl ?>complaints" nav-label="complaints" class="nav-link">Review Compliants</a></li>
                        <?php if($accessObject->hasAccess("lodge", "complaints")) { ?>
                        <li class="nav-item"><a href="javascript:void(0)" data-function="load-form" data-module="lodge_complaint" nav-label="complaints-lodge" class="nav-link">Lodge Complaint</a></li>
                        <?php } ?>
                    </ul>
                    </div>
                </li>
                <?php } ?>
                <?php if($accessObject->hasAccess("view", "adverts")) { ?>
                <li class="nav-item">
                    <a href="<?= $baseUrl ?>adverts-list" class="nav-link">
                    <i class="link-icon" data-feather="list"></i>
                    <span class="link-title">Advertisements</span>
                    </a>
                </li>
                <?php } ?>
                <?php if($accessObject->hasAccess("view", "reports")) { ?>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#forms" role="button" aria-expanded="false" aria-controls="forms">
                    <i class="link-icon" data-feather="inbox"></i>
                    <span class="link-title">Reports</span>
                    <i class="link-arrow" data-feather="chevron-down"></i>
                    </a>
                    <div class="collapse" id="forms">
                    <ul class="nav sub-menu">
                        <li class="nav-item">
                            <a href="<?= $baseUrl ?>reports-request" nav-label="reports-request" class="nav-link">Request Peport</a>
                        </li>
                        <?php if($accessObject->hasAccess("generate", "reports")) { ?>
                        <li class="nav-item">
                            <a href="<?= $baseUrl ?>reports-generate" nav-label="reports-generate" class="nav-link">Generate Report</a>
                        </li>
                        <?php } ?>
                    </ul>
                    </div>
                </li>
                <?php } ?>
                <?php if($accessObject->hasAccess("analytics", "reports")) { ?>
                <li class="nav-item">
                    <a href="<?= $baseUrl ?>analitics" class="nav-link">
                    <i class="link-icon" data-feather="pie-chart"></i>
                    <span class="link-title">Analitics</span>
                    </a>
                </li>
                <?php } ?>
                <?php if($accessObject->hasAccess("view", "users")) { ?>
                <li class="nav-item">
                    <a href="<?= $baseUrl ?>users-list" class="nav-link">
                    <i class="link-icon" data-feather="user-plus"></i>
                    <span class="link-title">Manage Admins</span>
                    </a>
                </li>
                <?php } ?>
                
                <li class="nav-item nav-category">Communication</li>
                <?php if($accessObject->hasAccess("chats", "communication")) { ?>
                <li class="nav-item">
                    <a href="<?= $baseUrl ?>chats" class="nav-link">
                    <i class="link-icon fa fa-comment"></i>
                    <span class="link-title">Chat</span>
                    </a>
                </li>
                <?php } ?>
                <?php if($accessObject->hasAccess("view_email", "communication")) { ?>
                <li class="nav-item">
                    <a href="<?= $baseUrl ?>emails" class="nav-link">
                    <i class="link-icon" data-feather="mail"></i>
                    <span class="link-title">Emails</span>
                    </a>
                </li>
                <?php } ?>
                <!--<?php if($accessObject->hasAccess("view_sms", "communication")) { ?>
                <li class="nav-item">
                    <a href="<?= $baseUrl ?>sms" class="nav-link">
                    <i class="link-icon fa fa-sms"></i>
                    <span class="link-title">SMS</span>
                    </a>
                </li>
                <?php } ?>-->
                <?php if($accessObject->hasAccess("view", "announcements")) { ?>
                <li class="nav-item">
                    <a href="<?= $baseUrl ?>announcements" class="nav-link">
                    <i class="link-icon" data-feather="message-square"></i>
                    <span class="link-title">Announcements</span>
                    </a>
                </li>
                <?php } ?>
                <?php if($accessObject->hasAccess("notifications", "communication")) { ?>
                <li class="nav-item">
                    <a href="<?= $baseUrl ?>notifications" class="nav-link">
                    <i class="link-icon" data-feather="bell"></i>
                    <span class="link-title">Notifications</span>
                    </a>
                </li>
                <?php } ?>
                
                <li class="nav-item nav-category">Educational Materials</li>
                <li class="nav-item">
                    <a href="<?= $baseUrl ?>eduplus" class="nav-link">
                    <i class="link-icon" data-feather="hash"></i>
                    <span class="link-title">InsureEdu Plus</span>
                    </a>
                </li>
                </ul>
            </div>
        </nav>
        
        <div class="page-wrapper">
            
            <nav class="navbar">
                <a href="#" class="sidebar-toggler">
                    <i data-feather="menu"></i>
                </a>
                <div class="navbar-content">
                    <form class="search-form" action="<?= $baseUrl ?>api/search">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i data-feather="search"></i>
                                </div>
                            </div>
                            <input type="text" name="q" class="form-control" id="navbarForm" placeholder="Search here...">
                        </div>
                    </form>
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown nav-apps">
                            <span class="theme_color_change"><i data-color="<?= ($theme_color == "sidebar-dark") ? "sidebar-light" : "sidebar-dark" ?>" title="Change theme color" class="fa <?= ($theme_color == "sidebar-dark") ? "text-gray" : "text-black" ?> cursor fa-circle"></i></span>
                        </li>
                        <li class="nav-item dropdown nav-apps">
                            <a class="nav-link dropdown-toggle" href="#" id="appsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i data-feather="grid"></i>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="appsDropdown">
                                <div class="dropdown-header d-flex align-items-center justify-content-between">
                                    <p class="mb-0 font-weight-medium">Quick Links</p>
                                    <a href="<?= $baseUrl ?>profile?preferences" data-loader="preferences" class="text-muted">Edit</a>
                                </div>
                                <div class="dropdown-body">
                                    <div class="d-flex align-items-center apps">
                                        <?= quick_links($my_quick_links ?? null) ?>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <?php if($accessObject->hasAccess("view_email", "communication")) { ?>
                        <li class="nav-item dropdown nav-messages">
                            <a class="nav-link dropdown-toggle" href="#" id="messageDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i data-feather="mail"></i>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="messageDropdown">
                                <div class="dropdown-header d-flex align-items-center justify-content-between">
                                    <p class="mb-0 font-weight-medium"><span data-id="<?= $loggedUserId ?>" data-record="messages_count">0</span> New Messages</p>
                                    <a href="javascript:;" class="text-muted">Mark as Read</a>
                                </div>
                                <div class="dropdown-body" data-threads="messages"></div>
                                <div class="dropdown-footer d-flex align-items-center justify-content-center">
                                    <a href="<?= $baseUrl ?>emails">View all</a>
                                </div>
                            </div>
                        </li>
                        <?php } ?>
                        <?php if($accessObject->hasAccess("notifications", "communication")) { ?>
                        <li class="nav-item dropdown nav-notifications">
                            <a class="nav-link dropdown-toggle" href="#" id="notificationDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i data-feather="bell"></i>
                                <div class="indicator notices-available"><div class="circle"></div></div>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="notificationDropdown">
                                <div class="dropdown-header d-flex align-items-center justify-content-between">
                                    <p class="mb-0 font-weight-medium"><span data-id="<?= $loggedUserId ?>" data-record="notices_count">0</span> New Notifications</p>
                                    <a href="javascript:;" data-id="<?= $userData->user_id ?>" data-resource="mark_notications_as_read" data-function="mark_all_as_read" class="text-muted">Mark as Read</a>
                                </div>
                                <div class="dropdown-body" data-threads="notifications"></div>
                                <div class="dropdown-footer d-flex align-items-center justify-content-center">
                                    <a href="<?= $baseUrl ?>notifications">View all</a>
                                </div>
                            </div>
                        </li>
                        <?php } ?>
                        <li class="nav-item dropdown nav-profile">
                            <a class="nav-link dropdown-toggle" href="#" id="profileDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="user_profile_image" src="<?= $baseUrl ?><?= $userData->image ?>" alt="profile">
                            </a>
                            <div class="dropdown-menu" aria-labelledby="profileDropdown">
                                <div class="dropdown-header d-flex flex-column align-items-center">
                                    <div class="figure mb-3">
                                        <img class="user_profile_image" src="<?= $baseUrl ?><?= $userData->image ?>" alt="">
                                    </div>
                                    <div class="info text-center">
                                        <p class="name font-weight-bold mb-0"><?= $userData->name ?></p>
                                        <p class="email text-muted mb-3"><?= $userData->email ?></p>
                                    </div>
                                </div>
                                <div class="dropdown-body">
                                    <ul class="profile-nav p-0 pt-3">
                                        <li class="nav-item">
                                            <a href="<?= $baseUrl ?>profile" class="nav-link">
                                                <i data-feather="user"></i>
                                                <span>Profile</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="<?= $baseUrl ?>profile?preferences" data-loader="preferences" class="nav-link">
                                                <i data-feather="edit"></i>
                                                <span>Preferences</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="<?= $baseUrl ?><?= $isIC || $isAgent || $isClient ? "companies-view": "configuration"; ?>" class="nav-link">
                                                <i data-feather="repeat"></i>
                                                <span>Configuration</span>
                                            </a>
                                        </li>
                                        <?php if($isAdmin || !$isAdmin) { ?>
                                        <li class="nav-item">
                                            <a href="<?= $baseUrl ?>endpoints" class="nav-link">
                                                <i data-feather="settings"></i>
                                                <span>Api Endpoints</span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                        <li class="nav-item">
                                            <a href="javascript:;" class="nav-link" id="logout-account">
                                                <i data-feather="log-out"></i>
                                                <span>Log Out</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>