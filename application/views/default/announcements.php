<?php
// set the title
$page_title = "Announcements";

// additional css and javascript files
$loadedJS = [
    "assets/js/script/announcements.js",
];

// require the headtags
require "headtags.php";

// create a new object of the forms class
$formsObj = load_class("forms", "controllers");
?>
<div class="page-content">
    <?php if(!$accessObject->hasAccess("view", "announcements")) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        <?= form_loader("fixed"); ?>

        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <?php if($accessObject->hasAccess("add", "announcements")) { ?>
                <a href="javascript:void(0)" data-function="load-form" data-module="post_announcement" type="button" class="btn btn-primary btn-icon-text mb-2 mb-md-0">
                    <i class="btn-icon-prepend" data-feather="folder-plus"></i>
                    Post Announcement
                </a>
                <?php } ?>
            </div>
        </div>

        <div class="row" id="announcements_list"></div>

    <?php } ?>
</div>
<?php require "foottags.php"; ?>