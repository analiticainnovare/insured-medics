<?php
// set the title
$page_title = "Dashboard";

// additional css and javascript files
$loadedJS = [
    "assets/vendors/chartjs/Chart.min.js",
	"assets/js/script/reports.js"
];

// require the headtags
require "headtags.php";
?>
<div class="page-content">
	<div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
		<div>
			<h4 class="mb-3 mb-md-0">Hello, <?= $userData->firstname ?>!</h4>
			<p>Welcome back to <?= $medicsClass->appName ?></p>
		</div>
		<div id="report_generation_init" data-stream_period="current" data-report_stream="clients_report,summary_report,policy_report,claims_report">
			<div class="btn-group" role="group" aria-label="period_selector">
				<?= report_period_buttons() ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-xl-12">
			<div class="row flex-grow">
				<?php if($accessObject->hasAccess("view", "clients")) { ?>
				<div class="col-md-4 grid-margin stretch-card">
					<div class="card">
						<div class="card-body pb-2">
							<div class="row">
								<div class="col-lg-4 col-md-12 col-xl-4">
									<div class="d-flex justify-content-between align-items-baseline">
										<h6 class="card-title mb-0">Clients</h6>
									</div>
									<h3 class="mb-2" data-record="clients_count">0</h3>
									<div class="d-flex align-items-baseline">
										<p data-item="summary_percent" data-record="clients_count">
											<span>0.00%</span>
											<i data-feather="arrow-up" class="icon-sm mb-1"></i>
										</p>
									</div>
								</div>
								<div class="col-lg-8 col-md-12 p-0 col-xl-8 simple_clients_chart-wrapper">
									<canvas style="height:70px; width:100%" id="simple_clients_chart"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
				<?php if($accessObject->hasAccess("view", "user_policy")) { ?>
				<div class="col-md-4 grid-margin stretch-card">
					<div class="card">
						<div class="card-body pb-2">
							<div class="row">
								<div class="col-lg-4 col-md-12 col-xl-4">
									<div class="d-flex justify-content-between align-items-baseline">
										<h6 class="card-title mb-0">Policies</h6>
									</div>
									<h3 class="mb-2" data-record="user_policy_count">0</h3>
									<div class="d-flex align-items-baseline">
										<p data-item="summary_percent" data-record="user_policy_count">
											<span>0.00%</span>
											<i data-feather="arrow-up" class="icon-sm mb-1"></i>
										</p>
									</div>
								</div>
								<div class="col-lg-8 col-md-12 col-xl-8 simple_policy_chart-wrapper">
									<canvas style="height:70px; width:100%" id="simple_policy_chart"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
				<?php if($accessObject->hasAccess("view", "claims")) { ?>
				<div class="col-md-4 grid-margin stretch-card">
					<div class="card">
						<div class="card-body pb-2">
							<div class="row">
								<div class="col-lg-4 col-md-12 col-xl-4">
									<div class="d-flex justify-content-between align-items-baseline">
										<h6 class="card-title mb-0">Claims</h6>
									</div>
									<h3 class="mb-2" data-record="user_policy_claims_count">0</h3>
									<div class="d-flex align-items-baseline">
										<p data-item="summary_percent" data-record="user_policy_claims_count">
											<span>0.00%</span>
											<i data-feather="arrow-up" class="icon-sm mb-1"></i>
										</p>
									</div>
								</div>
								<div class="col-lg-4 col-md-12 col-xl-8 simple_claims_chart-wrapper">
									<canvas style="height:70px; width:100%" id="simple_claims_chart"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
				<?php if(in_array($userData->user_type, ["user", "business", "broker"])) { ?>
				<div class="col-md-4 grid-margin stretch-card">
					<div class="card">
						<div class="card-body pb-2">
							<div class="row">
								<div class="col-6 col-md-12 col-xl-5">
									<div class="d-flex justify-content-between align-items-baseline">
										<h6 class="card-title mb-0">Proposals</h6>
									</div>
									<h3 class="mb-2" data-record="period-proposals-count">0</h3>
									<div class="d-flex align-items-baseline">
										<p data-item="summary_percent" data-record="user_policy_count">
											<span>0.00%</span>
											<i data-feather="arrow-up" class="icon-sm mb-1"></i>
										</p>
									</div>
								</div>
								<div class="col-6 col-md-12 col-xl-7">
									<div id="proposals_chart" class="mt-md-3 mt-xl-0"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
				<?php if(!in_array($userData->user_type, ["user", "business"])) { ?>
				<div class="col-lg-12 col-xl-12 grid-margin slim-scroll">
					<div class="card overflow-hidden">
						<div class="card-body pb-2">
							<div class="d-flex justify-content-between align-items-baseline mb-4 mb-md-3">
								<h6 class="card-title mb-0">Revenue</h6>
							</div>
							<div class="row align-items-start mb-2">
								<div class="col-md-7">
									<p class="text-muted tx-13 mb-3 mb-md-0">
										Revenue realized from the payment of premiums by clients over the period of time
									</p>
								</div>
							</div>
							<div class="policy_premium_chart-wrapper">
								<canvas height="300px" id="policy_premium_chart"></canvas>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
				<?php if(!in_array($userData->user_type, ["user", "business"])) { ?>
				<div class="col-lg-<?= ($isAdmin || $isIC) ? 8 : 12 ?> mb-4">
					<div class="card overflow-hidden">
						<div class="card-body pb-2">
							<div class="d-flex justify-content-between align-items-baseline mb-4 mb-md-3">
								<h6 class="card-title mb-0">PREMIUMS vs CLAIMS</h6>
							</div>
							<div class="row align-items-start mb-2">
								
							</div>
							<div class="claims_against_premium-wrapper">
								<canvas height="300px" id="claims_against_premium"></canvas>
							</div>
						</div>
					</div>
				</div>
				<?php if($isAdmin || $isIC) { ?>
				<div class="col-lg-4 col-xl-4 grid-margin slim-scroll mb-4">
					<div class="card overflow-hidden">
						<div class="card-body pb-2">
							<div class="d-flex justify-content-between align-items-baseline mb-4 mb-md-3">
								<h6 class="card-title mb-0">USERS GROUPING</h6>
							</div>
							<div class="row align-items-start mb-2"></div>
							<div class="users_group_chart-wrapper">
								<canvas height="300px" id="users_group_chart"></canvas>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
				<?php } ?>
				<?php if(!in_array($userData->user_type, ["user", "business", "bank"])) { ?>
					<?php if($accessObject->hasAccess("view", "complaints")) { ?>
						<div class="col-lg-5 col-xl-4 grid-margin grid-margin-xl-0 stretch-card" id="quick_minimal_load" data-stream="complaints" data-stream_id="<?= $loggedUserId ?>">
							<div class="card">
								<div class="card-body">
									<div class="d-flex justify-content-between align-items-baseline mb-2">
										<h6 class="card-title mb-0">Recent Complaints</h6>
										<div class="dropdown mb-2">
											<button class="btn p-0" type="button" id="dropdownMenuButton6" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<i class="icon-lg text-muted pb-3px" data-feather="more-horizontal"></i>
											</button>
											<div class="dropdown-menu" aria-labelledby="dropdownMenuButton6">
												<a class="dropdown-item d-flex align-items-center" href="<?= $baseUrl ?>complaints"><i data-feather="eye" class="icon-sm mr-2"></i> <span class="">View All</span></a>
											</div>
										</div>
									</div>
									<div class="d-flex flex-column" data-threads="complaints_list"></div>
								</div>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
				<?php if(in_array($userData->user_type, ["nic"])) { ?>
					<?php if($accessObject->hasAccess("view", "licenses")) { ?>
						<div class="col-lg-7 mb-2 col-xl-8 stretch-card">
							<div class="card">
								<div class="card-header">
									<h6 class="card-title mb-0">License Applications</h6>
								</div>
								<div class="card-body">
									<div class="table-responsive slim-scroll">
										<table class="table table-hover dataTable mb-0">
											<thead>
												<tr>
													<th class="pt-0">#</th>
													<th class="pt-0">Company Name</th>
													<th class="pt-0">License Type</th>
													<th class="pt-0">Application Date</th>
													<th class="pt-0">Status</th>
													<th class="pt-0">Assign</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
				<?php if($accessObject->hasAccess("view", "user_policy")) { ?>
					<div class="<?= !in_array($userData->user_type, ["user", "business"]) ? "col-lg-7 col-xl-8" : "col-lg-12 col-xl-12" ?> stretch-card">
						<div class="card">
							<div class="card-header">
								<h6 class="card-title mb-0">Pending User Policies</h6>
							</div>
							<div class="card-body">
								<div class="table-responsive slim-scroll">
									<table class="table table-hover mb-0" data-status="Pending" id="user_policies">
										<thead>
											<tr>
												<th class="pt-0">#</th>
												<th class="pt-0">Policy Info</th>
												<th class="pt-0">Start Date</th>
												<th class="pt-0">Premium</th>
												<th class="pt-0">Next Due Date</th>
												<th class="pt-0">Status</th>
												<th class="pt-0">Signup Date</th>
												<th></th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				<?php } ?>
				<?php if(in_array($userData->user_type, ["insurance_company"])) { ?>
				<div class="col-lg-12 mt-3 col-xl-12">
					<div class="card overflow-hidden">
						<div class="card-body">
							<div class="d-flex justify-content-between align-items-baseline mb-4 mb-md-3">
								<h6 class="card-title mb-0">Pending Policy Claims</h6>
							</div>
							<div class="table-responsive slim-scroll">
							<table class="table dataTable table-hover mb-0" data-status="Pending" id="claims_list">
									<thead>
										<tr>
											<th width="5%" class="pt-0">#</th>
											<th width="20%" class="pt-0">Name</th>
											<th class="pt-0">Policy Details</th>
											<th class="pt-0">Date of Claim</th>
											<th class="pt-0">Assigned to</th>
											<th width="6%">Status</th>
											<th class="pt-0"></th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>

						</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<?php require "foottags.php"; ?>