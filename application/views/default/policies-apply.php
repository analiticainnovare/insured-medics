<?php
// set the title
$page_title = "Apply for Policy";

// require the headtags
require "headtags.php";
?>
<div class="page-content">
	<?php if(!$accessObject->hasAccess("signup", "company_policy")) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <?php if($accessObject->hasAccess("view", "user_policy")) { ?>
                <a href="<?= $baseUrl ?>policies" type="button" class="btn btn-primary btn-icon-text mb-2 mb-md-0">
                    <i class="btn-icon-prepend" data-feather="list"></i>
                    Policies
                </a>
                <?php } ?>
            </div>
        </div>

        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    
                </div>
            </div>
        </div>
    <?php } ?>    
</div>
<?php require "foottags.php"; ?>