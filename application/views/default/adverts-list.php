<?php
// set the title
$page_title = "Advertisements";

// additional css and javascript files
$loadedJS = [
    "assets/js/script/adverts.js",
];

// require the headtags
require "headtags.php";

?>
<div class="page-content">
    <?php if(!$accessObject->hasAccess("view", "adverts")) { ?>
        <?= permission_denied() ?>
    <?php } else { ?>
        <?= form_loader("fixed"); ?>

        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
            <div>
                <h4 class="mb-3 mb-md-0"><?= $page_title ?></h4>
            </div>
            <div class="d-flex align-items-center flex-wrap text-nowrap">
                <?php if($accessObject->hasAccess("add", "adverts")) { ?>
                <a href="javascript:void(0)" data-function="load-form" data-module="post_advertisement" type="button" class="btn btn-primary btn-icon-text mb-2 mb-md-0">
                    <i class="btn-icon-prepend" data-feather="folder-plus"></i>
                    Post an Ad
                </a>
                <?php } ?>
            </div>
        </div>

        <?php if($accessObject->hasAccess("add", "adverts")) { ?>
            <div class="col-lg-12 p-0 col-xl-12 stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline mb-2">
                            <h6 class="card-title mb-0"></h6>
                        </div>
                        <div class="table-responsive slim-scroll">
                            <table class="table dataTable table-hover mb-0" data-noinit="datatable" id="adverts_list">
                                <thead>
                                    <tr>
                                        <th width="6%">#</th>
                                        <th>Title</th>
                                        <th>Objective</th>
                                        <th>Total Budget</th>
                                        <th>Date Range</th>
                                        <th>Summary Stats</th>
                                        <th width="7%">Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php } else { ?>

        <?php } ?>

    <?php } ?>
</div>
<?php require "foottags.php"; ?>